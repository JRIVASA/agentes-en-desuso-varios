Attribute VB_Name = "Funciones"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long

Public Function ConectarBaseDatos(Optional pTiempoEspera) As Boolean
    Dim mConnectionTimeOut As Long
    

    On Error GoTo Errores
    mConnectionTimeOut = IIf(IsMissing(pTiempoEspera), gConnectionTimeOut, pTiempoEspera)
    
    If Ent.BDD.State = adStateOpen Then Ent.BDD.Close
    Ent.BDD.ConnectionTimeout = mConnectionTimeOut
    Ent.BDD.CommandTimeout = gCommandTimeOut
    Ent.BDD.ConnectionString = "Provider= " & PROVIDER_LOCAL & ";Persist Security Info=False;User ID=" & UserDb & ";Password=" & UserPwd & ";Initial Catalog=vad10;Data Source=" & SRV_REMOTE
    
    If Ent.BDD2.State = adStateOpen Then Ent.BDD.Close
    Ent.BDD2.ConnectionTimeout = mConnectionTimeOut
    Ent.BDD2.CommandTimeout = gCommandTimeOut
    Ent.BDD2.ConnectionString = "Provider= " & PROVIDER_LOCAL & ";Persist Security Info=False;User ID=" & UserDb & ";Password=" & UserPwd & ";Initial Catalog=vad10;Data Source=" & SRV_REMOTE
    
    
    Ent.BDD.Open
    'Ent.BDD2.Open
    
    ConectarBaseDatos = True
    Exit Function
Errores:
   ' mensaje True, Err.Description
End Function


Sub main()
    
    UserDb = sgetini(App.Path & "\setup.ini", "Entrada", "User", "?")
    If UserDb = "?" Then
        UserDb = "sa"
    End If
    
    UserPwd = sgetini(App.Path & "\setup.ini", "Entrada", "Password", "?")
    If UserPwd = "?" Then
        UserPwd = ""
    End If
    
    
    SRV_REMOTE = sgetini(App.Path & "\setup.ini", "Server", "srv_remote", "?")
    If SRV_REMOTE = "?" Then
        'Call mensaje(True, "El servidor remoto no se encuentra setupurado.")
        End
    End If
    
    PROVIDER_LOCAL = sgetini(App.Path & "\setup.ini", "Proveedor", "Proveedor", "?")
    If PROVIDER_LOCAL = "?" Then
        'Call mensaje(True, "El proveedor de servicio local no se encuentra setupurado.")
        End
    End If

    
   Call ConectarBaseDatos
   Dim clsL As New clsManejoLotes
   clsL.analizarDescargos
   
End Sub

Public Function sgetini(SIniFile As String, SSection As String, SKey _
    As String, SDefault As String) As String
    Dim STemp As String * 256
    Dim NLength As Integer
    
    STemp = Space$(256)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, 255, SIniFile)
    sgetini = Left$(STemp, NLength)
End Function

Public Function CrearCadenaConexion(pUser As String, pBaseDatos As String, pServidor As String) As String
    Dim miCadena As String
    miCadena = "Provider=SQLOLEDB.1;Persist Security Info=False; " _
            & "User ID=" & pUser & ";Initial Catalog=" & pBaseDatos _
            & ";Data Source=" & pServidor
    'Debug.Print micadena
    CrearCadenaConexion = miCadena
End Function
Public Function CrearCadenaConexionShape(pUser As String, pBaseDatos As String, pServidor As String) As String
    CrearCadenaConexionShape = "Provider=MSDataShape.1;Persist Security Info=false;" _
        & "Connect Timeout=5;Data Source=" & pServidor & ";User ID=" & pUser _
        & ";Initial Catalog=" & pBaseDatos & ";Data Provider=SQLOLEDB.1"
End Function

Public Function CrearConexion(pCn, pCadena) As Boolean
    On Error GoTo Err_Manager
        Set pCn = New ADODB.Connection
        pCn.CommandTimeout = 0
        If pCn.State = adStateOpen Then pCn.Close
        pCn.CursorLocation = adUseServer
        pCn.Open pCadena
        CrearConexion = True
        Exit Function
    
Err_Manager:
    MsgBox Err.Description, vbCritical
    CrearConexion = False
    MsgBox pCadena
End Function

