VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsManejoLotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public localidad As String
Public Sub analizarDescargos()
    
    Dim rsParam As New ADODB.Recordset
    
    aux1 = False
    sql = "select c_codigo as localidad from MA_SUCURSALES "
    'Ent.BDD.BeginTrans
    rsParam.Open sql, Ent.BDD
    
    While Not rsParam.EOF
        Ent.BDD.BeginTrans
        sql = "select * from tr_productos_lotes_pend " _
        & " where c_localidad = '" & rsParam!localidad & "' "
        localidad = rsParam!localidad
        Call recorrerPendientes(sql)
        
        sql = "SELECT d_FECHA AS FECHA,ORDEN,c_DOCUMENTO AS DOCUMENTO " _
        & " ,C_CONCEPTO AS CONCEPTO,codconcepto AS CODCONCEPTO,C_CODDEPOSITO " _
        & " AS DEPOSITO, c_codlocalidad AS LOCALIDAD FROM " _
        & " ( " & crearBusqueda(rsParam!localidad) & " ) TABLEX " _
        & " ORDER BY FECHA,ORDEN"
        'Debug.Print sql
        Call RecorrerDocumentos(sql)
        Ent.BDD.CommitTrans
        Call actualizarCorrelativos(rsParam!localidad)
        rsParam.MoveNext
    Wend
    'Ent.BDD.CommitTrans
Exit Sub
Errores:
MsgBox Err.Description
'Ent.BDD.RollbackTrans

End Sub
Private Sub actualizarCorrelativos(localidad As String)
    Dim rsCorrelativo As New ADODB.Recordset
    Dim rsMayor As New ADODB.Recordset
    sql = "select c_concepto,c_documento,f_fecha from MA_PRODUCTOS_LOTES_CORRELATIVOS where " _
    & " c_localidad = '" & localidad & "'"
  '  Debug.Print sql
    rsCorrelativo.Open sql, Ent.BDD
    If Not rsCorrelativo.EOF Then
    While Not rsCorrelativo.EOF
    
        sqlgroup = " group by tpl.c_concepto"
        
        sqlwhere = " where tpl.c_concepto = '" & rsCorrelativo!c_concepto & "' "
        
        
        If UCase(rsCorrelativo!c_concepto) = "DEV" Or UCase(rsCorrelativo!c_concepto) = "VEN" Then
        sqlwhere = sqlwhere & "and tpl.c_documentoRel like '" & Mid(rsCorrelativo!c_documento, 1, 3) & "%'"
        End If
        
        caseFecha = "case when mv.d_FECHA IS not null then mv.d_FECHA else " _
        & " case when mc.d_FECHA IS not null then mc.d_FECHA else " _
        & " case when mi.d_FECHA IS not null then mi.d_FECHA end end end "
        
        sqltr = " select  max (c_documentoRel) as doc, max (" & caseFecha & ") as fecha from TR_PRODUCTOS_LOTES as tpl " _
        & " left join  MA_VENTAS  as mv on mv.c_DOCUMENTO = tpl.c_documentoRel and mv.C_CONCEPTO = tpl.c_concepto " _
        & " left join  MA_COMPRAS as mc on mc.c_DOCUMENTO = tpl.c_documentoRel and mc.C_CONCEPTO = tpl.c_concepto " _
        & " left join  MA_INVENTARIO as mi on mi.c_DOCUMENTO = tpl.c_documentoRel and mi.c_CONCEPTO = tpl.c_concepto "
                
        sqlpend = " select  max (c_documentoRel) as doc,  max (" & caseFecha & ") as fecha from TR_PRODUCTOS_LOTES_PEND as tpl " _
        & " left join  MA_VENTAS  as mv on mv.c_DOCUMENTO = tpl.c_documentoRel and mv.C_CONCEPTO = tpl.c_concepto " _
        & " left join  MA_COMPRAS as mc on mc.c_DOCUMENTO = tpl.c_documentoRel and mc.C_CONCEPTO = tpl.c_concepto " _
        & " left join  MA_INVENTARIO as mi on mi.c_DOCUMENTO = tpl.c_documentoRel and mi.c_CONCEPTO = tpl.c_concepto "
                        
        
        sqltr = sqltr & " " & sqlwhere & " " & sqlgroup
        sqlpend = sqlpend & " " & sqlwhere & " " & sqlgroup
        
        sql2 = sqltr & " UNION " & sqlpend
        
        
        If rsMayor.State = adStateOpen Then rsMayor.Close
        rsMayor.CursorLocation = adUseClient
        rsMayor.Open sql2, Ent.BDD, adOpenDynamic, adLockOptimistic
        
        If Not rsMayor.EOF Then
        
        
       Ent.BDD.Execute (" update MA_PRODUCTOS_LOTES_CORRELATIVOS set " _
       & " c_documento = '" & rsMayor!doc & "' ," _
       & " f_fecha = '" & rsMayor!fecha & "' " _
       & " where localidad = '" & localidad & "' and c_concepto = '" & rsCorrelativo!c_concepto & "'  " _
       & " and  c_documento = '" & rsCorrelativo!c_documento & "' " _
       )
       
        End If
        rsCorrelativo.MoveNext
    Wend
    'rsCorrelativo.UpdateBatch
    End If

End Sub

Private Function recorrerPendientes(sql) As String
    Dim rsDocumento As New ADODB.Recordset
    Dim rsDetalle As New ADODB.Recordset
    'On Error GoTo Errores
    
    'Ent.BDD.BeginTrans
    rsDocumento.Open sql, Ent.BDD
    
    If Not rsDocumento.EOF Then
       
        Call procesarPendientes(rsDocumento)
        
        'rsDocumento.MoveNext
    End If
   
   'Ent.BDD.CommitTrans
   Exit Function
Errores:
MsgBox Err.Description
'Ent.BDD.RollbackTrans

End Function
Private Sub procesarPendientes(rsPendiente As ADODB.Recordset)

Dim fVencimiento As Object
Set fVencimiento = CreateObject("manejoLotes.clsInterfazVen")
While Not rsPendiente.EOF

Select Case UCase(rsPendiente!c_tipomov)
    Case "CARGO"
        
        If UCase(rsPendiente!c_concepto) = "DEV" Then
            fVencimiento.grabarPendientes Ent.BDD, localidad, _
                       rsPendiente!c_deposito, _
                       rsPendiente!c_concepto, _
                       rsPendiente!c_codigo, _
                       rsPendiente!cantidad, _
                       rsPendiente!c_tipomov, _
                       rsPendiente!c_documentoRel
        End If
    
    
    Case "DESCARGO"
    

             fVencimiento.grabarPendientes Ent.BDD, localidad, _
                       rsPendiente!c_deposito, _
                       rsPendiente!c_concepto, _
                       rsPendiente!c_codigo, _
                       rsPendiente!cantidad, _
                       rsPendiente!c_tipomov, _
                       rsPendiente!c_documentoRel
        
End Select
rsPendiente.MoveNext
Wend


End Sub

Private Function crearBusqueda(localidad As String) As String
    Dim correlativoFecha As String
    Dim rs As New ADODB.Recordset
    
    'cadenaConceptos = "NDC,DCM,NDE,VEN,DEV,AJU,INV,PRD,TRS,TRA"
    'correlativoFecha = getCorrelativoFecha()
    'conceptos = Split(cadenaConceptos, ",")
    '   For i = LBound(conceptos) To UBound(conceptos)
        
    sql = "select c_concepto,c_documento, convert (varchar ,f_fecha,103) as fecha ,b_esVentaPos, " _
   & " localidad from MA_PRODUCTOS_LOTES_correlativos where localidad ='" & localidad & "' "

    'Debug.Print sql
    rs.Open sql, Ent.BDD
    While Not rs.EOF
        
        'Select Case UCase(rs!c_concepto)
'        Case "VEN", "DEV"
'            If aux1 = False Then
'                    sqltemp = detalleBusqueda(rs!c_documento, rs!f_fecha, rs!f_fecha, rs!localidad)
'                    sqltemp = sqltemp & " UNION " & detalleBusqueda(rs!c_documento, rs!f_fecha, rs!f_fecha, rs!localidad)
'                    aux1 = True
'            Else
'                    sqltemp = sqltemp & " UNION " & detalleBusqueda("000000", correlativoFecha, conceptos(i), localidad)
'                    sqltemp = sqltemp & " UNION " & detalleBusqueda("P000000", correlativoFecha, conceptos(i), localidad)
'            End If
            
        'Case Else
          
            If aux1 = False Then
                    sqltemp = detalleBusqueda(rs!c_documento, rs!fecha, rs!c_concepto, rs!localidad)
                    aux1 = True
            Else
                
                    sqltemp = sqltemp & " UNION " & detalleBusqueda(rs!c_documento, rs!fecha, rs!c_concepto, rs!localidad)
            End If
        'End Select
        rs.MoveNext
    Wend
        crearBusqueda = sqltemp
    
End Function


Private Function detalleBusqueda(c_documento As String, fecha As String, ByVal concepto As String, cs_localidad As String) As String
Dim conex1 As New ADODB.Connection
Dim tablaMaestro As String
   
   Select Case UCase(concepto)
   Case "NDC", "DCM"
   detalleBusqueda = " SELECT COM.d_FECHA,COM.c_DOCUMENTO,COM.C_CONCEPTO,COM.codconcepto,COM.C_CODDEPOSITO, COM.c_CODLOCALIDAD, ORD.ORDEN FROM MA_COMPRAS AS COM INNER JOIN ma_productos_lotes_orden AS ORD ON COM.C_CONCEPTO=ORD.CONCEPTO " & _
    " WHERE COM.c_CODLOCALIDAD ='" & cs_localidad & "' and COM.c_DOCUMENTO NOT LIKE 'E%'   AND COM.C_CONCEPTO='" & concepto & "' and  com.d_Fecha >='" & fecha & "' and com.c_documento >'" & c_documento & "' "
  
            If FechaSetup = "" Then
            filtroFecha = ""
            Else
            filtroFecha = "  and com.d_FECHA <'" & FechaSetup & "' "
            End If
    
    Case "NDE"
   detalleBusqueda = " SELECT VEN.d_FECHA,VEN.c_DOCUMENTO,VEN.C_CONCEPTO,VEN.codconcepto,VEN.C_CODDEPOSITO, VEN.c_CODLOCALIDAD, ORD.ORDEN  FROM MA_VENTAS AS VEN INNER JOIN ma_productos_lotes_orden AS ORD ON VEN.C_CONCEPTO=ORD.CONCEPTO  " & _
   "  WHERE VEN.c_CODLOCALIDAD ='" & cs_localidad & "' and  VEN.c_DOCUMENTO NOT LIKE 'P%' AND  VEN.c_DOCUMENTO NOT LIKE 'A%' AND  VEN.c_DOCUMENTO NOT LIKE 'E%'   AND VEN.C_CONCEPTO= '" & concepto & "' and ven.d_Fecha >='" & fecha & "' and ven.c_documento >'" & c_documento & "'"
              If FechaSetup = "" Then
                filtroFecha = ""
               Else
                filtroFecha = "  and ven.d_FECHA <'" & FechaSetup & "' "
              End If
     Case "VEN", "DEV"
   caracter = Mid(c_documento, 1, 1)
  
   If UCase(caracter) = "P" Then
    '04/11/2011  agregado condicion para los cierres del food
        caracter = Mid(c_documento, 1, 2)
        If caracter <> "PF" Then
            detalleBusqueda = " SELECT VEN.d_FECHA,VEN.c_DOCUMENTO,VEN.C_CONCEPTO,VEN.codconcepto,VEN.C_CODDEPOSITO,VEN.c_CODLOCALIDAD ,ORD.ORDEN FROM MA_VENTAS AS VEN INNER JOIN ma_productos_lotes_orden AS ORD ON VEN.C_CONCEPTO=ORD.CONCEPTO  " & _
            "  WHERE VEN.c_CODLOCALIDAD ='" & cs_localidad & "' and  VEN.c_DOCUMENTO LIKE 'P%'  and VEN.C_CONCEPTO= '" & concepto & "' and ven.d_Fecha >='" & fecha & "' and VEN.C_DOCUMENTO > '" & c_documento & "' "
        Else
            detalleBusqueda = " SELECT VEN.d_FECHA,VEN.c_DOCUMENTO,VEN.C_CONCEPTO,VEN.codconcepto,VEN.C_CODDEPOSITO,VEN.c_CODLOCALIDAD ,ORD.ORDEN FROM MA_VENTAS AS VEN INNER JOIN ma_productos_lotes_orden AS ORD ON VEN.C_CONCEPTO=ORD.CONCEPTO  " & _
            "  WHERE VEN.c_CODLOCALIDAD ='" & cs_localidad & "' and  VEN.c_DOCUMENTO LIKE 'PF%'  and VEN.C_CONCEPTO= '" & concepto & "' and ven.d_Fecha >='" & fecha & "' and VEN.C_DOCUMENTO > '" & c_documento & "' "
        End If
    Else
    detalleBusqueda = " SELECT VEN.d_FECHA,VEN.c_DOCUMENTO,VEN.C_CONCEPTO,VEN.codconcepto,VEN.C_CODDEPOSITO, VEN.c_CODLOCALIDAD, ORD.ORDEN FROM MA_VENTAS AS VEN INNER JOIN ma_productos_lotes_orden AS ORD ON VEN.C_CONCEPTO=ORD.CONCEPTO  " & _
   "  WHERE  VEN.c_CODLOCALIDAD ='" & cs_localidad & "' and  VEN.c_DOCUMENTO NOT LIKE 'P%' and VEN.c_DOCUMENTO NOT LIKE 'PF%'   AND VEN.c_DOCUMENTO NOT LIKE 'E%'  AND VEN.C_CONCEPTO= '" & concepto & "'and ven.d_Fecha >='" & fecha & "' and VEN.C_DOCUMENTO > '" & c_documento & "' "
    End If
    
    If FechaSetup = "" Then
     filtroFecha = ""
    Else
     filtroFecha = "  and ven.d_FECHA <'" & FechaSetup & "' "
    End If
   
   Case "PRD"
   detalleBusqueda = " SELECT INV.d_FECHA,INV.c_DOCUMENTO,INV.C_CONCEPTO,INV.codconcepto,INV.c_dep_ORIG as c_coddeposito,INV.c_CODLOCALIDAD,ORD.ORDEN FROM MA_INVENTARIO AS INV INNER JOIN MA_PRODUCTOS_LOTES_ORDEN AS ORD ON INV.C_CONCEPTO=ORD.CONCEPTO " & _
   " WHERE INV.c_CODLOCALIDAD ='" & cs_localidad & "' and  INV.c_DOCUMENTO NOT LIKE 'E%'  AND INV.C_CONCEPTO='" & concepto & "' and INV.d_Fecha >='" & fecha & "' and inv.c_documento > '" & c_documento & "' "
   
             If FechaSetup = "" Then
                filtroFecha = ""
               Else
                filtroFecha = "  and INV.d_FECHA <'" & FechaSetup & "' "
              End If
   
   
   Case "AJU"
   detalleBusqueda = " SELECT INV.d_FECHA,INV.c_DOCUMENTO,INV.C_CONCEPTO,INV.codconcepto,INV.c_dep_ORIG as c_coddeposito,INV.c_CODLOCALIDAD,ORD.ORDEN FROM MA_INVENTARIO AS INV INNER JOIN ma_productos_lotes_orden AS ORD ON INV.C_CONCEPTO=ORD.CONCEPTO " & _
   " WHERE INV.c_CODLOCALIDAD ='" & cs_localidad & "' and  INV.c_DOCUMENTO NOT LIKE 'E%'   AND INV.C_CONCEPTO='" & concepto & "' and INV.d_Fecha >='" & fecha & "' " & _
   " and c_documento not in  (select inv.c_DOCUMENTO from ma_inventario as inv inner join TR_INVENTARIO on " & _
   " inv.c_documento = TR_INVENTARIO.c_documento And inv.c_concepto = TR_INVENTARIO.c_concepto " & _
   " where (inv.CODCONCEPTO ='58' and c_TIPOMOV ='Descargo') or (inv.CODCONCEPTO ='60' and c_TIPOMOV ='Cargo')  and INV.d_Fecha >='" & fecha & "' ) and inv.c_documento = '" & c_documento & "' "
          
             If FechaSetup = "" Then
                filtroFecha = ""
               Else
                filtroFecha = "  and INV.d_FECHA <'" & FechaSetup & "' "
              End If
   
   Case Else
   detalleBusqueda = " SELECT INV.d_FECHA,INV.c_DOCUMENTO,INV.C_CONCEPTO,INV.codconcepto,INV.c_dep_ORIG as c_coddeposito,INV.c_CODLOCALIDAD,ORD.ORDEN FROM MA_INVENTARIO AS INV INNER JOIN ma_productos_lotes_orden AS ORD ON INV.C_CONCEPTO=ORD.CONCEPTO " & _
   " WHERE INV.c_CODLOCALIDAD ='" & cs_localidad & "' and  INV.c_DOCUMENTO NOT LIKE 'E%'   AND INV.C_CONCEPTO='" & concepto & "' and INV.d_Fecha >='" & fecha & "' and inv.c_documento >'" & c_documento & "'"
             If FechaSetup = "" Then
                filtroFecha = ""
               Else
                filtroFecha = "  and INV.d_FECHA <'" & FechaSetup & "' "
              End If
  End Select
  
  
  detalleBusqueda = detalleBusqueda & filtroFecha

 
End Function
Private Function RecorrerDocumentos(sql) As String
    Dim rsDocumento As New ADODB.Recordset
    Dim rsDetalle As New ADODB.Recordset
    'On Error GoTo Errores
   ' Ent.BDD.BeginTrans
   
   'Debug.Print sql
    rsDocumento.Open sql, Ent.BDD
    
    While Not rsDocumento.EOF
        

        sql = "SELECT c_CODARTICULO,SUM(cantidad) as cantidad,COSTO,c_TIPOMOV,c_documento_origen,c_tipodoc_origen FROM (" & _
        " select c_CODARTICULO,case when c_concepto='COM' then n_cant_teorica when c_concepto ='DCM'then n_cant_teorica else n_cantidad end as cantidad,n_COSTO as costo  ,c_TIPOMOV,c_documento_origen ,c_tipodoc_origen from TR_INVENTARIO where c_codlocalidad = '" & rsDocumento!localidad & "' and c_DOCUMENTO = '" & rsDocumento!documento & "' and c_CONCEPTO ='" & rsDocumento!concepto & "' ) TABLEX GROUP BY c_CODARTICULO,COSTO,c_TIPOMOV,c_documento_origen,c_tipodoc_origen order by costo"
                
        If rsDetalle.State = 1 Then rsDetalle.Close
        rsDetalle.Open sql, Ent.BDD, adOpenForwardOnly
        
        If Not rsDetalle.EOF Then
        Call procesarDocumento(rsDetalle, rsDocumento!concepto, rsDocumento!documento, rsDocumento!localidad, rsDocumento!deposito)
        End If
        rsDocumento.MoveNext
    
    Wend
   
   'Ent.BDD.CommitTrans
   Exit Function
Errores:
MsgBox Err.Description
'Ent.BDD.RollbackTrans
End Function
Private Function documentoRelacionadoExiste(deposito As String, _
documento As String, _
concepto As String) As Boolean
Dim rsBusqueda As New ADODB.Record
sqlqhere = " where c_concepto ='" & concepto & "' " _
& " and c_documentoRel = '" & documento & "' " _
& " and c_deposito ='" & deposito & "'"
sql1 = " select c_codigo as cantidad from TR_PRODUCTOS_LOTES  " & sqlwhere
sql2 = " select c_codigo as cantidad from TR_PRODUCTOS_LOTES_PEND  " & sqlwhere
sql3 = sql1 & " union " & sql2

        rsBusqueda.Open sql3, Ent.BDD
        If Not rs.EOF Then
        documentoRelacionadoExiste = True
        Else
        documentoRelacionadoExiste = False
        End If
End Function
Private Sub procesarDocumento(rsDetalle As ADODB.Recordset, concepto As String, documento As String, localidad As String, deposito As String)

Dim fVencimiento As Object
Set fVencimiento = CreateObject("manejoLotes.clsInterfazVen")
While Not rsDetalle.EOF
        'rsDetalle!c_documento_origen,rsDetalle!c_tipodoc_origen
        If UCase(rsDocumento!concepto) = "DCM" Then
        If documentoRelacionadoExiste(deposito, _
        rsDetalle!c_documento_origen, _
        rsDetalle!c_tipodoc_origen) = True Then Exit Sub
        End If


Select Case UCase(rsDetalle!c_tipomov)
    Case "CARGO"
        
        If UCase(concepto) = "DEV" Then
            fVencimiento.DescargarLotes Ent.BDD, localidad, _
                       deposito, _
                       documento, concepto, _
                       rsDetalle!c_CODARTICULO, _
                       rsDetalle!cantidad, rsDetalle!c_tipomov
        End If
    
    
    Case "DESCARGO"
    

    fVencimiento.DescargarLotes Ent.BDD, localidad, _
                       deposito, _
                       documento, concepto, _
                       rsDetalle!c_CODARTICULO, _
                       rsDetalle!cantidad, rsDetalle!c_tipomov
        
End Select
rsDetalle.MoveNext
Wend
End Sub


Private Function getCorrelativoFecha() As String

    Dim rs As New ADODB.Recordset
    
        
        sql = "select CU_DESCRIPCION  from MA_CORRELATIVOS where CU_Campo = 'fecha_manejo_lotes'"

        rs.Open sql, Ent.BDD
        If Not rs.EOF Then getCorrelativoFecha = rs!CU_DESCRIPCION
        
End Function
