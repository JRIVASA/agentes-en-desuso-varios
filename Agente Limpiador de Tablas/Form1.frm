VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Agente Limpia Tablas"
   ClientHeight    =   5745
   ClientLeft      =   105
   ClientTop       =   330
   ClientWidth     =   5790
   Icon            =   "Form1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5745
   ScaleWidth      =   5790
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton OptTruncar 
      Caption         =   "Truncar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   2040
      TabIndex        =   14
      Top             =   3720
      Width           =   2000
   End
   Begin VB.OptionButton OptDelete 
      Caption         =   "Eliminar Datos"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   120
      TabIndex        =   13
      Top             =   3720
      Width           =   2000
   End
   Begin VB.TextBox txtRegistro 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   16
      Top             =   4440
      Width           =   5535
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Eliminar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   4260
      Picture         =   "Form1.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   3000
      Width           =   1335
   End
   Begin VB.TextBox txtTabla 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   11
      Top             =   3240
      Width           =   3375
   End
   Begin VB.Frame Frame1 
      Height          =   2800
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   5655
      Begin VB.TextBox txtContrase�a 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   8
         Top             =   2280
         Width           =   3375
      End
      Begin VB.TextBox txtUsuario 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   6
         Text            =   "sa"
         Top             =   1680
         Width           =   3375
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Comprobar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   4200
         Picture         =   "Form1.frx":6F54
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox txtBaseDatos 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   4
         Top             =   1080
         Width           =   3375
      End
      Begin VB.TextBox txtServidor 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   3375
      End
      Begin VB.Label Label6 
         Caption         =   "Contrase�a"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   2040
         Width           =   3135
      End
      Begin VB.Label Label5 
         Caption         =   "Usuario"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1440
         Width           =   3135
      End
      Begin VB.Label Label2 
         Caption         =   "Base de Datos"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   840
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   "Servidor"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   3135
      End
   End
   Begin VB.Label Label4 
      Caption         =   "Registro"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   4080
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Tabla"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   3000
      Width           =   855
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

    Command1.Enabled = False
    
    If Crear_Conexiones(UCase(Trim(txtBaseDatos)), UCase(Trim(txtServidor)), txtUsuario, txtContrase�a) Then
        Command1.Enabled = False
        txtBaseDatos.Enabled = False
        txtServidor.Enabled = False
        txtUsuario.Enabled = False
        txtContrase�a.Enabled = False
        Label1.Enabled = False
        Label2.Enabled = False
        Label5.Enabled = False
        Label6.Enabled = False
        HabilitarControles
        txtRegistro.Text = "Conexi�n creada con Exito." & vbCrLf
    Else
        txtRegistro.Text = "Conexi�n Fallida." & vbCrLf
    End If
    
    Command1.Enabled = True
    
End Sub

Private Sub Command2_Click()

    Dim sql As String
    Dim mRs As New ADODB.Recordset
    Dim mTrans As Boolean, mReg As Integer
    
    On Error GoTo error
    
        If Not Trim(txtTabla.Text) = "" Then
            DoEvents
            Command2.Enabled = False
            
            If OptDelete Then
                sql = "DELETE FROM " & UCase(Trim(txtTabla))
            Else
                sql = "TRUNCATE TABLE " & UCase(Trim(txtTabla))
            End If
            
            mTrans = True: Conexion.BeginTrans
            '' Debug.Print sql
            Conexion.CommandTimeout = 0
            Conexion.Execute sql, mReg
            
            If mReg > 0 Or OptTruncar Then
                Conexion.CommitTrans
                txtRegistro.Text = txtRegistro.Text & "Tabla Afectada: " & UCase(Trim(txtTabla)) & vbCrLf
            Else
                Conexion.CommitTrans
                txtRegistro.Text = txtRegistro.Text & "La Tabla " & UCase(Trim(txtTabla)) & " no conten�a datos." & vbCrLf
            End If
            
            Command2.Enabled = True
        End If
        
    Exit Sub
        
error:
        If mTrans Then Conexion.RollbackTrans
        Call MsgBox("Error: " & Err.Description, vbInformation)
        Command2.Enabled = True
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    Select Case KeyCode
        Case vbKeyF12
            Unload Me
    End Select
    
End Sub

Private Sub HabilitarControles()

    Label3.Enabled = True
    Label4.Enabled = True
    txtTabla.Enabled = True
    Command2.Enabled = True
    
End Sub

Private Sub Form_Load()
    OptDelete.Value = True
End Sub
