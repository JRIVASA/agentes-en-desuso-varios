Attribute VB_Name = "ModuloPrincipal"
Private Sub Main()

    Dim Funciones As New Funciones
    Call Funciones.Ejecutar
    
End Sub

Public Sub EscribirErrores(pError As String)

    On Error GoTo error
    Dim mCanal As Integer
    
    mCanal = FreeFile
        
    Open App.Path & "\LogError.txt" For Output Access Write As #mCanal
    Print #mCanal, pError
    Close #mCanal
        
    Exit Sub
    
error:
   Debug.Print Err.Description
        
End Sub
