VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Public Function sgetini(SIniFile As String, SSection As String, SKey _
    As String, SDefault As String) As String
    Dim STemp As String * 256
    Dim NLength As Integer
    
    STemp = Space$(256)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, 255, SIniFile)
    sgetini = Left$(STemp, NLength)
End Function

Public Sub Ejecutar()

On Error GoTo error1

    Dim Rs_Proveedores As New ADODB.Recordset
    
    Dim TransaccionIniciada As Boolean
    
    Call ConectarBDD
    'ENT.BDD.Open
    
    SQL = "select c_codproveed, c_descripcio, c_rif, c_razon, n_porcentaje_seniat from MA_PROVEEDORES WHERE C_DESCRIPCIO NOT LIKE '?%'"
    Rs_Proveedores.Open SQL, ENT.BDD, adOpenDynamic, adLockBatchOptimistic
    
    ENT.BDD.BeginTrans
    
    TransaccionIniciada = True
    
    'a = 0
    'b = 0
    
    Debug.Print Rs_Proveedores.RecordCount

    Dim Errores As String

    While Not Rs_Proveedores.EOF
    
        Rs_Proveedores.Update
    
        Dim DatosSeniat As Variant
        DatosSeniat = Modulo_Seniat.Consulta_Datos(CStr(Rs_Proveedores!c_rif))
    
        If DatosSeniat(0) = "" Then
        
            Errores = Errores & IIf(Len(Errores) > 0, vbNewLine, "") & "Datos del Proveedor: " & "Rif: " & CStr(Rs_Proveedores!c_rif) & " Codigo: " & CStr(Rs_Proveedores!C_CODPROVEED) & _
            " Descripcion: " & CStr(Rs_Proveedores!c_descripcio) & " - Causa del Error: " & DatosSeniat(1) & "."
            
        'a = a + 1
        
        Else
        
            Rs_Proveedores!c_razon = DatosSeniat(0)
            Rs_Proveedores!n_porcentaje_seniat = DatosSeniat(3)
            
            '______________________ DATOS ACTUALIZADOS ________________________'
            
            
    '        If rs_proveedores!C_CODPROVEED = "E008841899" Then
    '        Debug.Print "X"
    '        End If
            
            If ContribuyenteEspecial Then
            
            Dim rs_retenciones As New ADODB.Recordset
            Dim rs_retxpro As New ADODB.Recordset
            
            Dim TieneRetencionAsociada_Correcta As Boolean
            
            rs_retenciones.Open " SELECT CS_RETENCION FROM MA_RETENCIONES WHERE cs_correlativo = 'RETENCION_IVA' AND" _
            & " NU_PORCENTAJE_APLICAR = " & DatosSeniat(3), ENT.BDD, adOpenStatic, adLockReadOnly, adCmdText
            
            If Not rs_retenciones.EOF Then
            
                SQLPXR = "SELECT * FROM MA_PROVEEDORESXRETENCIONES PXR INNER JOIN MA_RETENCIONES RET" _
                & " ON PXR.CU_RETENCION = RET.CS_RETENCION WHERE CU_PROVEEDOR = '" _
                & Rs_Proveedores!C_CODPROVEED & "' AND CS_CORRELATIVO = 'RETENCION_IVA'"
            
                rs_retxpro.Open SQLPXR, ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
                
                TieneRetencionAsociada_Correcta = False
                
                While Not rs_retxpro.EOF
                
                    If rs_retxpro!NU_PORCENTAJE_APLICAR <> CDbl(DatosSeniat(3)) Then
                    
                        ENT.BDD.Execute "DELETE FROM MA_PROVEEDORESXRETENCIONES WHERE CU_PROVEEDOR = " _
                        & "'" & Rs_Proveedores!C_CODPROVEED & "' AND CU_RETENCION = '" & rs_retxpro!CU_RETENCION & "'"
                        
                    Else
                    
                        TieneRetencionAsociada_Correcta = True
                    
                    End If
                    
                    rs_retxpro.MoveNext
                    
                Wend
                
                rs_retxpro.Close
                
                If Not TieneRetencionAsociada_Correcta Then
                
                ENT.BDD.Execute "INSERT INTO MA_PROVEEDORESXRETENCIONES (CU_PROVEEDOR, CU_RETENCION)" _
                & " VALUES ('" & Rs_Proveedores!C_CODPROVEED & "', '" & rs_retenciones!CS_RETENCION & "')"
                
                End If
                
            End If
            
            rs_retenciones.Close
                    
            End If
            '_____________________ RETENCIONES ASOCIADAS _____________________'
            
            
            'b = b + 1
        
        End If
    
        Rs_Proveedores.UpdateBatch
    
        Rs_Proveedores.MoveNext
    
        DoEvents
    
    Wend
    
    If Len(Errores) > 0 Then ModuloPrincipal.EscribirErrores Errores
    
    Debug.Print a & "-" & b
    ENT.BDD.CommitTrans
    'Call MsgBox("Proceso Concluido al " & Now() & vbCrLf & W & " Productos Actualizados.", vbOKOnly, "Reconstructor de DepoProd")
    
    Exit Sub
    
error1:

    If TransaccionIniciada Then ENT.BDD.RollbackTrans
    
    ModuloPrincipal.EscribirErrores "Ha ocurrido un error en tiempo de ejecución," & vbNewLine & "No se realizaron cambios en la base de datos." _
    & vbNewLine & "Por favor, reporte lo siguiente:" & vbNewLine & vbNewLine & Err.Description
    
    Debug.Print Err.Description
    
End Sub

Private Sub ConectarBDD()

    On Error GoTo Errores
    Dim Setup As String
    Setup = App.Path & "\setup.ini"
    
'BUSCAR VALORES
    SRV_LOCAL = sgetini(Setup, "Server", "srv_local", "?")
    If SRV_LOCAL = "?" Then
        Call MsgBox("El servidor local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    'SRV_REMOTE = sgetini(Setup, "Server", "srv_remote", "?")
    'If SRV_REMOTE = "?" Then
    
    '    Call MsgBox("El servidor remoto no se encuentra configurado.", vbCritical, "Error")
    '    End
    'End If
    
    PROVIDER_LOCAL = sgetini(Setup, "Proveedor", "Proveedor", "?")
    If PROVIDER_LOCAL = "?" Then
        Call MsgBox("El proveedor de servicio local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    UserDb = sgetini(Setup, "Server", "User", "?")
    If UserDb = "?" Then
        UserDb = "sa"
    End If
    
    UserPwd = sgetini(Setup, "Server", "Password", "?")
    If UserPwd = "?" Then
        UserPwd = ""
    End If
    
    ContribuyenteEspecial = CBool(sgetini(Setup, "Configuracion", "ContribuyenteEspecial", "0"))
    
'INICIO CONEXION
    If ENT.BDD.State = adStateOpen Then ENT.BDD.Close
    ENT.BDD.ConnectionString = "Provider= " & PROVIDER_LOCAL & ";Persist Security Info=False;User ID=" & UserDb & ";Password=" & UserPwd & ";Initial Catalog=VAD10;Data Source=" & SRV_LOCAL
    ENT.BDD.CommandTimeout = 0
    ENT.BDD.Open
    
    ConectarBaseDatos = True
    Exit Sub
Errores:

    Call MsgBox(Err.Description, vbCritical, "Error")
    
End Sub




