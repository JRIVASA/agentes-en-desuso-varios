﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Compression;
using System.IO;

namespace ftp_SIVPRE
{
    class FileHelper
    {

    public static byte[] Decompress(byte[] gzip)
    {
	// Create a GZIP stream with decompression mode.
	// ... Then create a buffer and write into while reading from the GZIP stream.
        using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
	{
	    const int size = 4096;
	    byte[] buffer = new byte[size];
	    using (MemoryStream memory = new MemoryStream())
	    {
		int count = 0;
		do
		{
		    count = stream.Read(buffer, 0, size);
		    if (count > 0)
		    {
			memory.Write(buffer, 0, count);
		    }
		}
		while (count > 0);
		return memory.ToArray();
	    }
	}
    }

    public static FileStream Compress(byte[] raw, FileStream destFileStream)
    {
        //FileStream destFileStream = File.Create(fileName.Replace("txt", "txt.gz"));
     //   using (MemoryStream memory = new MemoryStream())
	//{



        using (GZipStream gzip = new GZipStream(destFileStream, CompressionMode.Compress, true))
	    {
		gzip.Write(raw, 0, raw.Length);

	    }
        destFileStream.Close();
        return destFileStream;
        // return memory.ToArray();
	//}
    }


    public static string GetLastFileInDirectory(string directory, string pattern = "*.*")
    {
        if (directory.Trim().Length == 0)
            return string.Empty; //Error handler can go here

        if ((pattern.Trim().Length == 0) || (pattern.Substring(pattern.Length - 1) == "."))
            return string.Empty; //Error handler can go here

        if (Directory.GetFiles(directory, pattern).Length == 0)
            return string.Empty; //Error handler can go here

        //string pattern = "*.txt"

        var dirInfo = new DirectoryInfo(directory);
        var file = (from f in dirInfo.GetFiles(pattern) orderby f.LastWriteTime descending select f).First();

        return file.ToString();
    }

    public static string[] GetAllFilesInDirectory(string directory, string pattern = "*.*") {


        if (directory.Trim().Length == 0)
            return null; //Error handler can go here

        if ((pattern.Trim().Length == 0) || (pattern.Substring(pattern.Length - 1) == "."))
            return null; //Error handler can go here

        if (Directory.GetFiles(directory, pattern).Length == 0)
            return null; //Error handler can go here

        string[] filePaths = Directory.GetFiles(directory, pattern);


        return filePaths;
    
    }

  

}
    }

