﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Diagnostics;
using Renci.SshNet;
using Ionic.Zip;

namespace ftp_SIVPRE
{
    class SIVPREHandler
    {
        private ftp ftpSIVPRE;
        private string server;
        private string user;
        private string pass;
        private string localFilePath;
        private string remoteFilePath;
        private FSLogger log;

        public SIVPREHandler()
        {
            log = new FSLogger(System.Environment.CurrentDirectory+"\\log.txt");
            server = Properties.Settings.Default.servidor;
            server = "" + server + "";
            localFilePath = Properties.Settings.Default.rutaCarga;
            remoteFilePath = Properties.Settings.Default.rutaDescarga;
            user = Properties.Settings.Default.usuario;
            pass = Properties.Settings.Default.pass;
            ftpSIVPRE = new ftp(server, user, pass);
        }

        public void uploadSIVPRE()
        {
            string uploadFile;
            string procesedNameCSV;
            string procesedNameZIP;
            string procesedNameHash;
            string procesedNameEncrypted;
            string localName;
            string fileName = "";
            localName = "";
            bool flag = true;
            
            //fileName = FileHelper.GetLastFileInDirectory("G:\\", "*.TXT");
            log.EscribirLog("Iniciando Envio");
            try
            {
                log.EscribirLog(localFilePath); 
                foreach (string file in FileHelper.GetAllFilesInDirectory(localFilePath, "*.CSV"))
                {
                    
                    if (!Properties.Settings.Default.ComprimirCarga) {
                        fileName = file.Replace(localFilePath, "");
                        localName = localFilePath + fileName;
                        uploadFile = "enviado/" + fileName;
                        //ftpSIVPRE.upload(uploadFile, localName);
                        //procesedName = localFilePath + "procesado\\" + fileName;
                        //File.Move(localName, procesedName);
                    
                    } else {

                        fileName = file.Replace(localFilePath, "");
                        localName = localFilePath + fileName;
                        uploadFile = "enviado/" + fileName;
                        DirectoryInfo directorySelected = new DirectoryInfo(localFilePath);

                        foreach (FileInfo fileToCompress in directorySelected.GetFiles("*.csv"))//())
                        {
                            Compress(fileToCompress);
                        }
                        //FileStream compressFile = File.Create(localName.Replace("csv", "zip"));
                        //aca se crea el archivo comprimido voy a intentar que se cree el archivo con la extion en vez de renombrar el archivo
                        //FileHelper.Compress(File.ReadAllBytes(localName), compressFile);

                        try
                        {
                            string line;
                            string Texto = "";
                            // Read the file and display it line by line.
                            System.IO.StreamReader archivo =
                                new System.IO.StreamReader(Properties.Settings.Default.RutaBAT + "enc.bat"); //(@"enc.bat");
                            while ((line = archivo.ReadLine()) != null)
                            {
                                //System.Console.WriteLine(line);
                                Texto = line;
                            }

                            using(StreamWriter fileWrite = new StreamWriter(Properties.Settings.Default.RutaBAT + "temp.bat")) //(@"temp.bat"))
                            {
                                Texto = Texto.Replace("*RUTAOPENSSL*", Properties.Settings.Default.RutaOpenSSL);
                                Texto = Texto.Replace("*RUTALLAVE*", Properties.Settings.Default.RutaLlave);
                                Texto = Texto.Replace("*RUTAZIP*", Properties.Settings.Default.RutaZIP);
                                Texto = Texto.Replace("*ARCHIVOZIP*", fileName);
                                Texto = Texto.Replace("*RUTAENCRYPTED*", Properties.Settings.Default.RutaEncrypted);
                                Texto = Texto.Replace("*ARCHIVOENCRYPTED*", fileName);
                                fileWrite.WriteLine(Texto);
                            }
                            Process.Start(Properties.Settings.Default.RutaBAT + "temp.bat"); //(@"temp.bat");
                            System.Threading.Thread.Sleep(3000);
                            File.Delete(Properties.Settings.Default.RutaBAT + "temp.bat"); //(@"temp.bat");
                            String HashMD5 = (GetMD5HashFromFile(Properties.Settings.Default.RutaEncrypted + fileName+".encrypted")).ToLower();
                            using (StreamWriter fileWrite = new StreamWriter(Properties.Settings.Default.RutaEncrypted + fileName.Replace("csv", "txt")))
                            {
                                fileWrite.WriteLine("\"" + HashMD5 + "\"," + "\""+fileName + ".zip\"");
                            }
                            File.Copy(Properties.Settings.Default.RutaEncrypted + fileName + ".encrypted", Properties.Settings.Default.RutaEncrypted + fileName.Replace(".csv", "") + ".encrypted");
                            System.Threading.Thread.Sleep(3000);
                            File.Delete(Properties.Settings.Default.RutaEncrypted + fileName + ".encrypted");

                            //////////////////////////////////////////////////////////////////////////////

                            int port = 22;
                            string host = Properties.Settings.Default.servidor;
                            string username = Properties.Settings.Default.usuario;
                            string password = Properties.Settings.Default.pass;
                            string workingdirectory = "/enviado";
                            string uploadfile = "";

                            Console.WriteLine("Creating client and connecting");
                            using (var client = new SftpClient(host, port, username, password))
                            {
                                client.Connect();
                                Console.WriteLine("Connected to {0}", host);

                                client.ChangeDirectory(workingdirectory);
                                Console.WriteLine("Changed directory to {0}", workingdirectory);

                                var listDirectory = client.ListDirectory(workingdirectory);
                                Console.WriteLine("Listing directory:");
                                foreach (var fi in listDirectory)
                                {
                                    Console.WriteLine(" - " + fi.Name);
                                }

                                uploadfile = Properties.Settings.Default.RutaEncrypted + fileName.Replace("csv", "txt");
                                using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                                {
                                    Console.WriteLine("Uploading {0} ({1:N0} bytes)",
                                                        uploadfile, fileStream.Length);
                                    //client.BufferSiz = 4 * 1024; // bypass Payload error large files
                                    client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                                }
                                uploadfile = Properties.Settings.Default.RutaEncrypted + fileName.Replace(".csv", "") + ".encrypted";
                                using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                                {
                                    Console.WriteLine("Uploading {0} ({1:N0} bytes)",
                                                        uploadfile, fileStream.Length);
                                    //client.BufferSiz = 4 * 1024; // bypass Payload error large files
                                    client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                                }

                                client.Disconnect();
                            }

                            ////////////////////////////////////////////////////////////////////////////////

                            //ftpSIVPRE.upload(uploadFile.Replace("txt", "txt.gz"), localName.Replace("txt", "txt.gz"));
                            procesedNameCSV = localFilePath + "procesado\\" + fileName;
                            procesedNameZIP = localFilePath + "procesado\\" + fileName + ".zip";
                            procesedNameHash = Properties.Settings.Default.RutaEncrypted + "procesado\\" + fileName.Replace("csv", "txt");
                            procesedNameEncrypted = Properties.Settings.Default.RutaEncrypted+ "procesado\\" + fileName.Replace(".csv", "") + ".encrypted";
                            File.Move(localName, procesedNameCSV);
                            File.Move(localFilePath + fileName + ".zip", procesedNameZIP);
                            File.Move(Properties.Settings.Default.RutaEncrypted + fileName.Replace("csv", "txt"), procesedNameHash);
                            File.Move(Properties.Settings.Default.RutaEncrypted + fileName.Replace(".csv", "") + ".encrypted", procesedNameEncrypted);
                        }
                        catch (Exception ex)
                        {
                            Console.Write(ex);

                        }
                    }    
                    

                }
            }
            catch (NullReferenceException)
            {
                log.EscribirLog( "No hay archivos por transferir");

            }
            catch (Exception ex) {
                log.EscribirLog(ex);
                flag = false;
            }
            finally { 
             if (flag) 
                 log.EscribirLog("Finalizado el envio"); 
             else
                 log.EscribirLog("fallos durante el envio");
            }
        }

        protected string GetMD5HashFromFile(string fileName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

        byte[] ComputeHash(string filePath)
        {
            using (var md5 = MD5.Create())
            {
                return md5.ComputeHash(File.ReadAllBytes(filePath));
            }
        }

        public static void Compress(FileInfo fileToCompress)
        {
            using (FileStream originalFileStream = fileToCompress.OpenRead())
            {
                if ((File.GetAttributes(fileToCompress.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                {
                    using (FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".zip"))
                    {
                        using (GZipStream compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                        {
                            originalFileStream.CopyTo(compressionStream);
                        }
                    }
                }
            }
        }

        //////////////////////
        //ESTE CODIGO POSIBLEMENTE FUNCIONE PERO EN ESTE CASO NO ME SIRVE DEBIDO A QUE SOLO COMPRIME FOLDERS (NO SE LLOGO A INTENTAR MODIFICARLO
        //////////////////////
        //////////////static public Boolean ComprimirFolder(string Ruta, string Nombre)
        //////////////{

        //////////////    try
        //////////////    {
        //////////////        using (ZipFile zip = new ZipFile())
        //////////////        {
        //////////////            zip.AddDirectory(Ruta);

        //////////////            zip.Save(HttpContext.Current.Server.MapPath("temp/" + Nombre + ".zip"));

        //////////////        }

        //////////////        return true;
        //////////////    }
        //////////////    catch
        //////////////    {
        //////////////        return false;

        //////////////    }

        //////////////}
        static public Boolean DescromprimirZip(string ArchivoZip, string RutaGuardar)
        {
            try
            {
                using (ZipFile zip = ZipFile.Read(ArchivoZip))
                {

                    zip.ExtractAll(RutaGuardar);
                    zip.Dispose();

                }

                return true;
            }
            catch
            {
                return false;

            }

        }


        public  void downloadSIVPRE()
        {
            //string downloadFile;
            //string fileName;
            //string localName;
            //string[] registroFTP;
            
            //byte[] file;
            //byte[] decompressed;

            bool flag = true;
            String ArchivoDescargar = "";
            try
            {


                //////////////////////////////////////////////////////////////////////////////

                int port = 22;
                string host = Properties.Settings.Default.servidor;
                string username = Properties.Settings.Default.usuario;
                string password = Properties.Settings.Default.pass;
                string workingdirectory = "/recibido";
                string uploadfile = "";

                Console.WriteLine("Creating client and connecting");
                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    var listDirectory = client.ListDirectory(workingdirectory);
                    Console.WriteLine("Listing directory:");
                    foreach (var fi in listDirectory)
                    {
                        Console.WriteLine(" - " + fi.Name);
                        if (fi.Name.Contains(".zip"))
                        {
                            ArchivoDescargar = fi.Name;
                        }
                    }
                    using (var file = File.OpenWrite(ArchivoDescargar))
                    {
                        client.DownloadFile(workingdirectory+"/"+ArchivoDescargar, file);
                        flag = true;
                    }
                    client.Disconnect();
                }

                ////////////////////////////////////////////////////////////////////////////////


            }
            catch (NullReferenceException)
            {
                log.EscribirLog("No hay archivos por descargar");
                flag = false;

            }
            catch (Exception ex)
            {
                
                log.EscribirLog(ex);
                flag = false;
            }
            finally
            {
                if (flag)
                {
                    File.Move(ArchivoDescargar, Properties.Settings.Default.rutaDescarga + ArchivoDescargar);
                    File.Delete(ArchivoDescargar);
                    if (!(DescromprimirZip(Properties.Settings.Default.rutaDescarga + ArchivoDescargar, Properties.Settings.Default.rutaDescarga)))
                    {
                        log.EscribirLog("Fallo al momento de descomprimir.");
                    }
                    else
                    {
                        File.Move(Properties.Settings.Default.rutaDescarga + ArchivoDescargar, Properties.Settings.Default.rutaDescarga +"zips\\"+ ArchivoDescargar);
                    }

                    log.EscribirLog("Finalizada la descarga.");
                }
                else
                {
                    log.EscribirLog("fallos durante el proceso de descarga.");
                }
            }

        }

    }
}
