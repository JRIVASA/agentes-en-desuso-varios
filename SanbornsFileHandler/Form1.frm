VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3030
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4560
   LinkTopic       =   "Form1"
   ScaleHeight     =   3030
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    
    On Error GoTo Error
    
    If App.PrevInstance Then End
    
    Dim Setup As String
    
    Setup = FindPath("Setup.ini", NO_SEARCH_MUST_FIND_EXACT_MATCH)
    
    Dim RutaArchivoCabecero As String
    Dim RutaArchivoDetalle  As String
    Dim RutaArchivoControl  As String
    
    RutaBaseArchivos = ObtenerConfiguracion(Setup, "Config", "RutaBaseArchivos", vbNullString)
    RutaDestinoArchivos = ObtenerConfiguracion(Setup, "Config", "RutaDestinoArchivos", vbNullString)
    ArchivoControl = (Val(ObtenerConfiguracion(Setup, "Config", "ArchivoControl", vbNullString)) = 1)
    AgruparVariasTransacciones = (Val(ObtenerConfiguracion(Setup, "Config", "AgruparVariasTransacciones", vbNullString)) = 1)
    
    If Trim(RutaBaseArchivos) = vbNullString Then
        SaveTextFile vbNullString, FindPath("Error_RutaBaseArchivos_NoDefinida.log", NO_SEARCH_MUST_FIND_EXACT_MATCH)
        End
    Else
        KillSecure FindPath("Error_RutaBaseArchivos_NoDefinida.log", NO_SEARCH_MUST_FIND_EXACT_MATCH)
    End If
    
    If Trim(RutaDestinoArchivos) = vbNullString Then
        SaveTextFile vbNullString, FindPath("Error_RutaDestinoArchivos_NoDefinida.log", NO_SEARCH_MUST_FIND_EXACT_MATCH)
        End
    Else
        KillSecure FindPath("Error_RutaDestinoArchivos_NoDefinida.log", NO_SEARCH_MUST_FIND_EXACT_MATCH)
    End If
    
    If Not AgruparVariasTransacciones Then
        
        If Dir(FindPath(vbNullString, _
        NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos) & _
        "*.ASC") <> vbNullString Then End
        
        Dim TmpArchivoCabecero As String, TmpArchivoDetalle As String, TmpArchivoControl As String
        Dim TmpRutaFinalCabecero As String, TmpRutaFinalDetalle As String, TmpRutaFinalControl As String
        
        TmpArchivoCabecero = Dir(FindPath(vbNullString, _
        NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos) & _
        "*PLUTXN*.ASC")
        
        If TmpArchivoCabecero <> vbNullString Then
            
            TmpPos1 = InStr(1, TmpArchivoCabecero, "_")
            
            TmpPos2 = InStr(TmpPos1 + 1, TmpArchivoCabecero, "_")
            
            TmpPos3 = InStr(TmpPos2 + 1, TmpArchivoCabecero, "_")
            
            TmpPos4 = InStr(TmpPos3 + 1, TmpArchivoCabecero, "_")
            
            TmpPos5 = InStr(TmpPos4 + 1, TmpArchivoCabecero, "_")
            
            TmpPos6 = InStr(TmpPos5 + 1, TmpArchivoCabecero, "_")
            
            TmpPos7 = InStr(TmpPos6 + 1, TmpArchivoCabecero, "_")
            
            TmpPos8 = InStr(TmpPos7 + 1, TmpArchivoCabecero, "_")
            
            TmpCorrelativo = Mid(TmpArchivoCabecero, TmpPos1 + 1, TmpPos2 - TmpPos1 - 1)
            
            TmpArchivoDetalle = Dir(FindPath(vbNullString, _
            NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos) & _
            "*LINKTXN*" & TmpCorrelativo & "*.ASC")
            
            If TmpArchivoDetalle <> vbNullString Then
                
                TmpRutaFinalCabecero = FindPath("PLUTXN" & "001" & "00000" & "000" & _
                Mid(TmpArchivoCabecero, TmpPos3 + 1, TmpPos4 - TmpPos3 - 1) & _
                Mid(TmpArchivoCabecero, TmpPos4 + 1, TmpPos5 - TmpPos4 - 1) & _
                Mid(TmpArchivoCabecero, TmpPos5 + 1, TmpPos6 - TmpPos5 - 1) & _
                Mid(TmpArchivoCabecero, TmpPos6 + 1, TmpPos7 - TmpPos6 - 1) & ".ASC", _
                NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                
                TmpRutaFinalDetalle = FindPath("LINKTXN.ASC", NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                
                Dim ContenidoFinalCabecero, ContenidoFinalDetalle, ContenidoFinalControl
                
                If PathExists(TmpRutaFinalCabecero) Then ContenidoFinalCabecero = LoadTextFile(TmpRutaFinalCabecero)
                If PathExists(TmpRutaFinalDetalle) Then ContenidoFinalDetalle = LoadTextFile(TmpRutaFinalDetalle)
                If PathExists(TmpRutaFinalControl) Then ContenidoFinalControl = LoadTextFile(TmpRutaFinalControl)
                
                ContenidoFinalCabecero = _
                LoadTextFile(FindPath(TmpArchivoCabecero, NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos))
                
                ContenidoFinalDetalle = _
                LoadTextFile(FindPath(TmpArchivoDetalle, NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos))
                
                If SaveTextFile(ContenidoFinalCabecero, TmpRutaFinalCabecero) _
                And SaveTextFile(ContenidoFinalDetalle, TmpRutaFinalDetalle) Then
                    
                    KillSecure FindPath(TmpArchivoCabecero, NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos)
                    KillSecure FindPath(TmpArchivoDetalle, NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos)
                    
                End If
                
                If ArchivoControl Then
                    TmpRutaFinalControl = FindPath("newparm.trg", NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                    SaveTextFile ContenidoFinalControl, TmpRutaFinalControl
                End If
                
            End If
            
        End If
        
    Else
        
        Dim EnProceso  As String: EnProceso = "\EnProceso"
        Dim TmpArchivosEnProceso As String, TmpRutaArchivoEnProceso As String, TmpRutaArchivoReprocesar As String
        
        CreateFullDirectoryPath FindPath(vbNullString, _
        NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos & EnProceso)
        
        TmpArchivosEnProceso = Dir(FindPath(vbNullString, _
        NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos & EnProceso) & _
        "*.ASC")
         
        Do While TmpArchivosEnProceso <> vbNullString
            
            TmpRutaArchivoEnProceso = FindPath(TmpArchivosEnProceso, _
            NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos & EnProceso)
            
            TmpRutaArchivoReprocesar = FindPath(TmpArchivosEnProceso, _
            NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos)
            
            SaveTextFile LoadTextFile(TmpRutaArchivoEnProceso), TmpRutaArchivoReprocesar
            KillSecure TmpRutaArchivoEnProceso
            
            TmpArchivosEnProceso = Dir
            
        Loop
        
        TmpArchivoCabecero = Dir(FindPath(vbNullString, _
        NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos) & _
        "*PLUTXN*.ASC")
        
        Dim Eliminar As New Collection
        
        Do While TmpArchivoCabecero <> vbNullString
            
            TmpPos1 = InStr(1, TmpArchivoCabecero, "_")
            
            TmpPos2 = InStr(TmpPos1 + 1, TmpArchivoCabecero, "_")
            
            TmpPos3 = InStr(TmpPos2 + 1, TmpArchivoCabecero, "_")
            
            TmpPos4 = InStr(TmpPos3 + 1, TmpArchivoCabecero, "_")
            
            TmpPos5 = InStr(TmpPos4 + 1, TmpArchivoCabecero, "_")
            
            TmpPos6 = InStr(TmpPos5 + 1, TmpArchivoCabecero, "_")
            
            TmpPos7 = InStr(TmpPos6 + 1, TmpArchivoCabecero, "_")
            
            TmpPos8 = InStr(TmpPos7 + 1, TmpArchivoCabecero, "_")
            
            TmpCorrelativo = Mid(TmpArchivoCabecero, TmpPos1 + 1, TmpPos2 - TmpPos1 - 1)
            
            TmpArchivoDetalle = Dir(FindPath(vbNullString, _
            NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos) & _
            "*LINKTXN*" & TmpCorrelativo & "*.ASC")
            
            If TmpArchivoDetalle <> vbNullString Then
                
                If TmpRutaFinalCabecero = vbNullString Then
                    
                    TmpRutaFinalCabecero = Dir(FindPath(vbNullString, _
                    NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos) & _
                    "*PLUTXN*.ASC")
                    
                    If TmpRutaFinalCabecero <> vbNullString Then
                        TmpRutaFinalCabecero = _
                        FindPath(TmpRutaFinalCabecero, NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                    End If
                    
                End If
                
                If TmpRutaFinalCabecero = vbNullString Then _
                TmpRutaFinalCabecero = FindPath("PLUTXN" & "001" & "00000" & "000" & _
                Mid(TmpArchivoCabecero, TmpPos3 + 1, TmpPos4 - TmpPos3 - 1) & _
                Mid(TmpArchivoCabecero, TmpPos4 + 1, TmpPos5 - TmpPos4 - 1) & _
                Mid(TmpArchivoCabecero, TmpPos5 + 1, TmpPos6 - TmpPos5 - 1) & _
                Mid(TmpArchivoCabecero, TmpPos6 + 1, TmpPos7 - TmpPos6 - 1) & ".ASC", _
                NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                
                If TmpRutaFinalDetalle = vbNullString Then _
                TmpRutaFinalDetalle = FindPath("LINKTXN.ASC", NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                
                ' --------------------- '
                
                If Len(ContenidoFinalCabecero) = 0 Then ContenidoFinalCabecero = LoadTextFile(TmpRutaFinalCabecero)
                If Len(ContenidoFinalCabecero) > 0 Then ContenidoFinalCabecero = ContenidoFinalCabecero & GetLines
                
                If Len(ContenidoFinalDetalle) = 0 Then ContenidoFinalDetalle = LoadTextFile(TmpRutaFinalDetalle)
                If Len(ContenidoFinalDetalle) > 0 Then ContenidoFinalDetalle = ContenidoFinalDetalle & GetLines
                
                ' --------------------- '
                
                Eliminar.Add Array( _
                FindPath(CStr(TmpArchivoCabecero), NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos), _
                FindPath(CStr(TmpArchivoDetalle), NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos), _
                FindPath(CStr(TmpArchivoCabecero), NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos & EnProceso), _
                FindPath(CStr(TmpArchivoDetalle), NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos & EnProceso) _
                )
                
                SaveTextFile LoadTextFile(Eliminar(Eliminar.Count)(0)), Eliminar(Eliminar.Count)(2)
                SaveTextFile LoadTextFile(Eliminar(Eliminar.Count)(1)), Eliminar(Eliminar.Count)(3)
                KillSecure Eliminar(Eliminar.Count)(0)
                KillSecure Eliminar(Eliminar.Count)(1)
                
                ContenidoFinalCabecero = ContenidoFinalCabecero & _
                LoadTextFile(Eliminar(Eliminar.Count)(2))
                
                ContenidoFinalDetalle = ContenidoFinalDetalle & _
                LoadTextFile(Eliminar(Eliminar.Count)(3))
                
            End If
            
            TmpArchivoCabecero = Dir(FindPath(vbNullString, _
            NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaBaseArchivos) & _
            "*PLUTXN*.ASC")
            
        Loop
        
        ' --------------------- '
        
        If ContenidoFinalCabecero <> vbNullString And ContenidoFinalDetalle <> vbNullString Then
            
            If SaveTextFile(ContenidoFinalCabecero, TmpRutaFinalCabecero) _
            And SaveTextFile(ContenidoFinalDetalle, TmpRutaFinalDetalle) Then
                
                For Each TmpArchivo In Eliminar
                    
                    KillSecure CStr(TmpArchivo(2))
                    KillSecure CStr(TmpArchivo(3))
                    
                Next
                
            End If
            
            If ArchivoControl Then
                TmpRutaFinalControl = FindPath("newparm.trg", NO_SEARCH_MUST_FIND_EXACT_MATCH, RutaDestinoArchivos)
                SaveTextFile ContenidoFinalControl, TmpRutaFinalControl
            End If
            
        End If
        
    End If
    
Error:
    
    End
    
End Sub
