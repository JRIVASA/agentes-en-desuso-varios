VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Srv_Destino As String
Public Srv_Origen As String

Public BD_Destino As String
Public BD_Origen As String

Public UserDBDestino As String
Public UserDBOrigen As String

Public UserPwdDestino As String
Public UserPwdOrigen As String

Public CodigoEmpresa As String

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Public Function sgetini(SIniFile As String, SSection As String, SKey _
    As String, SDefault As String) As String
    Dim STemp As String * 256
    Dim NLength As Integer
    
    STemp = Space$(256)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, 255, SIniFile)
    sgetini = Left$(STemp, NLength)
End Function

Public Sub Ejecutar()
Dim rs As New ADODB.Recordset
Call ConectarBDD

    'TABLA ORIGEN
    Dim setup As String
    setup = App.Path & "\setup.ini"
    tbOrigen = sgetini(setup, "Server", "TablaOrigen", "?")
    If tbOrigen = "?" Then
        Call MsgBox("La tabla de la interfaz no est� definida.", vbCritical, "Error")
        Exit Sub
    End If

On Error GoTo error1
    ENT.BDD.BeginTrans
    ENT2.BDD.BeginTrans
    
        Set rs = ENT.BDD.Execute("SELECT (MAX(isnull(Codigo,0)) + 1) AS Codigo FROM MA_COMPROBANTES")
        If IsNull(rs!codigo) Then
            codigo = 1
        Else
            codigo = rs!codigo
        End If
        
        sql = "SELECT * FROM " & tbOrigen & " WHERE (ISNULL(nro_comprobante,'') = '' OR nro_comprobante = '0') AND compania = '" & CodigoEmpresa & "'"
        Set rs = ENT2.BDD.Execute(sql)
        
        sql = "INSERT INTO MA_COMPROBANTES (Codigo, Fecha, Status, Descripcion, Observacion, Cerrado, Estado) values " & _
            " ('" & codigo & "', CONVERT(DATE, GETDATE()), 1, 'Comprobante autom�tico - Interfaz isACCOUNT - RRHH', " & _
            " 'Comprobante autom�tico - Interfaz isACCOUNT - RRHH', 1,'DCO')"
        ENT.BDD.Execute (sql)
        
        While Not rs.EOF
            sql = "INSERT INTO TR_COMPROBANTES (Codigo, IDDeCuenta, Tipo, Monto, Referencia, Descripcion, Fecha, IDDeComprobante, USUARIO, IDDeCentroCosto, sucursal) values" & _
                " ('" & rs!codigo_contable & "'," & BuscarIdCuenta(rs!codigo_contable) & ", " & rs!Tipo & ", " & rs!importe & ", '" & rs!referencia & "', " & _
                " '" & rs!Descripcion & "', CONVERT(DATE, '" & rs!fecha & "'), (SELECT MAX(IdDeComprobante) AS IdComprobante FROM MA_COMPROBANTES), 'AGENTE', " & BuscarIdCentroDeCosto(rs!centro_costo) & ", '" & rs!sucursal & "')"
            ENT.BDD.Execute (sql)
            rs.MoveNext
        Wend
        
        ENT2.BDD.Execute ("UPDATE " & tbOrigen & " SET nro_comprobante = " & codigo & ", fecha_proceso = GETDATE() WHERE ISNULL(nro_comprobante,'') = '' OR nro_comprobante = '0'")
        
    ENT.BDD.CommitTrans
    ENT2.BDD.CommitTrans
    Exit Sub
error1:
Debug.Print sql
    ENT.BDD.RollbackTrans
    ENT2.BDD.RollbackTrans
    MsgBox Err.Description
End Sub

Private Sub ConectarBDD()
    On Error GoTo Errores
    Dim setup As String
    setup = App.Path & "\setup.ini"
    
    'SERVIDORES
    Srv_Origen = sgetini(setup, "Server", "ServidorOrigen", "?")
    If Srv_Origen = "?" Then
        Call MsgBox("El servidor de origen no est� configurado.", vbCritical, "Error")
        Exit Sub
    End If
    
    Srv_Destino = sgetini(setup, "Server", "ServidorDestino", "?")
    If Srv_Destino = "?" Then
        Call MsgBox("El servidor de destino no est� configurado.", vbCritical, "Error")
        Exit Sub
    End If
    
    'BASE DE DATOS
    BD_Origen = sgetini(setup, "Server", "BaseDeDatosOrigen", "?")
    If BD_Origen = "?" Then
        Call MsgBox("La base de datos de origen no est� configurada.", vbCritical, "Error")
        Exit Sub
    End If
    
    BD_Destino = sgetini(setup, "Server", "BaseDeDatosDestino", "?")
    If BD_Destino = "?" Then
        Call MsgBox("La base de datos de destino no est� configurada.", vbCritical, "Error")
        Exit Sub
    End If
    
    'EMPRESA
    CodigoEmpresa = sgetini(setup, "Server", "CodigoEmpresa", "01")
    
    'USUARIO Y CONTRASE�A
    UserDBOrigen = sgetini(setup, "Entrada", "UsuarioBaseDeDatosOrigen", "?")
    If UserDBOrigen = "?" Then
        UserDBOrigen = "sa"
    End If
    
    UserPwdOrigen = sgetini(setup, "Entrada", "PasswordBaseDeDatosOrigen", "?")
    If UserPwdOrigen = "?" Then
        UserPwdOrigen = ""
    End If
    
    UserDBDestino = sgetini(setup, "Entrada", "UsuarioBaseDeDatosDestino", "?")
    If UserDBDestino = "?" Then
        UserDBDestino = "sa"
    End If
    
    UserPwdDestino = sgetini(setup, "Entrada", "PasswordBaseDeDatosDestino", "?")
    If UserPwdDestino = "?" Then
        UserPwdDestino = ""
    End If

'INICIO CONEXION

    'ENT = DESTINO
    If ENT.BDD.State = adStateOpen Then ENT2.BDD.Close
    ENT.BDD.ConnectionString = "Driver={SQL Server};Server=" & Srv_Destino & ";Database=" & BD_Destino & ";Uid=" & UserDBDestino & ";Pwd=" & UserPwdDestino & ";"
    ENT.BDD.CommandTimeout = 0
    ENT.BDD.Open
    
    'ENT2 = ORIGEN
    If ENT2.BDD.State = adStateOpen Then ENT2.BDD.Close
    ENT2.BDD.ConnectionString = "Driver={SQL Server};Server=" & Srv_Origen & ";Database=" & BD_Origen & ";Uid=" & UserDBOrigen & ";Pwd=" & UserPwdOrigen & ";"
    ENT2.BDD.CommandTimeout = 0
    ENT2.BDD.Open

    ConectarBaseDatos = True
    Exit Sub
Errores:
    Call MsgBox(Err.Description, vbCritical, "Error")
End Sub

Private Function Reasignar_SInfo_ConStr(pConStr As String) As String
    
    Debug.Print pConStr
    
    Dim TempPassword As String
    
    If UCase(pConStr) Like "*PASSWORD=*" Or UCase(pConStr) Like "*PWD=*" Then
    
        'La conexi�n tiene los valores, no deber�a haber problemas al abrirla.
        
        Reasignar_SInfo_ConStr = pConStr
        
    Else
    
        If (UCase(pConStr) Like "*" & UCase(UserDBDestino) & "*") Then
            TempPassword = UserPwdDestino
        ElseIf (UCase(pConStr) Like "*" & UCase(UserDBOrigen) & "*") Then
            TempPassword = UserPwdOrigen
        Else
            TempPassword = ""
        End If
    
        Reasignar_SInfo_ConStr = pConStr & IIf(Right(pConStr, 1) <> ";", ";", "") & "Password=" & TempPassword & ";"
    
    End If
    
    Debug.Print Reasignar_SInfo_ConStr
    
End Function

Private Function BuscarIdCuenta(pCodigo) As Long
    Dim mRs As New ADODB.Recordset
    
    If ENT.BDD.State = adStateClosed Then ENT.BDD.Open Reasignar_SInfo_ConStr(ENT.BDD.ConnectionString)
    mRs.Open "SELECT IDDeCuenta FROM ma_cuentas WHERE codigo = '" & pCodigo & "' ", ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarIdCuenta = mRs!IDDeCuenta
    End If
    mRs.Close
End Function

Private Function BuscarIdCentroDeCosto(pDescripcion) As Long
    Dim mRs As New ADODB.Recordset
    
    If ENT.BDD.State = adStateClosed Then ENT.BDD.Open Reasignar_SInfo_ConStr(ENT.BDD.ConnectionString)
    mRs.Open "SELECT IDDeCentroCosto FROM MA_CentroCosto WHERE Descripcion ='" & pDescripcion & "' ", ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarIdCentroDeCosto = mRs!IDDeCentroCosto
    Else
        BuscarIdCentroDeCosto = -1
    End If
    mRs.Close
End Function
