Attribute VB_Name = "CosolaADM"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpString _
    As Any, ByVal lpFileName As String) As Long

'Para obtener el tama�o de la pantalla
Private Const ABM_GETTASKBARPOS = &H5

Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Type APPBARDATA
    cbSize As Long
    hWnd As Long
    uCallbackMessage As Long
    uEdge As Long
    rc As RECT
    lParam As Long
End Type

Private Declare Function SHAppBarMessage Lib "shell32.dll" (ByVal dwMessage As Long, pData As APPBARDATA) As Long

Public RootTitle As String

Public Type ProgramArgumentsData
    
    RawParameterString          As String
    ParameterCollection()       As String
    ParamID                     As String
    ParamExists                 As Boolean
    ParamValue                  As String
    
End Type

Public Const ProgramArgumentsDataDelimiter = "|p|"

Public ProgramArgs As ProgramArgumentsData

Public Function LocateParam(ByVal ParamIDString As String, _
ByRef pParamsArr() As String, Optional ByRef OutParamValue, _
Optional ByRef OutRawParam) As Boolean
    
    LocateParam = False
    
    Dim RawParam
    
    For Each RawParam In pParamsArr
        If UCase(RawParam) Like "*" & UCase(ParamIDString) & "*" Then
            If Left(UCase(LTrim(RawParam)), Len(ParamIDString)) = UCase(ParamIDString) Then
                
                LocateParam = True
                
                If Not IsMissing(OutParamValue) Then
                    OutParamValue = GetParamValue(ParamIDString, RawParam)
                End If
                
                If Not IsMissing(OutRawParam) Then
                    OutRawParam = RawParam
                End If
                
                Exit Function
                
            End If
        End If
    Next
    
End Function

Public Function GetParamValue(ByVal ParamIDString As String, ByVal pRawParam As String) As String
    
    Dim ParamIDLength As Long
    
    ParamIDLength = Len(ParamIDString)
    
    If Len(ParamIDString) < Len(LTrim(pRawParam)) Then
        GetParamValue = Strings.Mid(LTrim(pRawParam), Len(ParamIDString) + 1, Len(pRawParam) - Len(ParamIDString) + 1)
    Else
        GetParamValue = vbNullString
    End If
    
End Function

Sub Main()

    With ProgramArgs
        
        .RawParameterString = Command$()
        .ParameterCollection = Strings.Split(ProgramArgs.RawParameterString, ProgramArgumentsDataDelimiter)
        
        ' --------------
        
        .ParamID = "/TargetServer="
        .ParamExists = LocateParam(.ParamID, .ParameterCollection)
        
        If .ParamExists Then
            
            .ParamValue = GetParamValue(.ParamID, .RawParameterString)
            
            If .ParamValue <> vbNullString Then
                Server = .ParamValue
            Else
                GoTo UndefinedServer
            End If
            
        Else
            
UndefinedServer:
            
            Server = sGetIni(App.Path & "\Setup.ini", "SERVER", "Srv_Local", ".")
            
        End If
        
        ' --------------
        
    End With
    
    If App.PrevInstance Then End
    Dim mConfig As ClsConsolaAdministrativa
    Set mConfig = New ClsConsolaAdministrativa
    'mConfig.IniciarInterfaz ".", etiConfiguracion
    RootTitle = sGetIni(App.Path & "\Setup.ini", "GENERAL", "ROOT_TITLE", "Stellar Platform")
    
    mConfig.IniciarInterfaz Server, etiConfiguracion
    Set mConfig = Nothing
    
    End
    
End Sub

Public Function sGetIni(SIniFile As String, SSection As String, _
sKey As String, SDefault As String) As String
    
    Const ParamMaxLength = 10000
    
    Dim STemp As String * ParamMaxLength
    Dim NLength As Integer
    
    STemp = Space$(ParamMaxLength)
    
    NLength = GetPrivateProfileString(SSection, sKey, SDefault, STemp, ParamMaxLength, SIniFile)
    
    sGetIni = Left$(STemp, NLength)
    
End Function

Public Sub sWriteIni(SIniFile As String, SSection As String, _
sKey As String, sData As String)
    WritePrivateProfileString SSection, sKey, sData, SIniFile
End Sub

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function GetTaskBarInfo() As APPBARDATA
    
    On Error Resume Next
    
    Dim ABD As APPBARDATA
    
    SHAppBarMessage ABM_GETTASKBARPOS, ABD
    
    GetTaskBarInfo = ABD
    
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    
    Dim TbInfo As APPBARDATA
    
    TbInfo = GetTaskBarInfo
    
    If TbInfo.rc.Bottom = 0 And TbInfo.rc.Top = 0 And TbInfo.rc.Right = 0 And TbInfo.rc.Left = 0 Then
        TbInfo.uEdge = -1
    End If
    
    Select Case TbInfo.uEdge
        
        Case 3
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) - (((TbInfo.rc.Bottom - TbInfo.rc.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If Forma.Top < 0 Then
                Forma.Top = 0
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left) < 0 Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 2
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) - (((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < 0 Then
                Forma.Left = 0
            End If
            
        Case 1
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2)) + (((TbInfo.rc.Bottom - TbInfo.rc.Top) * Screen.TwipsPerPixelY) / 2)
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2))
            
            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If (Screen.Width - Forma.Width - Forma.Left < 0) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case 0
            
            ' Intentar Centrar tomando en cuenta la TaskBar.
            
            Forma.Top = ((Screen.Height / 2) - (Forma.Height / 2))
            Forma.Left = ((Screen.Width / 2) - (Forma.Width / 2)) + (((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) / 2)

            ' Si no es completamente visible hacerla visible ignorando lo anterior.
            
            If (Screen.Height - Forma.Height - Forma.Top) < 0 Then
                Forma.Top = Screen.Height - Forma.Height
            End If
            
            If Forma.Left < ((TbInfo.rc.Right - TbInfo.rc.Left) * Screen.TwipsPerPixelX) Then
                Forma.Left = Screen.Width - Forma.Width
            End If
            
        Case -1
            
            Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
            Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
            
            If Forma.Left < 0 Then Forma.Left = 0
            If Forma.Top < 0 Then Forma.Top = 0
            
    End Select
    
    ' El Form Navegador tiene un footer que siempre debe quedar en bottom.
    
    If EsFormNavegador Then
        Forma.Frame1.Top = (Forma.Height - Forma.Frame1.Height)
    End If
    
End Sub

Public Sub ListSafeItemSelection(pCmb As Object, ByVal pStrVal As String)
    On Error Resume Next
    pCmb.Text = pStrVal
End Sub
