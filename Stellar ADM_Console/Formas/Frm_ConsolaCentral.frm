VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_ConsolaCentral 
   Appearance      =   0  'Flat
   BackColor       =   &H00EBEBEB&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Begin VB.Frame FrmUsuario 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBEB&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   9855
      Left            =   13920
      TabIndex        =   32
      Top             =   600
      Visible         =   0   'False
      Width           =   8295
      Begin VB.TextBox Text5 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   45
         Text            =   "Text1"
         Top             =   6240
         Width           =   3855
      End
      Begin VB.TextBox Text4 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   43
         Text            =   "Text1"
         Top             =   5040
         Width           =   3855
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   41
         Text            =   "Text1"
         Top             =   3840
         Width           =   3855
      End
      Begin VB.TextBox Text2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   39
         Text            =   "Text1"
         Top             =   2520
         Width           =   5655
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   37
         Text            =   "Text1"
         Top             =   1320
         Width           =   3855
      End
      Begin VB.Frame Frame12 
         Appearance      =   0  'Flat
         BackColor       =   &H00E2962E&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   4080
         TabIndex        =   35
         Top             =   9360
         Width           =   1815
         Begin VB.Label Label8 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Aceptar"
            BeginProperty Font 
               Name            =   "Arial Rounded MT Bold"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   36
            Top             =   105
            Width           =   1575
         End
      End
      Begin VB.Frame Frame11 
         Appearance      =   0  'Flat
         BackColor       =   &H00E2962E&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   6360
         TabIndex        =   33
         Top             =   9360
         Width           =   1815
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Arial Rounded MT Bold"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   34
            Top             =   105
            Width           =   1575
         End
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Nivel"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   46
         Top             =   5880
         Width           =   1935
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Password"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   44
         Top             =   4680
         Width           =   1935
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Login"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   42
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   40
         Top             =   2160
         Width           =   1935
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   38
         Top             =   960
         Width           =   1935
      End
      Begin VB.Image Image5 
         Height          =   540
         Left            =   240
         Picture         =   "Frm_ConsolaCentral.frx":0000
         Top             =   120
         Width           =   540
      End
   End
   Begin VB.Frame Frame9 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      Caption         =   "Frame9"
      ForeColor       =   &H80000008&
      Height          =   9735
      Left            =   480
      TabIndex        =   30
      Top             =   720
      Width           =   6015
      Begin MSComctlLib.TreeView Arbol 
         Height          =   9615
         Left            =   240
         TabIndex        =   31
         Top             =   120
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   16960
         _Version        =   393217
         LabelEdit       =   1
         Style           =   3
         ImageList       =   "iconos"
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame8 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Caption         =   "Frame8"
      Height          =   525
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Visible         =   0   'False
      Width           =   14775
   End
   Begin VB.Frame FrmPais 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBEB&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   9855
      Left            =   12120
      TabIndex        =   3
      Top             =   720
      Visible         =   0   'False
      Width           =   8295
      Begin VB.TextBox Txt_CodigoPais 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   840
         TabIndex        =   47
         Text            =   "Text1"
         Top             =   1440
         Width           =   3855
      End
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H00E2962E&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   6360
         TabIndex        =   15
         Top             =   9360
         Width           =   1815
         Begin VB.Label Lbl_cancelarPais 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Arial Rounded MT Bold"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   16
            Top             =   105
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00E2962E&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   4080
         TabIndex        =   13
         Top             =   9360
         Width           =   1815
         Begin VB.Label Lbl_AceptarPais 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Aceptar"
            BeginProperty Font 
               Name            =   "Arial Rounded MT Bold"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   14
            Top             =   105
            Width           =   1575
         End
      End
      Begin VB.TextBox Txt_PaisNombre 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   840
         TabIndex        =   4
         Text            =   "Text1"
         Top             =   2520
         Width           =   5295
      End
      Begin VB.Label Lbl_TituloPais 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pais"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   495
         Left            =   960
         TabIndex        =   49
         Top             =   150
         Width           =   2295
      End
      Begin VB.Label Lbl_CodigoPais 
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   840
         TabIndex        =   48
         Top             =   1080
         Width           =   1935
      End
      Begin VB.Image Image3 
         Height          =   540
         Left            =   240
         Picture         =   "Frm_ConsolaCentral.frx":0A7A
         Top             =   120
         Width           =   540
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   840
         TabIndex        =   10
         Top             =   2160
         Width           =   1935
      End
   End
   Begin VB.Frame Frm_Organizacion 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBEBEB&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      ForeColor       =   &H80000008&
      Height          =   9855
      Left            =   10680
      TabIndex        =   7
      Top             =   720
      Visible         =   0   'False
      Width           =   8295
      Begin VB.Frame Frame6 
         Appearance      =   0  'Flat
         BackColor       =   &H00E2962E&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   6360
         TabIndex        =   24
         Top             =   9360
         Width           =   1815
         Begin VB.Label Lbl_cancelarOrg 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Arial Rounded MT Bold"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   25
            Top             =   105
            Width           =   1575
         End
      End
      Begin VB.Frame Frame5 
         Appearance      =   0  'Flat
         BackColor       =   &H00E2962E&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   4080
         TabIndex        =   22
         Top             =   9360
         Width           =   1815
         Begin VB.Label Lbl_AceptarOrg 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "Aceptar"
            BeginProperty Font 
               Name            =   "Arial Rounded MT Bold"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   120
            TabIndex        =   23
            Top             =   120
            Width           =   1575
         End
      End
      Begin VB.ComboBox Combo_Pais 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   4920
         Width           =   3375
      End
      Begin VB.TextBox Txt_PaisOrg 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1200
         TabIndex        =   19
         Text            =   "Text1"
         Top             =   4920
         Width           =   2895
      End
      Begin VB.TextBox Txt_ServidorOrg 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   17
         Text            =   "Text1"
         Top             =   3720
         Width           =   3735
      End
      Begin VB.TextBox Txt_NombreOrg 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   9
         Text            =   "Text1"
         Top             =   2520
         Width           =   5415
      End
      Begin VB.TextBox Txt_CodigoOrg 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   1200
         TabIndex        =   8
         Text            =   "Text1"
         Top             =   1440
         Width           =   3615
      End
      Begin VB.Label Label14 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Region"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   495
         Left            =   1080
         TabIndex        =   50
         Top             =   120
         Width           =   2295
      End
      Begin VB.Image Image4 
         Height          =   540
         Left            =   240
         Picture         =   "Frm_ConsolaCentral.frx":14F4
         Top             =   120
         Width           =   540
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Pais"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   20
         Top             =   4560
         Width           =   2055
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Servidor"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   18
         Top             =   3360
         Width           =   2055
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   12
         Top             =   2160
         Width           =   1935
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Arial Unicode MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   1200
         TabIndex        =   11
         Top             =   1080
         Width           =   1935
      End
   End
   Begin VB.Frame BT_Org 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   6840
      TabIndex        =   26
      Top             =   3840
      Visible         =   0   'False
      Width           =   8175
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Agregar Usuario"
         BeginProperty Font 
            Name            =   "Arial Rounded MT Bold"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   140
         TabIndex        =   27
         Top             =   260
         Width           =   7935
      End
   End
   Begin VB.Frame BT_Region 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   6840
      TabIndex        =   5
      Top             =   2040
      Width           =   8175
      Begin VB.Image Image2 
         Height          =   540
         Left            =   360
         Picture         =   "Frm_ConsolaCentral.frx":1F6E
         Top             =   240
         Width           =   540
      End
      Begin VB.Label Lbl_AgregarOrg 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Agregar Region"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   495
         Left            =   120
         TabIndex        =   6
         Top             =   375
         Width           =   7935
      End
   End
   Begin VB.Frame Bt_Pais 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   6840
      TabIndex        =   1
      Top             =   720
      Width           =   8175
      Begin VB.Image Image1 
         Height          =   540
         Left            =   360
         Picture         =   "Frm_ConsolaCentral.frx":29E8
         Top             =   240
         Width           =   540
      End
      Begin VB.Label Lbl_Pais 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Agregar Pais"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   375
         Width           =   7935
      End
   End
   Begin MSFlexGridLib.MSFlexGrid GrdRegiones 
      Height          =   9735
      Left            =   480
      TabIndex        =   0
      Top             =   720
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   17171
      _Version        =   393216
      Cols            =   1
      FixedCols       =   0
      BackColorFixed  =   4210752
      BackColorBkg    =   16777215
      AllowBigSelection=   0   'False
      GridLines       =   0
      GridLinesFixed  =   0
      BorderStyle     =   0
      Appearance      =   0
   End
   Begin MSComctlLib.ImageList iconos 
      Left            =   0
      Top             =   10320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   18
      ImageHeight     =   18
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_ConsolaCentral.frx":3462
            Key             =   "raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_ConsolaCentral.frx":3A6C
            Key             =   "dpto"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_ConsolaCentral.frx":4076
            Key             =   "grupo"
         EndProperty
      EndProperty
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      X1              =   -120
      X2              =   20000
      Y1              =   540
      Y2              =   540
   End
   Begin VB.Label Cerrar 
      BackStyle       =   0  'Transparent
      Caption         =   "X"
      BeginProperty Font 
         Name            =   "Arial Rounded MT Bold"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00BB2500&
      Height          =   495
      Left            =   15000
      TabIndex        =   28
      Top             =   90
      Width           =   615
   End
End
Attribute VB_Name = "Frm_ConsolaCentral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls                                             As ClsConsolaAdministrativa
Public PaisCls                                          As clsPais
Public clsGPais                                         As clsGrupoPais

Private Sub Arbol_DblClick()
    If Not Arbol.SelectedItem Is Nothing Then
        If Left(UCase(Arbol.SelectedItem.Key), 1) = "G" Then
            Lbl_AgregarOrg_Click
            
            Dim Org As New ClsOrganizacion
            
            If Org.CargarOrganizacion(fCls.Conexion, Mid(Arbol.SelectedItem.Key, 2)) Then
            
                Txt_CodigoOrg = Org.Codigo
                Txt_NombreOrg = Org.Nombre
                Txt_ServidorOrg = Org.RutaServidor
                ListSafeItemSelection Combo_Pais, Org.PaisRelacion(fCls.Conexion)
                                
            End If
            
        End If
    End If
End Sub

Private Sub Bt_Pais_Click()
    Lbl_Pais_Click
End Sub

Private Sub BT_Region_Click()
    Lbl_AgregarOrg_Click
End Sub

Private Sub Cerrar_Click()
    Unload Me
End Sub

Private Sub Combo_Pais_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Lbl_AceptarOrg_Click
    End If
End Sub

Private Sub Form_Load()
    AjustarPantalla Me
    Call LimpiarCampos
    Me.Frm_Organizacion.Left = 6720
    Me.FrmPais.Left = 6720
    Me.FrmUsuario.Left = 6720
    'IniciarGridGrupos
    'CargarGrid
    actualizar_arbol
    Bt_Pais.Visible = True
    BT_Region.Top = Bt_Pais.Top
    Me.Top = 0
End Sub

Private Sub IniciarGridGrupos()
    With Me.GrdRegiones
        .Clear
        .Rows = 2
        .Cols = 3
        .FixedCols = 0
        .FixedRows = 0
        .ColWidth(0) = 800
        .ColWidth(1) = 10000
        .ColWidth(2) = 0
        .ScrollBars = flexScrollBarVertical
        .GridLines = flexGridNone
        .RowHeightMin = .RowHeight(0) * 2
        .Font.Size = 10
        .Font.Bold = True
    End With
End Sub

Private Sub CargarGrid()
    Dim mP As clsGrupoPais
    'Dim mO As clsGrupoOrg
    Dim mFila As Long
    Dim FilaGrupo As Long, FilaItem As Long
    Dim mHeight As Long
    Dim PRs As ADODB.Recordset
    
    Set PRs = New ADODB.Recordset
    'PRs.CursorLocation adUseClient
    Sql = "SELECT * FROM MA_PAISES"
    PRs.Open Sql, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, 1
    Set clsGPais = New clsPais
    Do While Not PRs.EOF
        'Set mP = clsGPais.
    Loop
    
'    For Each mG In fConfigPlato.Plato.Grupos
'        FilaGrupo = FilaGrupo + 1
'        If Not mG.Sistema Then
'            FilaItem = 0
'            mdescri = UCase(mG.NombreGrupo)
'
'            If mG.Costeable Then
'                mdescri = mdescri & " ($)"
'            Else
'               If mG.Requerido Then
'                   mdescri = mdescri & " (" & StellarMensaje(624) & " " & mG.Cantidad & ")" '" (Cantidad Minima " & mG.Cantidad & ")"
'               End If
'            End If
'
'            I = IIf(Pos.AgruparGruposPlatos, -1, 1)
'            AgregarLineaGrid mFila, CStr(mdescri), FilaGrupo * I, True
'
'            If Not Pos.AgruparGruposPlatos Then
'                For Each mI In mG.Items
'                    mFila = mFila + 1
'                    FilaItem = FilaItem + 1
'                    AgregarLineaGrid mFila, mI.Descripcion, FilaGrupo * Separador + FilaItem, False
'                Next
'            End If
'
'            mFila = mFila + 1
'        End If
'    Next
'
'    If mFila > 0 Then grdGrupos.RemoveItem mFila
'
'    mHeight = grdGrupos.RowHeight(mFila) * grdGrupos.Rows - 1
'
'    If mHeight > MaxHeight Then
'        grdGrupos.Height = MaxHeight
'    Else
'        grdGrupos.Height = mHeight + 50
'    End If
End Sub

Private Sub Lbl_AceptarOrg_Click()
    Dim clsORG As ClsOrganizacion
    Set clsORG = New ClsOrganizacion
    
    clsORG.Codigo = Trim(Me.Txt_CodigoOrg.Text)
    clsORG.Nombre = Me.Txt_NombreOrg.Text
    clsORG.RutaServidor = Me.Txt_ServidorOrg.Text
    Relacion = Me.Combo_Pais.ListIndex
    If Relacion <> -1 Then
        clsORG.Relacion = fCls.ArrResultado(Relacion)
    Else
        'clsORG.Relacion = "RAIZ"
        MsgBox "Debe Seleccionar un pa�s."
        Exit Sub
    End If
    'localidad y deposito modo caiman, si esto procede en un futuro cambiar esto y hacerlo bien
    'por defecto apunto a la localida 01 y deposito de averia(0102)
    clsORG.Localidad = "01"
    clsORG.Deposito = "0102"
    If (fCls.GuardarOrganizacion(fCls.Conexion, clsORG)) Then
        Call CerrarFrames
        actualizar_arbol
    End If
End Sub

Private Sub Lbl_AgregarOrg_Click()
    Me.FrmPais.Visible = False
    Me.Frm_Organizacion.Visible = True
    Call LimpiarCampos
    CargarComboPais
    Me.Txt_CodigoOrg.SetFocus
    If Not Arbol.SelectedItem Is Nothing Then
        If Left(UCase(Arbol.SelectedItem.Key), 1) = "D" Then
            ListSafeItemSelection Combo_Pais, Arbol.SelectedItem.Text
        End If
    End If
End Sub

Private Sub Lbl_AceptarPais_Click()
    Dim clsPais As clsPais
    Set clsPais = New clsPais
    
    clsPais.Codigo = Trim(Me.Txt_CodigoPais.Text)
    clsPais.Pais = Me.Txt_PaisNombre.Text
    If (fCls.GuardarPais(fCls.Conexion, clsPais)) Then
        Call CerrarFrames
        actualizar_arbol
    End If
End Sub

Private Sub Lbl_cancelarOrg_Click()
    Call CerrarFrames
End Sub

Private Sub Lbl_cancelarPais_Click()
    Call CerrarFrames
End Sub

Private Sub Lbl_Pais_Click()
    Me.FrmPais.Visible = True
    Me.Frm_Organizacion.Visible = False
    Call LimpiarCampos
End Sub

Private Sub LimpiarCampos()
    Me.Txt_CodigoOrg = ""
    Me.Txt_NombreOrg = ""
    Me.Txt_PaisOrg = ""
    Me.Txt_ServidorOrg = ""
    Me.Txt_PaisNombre = ""
    Me.Txt_CodigoPais = ""
    Me.Combo_Pais.Clear
    'actualizar_arbol
End Sub

Private Sub CerrarFrames()
    Me.Frm_Organizacion.Visible = False
    Me.FrmPais.Visible = False
    Call LimpiarCampos
End Sub

Private Sub CargarComboPais()
    If (fCls.BuscarComboPais(fCls.Conexion, Me.Combo_Pais)) Then
    
    End If
End Sub

Private Sub actualizar_arbol()
On Error GoTo error_actualizar
    Dim mRs As ADODB.Recordset
    
    Arbol.Nodes.Clear
    
    Set mRs = fCls.Conexion.Execute("SELECT TOP (1) cs_Organizacion FROM MA_ORGANIZACION WHERE bs_Principal = 1")
    
    If Not mRs.EOF Then RootTitle = mRs!CS_ORGANIZACION
    
    Crear_menu "lcnode", "org", "", "RAIZ", RootTitle, "raiz", "org"
    '*************************************
    '*** Pais
    '*************************************
    
    Dim CampoNombre
    
    Select Case LCase(StellarMensaje(1))
        Case "es"
            CampoNombre = "NombreEspa�ol"
        Case "en"
            CampoNombre = "NombreIngles"
        Case Else
            CampoNombre = "NombreDefault"
    End Select
    
    Set mRs = New ADODB.Recordset
    
    msql = "SELECT DISTINCT MA_PAISES.* FROM MA_PAISES INNER JOIN MA_ORGANIZACION" & vbNewLine & _
    "ON MA_PAISES.Codigo2D = MA_ORGANIZACION.CS_RELACION AND bs_Principal = 0" '"SELECT * FROM MA_PAISES"
    
    mRs.Open msql, fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    'Set mRs.ActiveConnection = Nothing
    Do Until mRs.EOF()
'        '' obtiene los valores
        Relacion = "RAIZ"
        clave = "D" & Trim(mRs!Codigo2D)
        texto = mRs.Fields(CampoNombre).Value
        Imagen = "dpto"
        COMENTARIO = Trim(mRs!Codigo2D)
'        '' se genera el arbol a trav�s de los valores
        Crear_menu "lcnode", COMENTARIO, Relacion, clave, texto, Imagen, "dpto"
        mRs.MoveNext
    Loop
    '*************************************
    '*** Region
    '*************************************
    If mRs.State = adStateOpen Then mRs.Close
    mRs.Open "select * from ORGANIZACION.DBO.MA_ORGANIZACION where bs_Principal = 0", _
    fCls.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    'Set mRs.ActiveConnection = Nothing
    Do Until mRs.EOF()
'        '' obtiene los valores
        If Trim(mRs!CS_RELACION) <> "RAIZ" Then
            Relacion = "D" & Trim(mRs!CS_RELACION)
        Else
            Relacion = Trim(mRs!CS_RELACION)
        End If
        clave = "G" & Trim(mRs!CS_CODIGO)
        texto = mRs!CS_ORGANIZACION
        Imagen = "grupo"
        COMENTARIO = Trim(mRs!CS_CODIGO)
'        '' se genera el arbol a trav�s de los valores
        Crear_menu "lcnode", COMENTARIO, Relacion, clave, texto, Imagen, "grupo1"
        mRs.MoveNext
    Loop

    Arbol.Refresh
Exit Sub
error_actualizar:
    'Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    'Unload Me

End Sub

Private Sub Crear_menu(Campo0, Campo1, Campo2, Campo3, Campo4, Campo5, Campo6)
    
    On Error Resume Next
    
    If Campo1 <> Campo6 Then
        Set Campo0 = Arbol.Nodes.Add(Campo2, tvwChild, Campo3, Campo4, Campo5)
    Else
        Set Campo0 = Arbol.Nodes.Add(, , Campo3, Campo4, Campo5)
    End If
    
    Campo0.Tag = Campo1
    
    Campo0.Sorted = True
    Campo0.Expanded = True
    
    'campo0.EnsureVisible   ' Muestra todos los nodos.
    
End Sub

Private Sub Txt_CodigoOrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Txt_NombreOrg.SetFocus
    End If
End Sub

Private Sub Txt_NombreOrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Txt_ServidorOrg.SetFocus
    End If
End Sub

Private Sub Txt_ServidorOrg_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        Me.Combo_Pais.SetFocus
    End If
End Sub
