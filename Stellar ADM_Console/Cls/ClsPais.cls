VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsPais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCodigoPais As String
Private mvarPais As String
Private mvarRelacion    As String
Private mGrupos As String

Property Get Codigo() As String
    Codigo = mvarCodigoPais
End Property

Property Let Codigo(pValor As String)
    mvarCodigoPais = pValor
End Property

Property Get Pais() As String
    Pais = mvarPais
End Property

Property Let Pais(ByVal pValor As String)
    mvarPais = pValor
End Property

Private Sub Class_Initialize()
    'Set GrupoOrg = New clsGrupoOrg
End Sub

Private Sub Class_Terminate()
    'Set mGrupos = Nothing
End Sub
