VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsOrganizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCodigoOrganizacion
Private mvarNombreOrganizacion
Private mvarRutaServidor
Private mvarRelacion
Private mvarLocalidad
Private mvarDeposito

Property Get Codigo() As String
    Codigo = mvarCodigoOrganizacion
End Property

Property Let Codigo(ByVal Valor As String)
    mvarCodigoOrganizacion = Valor
End Property

Property Get Nombre() As String
    Nombre = mvarNombreOrganizacion
End Property

Property Let Nombre(ByVal Valor As String)
    mvarNombreOrganizacion = Valor
End Property

Property Get RutaServidor() As String
    RutaServidor = mvarRutaServidor
End Property

Property Let RutaServidor(ByVal Valor As String)
    mvarRutaServidor = Valor
End Property

Property Get Relacion() As String
    Relacion = mvarRelacion
End Property

Property Let Relacion(ByVal Valor As String)
    mvarRelacion = Valor
End Property

Property Let Localidad(ByVal Valor As String)
    mvarLocalidad = Valor
End Property

Property Get Localidad() As String
    Localidad = mvarLocalidad
End Property

Property Let Deposito(ByVal Valor As String)
    mvarDeposito = Valor
End Property

Property Get Deposito() As String
    Deposito = mvarDeposito
End Property

Public Function CargarOrganizacion(pCn As ADODB.Connection, pCodigo As String) As Boolean
    
    On Error GoTo Error
    
    Dim mRs As Recordset
    Set mRs = New Recordset
    
    Set mRs = pCn.Execute("SELECT * FROM ORGANIZACION.DBO.MA_ORGANIZACION WHERE CS_CODIGO = '" & pCodigo & "'")
    
    If Not mRs.EOF Then
        mvarCodigoOrganizacion = mRs!CS_CODIGO
        mvarNombreOrganizacion = mRs!CS_ORGANIZACION
        mvarRutaServidor = mRs!CS_SERVIDOR
        mvarLocalidad = mRs!CS_LOCALIDAD
        mvarDeposito = mRs!CS_DEPOSITO
        mvarRelacion = mRs!CS_RELACION
    End If
    
    CargarOrganizacion = True
    
    Exit Function
    
Error:
    
End Function

Public Function PaisRelacion(pCn As ADODB.Connection, Optional pCodigo) As String
    
    On Error GoTo Error
    
    Dim mRs As Recordset
    Set mRs = New Recordset
    
    Set mRs = pCn.Execute("SELECT * FROM ORGANIZACION.DBO.MA_PAISES WHERE Codigo2D = '" _
    & IIf(IsMissing(pCodigo), mvarRelacion, pCodigo) & "'")
    
    Dim CampoNombre
    
    Select Case LCase(StellarMensaje(1))
        Case "es"
            CampoNombre = "NombreEspaņol"
        Case "en"
            CampoNombre = "NombreIngles"
        Case Else
            CampoNombre = "NombreDefault"
    End Select
    
    If Not mRs.EOF Then PaisRelacion = mRs.Fields(CampoNombre).Value
    
    Exit Function
    
Error:
    
End Function
