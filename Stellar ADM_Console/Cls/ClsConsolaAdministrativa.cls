VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsConsolaAdministrativa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eTipoInterfaz
    etiConfiguracion
End Enum

Private mConexion                                               As ADODB.Connection
Private cPais                                                   As clsPais
Private cGrupoPais                                              As clsGrupoPais
Private mServidor                                               As String
Private mBaseDatos                                              As String
Private mUsuario                                                As String
Private mPassword                                               As String
Private mProveedor                                              As String
Private mCmdTO                                                  As Long
Private mCnnTO                                                  As Long
Private mTipoInterfaz                                           As eTipoInterfaz
Public ArrResultado

Public Sub IniciarInterfaz(ByVal Srv As String, TipoInterfaz As eTipoInterfaz, Optional BD As String = "ORGANIZACION", Optional User As String = "sa", Optional Pwd As String = "", Optional CmdTo As Long = 30, Optional CnnTo As Long = 10, Optional ProveedorDatos As String = "SQLOLEDB.1")
    On Error GoTo errorInicial
    mServidor = Srv
    'mServidor = "10.10.1.250\cliente2014"
    mBaseDatos = BD
    mUsuario = User
    mPassword = Pwd
    mCmdTO = CmdTo
    mCnnTO = CnnTo
    mProveedor = ProveedorDatos
    mTipoInterfaz = TipoInterfaz
    Select Case mTipoInterfaz
        Case etiConfiguracion
            ConectarBD
            If Me.Conexion.State = adStateClosed Then End
            Set Frm_ConsolaCentral.fCls = Me
            Frm_ConsolaCentral.Show vbModal
            Set Frm_ConsolaCentral = Nothing
        Case Else
    End Select
errorInicial:
    Err.Clear
End Sub

Property Get Conexion() As ADODB.Connection
    Set Conexion = mConexion
End Property

Public Function ConectarBD() As Boolean
    On Error GoTo Errores
    Set mConexion = New ADODB.Connection
    mConexion.ConnectionTimeout = mCnnTO
    mConexion.Open "PROVIDER=" & mProveedor & ";INITIAL CATALOG=" & mBaseDatos & ";USER ID=" & mUsuario _
        & ";PASSWORD=" & mPassword & ";DATA SOURCE=" & mServidor
    ConectarBD = True
    Exit Function
Errores:
    MsgBox Err.Description, vbCritical, "Conectando " & mServidor
   ' Mensajes Err.Description
    Err.Clear
End Function

Public Function GuardarPais(ByVal pCn As ADODB.Connection, clsPais As clsPais) As Boolean
    
    On Error GoTo ErrorGuardar
    
    Dim mRs As ADODB.Recordset
    Dim Sql As String
    Sql = "SELECT * FROM ORGANIZACION.DBO.MA_PAISES WHERE Codigo2D = '" & clsPais.Codigo & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open Sql, pCn, adOpenKeyset, adLockOptimistic, adCmdText
    
    If mRs.EOF Then
        mRs.AddNew
        mRs!Codigo2D = clsPais.Codigo
    End If
    
    mRs!Codigo3D = ""
    mRs!Codigo4D = ""
    mRs!NombreDefault = clsPais.Pais
    mRs!NombreIngles = clsPais.Pais
    mRs!NombreEspaņol = clsPais.Pais
    mRs.Update
    
    GuardarPais = True
    
    Exit Function
ErrorGuardar:
    GuardarPais = False
End Function

Public Function GuardarOrganizacion(ByVal pCn As ADODB.Connection, clsORG As ClsOrganizacion) As Boolean
    
    On Error GoTo ErrorGuardar
    
    Dim mRs As ADODB.Recordset
    Dim Sql As String

    Sql = "Select * from organizacion.dbo.MA_ORGANIZACION where cs_codigo='" & clsORG.Codigo & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open Sql, pCn, adOpenKeyset, adLockOptimistic, adCmdText
    
    If mRs.EOF Then
        mRs.AddNew
        mRs!CS_CODIGO = clsORG.Codigo
    End If
    
    mRs!CS_ORGANIZACION = clsORG.Nombre
    mRs!CS_SERVIDOR = clsORG.RutaServidor
    mRs!CS_RELACION = clsORG.Relacion
    mRs!CS_LOCALIDAD = clsORG.Localidad
    mRs!CS_DEPOSITO = clsORG.Deposito
    mRs.Update
    GuardarOrganizacion = True
    Exit Function
ErrorGuardar:
    Err.Clear
    GuardarOrganizacion = False
End Function

Public Function BuscarComboPais(ByVal pCn As ADODB.Connection, Combo As ComboBox) As Boolean
    Dim Sql As String
    Dim mRs As ADODB.Recordset
    Set mRs = New ADODB.Recordset
    Dim i As Integer
    On Error GoTo ErrorCargarCombo
    
    Sql = "SELECT * FROM ORGANIZACION.DBO.MA_PAISES"
    mRs.Open Sql, pCn, adOpenKeyset, adLockReadOnly, adCmdText
    
    Dim CampoNombre
    
    Select Case LCase(StellarMensaje(1))
        Case "es"
            CampoNombre = "NombreEspaņol"
        Case "en"
            CampoNombre = "NombreIngles"
        Case Else
            CampoNombre = "NombreDefault"
    End Select
    
    With Combo
    i = 0
    ReDim ArrResultado(mRs.RecordCount)
        Do Until mRs.EOF
            .AddItem mRs.Fields(CampoNombre).Value, i
            ArrResultado(i) = mRs!Codigo2D
            i = i + 1
            mRs.MoveNext
        Loop
    End With
    BuscarComboPais = True
    Exit Function
ErrorCargarCombo:
    Err.Clear
    BuscarComboPais = False
End Function
