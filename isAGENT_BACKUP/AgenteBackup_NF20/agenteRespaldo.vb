﻿Option Explicit On
Imports System.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports System.IO

Public Class AgenteRespaldo
    'SETUP
    Dim rutaINI As String
    Dim reader As iniReader = New iniReader()
    Public datasource As String
    Dim rutaBackup As String
    Dim vad10 As String
    Dim vad20 As String
    Dim admLocalFood As String
    Dim posLocalFood As String
    Dim admLocal As String
    Dim posLocal As String
    Dim shrink As String
    Dim limpiarRespaldos As String
    Dim diasBackup As String
    Dim comprimeRespaldo As String
    'Variables
    Dim dFechaFormat As DateTime
    'Thread
    'Private backupThread As AgenteRespaldo
    Private thread As Thread

    Sub readINI()
        rutaINI = Application.StartupPath & "\setup.ini"
        datasource = reader.IniGet(rutaINI, "AgenteRespaldo", "servidor", ".")
        rutaBackup = reader.IniGet(rutaINI, "AgenteRespaldo", "rutaBackup", "C:\")
        vad10 = reader.IniGet(rutaINI, "AgenteRespaldo", "VAD10", "0")
        vad20 = reader.IniGet(rutaINI, "AgenteRespaldo", "VAD20", "0")
        admLocalFood = reader.IniGet(rutaINI, "AgenteRespaldo", "ADM_LOCAL_FOOD", "0")
        posLocalFood = reader.IniGet(rutaINI, "AgenteRespaldo", "POS_LOCAL_FOOD", "0")
        admLocal = reader.IniGet(rutaINI, "AgenteRespaldo", "ADM_LOCAL", "0")
        posLocal = reader.IniGet(rutaINI, "AgenteRespaldo", "POS_LOCAL", "0")
        shrink = reader.IniGet(rutaINI, "Mantenimiento", "shrink", "0")
        limpiarRespaldos = reader.IniGet(rutaINI, "Mantenimiento", "limpiarRespaldos", "0")
        diasBackup = reader.IniGet(rutaINI, "Mantenimiento", "diasBackup")
        comprimeRespaldo = reader.IniGet(rutaINI, "Mantenimiento", "comprimirRespaldos", "0")

        If stringVerifier.checkString(datasource) Or stringVerifier.checkString(rutaBackup) Or stringVerifier.checkString(vad10) Or stringVerifier.checkString(vad20) Or stringVerifier.checkString(admLocalFood) Or stringVerifier.checkString(posLocalFood) Then
            MsgBox("Faltan parámetros por configurar en el archivo SETUP", MsgBoxStyle.Critical, "isAGENT_BACKUP")
            Me.Close()
        End If

        If stringVerifier.checkString(admLocal) Or stringVerifier.checkString(posLocal) Or stringVerifier.checkString(shrink) Or stringVerifier.checkString(limpiarRespaldos) Or stringVerifier.checkString(diasBackup) Then
            MsgBox("Faltan parámetros por configurar en el archivo SETUP", MsgBoxStyle.Critical, "isAGENT_BACKUP")
            Me.Close()
        End If

    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        readINI()
        Try
            thread = New Thread(New ThreadStart(AddressOf backupDatabase))
            thread.Start()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Me.Close()
        End Try
        'backupDatabase()
        'Me.Close()
    End Sub

    Sub backupDatabase()
        dFechaFormat = FormatDateTime(Now(), DateFormat.GeneralDate)
        Dim year As String = dFechaFormat.ToString("yyyy")
        Dim month As String = dFechaFormat.ToString("MM")
        Dim day As String = dFechaFormat.ToString("dd")
        Dim hour As String = dFechaFormat.ToString("HH")
        Dim minute As String = dFechaFormat.ToString("mm")
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=master;User id=sa;password="
        Dim backupVAD10 As String = "BACKUP DATABASE [VAD10] TO  DISK = N'" & rutaBackup & "\" & "VAD10" & "_" & year & month & day & hour & minute & ".bak'WITH  COPY_ONLY, NOFORMAT, NOINIT, NAME = N'VAD10-Full Database Backup',SKIP, NOREWIND, NOUNLOAD,"
        Dim backupVAD20 As String = "BACKUP DATABASE [VAD20] TO  DISK = N'" & rutaBackup & "\" & "VAD20" & "_" & year & month & day & hour & minute & ".bak'WITH  COPY_ONLY, NOFORMAT, NOINIT, NAME = N'VAD20-Full Database Backup',SKIP, NOREWIND, NOUNLOAD,"
        Dim backupAdmLocalFood As String = "BACKUP DATABASE [ADM_LOCAL_FOOD] TO  DISK = N'" & rutaBackup & "\" & "ADM_LOCAL_FOOD" & "_" & year & month & day & hour & minute & ".bak'WITH  COPY_ONLY, NOFORMAT, NOINIT, NAME = N'ADM_LOCAL_FOOD-Full Database Backup',SKIP, NOREWIND, NOUNLOAD,"
        Dim backupPosLocalFood As String = "BACKUP DATABASE [POS_LOCAL_FOOD] TO  DISK = N'" & rutaBackup & "\" & "POS_LOCAL_FOOD" & "_" & year & month & day & hour & minute & ".bak'WITH  COPY_ONLY, NOFORMAT, NOINIT, NAME = N'POS_LOCAL_FOOD-Full Database Backup',SKIP, NOREWIND, NOUNLOAD,"
        Dim backupAdmLocal As String = "BACKUP DATABASE [ADM_LOCAL] TO  DISK = N'" & rutaBackup & "\" & "ADM_LOCAL" & "_" & year & month & day & hour & minute & ".bak'WITH  COPY_ONLY, NOFORMAT, NOINIT, NAME = N'ADM_LOCAL-Full Database Backup',SKIP, NOREWIND, NOUNLOAD,"
        Dim backupPosLocal As String = "BACKUP DATABASE [POS_LOCAL] TO  DISK = N'" & rutaBackup & "\" & "POS_LOCAL" & "_" & year & month & day & hour & minute & ".bak'WITH  COPY_ONLY, NOFORMAT, NOINIT, NAME = N'POS_LOCAL-Full Database Backup',SKIP, NOREWIND, NOUNLOAD,"
        Dim shrinkDatabase As String
        Dim backupCleanup As String

        If Directory.Exists(rutaBackup) = False Then
            Directory.CreateDirectory(rutaBackup)
        End If

        Try
            If vad10 = 1 Then
                If comprimeRespaldo = 1 Then
                    backupVAD10 = backupVAD10 + " COMPRESSION,  STATS = 10"
                Else
                    backupVAD10 = backupVAD10 + "  STATS = 10"
                End If

                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(backupVAD10, con)
                    cmd.CommandTimeout = 0
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            End If

            If vad20 = 1 Then
                If comprimeRespaldo = 1 Then
                    backupVAD20 = backupVAD20 + " COMPRESSION,  STATS = 10"
                Else
                    backupVAD20 = backupVAD20 + "  STATS = 10"
                End If

                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(backupVAD20, con)
                    cmd.CommandTimeout = 0
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            End If

            If admLocalFood = 1 Then
                If comprimeRespaldo = 1 Then
                    backupAdmLocalFood = backupAdmLocalFood + " COMPRESSION,  STATS = 10"
                Else
                    backupAdmLocalFood = backupAdmLocalFood + "  STATS = 10"
                End If

                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(backupAdmLocalFood, con)
                    cmd.CommandTimeout = 0
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            End If

            If posLocalFood = 1 Then
                If comprimeRespaldo = 1 Then
                    backupPosLocalFood = backupPosLocalFood + " COMPRESSION,  STATS = 10"
                Else
                    backupPosLocalFood = backupPosLocalFood + "  STATS = 10"
                End If

                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(backupPosLocalFood, con)
                    cmd.CommandTimeout = 0
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            End If

            If admLocal = 1 Then
                If comprimeRespaldo = 1 Then
                    backupAdmLocal = backupAdmLocal + " COMPRESSION,  STATS = 10"
                Else
                    backupAdmLocal = backupAdmLocal + "  STATS = 10"
                End If

                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(backupAdmLocal, con)
                    cmd.CommandTimeout = 0
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            End If

            If posLocal = 1 Then
                If comprimeRespaldo = 1 Then
                    backupPosLocal = backupPosLocal + " COMPRESSION,  STATS = 10"
                Else
                    backupPosLocal = backupPosLocal + "  STATS = 10"
                End If

                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(backupPosLocal, con)
                    cmd.CommandTimeout = 0
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            End If

            'SHRINK DATABASE
            Select Case shrink = 1
                Case vad10 = 1
                    shrinkDatabase = "DBCC SHRINKDATABASE(N'VAD10', 10, TRUNCATEONLY)"
                    Using con As New SqlConnection(connection)
                        Dim cmd As New SqlCommand(shrinkDatabase, con)
                        cmd.CommandTimeout = 0
                        con.Open()
                        cmd.ExecuteScalar()
                        con.Close()
                    End Using

                Case vad20 = 1
                    shrinkDatabase = "DBCC SHRINKDATABASE(N'VAD20', 10, TRUNCATEONLY)"
                    Using con As New SqlConnection(connection)
                        Dim cmd As New SqlCommand(shrinkDatabase, con)
                        cmd.CommandTimeout = 0
                        con.Open()
                        cmd.ExecuteScalar()
                        con.Close()
                    End Using
            End Select

            'MAINTENANCE CLEANUP
            Select Case limpiarRespaldos = 1
                Case diasBackup > 0
                    Dim declareSql As String
                    Dim setSql As String
                    Dim executeSql As String

                    diasBackup = (-1 * diasBackup)

                    declareSql = "DECLARE @DeleteDate DATETIME"
                    setSql = "SET @DeleteDate = DATEADD(DAY, " & diasBackup & ", GETDATE())"
                    executeSql = "EXECUTE master.sys.xp_delete_file 0,N'" & rutaBackup & "',N'bak',@DeleteDate,1"

                    backupCleanup = declareSql & " " & setSql & " " & executeSql

                    Using con As New SqlConnection(connection)
                        Dim cmd As New SqlCommand(backupCleanup, con)
                        cmd.CommandTimeout = 0
                        con.Open()
                        cmd.ExecuteScalar()
                        con.Close()
                    End Using
            End Select

        Catch ex As Exception
            MsgBox(ex.Message)
            Me.Close()
        End Try
    End Sub
End Class