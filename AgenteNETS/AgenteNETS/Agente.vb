﻿Option Explicit On
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

'Contador Archivo rvcXXXX.txt
'Codigo del local (1005)
'Monto neto de las ventas sin iva del dia sin puntos y comas (replace) y redondeado (round)
'Hora en que se genera el archivo, en formato militar y entre comillas
'Fecha de la venta en formato dd/mm/aaaa y entre comillas
'Una coma al final del registro
'EJ: 1005,121766,"13:35","21/10/2004",

Public Class Agente
    'SETUP
    Dim rutaINI As String
    Dim reader As iniReader = New iniReader()
    Public datasource As String
    Dim codigoLocal As String
    Dim separador As String
    Dim comillas As String = Chr(34)
    Dim directorio As String
    Dim decimalSeparator As String
    Dim decimales As String
    Dim diferenciaDias As String
    Dim soloDetallado As String
    Dim fechaDetalle As String
    'Codigo
    Dim rutaTXT As String
    Dim sRenglon As String = Nothing
    Dim strStreamW As Stream = Nothing
    Dim strStreamWriter As StreamWriter = Nothing
    Dim ContenidoArchivo As String = Nothing
    Dim nowDateTime As DateTime
    Dim dFechaFormat As DateTime
    Dim i As Integer
    Public cantidadTXT As Integer = 0
    Public lote As Integer
    Public fileName As String
    Public firstTime As Boolean
    Public lastSaleDate As String
    Public lastDate As String
    Public ultimaFechaVenta As String
    Public dFecha As String
    Public detailedDateTXT As String

    'Metodo main
    Sub main()
        If PrevInstance() Then
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & "Ya existe una instancia de este agente en ejecucion.")
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & "Ya existe una instancia de este agente en ejecucion.")
                strStreamWriter.Close()
                Me.Close()
            End If
            Me.Close()
        End If

        Try
            readINI()
            If soloDetallado = 1 Then
                getLastDate()
                txtDetallado()
            Else
                'checkSalesDate()
                countTXT()
                createDirectory()
                getLastDate()
                verificarEjecucion()
                stellarQuery()

                txtDetallado()
                'fillTXT()
            End If
        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
        Me.Close()
    End Sub

    'Metodo de ejecucion principal.
    Private Sub Agente_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        Me.Hide()
        main()
    End Sub

    'Crea un txt detallado con las ventas de x's dia.
    Sub txtDetallado()
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD20;User id=sa;password="
        Dim totalDayQuery As String
        Dim detailedByPOSQuery As String
        If soloDetallado = 1 Then
            totalDayQuery = "select f_fecha, ROUND(sum(n_subtotal)," & decimales & ") as subtotal from MA_PAGOS where F_Fecha = '" & fechaDetalle & "' group by F_Fecha"
            detailedByPOSQuery = "select F_Fecha, C_Caja, C_Numero, ROUND(N_Subtotal," & decimales & ") as subtotal, C_CONCEPTO from ma_pagos where F_Fecha = '" & fechaDetalle & "' order by C_Caja, F_Hora"
        Else
            'totalDayQuery = "select f_fecha, ROUND(sum(n_subtotal)," & decimales & ") as subtotal from MA_PAGOS where F_Fecha = DATEADD(dd," & diferenciaDias & ",CAST(SYSDATETIME() AS varchar(10))) group by F_Fecha"
            'detailedByPOSQuery = "select F_Fecha, C_Caja, C_Numero, ROUND(N_Subtotal," & decimales & ") as subtotal, C_CONCEPTO from ma_pagos where F_Fecha = DATEADD(dd," & diferenciaDias & ",CAST(SYSDATETIME() AS varchar(10))) order by C_Caja, F_Hora"
            'MODIFICADO EL 22/06 POR ERROR EN CONVERT
            '19695633
            totalDayQuery = "select f_fecha, ROUND(sum(n_subtotal)," & decimales & ") as subtotal from MA_PAGOS where F_Fecha = DATEADD(dd," & diferenciaDias & ",(SELECT CONVERT (date, SYSDATETIME()) [date])) group by F_Fecha"
            detailedByPOSQuery = "select F_Fecha, C_Caja, C_Numero, ROUND(N_Subtotal," & decimales & ") as subtotal, C_CONCEPTO from ma_pagos where F_Fecha = DATEADD(dd," & diferenciaDias & ",(SELECT CONVERT (date, SYSDATETIME()) [date])) order by C_Caja, F_Hora"
        End If
        Dim da As SqlDataAdapter
        Dim dtTotalDay As New DataTable
        Dim dtTotalByPOS As New DataTable
        Dim dtDetailedByPOS As New DataTable
        Dim year As String = dFechaFormat.ToString("yyyy")
        Dim month As String = dFechaFormat.ToString("MM")
        Dim day As String = dFechaFormat.ToString("dd")

        'Verificar si el directorio existe
        If Directory.Exists(directorio) = False Then
            Directory.CreateDirectory(directorio)
        End If

        'Query para sacar el total de un dia determinado
        Try
            da = New SqlDataAdapter(totalDayQuery, connection)
            da.Fill(dtTotalDay)
            If dtTotalDay.Rows.Count > 0 Then
                If soloDetallado = 1 Then
                    rutaTXT = directorio & "\" & "TR_" & detailedDateTXT & ".txt"
                Else
                    rutaTXT = directorio & "\" & "TR_" & year & month & day & ".txt"
                End If
                strStreamW = File.Create(rutaTXT)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)

                Dim count As Integer
                Dim dFecha As String
                Dim nSubTotal As String
                Dim caja As String
                Dim numeroFactura As String
                Dim concepto As String
                For count = 0 To dtTotalDay.Rows.Count - 1
                    dFecha = dtTotalDay.Rows(count).Item(0).ToString().Substring(0, 10)
                    nSubTotal = dtTotalDay.Rows(count).Item(1).ToString().Replace(".", decimalSeparator)
                    strStreamWriter.WriteLine("******************** TOTAL DEL DIA ********************")
                    strStreamWriter.WriteLine(dFecha & separador & nSubTotal & separador)
                    strStreamWriter.WriteLine("")
                Next
                'strStreamWriter.Close()

                da = New SqlDataAdapter(detailedByPOSQuery, connection)
                da.Fill(dtDetailedByPOS)
                If dtDetailedByPOS.Rows.Count > 0 Then
                    strStreamWriter.WriteLine("********** TOTAL DEL DIA (DETALLADO POR CAJA) *********")
                    For count = 0 To dtDetailedByPOS.Rows.Count - 1
                        caja = dtDetailedByPOS.Rows(count).Item(1).ToString()
                        dFecha = dtDetailedByPOS.Rows(count).Item(0).ToString().Substring(0, 10)
                        numeroFactura = dtDetailedByPOS.Rows(count).Item(2).ToString()
                        concepto = dtDetailedByPOS.Rows(count).Item(4).ToString()
                        nSubTotal = dtDetailedByPOS.Rows(count).Item(3).ToString()
                        strStreamWriter.WriteLine(caja & separador & dFecha & separador & numeroFactura & separador & concepto & separador & nSubTotal & separador)
                    Next
                    strStreamWriter.Close()
                End If
            End If
        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
    End Sub
    'Verifica ruta, cantidad de TXT, numeracion. Crea el directorio
    Sub createDirectory()
        'Verificar si el directorio existe
        If Directory.Exists(directorio) = False Then
            Directory.CreateDirectory(directorio)
        End If

        'Contar la cantidad de txt
        Dim fileCounter As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
        fileCounter = My.Computer.FileSystem.GetFiles(directorio)
        'MsgBox("Cantidad de archivos: " & CStr(fileCounter.Count))
        cantidadTXT = CInt(fileCounter.Count)

        'Generar la numeracion del txt
        If cantidadTXT = 0 Then
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            fileName = "rcv0000"
            rutaTXT = directorio & "\" & fileName & ".txt"
        ElseIf cantidadTXT < 10 Then
            fileName = "rcv000" & lote
            rutaTXT = directorio & "\" & fileName & ".txt"
        ElseIf cantidadTXT < 100 Then
            fileName = "rcv00" & lote
            rutaTXT = directorio & "\" & fileName & ".txt"
        ElseIf cantidadTXT < 1000 Then
            fileName = "rcv0" & lote
            rutaTXT = directorio & "\" & fileName & ".txt"
        Else
            fileName = "rcv" & lote
            rutaTXT = directorio & "\" & fileName & ".txt"
        End If

        'Crea el archivo txt
        'strStreamW = File.Create(rutaTXT)
        'strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)

        'strStreamWriter.WriteLine("Probando")
        'strStreamWriter.Close()
    End Sub

    'Ojo con la fecha - Validar la primary key
    Sub checkSalesDate()

        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD10;User id=sa;password="
        'Obtener la ultima fecha de venta
        'Dim lastSalesQuery As String = "select max(d_fecha) from vt_stellar_nets"
        Dim lastSalesQuery As String = "select max(d_fecha) from ma_ventas"
        Dim nLote As String
        Dim da As SqlDataAdapter
        Dim dt As New DataTable

        Try
            da = New SqlDataAdapter(lastSalesQuery, connection)
            da.Fill(dt)
            If dt.Rows.Count > 0 Or stringVerifier.checkString(dt.Rows(0).Item(0).ToString()) = False Then
                'MsgBox(stringVerifier.checkString(dt.Rows(0).Item(0).ToString()))
                lastSaleDate = dt.Rows(0).Item(0).ToString().Substring(0, 10)
            Else
                Exit Sub
            End If

            dt.Clear()
            'Utilizar la fecha de venta obtenida arriba para esta consulta y verificar si ya fue tomada en cuenta, eliminar la linea de la BD.
            Dim todayQuery As String = "Select * from vt_stellar_nets where d_fecha = '" & lastSaleDate & "'"
            da = New SqlDataAdapter(todayQuery, connection)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                'si hay registros los elimina
                'borrar txt
                nLote = dt.Rows(0).Item(2).ToString()
                File.Delete(directorio & "\" & nLote & ".txt")

                'borrar registro
                dt.Clear()
                Dim deleteQuery As String = "delete from vt_stellar_nets where d_fecha = @d_fecha"
                Using con As New SqlConnection(connection)
                    Dim cmd As New SqlCommand(deleteQuery, con)
                    cmd.Parameters.AddWithValue("@d_fecha", lastSaleDate)
                    con.Open()
                    cmd.ExecuteScalar()
                    con.Close()
                End Using
            Else
                MsgBox("No hay registros ni archivo")
                Exit Sub
            End If
        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
    End Sub

    'Verifica si es la primera ejecución del agente
    Sub verificarEjecucion()
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD10;User id=sa;password="
        'Busca si existen registros en la tabla, para comprobar si es primera vez que se ejecuta el agente.
        Dim checkRowsQuery As String = "select * from vt_stellar_nets"
        Dim da As SqlDataAdapter
        Dim dt As New DataTable

        Try
            da = New SqlDataAdapter(checkRowsQuery, connection)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                'Ya fue ejecutado anteriormente y la tabla tiene registros
                firstTime = False
            Else
                'Es primera vez que se ejecuta, no hay registros
                firstTime = True
            End If
        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
    End Sub

    'Obtener la ultima fecha en la que se corrio el agente.
    Sub getLastDate()
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD10;User id=sa;password="
        Dim da As SqlDataAdapter
        Dim dt As New DataTable

        Dim lastDateQuery As String = "select MAX(d_fecha) from VT_STELLAR_NETS"
        dt.Clear()
        Try
            da = New SqlDataAdapter(lastDateQuery, connection)
            da.Fill(dt)
            'MsgBox(dt.Rows(0).Item(0).ToString())
            If stringVerifier.checkString(dt.Rows(0).Item(0).ToString()) = False Then
                lastDate = dt.Rows(0).Item(0).ToString().Substring(0, 10)
                If soloDetallado = 0 Then
                    detailedDateTXT = dt.Rows(0).Item(0).ToString().Substring(6, 4) & dt.Rows(0).Item(0).ToString().Substring(3, 2) & dt.Rows(0).Item(0).ToString().Substring(0, 2)
                Else
                    detailedDateTXT = fechaDetalle.Substring(6, 4) & fechaDetalle.Substring(3, 2) & fechaDetalle.Substring(0, 2)
                End If
                dt.Clear()
            End If
        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
    End Sub

    'Crea y llena el archivo .txt bajo el formato establecido
    Sub fillTXT()
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD10;User id=sa;password="
        Dim da As SqlDataAdapter
        Dim dt As New DataTable

        strStreamW = File.Create(rutaTXT)
        strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)

        'OJO SOLO PARA PRUEBAS LO SIGUIENTE ES FALSE
        'firstTime = False

        If firstTime = True Then
            Dim queryTXT As String = "SELECT ROUND(SUM(case when c_concepto = 'DEV' then n_subtotal * -1 else n_subtotal end)," & decimales & ") as cant, d_fecha as fecha FROM MA_VENTAS WHERE d_FECHA < DATEADD(dd," & diferenciaDias & ",SYSDATETIME()) group by d_fecha order by d_fecha"
            Try
                da = New SqlDataAdapter(queryTXT, connection)
                da.Fill(dt)
                Dim count As Integer
                Dim nSubTotal As String
                Dim dFecha As String
                For count = 0 To dt.Rows.Count - 1
                    nSubTotal = dt.Rows(count).Item(0).ToString().Replace(".", decimalSeparator)
                    dFecha = dt.Rows(count).Item(1).ToString()
                    nowDateTime = FormatDateTime(Now(), DateFormat.ShortTime)
                    dFechaFormat = FormatDateTime(dFecha, DateFormat.ShortDate)
                    Dim nowTime As String = Chr(34) + nowDateTime.ToString("HH:mm") + Chr(34)
                    Dim fechaVenta As String = Chr(34) + dFechaFormat.ToString("dd/MM/yyyy") + Chr(34)
                    strStreamWriter.WriteLine(codigoLocal & separador & nSubTotal & separador & nowTime & separador & fechaVenta & separador)
                Next
                strStreamWriter.Close()
            Catch ex As Exception
                If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                    strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                    strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                    strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                    strStreamWriter.Close()
                    Me.Close()
                Else
                    strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                    strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                    strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                    strStreamWriter.Close()
                    Me.Close()
                End If
            End Try
        Else
            'No es primera vez
            'between
            'Dim queryTXT As String = "SELECT ROUND(SUM(case when c_concepto = 'DEV' then n_subtotal * -1 else n_subtotal end),2) as cant, d_fecha as fecha FROM MA_VENTAS WHERE d_FECHA BETWEEN DATEADD(dd,1,'01/01/2012') AND DATEADD(dd,-2,SYSDATETIME()) group by d_fecha order by d_fecha"
            Dim queryTXT As String = "SELECT ROUND(SUM(case when c_concepto = 'DEV' then n_subtotal * -1 else n_subtotal end),2) as cant, d_fecha as fecha FROM MA_VENTAS WHERE d_FECHA BETWEEN DATEADD(dd,1,'" & lastDate & "') AND DATEADD(dd," & diferenciaDias & ",SYSDATETIME()) group by d_fecha order by d_fecha"
            'Consulta de prueba
            'Dim queryTXT As String = "SELECT ROUND(SUM(case when c_concepto = 'DEV' then n_subtotal * -1 else n_subtotal end),2) as cant, d_fecha as fecha FROM MA_VENTAS WHERE d_FECHA BETWEEN DATEADD(dd,1,'" & lastDate & "') AND DATEADD(dd," & diferenciaDias & ",'01/01/2012') group by d_fecha order by d_fecha"
            Try
                da = New SqlDataAdapter(queryTXT, connection)
                da.Fill(dt)
                Dim count As Integer
                Dim nSubTotal As String
                Dim dFecha As String
                If dt.Rows.Count = 0 Then
                    MsgBox("No hay registros", MsgBoxStyle.Information, "AgenteNETS")
                    Me.Close()
                End If
                For count = 0 To dt.Rows.Count - 1
                    nSubTotal = dt.Rows(count).Item(0).ToString().Replace(".", decimalSeparator)
                    dFecha = dt.Rows(count).Item(1).ToString()
                    nowDateTime = FormatDateTime(Now(), DateFormat.ShortTime)
                    dFechaFormat = FormatDateTime(dFecha, DateFormat.ShortDate)
                    Dim nowTime As String = Chr(34) + nowDateTime.ToString("HH:mm") + Chr(34)
                    Dim fechaVenta As String = Chr(34) + dFechaFormat.ToString("dd/MM/yyyy") + Chr(34)
                    strStreamWriter.WriteLine(codigoLocal & separador & nSubTotal & separador & nowTime & separador & fechaVenta & separador)
                Next
                strStreamWriter.Close()
            Catch ex As Exception
                If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                    strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                    strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                    strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                    strStreamWriter.Close()
                    Me.Close()
                Else
                    strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                    strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                    strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                    strStreamWriter.Close()
                    Me.Close()
                End If
            End Try
        End If
    End Sub

    'Toma los datos de las ultimas ventas y devoluciones para ingresarlos a la tabla VT_STELLAR_NETS
    Sub stellarQuery()
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD10;User id=sa;password="
        Dim queryCorrelativos As String = "SELECT top 1 MAX(cr_ven_adm) as cr_ven_adm, MAX(cr_dev_adm) as cr_dev_adm, MAX(cr_ven_pos) as cr_ven_pos, max(cr_dev_pos) as cr_dev_pos, d_FECHA as fecha from( select MAX(c_documento) as cr_ven_adm, null as cr_dev_adm, null as cr_ven_pos, null as cr_dev_pos, d_fecha from MA_VENTAS where c_DOCUMENTO not like 'P%' and c_DOCUMENTO not like 'E%' and c_CONCEPTO in ('VEN') group by d_FECHA union select null as cr_ven_adm, MAX(c_documento) as cr_dev_adm,  null as cr_ven_pos, null as cr_dev_pos, d_fecha from MA_VENTAS where c_DOCUMENTO not like 'P%' and c_DOCUMENTO not like 'E%' and c_CONCEPTO in ('DEV') group by d_FECHA UNION select null as cr_ven_adm, null as cr_dev_adm, MAX(c_documento) as cr_ven_pos, null as cr_dev_pos, d_fecha from MA_VENTAS where c_DOCUMENTO like 'P%' and c_DOCUMENTO not like 'E%' and c_CONCEPTO in ('VEN') group by d_FECHA UNION select null as cr_ven_adm, null as cr_dev_adm, null as cr_ven_pos, MAX(c_documento) as cr_dev_pos, d_fecha from MA_VENTAS where c_DOCUMENTO like 'P%' and c_DOCUMENTO not like 'E%' and c_CONCEPTO in ('DEV') group by d_FECHA) as tab WHERE d_FECHA < DATEADD(dd," & diferenciaDias & ",SYSDATETIME()) GROUP BY tab.d_FECHA ORDER BY tab.d_FECHA desc"
        Dim queryInsert As String = "insert into vt_stellar_nets values(@d_fecha, @n_lote, @ult_va, @ult_vp, @ult_da, @ult_dp, @fecha_generado)select @@Identity"
        Dim da As SqlDataAdapter
        Dim dt As New DataTable
        Dim count As Integer = 0
        Dim dFecha As String
        Dim ultVa As String
        Dim ultDa As String
        Dim ultVp As String
        Dim ultDp As String
        Dim fechaActual As String
        'dFechaFormat = FormatDateTime(Now(), DateFormat.ShortDate)
        dFechaFormat = FormatDateTime(Now(), DateFormat.GeneralDate)

        Try
            da = New SqlDataAdapter(queryCorrelativos, connection)
            da.Fill(dt)
            For count = 0 To dt.Rows.Count - 1
                dFecha = dt.Rows(count).Item(4).ToString()
                ultVa = dt.Rows(count).Item(0).ToString()
                ultVp = dt.Rows(count).Item(2).ToString()
                ultDa = dt.Rows(count).Item(1).ToString()
                ultDp = dt.Rows(count).Item(3).ToString()
            Next
            fechaActual = dFechaFormat.ToString("dd/MM/yyyy")

            'Insert para la tabla VT_STELLAR_NETS
            Using con As New SqlConnection(connection)
                Dim cmd As New SqlCommand(queryInsert, con)
                cmd.Parameters.AddWithValue("@d_fecha", dFecha.Substring(0, 10))
                cmd.Parameters.AddWithValue("@n_lote", fileName)
                cmd.Parameters.AddWithValue("@ult_va", ultVa)
                cmd.Parameters.AddWithValue("@ult_vp", ultVp)
                cmd.Parameters.AddWithValue("@ult_da", ultDa)
                cmd.Parameters.AddWithValue("@ult_dp", ultDp)
                'cmd.Parameters.AddWithValue("@fecha_generado", fechaActual)
                cmd.Parameters.AddWithValue("@fecha_generado", dFechaFormat)
                con.Open()
                cmd.ExecuteScalar()
                con.Close()
            End Using

            fillTXT()
        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
    End Sub

    'Cuenta la cantidad de lotes generados para establecer el numero del siguiente
    'Ejemplo: rcv0000.txt --> rcv0001.txt --> rcv000n.txt
    Sub countTXT()
        Dim connection As String = "Data Source=" & datasource & ";Initial Catalog=VAD10;User id=sa;password="
        Dim countQuery As String = "SELECT count(n_lote) from VT_STELLAR_NETS"
        Dim da As SqlDataAdapter
        Dim dt As New DataTable
        Dim count As Integer = 0

        Try
            da = New SqlDataAdapter(countQuery, connection)
            da.Fill(dt)
            For count = 0 To dt.Rows.Count - 1
                lote = dt.Rows(count).Item(0)
            Next
            cantidadTXT = lote

        Catch ex As Exception
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & ex.Message)
                strStreamWriter.Close()
                Me.Close()
            End If
        End Try
    End Sub

    'Para leer los parametros establecidos en el archivo setup.ini
    'StringVerifier = TRUE si es null o en blanco, FALSE si contiene algun caracter
    Sub readINI()
        rutaINI = Application.StartupPath & "\setup.ini"
        datasource = reader.IniGet(rutaINI, "AgenteNETS", "servidor")
        codigoLocal = reader.IniGet(rutaINI, "AgenteNETS", "codigolocal")
        separador = reader.IniGet(rutaINI, "AgenteNETS", "separadorTXT")
        directorio = reader.IniGet(rutaINI, "AgenteNETS", "directorioTXT")
        decimalSeparator = reader.IniGet(rutaINI, "AgenteNETS", "separadorDecimales")
        decimales = reader.IniGet(rutaINI, "AgenteNETS", "cantidadDecimales")
        diferenciaDias = reader.IniGet(rutaINI, "AgenteNETS", "diferenciaDias")
        soloDetallado = reader.IniGet(rutaINI, "AgenteNETS", "soloDetallado")
        fechaDetalle = reader.IniGet(rutaINI, "AgenteNETS", "fechaDetalle")
        If stringVerifier.checkString(datasource) Or stringVerifier.checkString(codigoLocal) Or stringVerifier.checkString(separador) Or stringVerifier.checkString(directorio) Or stringVerifier.checkString(decimalSeparator) Or stringVerifier.checkString(decimales) Or stringVerifier.checkString(soloDetallado) Or stringVerifier.checkString(fechaDetalle) Then
            MsgBox("Faltan parámetros por configurar en el archivo SETUP", MsgBoxStyle.Critical, "AgenteNETS")
            If File.Exists(Application.StartupPath & "\errorlog.txt") Then
                strStreamW = File.Open(Application.StartupPath & "\errorlog.txt", FileMode.Append)
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & "Faltan parámetros por configurar en el archivo SETUP")
                strStreamWriter.Close()
                Me.Close()
            Else
                strStreamW = File.Create(Application.StartupPath & "\errorlog.txt")
                strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)
                strStreamWriter.WriteLine(System.DateTime.Now & " Error: " & "Faltan parámetros por configurar en el archivo SETUP")
                strStreamWriter.Close()
                Me.Close()
            End If
        End If
    End Sub

    'Valida la ejecucion de una sola instancia a la vez
    Function PrevInstance() As Boolean
        If UBound(Diagnostics.Process.GetProcessesByName _
           (Diagnostics.Process.GetCurrentProcess.ProcessName)) _
           > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class






