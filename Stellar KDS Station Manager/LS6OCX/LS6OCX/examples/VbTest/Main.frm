VERSION 5.00
Object = "{785C818B-3B27-11D6-85F9-52544C194B3E}#1.0#0"; "LS6.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Main 
   Caption         =   "OCX Test"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13890
   LinkTopic       =   "OCX Test"
   MaxButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   13890
   StartUpPosition =   2  'CenterScreen
   Begin LS6Lib.LS6 LS 
      Height          =   3495
      Left            =   120
      TabIndex        =   121
      Top             =   120
      Width           =   4815
      _Version        =   65536
      _ExtentX        =   8493
      _ExtentY        =   6165
      _StockProps     =   0
   End
   Begin VB.CommandButton KSGetStatus 
      Caption         =   "KSGetStatus"
      Height          =   375
      Left            =   8040
      TabIndex        =   119
      Top             =   2040
      Width           =   1095
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Reset All LS6"
      Height          =   615
      Left            =   9000
      TabIndex        =   118
      Top             =   2760
      Width           =   975
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6480
      Top             =   2160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Clear Screen"
      Height          =   615
      Left            =   7680
      TabIndex        =   111
      Top             =   2760
      Width           =   975
   End
   Begin VB.TextBox txtX 
      Height          =   285
      Left            =   360
      TabIndex        =   54
      Text            =   "Text8"
      Top             =   3720
      Width           =   375
   End
   Begin VB.TextBox txtY 
      Height          =   285
      Left            =   1080
      TabIndex        =   53
      Text            =   "Text9"
      Top             =   3720
      Width           =   375
   End
   Begin VB.TextBox txtString 
      Height          =   315
      Left            =   3720
      TabIndex        =   52
      Text            =   "Text10"
      Top             =   3720
      Width           =   2415
   End
   Begin VB.TextBox txtX2 
      Height          =   285
      Left            =   1800
      TabIndex        =   51
      Text            =   "Text1"
      Top             =   3720
      Width           =   375
   End
   Begin VB.TextBox txtY2 
      Height          =   285
      Left            =   2400
      TabIndex        =   50
      Text            =   "Text2"
      Top             =   3720
      Width           =   375
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0E0FF&
      Caption         =   "LS6000 Command"
      Height          =   4455
      Left            =   6240
      TabIndex        =   48
      Top             =   3720
      Width           =   7575
      Begin VB.CommandButton GetKSStatus 
         Caption         =   "GetKSStatus"
         Height          =   375
         Left            =   120
         TabIndex        =   120
         Top             =   4080
         Width           =   1575
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Show Code>=128"
         Height          =   495
         Left            =   4800
         TabIndex        =   117
         Top             =   3360
         Width           =   1215
      End
      Begin VB.TextBox txtHideTimer 
         Height          =   285
         Left            =   6000
         TabIndex        =   109
         Text            =   "1"
         Top             =   2160
         Width           =   375
      End
      Begin VB.TextBox txtShowTimerY 
         Height          =   285
         Left            =   6960
         TabIndex        =   105
         Text            =   "10"
         Top             =   1650
         Width           =   375
      End
      Begin VB.TextBox txtShowTimerX 
         Height          =   285
         Left            =   6600
         TabIndex        =   104
         Text            =   "10"
         Top             =   1650
         Width           =   375
      End
      Begin VB.TextBox txtShowTimer 
         Height          =   285
         Left            =   6000
         TabIndex        =   103
         Text            =   "1"
         Top             =   1650
         Width           =   375
      End
      Begin VB.TextBox txtS 
         Height          =   375
         Left            =   7200
         TabIndex        =   98
         Text            =   "38"
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtM 
         Height          =   375
         Left            =   6840
         TabIndex        =   97
         Text            =   "8"
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtH 
         Height          =   375
         Left            =   6480
         TabIndex        =   96
         Text            =   "1"
         Top             =   960
         Width           =   375
      End
      Begin VB.TextBox txtSetTimer 
         Height          =   375
         Left            =   6000
         TabIndex        =   95
         Text            =   "1"
         Top             =   960
         Width           =   495
      End
      Begin VB.CommandButton cmdHidTimer 
         Caption         =   "Hide Timer"
         Height          =   495
         Left            =   4800
         TabIndex        =   94
         Top             =   1920
         Width           =   1215
      End
      Begin VB.CommandButton cmdShowTimer 
         Caption         =   "Show Timer"
         Height          =   495
         Left            =   4800
         TabIndex        =   93
         Top             =   1440
         Width           =   1215
      End
      Begin VB.CommandButton cmdSetTimer 
         Caption         =   "Set Timer"
         Height          =   495
         Left            =   4800
         TabIndex        =   92
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdGetVersion 
         Caption         =   "Get Version"
         Height          =   375
         Left            =   120
         TabIndex        =   86
         Top             =   3120
         Width           =   1575
      End
      Begin VB.CommandButton cmdVer 
         Caption         =   "Request Version"
         Height          =   375
         Left            =   120
         TabIndex        =   85
         Top             =   2760
         Width           =   1575
      End
      Begin VB.TextBox txtPanelStyle 
         Height          =   285
         Left            =   4440
         TabIndex        =   84
         Text            =   "5"
         Top             =   2130
         Width           =   375
      End
      Begin VB.TextBox txtRows 
         Height          =   285
         Left            =   3960
         TabIndex        =   83
         Text            =   "2"
         Top             =   2130
         Width           =   375
      End
      Begin VB.TextBox txtCols 
         Height          =   285
         Left            =   3480
         TabIndex        =   82
         Text            =   "4"
         Top             =   2130
         Width           =   375
      End
      Begin VB.TextBox txtCodePage 
         Height          =   285
         Left            =   6120
         TabIndex        =   81
         Text            =   "0"
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox txtCom 
         Height          =   285
         Left            =   3480
         TabIndex        =   80
         Text            =   "1"
         Top             =   3570
         Width           =   375
      End
      Begin VB.TextBox txtBPPitch 
         Height          =   285
         Left            =   4200
         TabIndex        =   79
         Text            =   "5"
         Top             =   3120
         Width           =   495
      End
      Begin VB.TextBox txtBPDuration 
         Height          =   285
         Left            =   3480
         TabIndex        =   78
         Text            =   "2"
         Top             =   3120
         Width           =   495
      End
      Begin VB.TextBox txtTimer 
         Height          =   285
         Left            =   3480
         TabIndex        =   77
         Text            =   "1"
         Top             =   1560
         Width           =   375
      End
      Begin VB.CommandButton cmdCodePage 
         Caption         =   "Code Page"
         Height          =   495
         Left            =   4800
         TabIndex        =   76
         Top             =   2880
         Width           =   1215
      End
      Begin VB.CommandButton cmdReqRs232Setting 
         Caption         =   "Request RS232 Driver Setting"
         Height          =   495
         Left            =   120
         TabIndex        =   75
         Top             =   2280
         Width           =   1575
      End
      Begin VB.CommandButton cmdReqRs232Status 
         Caption         =   "Request Rs232 Status"
         Height          =   495
         Left            =   1920
         TabIndex        =   74
         Top             =   3360
         Width           =   1575
      End
      Begin VB.CommandButton cmdBeepPitch 
         Caption         =   "Beep With Pitch"
         Height          =   495
         Left            =   1920
         TabIndex        =   73
         Top             =   2880
         Width           =   1575
      End
      Begin VB.TextBox txtFg 
         Height          =   375
         Left            =   3720
         TabIndex        =   72
         Text            =   "210"
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox txtBg 
         Height          =   375
         Left            =   2280
         TabIndex        =   71
         Text            =   "20"
         Top             =   360
         Width           =   615
      End
      Begin VB.CommandButton cmdClear256Color 
         Caption         =   "Clear 256 Color Screen "
         Height          =   495
         Left            =   1920
         TabIndex        =   68
         Top             =   2400
         Width           =   1575
      End
      Begin VB.CommandButton cmdDraw256Panel 
         Caption         =   "Draw 256 Color Panel"
         Height          =   495
         Left            =   1920
         TabIndex        =   67
         Top             =   1920
         Width           =   1575
      End
      Begin VB.CommandButton cmdNetOff 
         Caption         =   "Ethernet Icon Off"
         Height          =   375
         Left            =   120
         TabIndex        =   66
         Top             =   1920
         Width           =   1575
      End
      Begin VB.CommandButton cmdNetOn 
         Caption         =   "Ethernet Icon On"
         Height          =   375
         Left            =   120
         TabIndex        =   65
         Top             =   1560
         Width           =   1575
      End
      Begin VB.CommandButton cmdKbOff 
         Caption         =   "KB Icon Off"
         Height          =   375
         Left            =   120
         TabIndex        =   64
         Top             =   1200
         Width           =   1575
      End
      Begin VB.CommandButton cmdKbOn 
         Caption         =   "KB Icon On"
         Height          =   375
         Left            =   120
         TabIndex        =   63
         Top             =   840
         Width           =   1575
      End
      Begin VB.CommandButton cmdReqKbStatus 
         Caption         =   "Request KB Status"
         Height          =   375
         Left            =   120
         TabIndex        =   62
         Top             =   3600
         Width           =   1575
      End
      Begin VB.CommandButton cmdStatus256String 
         Caption         =   "Display String At Status Line"
         Height          =   495
         Left            =   120
         TabIndex        =   61
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton cmdReqTimer 
         Caption         =   "Request Timer Value"
         Height          =   495
         Left            =   1920
         TabIndex        =   60
         Top             =   1440
         Width           =   1575
      End
      Begin VB.CommandButton cmdDisp256String 
         Caption         =   "Display 256 Colors String "
         Height          =   495
         Left            =   1920
         TabIndex        =   49
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label31 
         Caption         =   "COM Port"
         Height          =   255
         Left            =   3480
         TabIndex        =   116
         Top             =   3360
         Width           =   1215
      End
      Begin VB.Label Label30 
         Caption         =   "Timer Number"
         Height          =   255
         Left            =   3480
         TabIndex        =   115
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label27 
         Caption         =   "Timer"
         Height          =   255
         Left            =   6000
         TabIndex        =   110
         Top             =   1920
         Width           =   615
      End
      Begin VB.Label Label26 
         Caption         =   "Y"
         Height          =   255
         Left            =   6960
         TabIndex        =   108
         Top             =   1440
         Width           =   255
      End
      Begin VB.Label Label25 
         Caption         =   "X"
         Height          =   255
         Left            =   6600
         TabIndex        =   107
         Top             =   1440
         Width           =   255
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "Timer"
         Height          =   195
         Left            =   6000
         TabIndex        =   106
         Top             =   1440
         Width           =   390
      End
      Begin VB.Label Label23 
         Caption         =   "Timer Number"
         Height          =   495
         Left            =   5880
         TabIndex        =   102
         Top             =   480
         Width           =   615
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "S"
         Height          =   195
         Left            =   7200
         TabIndex        =   101
         Top             =   720
         Width           =   105
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "M"
         Height          =   195
         Left            =   6840
         TabIndex        =   100
         Top             =   720
         Width           =   135
      End
      Begin VB.Label H 
         AutoSize        =   -1  'True
         Caption         =   "H"
         Height          =   195
         Left            =   6480
         TabIndex        =   99
         Top             =   720
         Width           =   120
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Pitch"
         Height          =   195
         Left            =   4200
         TabIndex        =   91
         Top             =   2880
         Width           =   360
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Duration"
         Height          =   195
         Left            =   3480
         TabIndex        =   90
         Top             =   2880
         Width           =   600
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Style"
         Height          =   195
         Left            =   4440
         TabIndex        =   89
         Top             =   1920
         Width           =   345
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Rows"
         Height          =   195
         Left            =   3960
         TabIndex        =   88
         Top             =   1920
         Width           =   405
      End
      Begin VB.Label Cols 
         AutoSize        =   -1  'True
         Caption         =   "Cols"
         Height          =   195
         Left            =   3480
         TabIndex        =   87
         Top             =   1920
         Width           =   300
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "FG:"
         Height          =   195
         Left            =   3120
         TabIndex        =   70
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "BG:"
         Height          =   195
         Left            =   1920
         TabIndex        =   69
         Top             =   360
         Width           =   270
      End
   End
   Begin VB.CommandButton cmdRemoveStation 
      Caption         =   "Remove Station"
      Height          =   375
      Left            =   5040
      TabIndex        =   47
      Top             =   1560
      Width           =   1335
   End
   Begin VB.TextBox txtPort 
      Height          =   375
      Left            =   8160
      TabIndex        =   46
      Text            =   "8888"
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtIp 
      Height          =   375
      Left            =   6360
      TabIndex        =   45
      Text            =   "192.168.0.100"
      Top             =   1200
      Width           =   1695
   End
   Begin VB.CommandButton cmdAddStation 
      Caption         =   "Add Station"
      Height          =   375
      Left            =   5040
      TabIndex        =   44
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CommandButton cmdKmStop 
      Caption         =   "Stop"
      Height          =   495
      Left            =   6480
      TabIndex        =   43
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton cmdKmRun 
      Caption         =   "Run"
      Height          =   495
      Left            =   5040
      TabIndex        =   42
      Top             =   120
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   7920
      Top             =   120
   End
   Begin VB.CommandButton cmdFlash 
      Caption         =   "Flash Text"
      Height          =   495
      Left            =   11160
      TabIndex        =   39
      Top             =   1440
      Width           =   975
   End
   Begin VB.TextBox txtLoop 
      Height          =   375
      Left            =   6720
      TabIndex        =   36
      Text            =   "1"
      Top             =   2880
      Width           =   375
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Large Data Testing"
      Height          =   615
      Left            =   5040
      TabIndex        =   34
      Top             =   2760
      Width           =   975
   End
   Begin VB.ComboBox cmbKS 
      Height          =   315
      ItemData        =   "Main.frx":0000
      Left            =   5400
      List            =   "Main.frx":004B
      Style           =   2  'Dropdown List
      TabIndex        =   29
      Top             =   720
      Width           =   735
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   495
      Left            =   11160
      TabIndex        =   25
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Load Default Input Parameters"
      Height          =   495
      Left            =   10920
      TabIndex        =   14
      Top             =   840
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0FF&
      Caption         =   "LS3000 KS Command"
      Height          =   3975
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   6135
      Begin VB.CommandButton Command4 
         Caption         =   "Send COM2"
         Height          =   495
         Left            =   2640
         TabIndex        =   112
         Top             =   2040
         Width           =   855
      End
      Begin VB.CommandButton cmdNormal 
         Caption         =   "Disable Blink"
         Height          =   375
         Left            =   2040
         TabIndex        =   41
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton cmdBlink 
         Caption         =   "Blink"
         Height          =   375
         Left            =   2040
         TabIndex        =   40
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox lblReturn 
         Height          =   975
         Left            =   960
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   37
         Text            =   "Main.frx":0096
         Top             =   2880
         Width           =   5055
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Display Special String"
         Height          =   495
         Left            =   120
         TabIndex        =   33
         Top             =   2040
         Width           =   1695
      End
      Begin VB.CommandButton cmdCopy 
         Caption         =   "Copy Screen String"
         Height          =   495
         Left            =   3480
         TabIndex        =   32
         Top             =   2040
         Width           =   1695
      End
      Begin VB.CommandButton cmdRestore 
         Caption         =   "Restore Box"
         Height          =   495
         Left            =   5160
         TabIndex        =   31
         Top             =   2040
         Width           =   855
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save Box"
         Height          =   495
         Left            =   5160
         TabIndex        =   30
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox txtChar 
         Height          =   285
         Left            =   5160
         TabIndex        =   24
         Text            =   "Text11"
         Top             =   1200
         Width           =   375
      End
      Begin VB.ComboBox cmbBack 
         Height          =   315
         Left            =   3480
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   1200
         Width           =   1095
      End
      Begin VB.ComboBox cmbFore 
         Height          =   315
         ItemData        =   "Main.frx":009C
         Left            =   1560
         List            =   "Main.frx":009E
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   1200
         Width           =   1215
      End
      Begin VB.TextBox txtScreenMode 
         Height          =   285
         Left            =   4560
         TabIndex        =   16
         Text            =   "1"
         Top             =   600
         Width           =   375
      End
      Begin VB.TextBox txtDuration 
         Height          =   285
         Left            =   4560
         TabIndex        =   15
         Text            =   "1"
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdDrawPic 
         Caption         =   "Draw Pic"
         Height          =   495
         Left            =   4320
         TabIndex        =   13
         Top             =   1560
         Width           =   855
      End
      Begin VB.CommandButton cmdAscii 
         Caption         =   "Ascii mode"
         Height          =   375
         Left            =   1080
         TabIndex        =   12
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdScan 
         Caption         =   "Scan Mode"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdSendRS232 
         Caption         =   "Send COM1"
         Height          =   495
         Left            =   1800
         TabIndex        =   10
         Top             =   2040
         Width           =   855
      End
      Begin VB.CommandButton cmdFillChar 
         Caption         =   "Fill Char"
         Height          =   495
         Left            =   960
         TabIndex        =   9
         Top             =   1560
         Width           =   855
      End
      Begin VB.CommandButton cmdDrawBox 
         Caption         =   "Draw Box"
         Height          =   495
         Left            =   2640
         TabIndex        =   8
         Top             =   1560
         Width           =   855
      End
      Begin VB.CommandButton cmdFillColor 
         Caption         =   "Fill Color"
         Height          =   495
         Left            =   3480
         TabIndex        =   7
         Top             =   1560
         Width           =   855
      End
      Begin VB.CommandButton cmdDisplayString 
         Caption         =   "Display String"
         Height          =   495
         Left            =   120
         TabIndex        =   6
         Top             =   1560
         Width           =   855
      End
      Begin VB.CommandButton cmdCursorOff 
         Caption         =   "Cursor Off"
         Height          =   375
         Left            =   1080
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdCursorOn 
         Caption         =   "Cursor On"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdCursorPos 
         Caption         =   "Cursor Position"
         Height          =   495
         Left            =   1800
         TabIndex        =   3
         Top             =   1560
         Width           =   855
      End
      Begin VB.CommandButton cmdDisplayMode 
         Caption         =   "Set Display Mode"
         Height          =   375
         Left            =   3120
         TabIndex        =   2
         Top             =   600
         Width           =   1455
      End
      Begin VB.CommandButton cmdBeep 
         Caption         =   "Beep"
         Height          =   375
         Left            =   3120
         TabIndex        =   1
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label11 
         Caption         =   "Return:"
         Height          =   255
         Left            =   360
         TabIndex        =   38
         Top             =   3480
         Width           =   495
      End
      Begin VB.Label lblAddr 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label12"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   27
         Top             =   3120
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Address"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   2880
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "Char:"
         Height          =   255
         Left            =   4680
         TabIndex        =   23
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label Label9 
         Caption         =   "Back:"
         Height          =   255
         Left            =   2880
         TabIndex        =   20
         Top             =   1200
         Width           =   615
      End
      Begin VB.Label Label8 
         Caption         =   "Fore:"
         Height          =   255
         Left            =   1080
         TabIndex        =   19
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label Label6 
         Caption         =   "Attribute:"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "1-40x25 2-64x25 3-80x25 4-106x30"
         Height          =   855
         Left            =   5160
         TabIndex        =   17
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Label Label29 
      Caption         =   "Timer Number"
      Height          =   495
      Left            =   0
      TabIndex        =   114
      Top             =   0
      Width           =   615
   End
   Begin VB.Label Label28 
      Caption         =   "Timer Number"
      Height          =   495
      Left            =   0
      TabIndex        =   113
      Top             =   0
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "X1:"
      Height          =   255
      Left            =   120
      TabIndex        =   59
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Label2 
      Caption         =   "Y1:"
      Height          =   255
      Left            =   840
      TabIndex        =   58
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Label5 
      Caption         =   "String/Pic:"
      Height          =   255
      Left            =   2880
      TabIndex        =   57
      Top             =   3720
      Width           =   855
   End
   Begin VB.Label Label13 
      Caption         =   "X2"
      Height          =   255
      Left            =   1560
      TabIndex        =   56
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Label14 
      Caption         =   "Y2"
      Height          =   255
      Left            =   2160
      TabIndex        =   55
      Top             =   3720
      Width           =   255
   End
   Begin VB.Label Label19 
      Caption         =   "Loop:"
      Height          =   255
      Left            =   6120
      TabIndex        =   35
      Top             =   2880
      Width           =   495
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      Caption         =   "KS:"
      Height          =   195
      Left            =   5040
      TabIndex        =   28
      Top             =   840
      Width           =   255
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Function getks() As Integer
    getks = cmbKS.ItemData(cmbKS.ListIndex)
End Function
Private Sub addColor(cmb As ComboBox, i As Integer)
With cmb
'============

.AddItem "Blue"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Blue
Else
    .ItemData(.NewIndex) = Ks_BG_Blue
End If
'=============
.AddItem "Green"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Green
Else
    .ItemData(.NewIndex) = Ks_BG_Green
End If

.AddItem "Red"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Red
Else
    .ItemData(.NewIndex) = Ks_BG_Red
End If

.AddItem "Intensity"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Intensity
Else
    .ItemData(.NewIndex) = Ks_BG_Intensity
End If

.AddItem "Black"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Black
Else
    .ItemData(.NewIndex) = Ks_BG_Black
End If

.AddItem "Purple"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Purple
Else
    .ItemData(.NewIndex) = Ks_BG_Purple
End If

.AddItem "Brown"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Brown
Else
    .ItemData(.NewIndex) = Ks_BG_Brown
End If

.AddItem "Aqua"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Aqua
Else
    .ItemData(.NewIndex) = Ks_BG_Aqua
End If

.AddItem "Grey"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Grey
Else
    .ItemData(.NewIndex) = Ks_BG_Grey
End If

.AddItem "Light Blue"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Light_Blue
Else
    .ItemData(.NewIndex) = Ks_BG_Light_Blue
End If

.AddItem "Pink"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Pink
Else
    .ItemData(.NewIndex) = Ks_BG_Pink
End If

.AddItem "Magenta"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Magenta
Else
    .ItemData(.NewIndex) = Ks_BG_Magenta
End If

.AddItem "Light Green"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Light_Green
Else
    .ItemData(.NewIndex) = Ks_BG_Light_Green
End If

.AddItem "Cyan"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Cyan
Else
    .ItemData(.NewIndex) = Ks_BG_Cyan
End If

.AddItem "Yellow"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_Yellow
Else
    .ItemData(.NewIndex) = Ks_BG_Yellow
End If

.AddItem "White"
If i = 0 Then
    .ItemData(.NewIndex) = Ks_FG_White
Else
    .ItemData(.NewIndex) = Ks_BG_White
End If

End With
End Sub

Private Sub cmdAllKsStatus_Click()

KM.KmRequireAllKsStatus

End Sub

Private Sub cmdAddStation_Click()
    KM.KmAddStation getks(), txtIp.Text, CInt(txtPort.Text)
End Sub

Private Sub cmdAddStation_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
     If Button = vbRightButton Then
   
     cmbKS.BackColor = vbYellow
     txtIp.BackColor = vbYellow
     txtPort.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtIp.BackColor = vbWhite
     txtPort.BackColor = vbWhite
   End If
End Sub

Private Sub cmdAscii_Click()
 KM.KsSetAsciiMode getks()

End Sub

Private Sub cmdAscii_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
   
     lblAddr.BackColor = vbYellow
     lblReturn.BackColor = vbYellow
     cmbKS.BackColor = vbYellow
     MsgBox "Press the bump bar and outputs will be displayed in yellow highlighted fields and screen!", vbInformation, "Hint"
     lblAddr.BackColor = vbWhite
     lblReturn.BackColor = vbWhite
     cmbKS.BackColor = vbWhite
   End If
End Sub

Private Sub cmdBeep_Click()
KM.KsBeep getks(), CInt(txtDuration.Text)


End Sub

Private Sub cmdBeepPitch_Click()
KM.KsBeepWithPitch getks(), CInt(txtBPDuration), CInt(txtBPPitch)

End Sub

Private Sub cmdBeepPitch_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtBPDuration.BackColor = vbYellow
     txtBPPitch.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtBPDuration.BackColor = vbWhite
     txtBPPitch.BackColor = vbWhite
   End If
End Sub

Private Sub cmdBlink_Click()
KM.KsSetBlink getks(), True

End Sub

Private Sub cmdBreakPoint_Click()
KM.KmRequestBreakPoint

End Sub

Private Sub cmdChangeAddr_Click()

End Sub

Private Sub cmdBlink_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "Background color must <0x80,for example" & vbCrLf & "light blue,pink,magenta,light green,cyan,yellow,or white", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
   End If
End Sub

Private Sub cmdClear_Click()
'==========================
cmbKS.ListIndex = 0
txtLoop = "1"
txtDuration = "1"
txtX = "0"
txtY = "0"
txtX2 = "20"
txtY2 = "20"
txtScreenMode = "3"
txtString = "Logic Controls,Inc."
'lblAttr = "0"
txtChar = "A"
cmbFore.ListIndex = 15
cmbBack.ListIndex = 0
lblReturn = ""
txtBg = "20"
txtFg = "210"
txtSetTimer = "1"
txtH = "1"
txtM = "8"
txtS = "38"
txtCols = "4"
txtRows = "2"
txtPanelStyle = "5"
txtShowTimer = "1"
txtShowTimerX = "10"
txtShowTimerY = "10"
txtTimer = "1"
txtHideTimer = "1"
txtBPDuration = "2"
txtBPPitch = "5"
txtCom = "1"
txtCodePage = "0"

lblAddr = ""

'txtNewAddr = ""
'lblCode = ""


End Sub

Private Sub cmdClose_Click()

KM.KmClose

End Sub

Private Sub cmdClear_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
    cmbKS.BackColor = vbYellow
    txtIp.BackColor = vbYellow
    txtPort.BackColor = vbYellow
    txtLoop.BackColor = vbYellow
    txtDuration.BackColor = vbYellow
    txtX.BackColor = vbYellow
    txtY.BackColor = vbYellow
    txtX2.BackColor = vbYellow
    txtY2.BackColor = vbYellow
    txtScreenMode.BackColor = vbYellow
    txtString.BackColor = vbYellow
    txtChar.BackColor = vbYellow
    cmbFore.BackColor = vbYellow
    cmbBack.BackColor = vbYellow
    lblReturn.BackColor = vbYellow
    txtBg.BackColor = vbYellow
    txtFg.BackColor = vbYellow
    txtSetTimer.BackColor = vbYellow
    txtH.BackColor = vbYellow
    txtM.BackColor = vbYellow
    txtS.BackColor = vbYellow
    txtCols.BackColor = vbYellow
    txtRows.BackColor = vbYellow
    txtPanelStyle.BackColor = vbYellow
    txtShowTimer.BackColor = vbYellow
    txtShowTimerX.BackColor = vbYellow
    txtShowTimerY.BackColor = vbYellow
    txtTimer.BackColor = vbYellow
    txtHideTimer.BackColor = vbYellow
    txtBPDuration.BackColor = vbYellow
    txtBPPitch.BackColor = vbYellow
    txtCom.BackColor = vbYellow
    txtCodePage.BackColor = vbYellow
    lblAddr.BackColor = vbYellow
    MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
    cmbKS.BackColor = vbWhite
    txtIp.BackColor = vbWhite
    txtPort.BackColor = vbWhite
    txtLoop.BackColor = vbWhite
    txtDuration.BackColor = vbWhite
    txtX.BackColor = vbWhite
    txtY.BackColor = vbWhite
    txtX2.BackColor = vbWhite
    txtY2.BackColor = vbWhite
    txtScreenMode.BackColor = vbWhite
    txtString.BackColor = vbWhite
    txtChar.BackColor = vbWhite
    cmbFore.BackColor = vbWhite
    cmbBack.BackColor = vbWhite
    lblReturn.BackColor = vbWhite
    txtBg.BackColor = vbWhite
    txtFg.BackColor = vbWhite
    txtSetTimer.BackColor = vbWhite
    txtH.BackColor = vbWhite
    txtM.BackColor = vbWhite
    txtS.BackColor = vbWhite
    txtCols.BackColor = vbWhite
    txtRows.BackColor = vbWhite
    txtPanelStyle.BackColor = vbWhite
    txtShowTimer.BackColor = vbWhite
    txtShowTimerX.BackColor = vbWhite
    txtShowTimerY.BackColor = vbWhite
    txtTimer.BackColor = vbWhite
    txtHideTimer.BackColor = vbWhite
    txtBPDuration.BackColor = vbWhite
    txtBPPitch.BackColor = vbWhite
    txtCom.BackColor = vbWhite
    txtCodePage.BackColor = vbWhite
    lblAddr.BackColor = vbWhite
End If

End Sub

Private Sub cmdClear256Color_Click()
KM.KsClearWindowsAreaWith256Color getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2), CInt(txtBg)

End Sub

Private Sub cmdClear256Color_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     txtBg.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
     txtBg.BackColor = vbWhite
   End If
End Sub

Private Sub cmdCodePage_Click()
KM.KsCodePageSelect getks(), CInt(txtCodePage)
End Sub

Private Sub cmdCodePage_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtCodePage.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "Click Display String to check!" & vbCrLf & "Values can be 0-3!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtCodePage.BackColor = vbWhite
   End If
End Sub

Private Sub cmdCopy_Click()
Dim s As String

s = KM.KsCopyScreenString(getks(), CInt(txtX), CInt(txtY), CInt(txtX2))
MsgBox (s)


End Sub

Private Sub cmdCopy_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "X2 means the length of the copy string.", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
   End If
End Sub

Private Sub cmdCursorOff_Click()
KM.KsCursorOff getks()

End Sub

Private Sub cmdCursorOn_Click()
KM.KsCursorOn getks()

End Sub

Private Sub cmdCursorPos_Click()
KM.KsSetCursorPosition getks(), CInt(txtX), CInt(txtY)
End Sub

Private Sub cmdDisableKs_Click()
KM.KmDisableKs getks()


End Sub

Private Sub cmdCursorPos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "Click Cursor On for testing.", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
   End If
End Sub

Private Sub cmdDisp256String_Click()
KM.KsDisplayStringWith256Color getks(), CInt(txtX), CInt(txtY), txtString, CInt(txtBg), CInt(txtFg)

End Sub

Private Sub cmdDisp256String_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtString.BackColor = vbYellow
     txtBg.BackColor = vbYellow
     txtFg.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "/t means tab, /n means linefeed, /r means carriagereturn", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtString.BackColor = vbWhite
     txtBg.BackColor = vbWhite
     txtFg.BackColor = vbWhite
   End If
End Sub

Private Sub cmdDisplayMode_Click()
KM.KsSetDisplayMode getks(), CInt(txtScreenMode)
cmdDisplayString_Click

End Sub

Private Sub cmdDisplayMode_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtScreenMode.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtScreenMode.BackColor = vbWhite
   End If
End Sub

Private Sub cmdDisplayString_Click()
'Dim s As String
's = Chr(10) + Chr(13) + "hello"
Dim ret As Boolean

ret = KM.KsDisplayString(getks(), CInt(txtX), CInt(txtY), txtString, cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
If (Not ret) Then
    MsgBox "error"
    
End If
' Chr(&H82)
'KM.KsDisplayString getks(), CInt(txtX), CInt(txtY), s, cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
End Sub

Private Sub cmdDisplayString_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtString.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "/t means tab, /n means linefeed, /r means carriagereturn", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtString.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
   End If
End Sub

Private Sub cmdDraw256Panel_Click()
KM.KsDrawPanelsWith256Color getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2), CInt(txtCols), CInt(txtRows), CInt(txtBg), CInt(txtFg), CInt(txtPanelStyle)

End Sub

Private Sub cmdDraw256Panel_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     txtCols.BackColor = vbYellow
     txtRows.BackColor = vbYellow
     txtBg.BackColor = vbYellow
     txtFg.BackColor = vbYellow
     txtPanelStyle.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
     txtCols.BackColor = vbWhite
     txtRows.BackColor = vbWhite
     txtBg.BackColor = vbWhite
     txtFg.BackColor = vbWhite
     txtPanelStyle.BackColor = vbWhite
   End If
End Sub

Private Sub cmdDrawBox_Click()
Dim ret As Boolean

ret = KM.KsDrawBox(getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2), cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
If (Not ret) Then
    MsgBox "error"
End If


End Sub

Private Sub cmdDrawBox_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
     If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
   End If
End Sub

Private Sub cmdDrawPic_Click()
 Dim filename As String
 CommonDialog1.Filter = "BitMap(*.bmp)|*.bmp"
 CommonDialog1.ShowOpen
 filename = CommonDialog1.filename
 KM.KsDisplayImage getks(), CInt(txtX), CInt(txtY), filename

End Sub

Private Sub cmdDrawPixel_Click()
KM.KsDrawPixel getks(), CInt(txtX), CInt(txtY), cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdEnableKs_Click()
KM.KmEnableKs getks()

End Sub

Private Sub cmdDrawPic_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
   End If
End Sub

Private Sub cmdExit_Click()
KM.KmClose

End

End Sub

Private Sub cmdFillChar_Click()
KM.KsFillChar getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2), txtChar, cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdFillChar_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     txtChar.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
     txtChar.BackColor = vbWhite
   End If
End Sub

Private Sub cmdFillColor_Click()
KM.KsFillAttribute getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2), cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)


End Sub

Private Sub cmdFillColor_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
     If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
   End If
End Sub

Private Sub cmdFlash_Click()
Timer1.Enabled = Not Timer1.Enabled

End Sub

Private Sub cmdHideClock_Click()
KM.KsHideClock getks()

End Sub

Private Sub cmdKmReset_Click()
KM.KmReset

End Sub

Private Sub cmdKmStatus_Click()
KM.KmRequestStatus

End Sub



Private Sub cmdFlash_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     txtChar.BackColor = vbYellow
     txtString.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
     txtChar.BackColor = vbWhite
     txtString.BackColor = vbWhite
   End If
End Sub

Private Sub cmdGetVersion_Click()
Dim s As String
s = KM.KsGetVersion(getks())
MsgBox s



End Sub

Private Sub cmdHidTimer_Click()
KM.KsHideTimer getks(), CInt(txtHideTimer)

End Sub

Private Sub cmdHidTimer_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtHideTimer.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtHideTimer.BackColor = vbWhite
   End If
End Sub

Private Sub cmdKbOff_Click()
KM.KsSetKeyboardIconOff getks()


End Sub

Private Sub cmdKbOn_Click()
KM.KsSetKeyboardIconOn getks()

End Sub



Private Sub cmdKmRun_Click()
Dim ret As Boolean

ret = KM.KmRun
If ret = False Then
    MsgBox ("Error when run km")
End If

End Sub

Private Sub cmdKmStop_Click()
KM.KmClose
End Sub

Private Sub cmdNetOff_Click()
KM.KsSetEthernetIconOff getks()




End Sub

Private Sub cmdNetOn_Click()
KM.KsSetEthernetIconOn getks()



End Sub

Private Sub cmdNormal_Click()
  KM.KsSetBlink getks(), False

End Sub

Private Sub cmdRemoveStation_Click()
KM.KmRemoveStation getks()


End Sub

Private Sub cmdReqCount_Click()
KM.KsRequestTimeCountingData getks()

End Sub

Private Sub cmdReqScreenString_Click()
KM.KsRequestScreenString getks(), CInt(txtX), CInt(txtY), 10

End Sub

Private Sub cmdRemoveStation_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
   End If
End Sub

Private Sub cmdReqKbStatus_Click()

 KM.KsRequestKeyboardStatus getks()

End Sub

Private Sub cmdReqRs232Setting_Click()
KM.KsRequestRS232DriverSetting getks()


End Sub

Private Sub cmdReqRs232Setting_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     lblReturn.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     lblReturn.BackColor = vbWhite
   End If
End Sub

Private Sub cmdReqRs232Status_Click()
KM.KsRequestRS232PortStatus getks(), CInt(txtCom)

End Sub

Private Sub cmdReqRs232Status_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtCom.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtCom.BackColor = vbWhite
   End If
End Sub

Private Sub cmdReqTimer_Click()
KM.KsRequestTimerValue getks(), CInt(txtTimer)

End Sub

Private Sub cmdReqTimer_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtTimer.BackColor = vbYellow
     lblReturn.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtTimer.BackColor = vbWhite
     lblReturn.BackColor = vbWhite
   End If
End Sub

Private Sub cmdRestore_Click()
KM.KsRestoreBox getks(), CInt(txtX), CInt(txtY)
End Sub



Private Sub cmdRestore_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
   End If
End Sub

Private Sub cmdSave_Click()
KM.KsSaveBox getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2)
End Sub

Private Sub cmdSave_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtX2.BackColor = vbYellow
     txtY2.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtX2.BackColor = vbWhite
     txtY2.BackColor = vbWhite
   End If
End Sub

Private Sub cmdScan_Click()
  KM.KsSetScanMode getks()
End Sub

Private Sub cmdScroll_Click()
KM.KsScrollWindowUp getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2)

End Sub

Private Sub cmdScan_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
   
     lblAddr.BackColor = vbYellow
     lblReturn.BackColor = vbYellow
     cmdKS.BackColor = vbYellow
     MsgBox "Press the bump bar and outputs will be displayed in yellow highlighted fields and screen!", vbInformation, "Hint"
     lblAddr.BackColor = vbWhite
     lblReturn.BackColor = vbWhite
     cmbKS.BackColor = vbWhite
   End If
End Sub

Private Sub cmdSendRS232_Click()
KM.KsSendRS232String getks(), txtString

End Sub

Private Sub cmdSetActive_Click()
KM.KmSetActive

End Sub

Private Sub cmdSet_Click()
Dim s As String
Dim Address As Integer
Address = getks()
s = txtIp

If (Address = 0) Then
    KM.LS00IpAndPort = s
    
ElseIf (Address = 1) Then
    KM.LS01IpAndPort = s
ElseIf Address = 2 Then
     KM.LS02IpAndPort = s
ElseIf Address = 3 Then
     KM.LS03IpAndPort = s
ElseIf Address = 4 Then
     KM.LS04IpAndPort = s
ElseIf Address = 5 Then
     KM.LS05IpAndPort = s
ElseIf Address = 6 Then
     KM.LS06IpAndPort = s
ElseIf Address = 7 Then
     KM.LS07IpAndPort = s
ElseIf Address = 8 Then
     KM.LS08IpAndPort = s
ElseIf Address = 9 Then
     KM.LS09IpAndPort = s
ElseIf Address = 10 Then
     KM.LS10IpAndPort = s
ElseIf Address = 11 Then
     KM.LS11IpAndPort = s
ElseIf Address = 12 Then
     KM.LS12IpAndPort = s
ElseIf Address = 13 Then
     KM.LS13IpAndPort = s
ElseIf Address = 14 Then
     KM.LS14IpAndPort = s
ElseIf Address = 15 Then
     KM.LS15IpAndPort = s
ElseIf Address = 16 Then
     KM.LS16IpAndPort = s
ElseIf Address = 17 Then
     KM.LS17IpAndPort = s
ElseIf Address = 18 Then
     KM.LS18IpAndPort = s

End If
txtIp = s

End Sub

Private Sub cmdSetBaud_Click()
KM.KsSetRS232BaudRate getks(), CInt(txtBaud)

End Sub

Private Sub cmdSetClock_Click()
KM.KsSetClock getks(), CInt(txtHour), CInt(txtMinute), CInt(txtSecond)

End Sub

Private Sub cmdSetNonactive_Click()
KM.KmSetNonActive

End Sub


Private Sub cmdShowClock_Click()
KM.KsDisplayClock getks(), CInt(txtX), CInt(txtY)

End Sub

Private Sub cmdSrl_Click()
KM.KsScrollWindow getks(), CInt(txtX), CInt(txtY), CInt(txtX2), CInt(txtY2), CInt(txtLines), cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdStartCount_Click()
KM.KsStartTimeCounting getks()

End Sub

Private Sub cmdStopCount_Click()
KM.KsStopTimeCounting c

End Sub

Private Sub cmdString_Click()
KM.KsSetStringMode getks()

End Sub

Private Sub cmdSendRS232_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtString.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtString.BackColor = vbWhite
   End If
End Sub

Private Sub cmdSetTimer_Click()
KM.KsSetTimer getks(), CInt(txtSetTimer), CInt(txtH), CInt(txtM), CInt(txtS)

End Sub

Private Sub cmdSetTimer_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtSetTimer.BackColor = vbYellow
     txtH.BackColor = vbYellow
     txtM.BackColor = vbYellow
     txtS.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtSetTimer.BackColor = vbWhite
     txtH.BackColor = vbWhite
     txtM.BackColor = vbWhite
     txtS.BackColor = vbWhite
   End If
End Sub

Private Sub cmdShowTimer_Click()
KM.KsDisplayTimer getks(), CInt(txtShowTimer), CInt(txtShowTimerX), CInt(txtShowTimerY), CInt(txtBg), CInt(txtFg)

End Sub

Private Sub cmdShowTimer_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtShowTimer.BackColor = vbYellow
     txtShowTimerX.BackColor = vbYellow
     txtShowTimerY.BackColor = vbYellow
     txtBg.BackColor = vbYellow
     txtFg.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtShowTimer.BackColor = vbWhite
     txtShowTimerX.BackColor = vbWhite
     txtShowTimerY.BackColor = vbWhite
     txtBg.BackColor = vbWhite
     txtFg.BackColor = vbWhite
   End If
End Sub

Private Sub cmdStatus256String_Click()
KM.KsDisplayStringAtStatusLine getks(), CInt(txtX), txtString, CInt(txtBg), CInt(txtFg)

End Sub

Private Sub cmdStatus256String_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtString.BackColor = vbYellow
     txtBg.BackColor = vbYellow
     txtFg.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtString.BackColor = vbWhite
     txtBg.BackColor = vbWhite
     txtFg.BackColor = vbWhite
   End If
End Sub

Private Sub cmdVer_Click()
    KM.KsRequestVersion getks()

End Sub

Private Sub Command1_Click()
'txtString = "Logic" + Chr(&H8) + Chr(&HA) + "Controls" + Chr(&HD) + Chr(&H9) + "INC."
txtString = "Logic\nControls\tINC.\b\rTest"
KM.KsDisplaySpecialString getks(), CInt(txtX), CInt(txtY), txtString, cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub Command1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtString.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "/t means tab, /n means linefeed, /r means carriagereturn", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtString.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
   End If
End Sub

Private Sub Command2_Click()
Dim i As Integer
Dim j As Integer

Dim k As Integer
Dim s As String
Dim Address As Integer

s = "A"
Dim ret As Boolean
Dim nloop As Integer
If (txtLoop.Text = "") Then
    nloop = 10
Else
    nloop = CInt(txtLoop.Text)
End If


Address = getks()

k = 0
For k = 1 To nloop
    For i = 0 To 24
        ret = KM.KsDisplayString(Address, 0, i, CStr(k), cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
        If Not ret Then
           '// MsgBox "error"
            
           Exit Sub
        End If

        For j = 3 To 79
            
          ret = KM.KsDisplayString(Address, j, i, "A", cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
          If Not ret Then
            'MsgBox "error"
            Exit Sub
            
          End If
            
        Next
    Next
  '  KM.KitchenClearScreen getks()
    
Next




End Sub

Private Sub Command2_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
   If Button = vbRightButton Then
   
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     txtLoop.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
     txtLoop.BackColor = vbWhite
   End If
End Sub

Private Sub Command3_Click()
    KM.KitchenClearScreen getks()
End Sub

Private Sub Command4_Click()
  KM.KSSendRS232COM2String getks(), txtString
End Sub

Private Sub Command4_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtString.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtString.BackColor = vbWhite
   End If
End Sub

Private Sub Command5_Click()
      Dim i As Integer
      Dim s As String
      Dim ar(0 To 256) As Byte
      
      s = ""
      For i = 128 To 255
          ar(i - 128) = i
          
      Next
      
      s = StrConv(ar, vbUnicode)
      
      KM.KsDisplayString getks(), CInt(txtX), CInt(txtY), s, cmbFore.ItemData(cmbFore.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
End Sub

Private Sub Command5_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
     txtX.BackColor = vbYellow
     txtY.BackColor = vbYellow
     txtString.BackColor = vbYellow
     cmbFore.BackColor = vbYellow
     cmbBack.BackColor = vbYellow
     MsgBox "Works with yellow highlighted parameters!" & vbCrLf & "/t means tab, /n means linefeed, /r means carriagereturn", vbInformation, "Hint"
     cmbKS.BackColor = vbWhite
     txtX.BackColor = vbWhite
     txtY.BackColor = vbWhite
     txtString.BackColor = vbWhite
     cmbFore.BackColor = vbWhite
     cmbBack.BackColor = vbWhite
   End If
End Sub




Private Sub Command6_Click()
  KM.KmReset
End Sub




Private Sub Form_Load()
cmbKS.ListIndex = 1
addColor cmbFore, 0
addColor cmbBack, 1
cmbFore.ListIndex = 15
cmbBack.ListIndex = 0
'cmbPort.ListIndex = 1
'cmbCom.ListIndex = 0


cmdClear_Click


End Sub

Private Sub Form_Unload(Cancel As Integer)
KM.KmClose

End Sub




Private Sub KM_OnKsEnterKeyPress(ByVal Address As Integer)
lblReturn.Text = "Enter Key Pressed"

End Sub

Private Sub KM_OnKsReturnCountAndKey(ByVal Address As Integer, ByVal KeyCode As Integer, ByVal C0 As Integer, ByVal C1 As Integer, ByVal C2 As Integer, ByVal C3 As Integer)
lblAddr.Caption = Address
lblReturn.Text = Chr(KeyCode) + " " + Str(C0) + " " + Str(C1) + " " + Str(C2) + " " + Str(C3)

End Sub

Private Sub GetKSStatus_Click()
    Dim status As Integer
    status = KM.KSGetStatus(getks())
    MsgBox "The status of LS6 #" + Str(getks()) + " is: " + Str(status)
End Sub

Private Sub GetKSStatus_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = vbRightButton Then
     cmbKS.BackColor = vbYellow
  End If
End Sub

Private Sub KM_OnKsConnected(ByVal Address As Integer)
lblReturn.Text = "ls#" + CStr(Address) + " connected"


End Sub

Private Sub KM_OnKsDisconnectd(ByVal Address As Integer)
lblReturn.Text = "ls#" + CStr(Address) + " disconnected"

End Sub

Private Sub KM_OnKSReturnKbStatus(ByVal Address As Integer, ByVal nStatus As Integer)
  Dim kbstatus As String
  If nStatus = 1 Then
    kbstatus = "Connected"
  Else
    kbstatus = "Disconnected"
  End If
  lblReturn = "KB is: " + kbstatus
End Sub

Private Sub KM_OnKsReturnKey(ByVal Address As Integer, ByVal AscKeyCode As Integer)
Dim key As Integer
'If user uses OCX command in a thread, the return code of LS6 can be got from this "read" function.
key = KM.Read(Address)
MsgBox CStr(Hex(key)) + "H ", vbInformation, "Key Value (Hexadecimal)"

lblAddr.Caption = Address
lblReturn.Text = lblReturn.Text + CStr(Hex(AscKeyCode)) + "H "

'lblCode.Caption = Hex(AscKeyCode)
txtString = CStr(Hex(AscKeyCode)) + "H "
cmdDisplayString_Click
End Sub

Private Sub KM_OnKSReturnRS232DriverSetting(ByVal Address As Integer, ByVal COM1 As Integer, ByVal COM2 As Integer)
lblReturn.Text = "return rs232 setting: " + CStr(COM1) + "  " + CStr(COM2)

End Sub

Private Sub KM_OnKSReturnRS232PortStatus(ByVal Address As Integer, ByVal COMPort As Integer, ByVal status As Integer)
lblReturn.Text = "return rs232 status:" + "com_port=" + CStr(COMPort) + ", status=" + CStr(status)

End Sub

Private Sub KM_OnKsReturnString(ByVal Address As Integer, ByVal strRet As String)
lblAddr.Caption = Address
lblReturn.Text = strRet

End Sub

Private Sub lblKsStatus8_Click()
End Sub

Private Sub KM_OnKSReturnTimer(ByVal Address As Integer, ByVal nTimer As Integer, ByVal nHour As Integer, ByVal nMinute As Integer, ByVal nSecond As Integer)
lblReturn.Text = "return timer value:" + "timer=" + CStr(nTimer) + ", Hour=" + CStr(nHour) + ", minute=" + CStr(nMinute) + ",second=" + CStr(nSecond)

End Sub

Private Sub KSGetStatus_Click()
    Dim s1 As String
    s1 = KM.KSGetStatus(getks())
    MsgBox s1
End Sub

Private Sub Timer1_Timer()
cmdFillChar_Click
Dim i As Long
i = 0
Do While i < 100
i = i + 1
    
Loop

cmdDisplayString_Click

End Sub

