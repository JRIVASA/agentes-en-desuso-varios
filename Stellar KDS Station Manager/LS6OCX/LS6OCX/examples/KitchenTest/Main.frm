VERSION 5.00
Object = "{785C818B-3B27-11D6-85F9-52544C194B3E}#1.0#0"; "LS6.ocx"
Begin VB.Form Form1 
   Caption         =   "Kitchen Test Program"
   ClientHeight    =   9885
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15060
   LinkTopic       =   "Form1"
   ScaleHeight     =   9885
   ScaleWidth      =   15060
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin LS6Lib.LS6 LS 
      Height          =   4455
      Left            =   120
      TabIndex        =   180
      Top             =   120
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   7858
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdRemoveIp 
      Caption         =   "Remove Station"
      Height          =   495
      Left            =   11160
      TabIndex        =   168
      Top             =   0
      Width           =   855
   End
   Begin VB.TextBox txtPort 
      Height          =   405
      Left            =   10560
      TabIndex        =   167
      Text            =   "8888"
      Top             =   0
      Width           =   495
   End
   Begin VB.TextBox txtIP 
      Height          =   375
      Left            =   9240
      TabIndex        =   166
      Text            =   "192.168.0.100"
      Top             =   0
      Width           =   1335
   End
   Begin VB.CommandButton cmdAddStation 
      Caption         =   "Add Station"
      Height          =   375
      Left            =   8520
      TabIndex        =   165
      Top             =   0
      Width           =   735
   End
   Begin VB.Frame Frame11 
      Caption         =   "Summary Panel"
      Height          =   1935
      Left            =   120
      TabIndex        =   126
      Top             =   9120
      Width           =   10695
      Begin VB.TextBox txtScrollLines 
         Height          =   375
         Left            =   8880
         TabIndex        =   162
         Text            =   "1"
         Top             =   480
         Width           =   375
      End
      Begin VB.TextBox txtSumItem 
         Height          =   375
         Left            =   2280
         TabIndex        =   146
         Text            =   "Hot Dog                           30"
         Top             =   1440
         Width           =   3135
      End
      Begin VB.TextBox txtSumTitle 
         Height          =   375
         Left            =   2280
         TabIndex        =   145
         Text            =   "Item Name                        Sum"
         Top             =   960
         Width           =   3135
      End
      Begin VB.TextBox txtSumCaption 
         Height          =   375
         Left            =   4200
         TabIndex        =   139
         Text            =   "Summary"
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox txtSumh 
         Height          =   375
         Left            =   3720
         TabIndex        =   138
         Text            =   "15"
         Top             =   480
         Width           =   495
      End
      Begin VB.TextBox txtSumw 
         Height          =   375
         Left            =   3240
         TabIndex        =   137
         Text            =   "50"
         Top             =   480
         Width           =   495
      End
      Begin VB.TextBox txtSumy 
         Height          =   375
         Left            =   2760
         TabIndex        =   136
         Text            =   "5"
         Top             =   480
         Width           =   495
      End
      Begin VB.TextBox txtSumx 
         Height          =   375
         Left            =   2280
         TabIndex        =   135
         Text            =   "15"
         Top             =   480
         Width           =   495
      End
      Begin VB.CommandButton cmdSumClear 
         Caption         =   "Clear"
         Height          =   495
         Left            =   5520
         TabIndex        =   134
         Top             =   1320
         Width           =   1695
      End
      Begin VB.CommandButton cmdSumScrollDown 
         Caption         =   "Scroll Down"
         Height          =   495
         Left            =   7200
         TabIndex        =   133
         Top             =   360
         Width           =   1695
      End
      Begin VB.CommandButton cmdSumScrollUp 
         Caption         =   "Scroll Up"
         Height          =   495
         Left            =   7200
         TabIndex        =   132
         Top             =   840
         Width           =   1695
      End
      Begin VB.CommandButton cmdSumHide 
         Caption         =   "Hide Sum Panel"
         Height          =   495
         Left            =   5520
         TabIndex        =   131
         Top             =   840
         Width           =   1695
      End
      Begin VB.CommandButton cmdShowSum 
         Caption         =   "Show Sum Panel"
         Height          =   495
         Left            =   5520
         TabIndex        =   130
         Top             =   360
         Width           =   1695
      End
      Begin VB.CommandButton cmdSumAddItem 
         Caption         =   "Add Item"
         Height          =   495
         Left            =   240
         TabIndex        =   129
         Top             =   1320
         Width           =   2055
      End
      Begin VB.CommandButton cmdSumSetTitle 
         Caption         =   "Set Title"
         Height          =   495
         Left            =   240
         TabIndex        =   128
         Top             =   840
         Width           =   2055
      End
      Begin VB.CommandButton cmdCreateSum 
         Caption         =   "Create Sum Panel"
         Height          =   495
         Left            =   240
         TabIndex        =   127
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Lines:"
         Height          =   195
         Left            =   9000
         TabIndex        =   163
         Top             =   240
         Width           =   420
      End
      Begin VB.Label Label28 
         Caption         =   "Please set attribute at ""Item-->Attritue"" area"
         ForeColor       =   &H00FF0000&
         Height          =   1275
         Left            =   9480
         TabIndex        =   147
         Top             =   240
         Width           =   1140
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Caption"
         Height          =   195
         Left            =   4560
         TabIndex        =   144
         Top             =   240
         Width           =   540
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "H"
         Height          =   195
         Left            =   3960
         TabIndex        =   143
         Top             =   240
         Width           =   120
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "W"
         Height          =   195
         Left            =   3360
         TabIndex        =   142
         Top             =   240
         Width           =   165
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "Y"
         Height          =   195
         Left            =   2880
         TabIndex        =   141
         Top             =   240
         Width           =   105
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "X"
         Height          =   195
         Left            =   2520
         TabIndex        =   140
         Top             =   240
         Width           =   105
      End
   End
   Begin VB.Frame Frame10 
      Caption         =   "Flash"
      Height          =   1815
      Left            =   120
      TabIndex        =   112
      Top             =   7200
      Width           =   5055
      Begin VB.CheckBox chkFlash 
         Caption         =   "Flash"
         Height          =   495
         Left            =   4200
         TabIndex        =   125
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox txtSHInterval 
         Height          =   375
         Left            =   4440
         TabIndex        =   119
         Text            =   "800"
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtFlashInterval 
         Height          =   375
         Left            =   2640
         TabIndex        =   118
         Text            =   "1500"
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtFlashMod 
         Height          =   375
         Left            =   3480
         TabIndex        =   117
         Text            =   "-1"
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox txtFlashItem 
         Height          =   375
         Left            =   2640
         TabIndex        =   116
         Text            =   "0"
         Top             =   1200
         Width           =   615
      End
      Begin VB.TextBox txtFlashID 
         Height          =   375
         Left            =   1680
         TabIndex        =   115
         Text            =   "1001"
         Top             =   1200
         Width           =   855
      End
      Begin VB.CommandButton cmdFlash 
         Caption         =   "Set Flash Object"
         Height          =   495
         Left            =   120
         TabIndex        =   114
         Top             =   1080
         Width           =   1455
      End
      Begin VB.CommandButton cmdFre 
         Caption         =   "Set Flash Frequency"
         Height          =   495
         Left            =   120
         TabIndex        =   113
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label22 
         Caption         =   "Modifier Index"
         Height          =   375
         Left            =   3480
         TabIndex        =   124
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label21 
         Caption         =   "Item Index"
         Height          =   375
         Left            =   2520
         TabIndex        =   123
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label20 
         Caption         =   "Order ID"
         Height          =   375
         Left            =   1680
         TabIndex        =   122
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label19 
         Caption         =   "ShowHide Delay"
         Height          =   495
         Left            =   3360
         TabIndex        =   121
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label18 
         Caption         =   "Flash Interval:"
         Height          =   375
         Left            =   2040
         TabIndex        =   120
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Frame Frame9 
      Caption         =   "Screen"
      Height          =   2415
      Left            =   120
      TabIndex        =   96
      Top             =   4680
      Width           =   5055
      Begin VB.CheckBox chkShow 
         Caption         =   "Show"
         Height          =   255
         Left            =   1560
         TabIndex        =   111
         Top             =   1920
         Width           =   735
      End
      Begin VB.CommandButton cmdPanel 
         Caption         =   "Display Panel Number"
         Height          =   495
         Left            =   120
         TabIndex        =   110
         Top             =   1800
         Width           =   1455
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Send Message"
         Height          =   375
         Left            =   120
         TabIndex        =   106
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox txtInfo 
         Height          =   375
         Left            =   1560
         TabIndex        =   105
         Text            =   "Logic Controls, Inc."
         Top             =   360
         Width           =   1455
      End
      Begin VB.CommandButton Command11 
         Caption         =   "Hide Message"
         Height          =   375
         Left            =   3240
         TabIndex        =   104
         Top             =   360
         Width           =   1455
      End
      Begin VB.CommandButton cmdScrChar 
         Caption         =   "Set Screen Char"
         Height          =   375
         Left            =   120
         TabIndex        =   103
         Top             =   1320
         Width           =   1455
      End
      Begin VB.TextBox txtH 
         Height          =   405
         Left            =   2040
         TabIndex        =   102
         Text            =   "45"
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox txtV 
         Height          =   405
         Left            =   3000
         TabIndex        =   101
         Text            =   "124"
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox txtC 
         Height          =   405
         Left            =   3960
         TabIndex        =   100
         Text            =   "43"
         Top             =   1320
         Width           =   615
      End
      Begin VB.CommandButton cmdBtmMsg 
         Caption         =   "Send Bottom Msg"
         Height          =   375
         Left            =   120
         TabIndex        =   99
         Top             =   840
         Width           =   1455
      End
      Begin VB.CommandButton cmdHidBtmMsg 
         Caption         =   "Hide Bottom Msg"
         Height          =   375
         Left            =   3240
         TabIndex        =   98
         Top             =   840
         Width           =   1455
      End
      Begin VB.TextBox txtBtmMsg 
         Height          =   315
         Left            =   1560
         TabIndex        =   97
         Text            =   "Kitchen Display System"
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "H"
         Height          =   195
         Left            =   1800
         TabIndex        =   109
         Top             =   1440
         Width           =   120
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "V"
         Height          =   195
         Left            =   2760
         TabIndex        =   108
         Top             =   1440
         Width           =   105
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "C"
         Height          =   195
         Left            =   3720
         TabIndex        =   107
         Top             =   1440
         Width           =   105
      End
   End
   Begin VB.TextBox txtOrders 
      Height          =   405
      Left            =   13440
      TabIndex        =   94
      Text            =   "100"
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton cmdHelp 
      Caption         =   "Help"
      Height          =   375
      Left            =   14160
      TabIndex        =   93
      Top             =   120
      Width           =   855
   End
   Begin VB.Frame Frame7 
      Caption         =   "Timer"
      Height          =   2655
      Left            =   5280
      TabIndex        =   82
      Top             =   6360
      Width           =   2895
      Begin VB.TextBox txtPing 
         Height          =   375
         Left            =   1560
         TabIndex        =   176
         Text            =   "500"
         Top             =   2160
         Width           =   1215
      End
      Begin VB.CommandButton cmdPingTimer 
         Caption         =   "Set Ping Timer interval"
         Height          =   375
         Left            =   120
         TabIndex        =   175
         Top             =   1800
         Width           =   2415
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "Update Wait Time"
         Height          =   375
         Left            =   120
         TabIndex        =   87
         Top             =   960
         Width           =   2415
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Start  Update Wait Time Timer"
         Height          =   375
         Left            =   120
         TabIndex        =   86
         Top             =   240
         Width           =   2415
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Stop Update Wait Time Timer"
         Height          =   375
         Left            =   120
         TabIndex        =   85
         Top             =   600
         Width           =   2415
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Set Timer Interval"
         Height          =   375
         Left            =   120
         TabIndex        =   84
         Top             =   1320
         Width           =   2415
      End
      Begin VB.TextBox txtTime 
         Height          =   375
         Left            =   2520
         TabIndex        =   83
         Text            =   "5"
         Top             =   1320
         Width           =   255
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Multiple User"
      Height          =   2655
      Left            =   8280
      TabIndex        =   71
      Top             =   6360
      Width           =   6855
      Begin VB.ComboBox cmbUser 
         Height          =   315
         ItemData        =   "Main.frx":0000
         Left            =   120
         List            =   "Main.frx":000A
         Style           =   2  'Dropdown List
         TabIndex        =   90
         Top             =   240
         Width           =   1695
      End
      Begin VB.CommandButton cmdLeftTitle 
         Caption         =   "Set Left Title"
         Height          =   375
         Left            =   2400
         TabIndex        =   81
         Top             =   600
         Width           =   2175
      End
      Begin VB.CommandButton cmdRightTitle 
         Caption         =   "Set Right Title"
         Height          =   375
         Left            =   2400
         TabIndex        =   80
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox txtMultiTitle 
         Height          =   375
         Left            =   4560
         TabIndex        =   79
         Text            =   "Top"
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton cmdMultiAddOrder 
         Caption         =   "Multi User Add Order"
         Height          =   375
         Left            =   120
         TabIndex        =   78
         Top             =   600
         Width           =   2175
      End
      Begin VB.CheckBox chkAuto 
         Caption         =   "Auto Arrange"
         Height          =   255
         Left            =   2400
         TabIndex        =   77
         Top             =   240
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CommandButton Command1 
         Caption         =   "MultiUser Restore Order"
         Height          =   375
         Left            =   120
         TabIndex        =   75
         Top             =   1080
         Width           =   2175
      End
      Begin VB.CommandButton Command2 
         Caption         =   "MultiUser Focus Move Prev"
         Height          =   375
         Left            =   120
         TabIndex        =   74
         Top             =   1560
         Width           =   2175
      End
      Begin VB.CommandButton Command3 
         Caption         =   "MultiUser Focus Move Next"
         Height          =   375
         Left            =   2400
         TabIndex        =   73
         Top             =   1560
         Width           =   2175
      End
      Begin VB.CommandButton Command4 
         Caption         =   "MultiUser Get Current Order"
         Height          =   375
         Left            =   120
         TabIndex        =   72
         Top             =   2040
         Width           =   2175
      End
      Begin VB.Label lblMultiID 
         Caption         =   "Label11"
         Height          =   255
         Left            =   2280
         TabIndex        =   76
         Top             =   2040
         Width           =   615
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Attribute"
      Height          =   2175
      Left            =   5280
      TabIndex        =   49
      Top             =   4080
      Width           =   2895
      Begin VB.CommandButton cmdScrAttr 
         Caption         =   "Set Scr Attr"
         Height          =   375
         Left            =   1440
         TabIndex        =   58
         Top             =   1440
         Width           =   1335
      End
      Begin VB.CommandButton cmdOrderAttr 
         Caption         =   "Set Order Attr"
         Height          =   375
         Left            =   1440
         TabIndex        =   57
         Top             =   960
         Width           =   1335
      End
      Begin VB.CommandButton cmdFocusAttr 
         Caption         =   "Set Focus attr"
         Height          =   375
         Left            =   120
         TabIndex        =   56
         Top             =   960
         Width           =   1335
      End
      Begin VB.CommandButton cmdNormalAttr 
         Caption         =   "Set Normal Attr"
         Height          =   375
         Left            =   120
         TabIndex        =   55
         Top             =   1440
         Width           =   1335
      End
      Begin VB.ComboBox cmbFont 
         Height          =   315
         ItemData        =   "Main.frx":001B
         Left            =   1080
         List            =   "Main.frx":001D
         Style           =   2  'Dropdown List
         TabIndex        =   51
         Top             =   240
         Width           =   1215
      End
      Begin VB.ComboBox cmbBack 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "Back:"
         Height          =   255
         Left            =   600
         TabIndex        =   54
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Label8 
         Caption         =   "Font:"
         Height          =   255
         Left            =   720
         TabIndex        =   53
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label6 
         BackColor       =   &H00C0FFC0&
         Caption         =   "Attribute:"
         Height          =   255
         Left            =   120
         TabIndex        =   52
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Item Operation"
      Height          =   2175
      Left            =   8280
      TabIndex        =   35
      Top             =   4080
      Width           =   6855
      Begin VB.CommandButton cmdItemsCount 
         Caption         =   "Items Count"
         Height          =   615
         Left            =   2760
         TabIndex        =   177
         Top             =   1440
         Width           =   615
      End
      Begin VB.TextBox txtItemContent 
         Height          =   405
         Left            =   1440
         TabIndex        =   173
         Text            =   "0"
         Top             =   1680
         Width           =   375
      End
      Begin VB.CommandButton cmdItemContent 
         Caption         =   "GetItemContent"
         Height          =   375
         Left            =   120
         TabIndex        =   172
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox txtItemContentCount 
         Height          =   375
         Left            =   5640
         TabIndex        =   170
         Text            =   "0"
         Top             =   1680
         Width           =   375
      End
      Begin VB.CommandButton cmdItemContentCount 
         Caption         =   "GetItemContentCount"
         Height          =   375
         Left            =   3600
         TabIndex        =   169
         Top             =   1680
         Width           =   2055
      End
      Begin VB.CommandButton cmdInsItemNote 
         Caption         =   "Insert Item Note"
         Height          =   375
         Left            =   5160
         TabIndex        =   155
         Top             =   600
         Width           =   1335
      End
      Begin VB.CommandButton cmdAddItemNote 
         Caption         =   "Add Item Note"
         Height          =   375
         Left            =   3960
         TabIndex        =   154
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtModifier 
         Height          =   285
         Left            =   6120
         TabIndex        =   152
         Text            =   "0"
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton cmdInsertModifier 
         Caption         =   "Insert Modifier"
         Height          =   375
         Left            =   5280
         TabIndex        =   151
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdDelModifier 
         Caption         =   "Del Modifier"
         Height          =   375
         Left            =   4320
         TabIndex        =   150
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton cmdChangeModAttr 
         Caption         =   "Change mod Attr"
         Height          =   375
         Left            =   3720
         TabIndex        =   149
         Top             =   1320
         Width           =   1335
      End
      Begin VB.CommandButton cmdChangeModName 
         Caption         =   "Change mod Name"
         Height          =   375
         Left            =   5040
         TabIndex        =   148
         Top             =   1320
         Width           =   1695
      End
      Begin VB.ComboBox cmbAdd 
         Height          =   315
         ItemData        =   "Main.frx":001F
         Left            =   2520
         List            =   "Main.frx":0029
         TabIndex        =   95
         Text            =   "Combo1"
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdAddModifer 
         Caption         =   "AddModifer"
         Height          =   375
         Left            =   1320
         TabIndex        =   89
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdItem 
         Caption         =   "Add Order Item"
         Height          =   375
         Left            =   120
         TabIndex        =   88
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton Command17 
         Caption         =   "GetItem"
         Height          =   375
         Left            =   120
         TabIndex        =   67
         Top             =   1320
         Width           =   1215
      End
      Begin VB.TextBox txtItem 
         Height          =   405
         Left            =   1320
         TabIndex        =   66
         Text            =   "0"
         Top             =   1320
         Width           =   495
      End
      Begin VB.CommandButton Command12 
         Caption         =   "Marked Item"
         Height          =   375
         Left            =   1320
         TabIndex        =   43
         Top             =   960
         Width           =   1095
      End
      Begin VB.CommandButton Command13 
         Caption         =   "Unmarked Item"
         Height          =   375
         Left            =   120
         TabIndex        =   42
         Top             =   960
         Width           =   1215
      End
      Begin VB.TextBox txtItemIndex 
         Height          =   375
         Left            =   4200
         TabIndex        =   41
         Text            =   "0"
         Top             =   240
         Width           =   495
      End
      Begin VB.CommandButton cmdModifyQty 
         Caption         =   "ModifyQty"
         Height          =   375
         Left            =   2520
         TabIndex        =   40
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdModifyName 
         Caption         =   "Modify Name"
         Height          =   375
         Left            =   1320
         TabIndex        =   39
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdModifyAttr 
         Caption         =   "Modify Attribute"
         Height          =   375
         Left            =   120
         TabIndex        =   38
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdInsertItem 
         Caption         =   "Insert Item"
         Height          =   375
         Left            =   3360
         TabIndex        =   37
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton cmdDeleteItem 
         Caption         =   "Delete Item"
         Height          =   375
         Left            =   2400
         TabIndex        =   36
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblItemsCount 
         Caption         =   "Label10"
         Height          =   375
         Left            =   3360
         TabIndex        =   178
         Top             =   1560
         Width           =   255
      End
      Begin VB.Label lblItemContent 
         AutoSize        =   -1  'True
         Caption         =   "   "
         Height          =   195
         Left            =   1920
         TabIndex        =   174
         Top             =   1800
         Width           =   135
      End
      Begin VB.Label lblItemContentCount 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   195
         Left            =   6000
         TabIndex        =   171
         Top             =   1680
         Width           =   90
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Modifier Index:"
         Height          =   195
         Left            =   4920
         TabIndex        =   153
         Top             =   360
         Width           =   1035
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Item Index:"
         Height          =   195
         Left            =   3240
         TabIndex        =   92
         Top             =   240
         Width           =   780
      End
      Begin VB.Label lblItem 
         AutoSize        =   -1  'True
         Caption         =   "    "
         Height          =   195
         Left            =   1920
         TabIndex        =   68
         Top             =   1440
         Width           =   180
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Order Operation"
      Height          =   3495
      Left            =   8280
      TabIndex        =   23
      Top             =   480
      Width           =   6855
      Begin VB.CommandButton btn_delete_all 
         Caption         =   "Delete All Items "
         Height          =   375
         Left            =   3960
         TabIndex        =   179
         Top             =   1200
         Width           =   1815
      End
      Begin VB.CheckBox chkFooter 
         Caption         =   "Enable Footer"
         Height          =   255
         Left            =   2400
         TabIndex        =   161
         Top             =   2640
         Width           =   1575
      End
      Begin VB.TextBox txtPanel 
         Height          =   405
         Left            =   4320
         TabIndex        =   158
         Text            =   "-1"
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox txtFooter 
         Height          =   405
         Left            =   5280
         TabIndex        =   157
         Text            =   "Hurry up me"
         Top             =   3000
         Width           =   1455
      End
      Begin VB.CommandButton cmdAddFooter 
         Caption         =   "Add Footer"
         Height          =   375
         Left            =   2640
         TabIndex        =   156
         Top             =   3000
         Width           =   1215
      End
      Begin VB.CommandButton cmdRoom 
         Caption         =   "How Many space for next order"
         Enabled         =   0   'False
         Height          =   495
         Left            =   120
         TabIndex        =   69
         Top             =   2880
         Width           =   1815
      End
      Begin VB.CommandButton Command16 
         Caption         =   "GetOrdersCount"
         Height          =   375
         Left            =   120
         TabIndex        =   64
         Top             =   2520
         Width           =   1815
      End
      Begin VB.CommandButton Command15 
         Caption         =   "GetOrderID"
         Height          =   375
         Left            =   3960
         TabIndex        =   62
         Top             =   2520
         Width           =   1815
      End
      Begin VB.TextBox txtIndex 
         Height          =   285
         Left            =   5760
         TabIndex        =   61
         Text            =   "0"
         Top             =   2520
         Width           =   495
      End
      Begin VB.CommandButton Command9 
         Caption         =   "Transfer Order To LS"
         Height          =   375
         Left            =   3960
         TabIndex        =   60
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox txtLs 
         Height          =   375
         Left            =   5760
         TabIndex        =   59
         Text            =   "2"
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdOrderTitle 
         Caption         =   "Add Order Title"
         Height          =   375
         Left            =   3960
         TabIndex        =   48
         Top             =   840
         Width           =   1815
      End
      Begin VB.TextBox txtOrderTitle 
         Height          =   375
         Left            =   5760
         TabIndex        =   47
         Text            =   "PAIDED"
         Top             =   840
         Width           =   855
      End
      Begin VB.CommandButton cmdTime 
         Caption         =   "Get Order Wait Time"
         Height          =   375
         Left            =   120
         TabIndex        =   44
         Top             =   2160
         Width           =   1815
      End
      Begin VB.CommandButton cmdDelOrder 
         Caption         =   "Delete Order"
         Height          =   375
         Left            =   2040
         TabIndex        =   34
         Top             =   1680
         Width           =   1815
      End
      Begin VB.CommandButton cmdDelCur 
         Caption         =   "Delete Current Order"
         Height          =   375
         Left            =   120
         TabIndex        =   33
         Top             =   1680
         Width           =   1815
      End
      Begin VB.CommandButton cmdRestore 
         Caption         =   "Restore Deleted Order"
         Height          =   375
         Left            =   3960
         TabIndex        =   32
         Top             =   1680
         Width           =   1815
      End
      Begin VB.CommandButton cmdFocusFirst 
         Caption         =   "Set Focus To First"
         Height          =   375
         Left            =   120
         TabIndex        =   30
         Top             =   1200
         Width           =   1815
      End
      Begin VB.CommandButton cmdFocusOrder 
         Caption         =   "Set Focus to Order"
         Height          =   375
         Left            =   120
         TabIndex        =   29
         Top             =   840
         Width           =   1815
      End
      Begin VB.CommandButton cmdMovePrev 
         Caption         =   "Focus Move Prev"
         Height          =   375
         Left            =   2040
         TabIndex        =   28
         Top             =   1200
         Width           =   1815
      End
      Begin VB.CommandButton cmdMoveNext 
         Caption         =   "Focus Move Next"
         Height          =   375
         Left            =   2040
         TabIndex        =   27
         Top             =   840
         Width           =   1815
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Get Current Order ID"
         Height          =   375
         Left            =   3960
         TabIndex        =   26
         Top             =   2160
         Width           =   1815
      End
      Begin VB.CommandButton cmdNew 
         Caption         =   "Add New order"
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdRush 
         Caption         =   "AddRushOrder"
         Height          =   375
         Left            =   2040
         TabIndex        =   24
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Footer:"
         Height          =   195
         Left            =   4800
         TabIndex        =   160
         Top             =   3120
         Width           =   495
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "Panel:"
         Height          =   195
         Left            =   3840
         TabIndex        =   159
         Top             =   3000
         Width           =   450
      End
      Begin VB.Label lblRoom 
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   70
         Top             =   2880
         Width           =   375
      End
      Begin VB.Label lblCount 
         Caption         =   "10"
         Height          =   255
         Left            =   1920
         TabIndex        =   65
         Top             =   2520
         Width           =   495
      End
      Begin VB.Label lblID 
         Caption         =   "1111"
         Height          =   375
         Left            =   6360
         TabIndex        =   63
         Top             =   2520
         Width           =   375
      End
      Begin VB.Label lblTime 
         Caption         =   "0"
         Height          =   255
         Left            =   1920
         TabIndex        =   46
         Top             =   2160
         Width           =   615
      End
      Begin VB.Label Label13 
         Caption         =   "sec"
         Height          =   255
         Left            =   2520
         TabIndex        =   45
         Top             =   2160
         Width           =   375
      End
      Begin VB.Label lblCurID 
         Caption         =   "Label10"
         Height          =   255
         Left            =   5760
         TabIndex        =   31
         Top             =   2160
         Width           =   495
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Item"
      Height          =   2175
      Left            =   5280
      TabIndex        =   11
      Top             =   1800
      Width           =   2895
      Begin VB.CommandButton cmdNewOrderID 
         Caption         =   "New"
         Height          =   255
         Left            =   2280
         TabIndex        =   91
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox txtId 
         Height          =   285
         Left            =   840
         TabIndex        =   16
         Text            =   "1001"
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox txtQty 
         Height          =   285
         Left            =   840
         TabIndex        =   15
         Text            =   "2"
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox txtName 
         Height          =   285
         Left            =   840
         TabIndex        =   14
         Text            =   "Ice Cream"
         Top             =   960
         Width           =   1335
      End
      Begin VB.ComboBox cmbOrdFont 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1320
         Width           =   1215
      End
      Begin VB.ComboBox cmbOrdBack 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Order ID:"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Qty:"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   375
      End
      Begin VB.Label Label3 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Name:"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label4 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Attribute:"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Font:"
         Height          =   255
         Left            =   720
         TabIndex        =   18
         Top             =   1320
         Width           =   375
      End
      Begin VB.Label Label7 
         Caption         =   "Back:"
         Height          =   255
         Left            =   600
         TabIndex        =   17
         Top             =   1680
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Start"
      Height          =   1335
      Left            =   5160
      TabIndex        =   6
      Top             =   480
      Width           =   3015
      Begin VB.CommandButton cmdSquare 
         Caption         =   "Set Square"
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   720
         Width           =   1335
      End
      Begin VB.ComboBox cmbSquare 
         Height          =   315
         ItemData        =   "Main.frx":003A
         Left            =   1440
         List            =   "Main.frx":003C
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   720
         Width           =   615
      End
      Begin VB.CommandButton cmdRedraw 
         Caption         =   "Ks Draw Kitchen"
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton cmdClr 
         Caption         =   "Clear Screen"
         Height          =   375
         Left            =   1440
         TabIndex        =   7
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label lblNote 
         Caption         =   "Label32"
         Height          =   615
         Left            =   2040
         TabIndex        =   164
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.CommandButton Command14 
      Caption         =   "AddTesting Data"
      Height          =   375
      Left            =   12120
      TabIndex        =   5
      Top             =   120
      Width           =   1335
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   735
      Left            =   12120
      TabIndex        =   4
      Top             =   9360
      Width           =   2775
   End
   Begin VB.ComboBox cmbKS 
      Height          =   315
      ItemData        =   "Main.frx":003E
      Left            =   7680
      List            =   "Main.frx":0088
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   0
      Width           =   735
   End
   Begin VB.CommandButton cmdStop 
      Caption         =   "Stop"
      Height          =   375
      Left            =   6120
      TabIndex        =   1
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Run"
      Height          =   375
      Left            =   5160
      TabIndex        =   0
      Top             =   0
      Width           =   975
   End
   Begin VB.Label Label12 
      Caption         =   "KS:"
      Height          =   255
      Left            =   7200
      TabIndex        =   3
      Top             =   0
      Width           =   375
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim lastidnum As Integer

Private Function getattribute() As Integer
   getattribute = cmbOrdFont.ItemData(cmbOrdFont.ListIndex) + cmbOrdBack.ItemData(cmbOrdBack.ListIndex)
   
End Function
Private Function getuser() As Integer
    getuser = cmbUser.ListIndex

End Function
Private Function getks() As Integer
    getks = cmbKS.ItemData(cmbKS.ListIndex)
    
End Function
Private Function sendoneorder(orderid As String, address As Integer, seed As Integer) As Integer
        Dim j As Integer
        
        LS.KitchenAddNewOrder address, orderid
        For j = 1 To (Rnd(seed) * 20 Mod 20 + 1)
        
            LS.KitchenAddOrderItem address, orderid, j, txtName.Text, getattribute()
            If Rnd(i * 2) * 10 Mod 2 Then
                LS.KitchenAddModifier address, orderid, Rnd(i) * 10 Mod 2, txtName, cmbOrdFont.ItemData(cmbOrdFont.ListIndex) + cmbOrdBack.ItemData(cmbOrdBack.ListIndex)
            End If

        Next

End Function

Private Sub addColor(cmb As ComboBox, i As Integer)
With cmb
'============
.AddItem "Blue"

If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Blue
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Blue
End If
'=============
.AddItem "Green"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Green
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Green
End If

.AddItem "Red"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Red
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Red
End If

.AddItem "Intensity"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Intensity
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Intensity
End If

.AddItem "Black"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Black
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Black
End If

.AddItem "Purple"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Purple
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Purple
End If

.AddItem "Brown"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Brown
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Brown
End If

.AddItem "Aqua"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Aqua
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Aqua
End If

.AddItem "Grey"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Grey
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Grey
End If

.AddItem "Light Blue"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Light_Blue
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Light_Blue
End If

.AddItem "Pink"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Pink
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Pink
End If

.AddItem "Magenta"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Magenta
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Magenta
End If

.AddItem "Light Green"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Light_Green
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Litht_Green
End If

.AddItem "Cyan"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Cyan
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Cyan
End If

.AddItem "Yellow"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_Yellow
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_Yellow
End If

.AddItem "White"
If i = 0 Then
    .ItemData(.NewIndex) = KsConst.Ks_FG_White
Else
    .ItemData(.NewIndex) = KsConst.Ks_BG_White
End If

End With
End Sub

Private Sub btn_delete_all_Click()
    Dim result As Integer
    result = LS.KitchenDeleteAllOrderItems(getks(), txtId)
    MsgBox result & " items have been deleted!"
End Sub

Private Sub chkAuto_Click()
    If chkAuto.Value = 1 Then
        LS.KitchenMultiUserAutoArrange getks(), True
    Else
        LS.KitchenMultiUserAutoArrange getks(), False
    End If

End Sub

Private Sub chkFooter_Click()
    LS.KitchenEnablePanelFooterMsg getks(), chkFooter.Value
    
End Sub

Private Sub cmbKS_Change()
Dim s As String
Dim address As Integer
address = getks()
If (address = 0) Then
    s = LS.LS00IpAndPort
ElseIf (address = 1) Then
    s = LS.LS01IpAndPort
ElseIf address = 2 Then
    s = LS.LS02IpAndPort
ElseIf address = 3 Then
    s = LS.LS03IpAndPort
ElseIf address = 4 Then
    s = LS.LS04IpAndPort
ElseIf address = 5 Then
    s = LS.LS05IpAndPort
ElseIf address = 6 Then
    s = LS.LS06IpAndPort
ElseIf address = 7 Then
    s = LS.LS07IpAndPort
ElseIf address = 8 Then
    s = LS.LS08IpAndPort
ElseIf address = 9 Then
    s = LS.LS09IpAndPort
ElseIf address = 10 Then
    s = LS.LS10IpAndPort
ElseIf address = 11 Then
    s = LS.LS11IpAndPort
ElseIf address = 12 Then
    s = LS.LS12IpAndPort
ElseIf address = 13 Then
    s = LS.LS13IpAndPort
ElseIf address = 14 Then
    s = LS.LS14IpAndPort
ElseIf address = 15 Then
    s = LS.LS15IpAndPort
ElseIf address = 16 Then
    s = LS.LS16IpAndPort
ElseIf address = 17 Then
    s = LS.LS17IpAndPort

End If
txtIP = s


End Sub

Private Sub cmbSquare_Click()
    Dim n As Integer
    n = cmbSquare.ItemData(cmbSquare.ListIndex)
    If (n = 2) Then
        lblNote.Caption = "Single User, Row=1, Col=2"
    ElseIf (n = 4) Then
        lblNote.Caption = "Single User, Row=2, Col=2"
    ElseIf (n = 8) Then
        lblNote.Caption = "Single User, Row=2, Col=4"
    ElseIf (n = -4) Then
        lblNote.Caption = "Double User, Row=2, Col=4"
    End If
    
    
End Sub

Private Sub cmdAddFooter_Click()
LS.KitchenAddOrderFooterMsg getks(), txtId, CInt(txtPanel.Text), txtFooter.Text, getattribute()

End Sub

Private Sub cmdAddItemNote_Click()
LS.KitchenAddItemNote getks(), txtId, txtName, getattribute()

End Sub

Private Sub cmdAddModifer_Click()
'ret = LS.KitchenAddOrderItem(getks(), txtId, CInt(txtQty), txtName, cmbOrdFont.ItemData(cmbOrdFont.ListIndex) + cmbOrdBack.ItemData(cmbOrdBack.ListIndex))
Dim nadd As Boolean

If cmbAdd.ListIndex = 0 Then
    nadd = True
ElseIf cmbAdd.ListIndex = 1 Then
    nadd = False
Else
    nadd = 0
End If
  



LS.KitchenAddModifier getks(), txtId, nadd, txtName, cmbOrdFont.ItemData(cmbOrdFont.ListIndex) + cmbOrdBack.ItemData(cmbOrdBack.ListIndex)

End Sub

Private Sub cmdAddStation_Click()
LS.KmAddStation getks(), txtIP.Text, CInt(txtPort)

End Sub

Private Sub cmdBtmMsg_Click()
LS.KitchenSendBottomMessage getks(), txtBtmMsg.Text, cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdChangeModAttr_Click()
LS.KitchenModifyItemModifierAttr getks(), txtId, CInt(txtItemIndex), CInt(txtModifier), cmbFont.ItemData(cmbFont.ListIndex) + cmbOrdBack.ItemData(cmbOrdBack.ListIndex)
End Sub

Private Sub cmdChangeModName_Click()
LS.KitchenModifyItemModifierString getks(), txtId, CInt(txtItemIndex), CInt(txtModifier), txtName

End Sub

Private Sub cmdClr_Click()
LS.KitchenClearScreen getks()

End Sub

Private Sub cmdCreateSum_Click()
LS.KitchenCreateSumPanel getks(), CInt(txtSumx.Text), CInt(txtSumy.Text), CInt(txtSumw.Text), CInt(txtSumh.Text), getattribute(), txtSumCaption.Text

End Sub

Private Sub cmdDelCur_Click()
LS.KitchenDeleteCurrentOrder getks()

End Sub

Private Sub cmdDeleteItem_Click()
LS.KitchenDeleteOrderItem getks(), txtId, CInt(txtItemIndex)


End Sub

Private Sub cmdDelModifier_Click()
LS.KitchenDelModifier getks(), txtId, txtItemIndex, txtModifier

End Sub

Private Sub cmdDelOrder_Click()
LS.KitchenDeleteOrder getks(), txtId

End Sub

Private Sub cmdExit_Click()
End

End Sub

Private Sub cmdFlash_Click()
LS.KitchenSetFlash getks(), txtFlashID.Text, CInt(txtFlashItem.Text), CInt(txtFlashMod.Text), chkFlash.Value

End Sub

Private Sub cmdFocusAttr_Click()
LS.KitchenSetOrderFocusAttr getks(), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdFocusFirst_Click()
LS.KitchenSetFocusToFirst getks()

End Sub

Private Sub cmdFocusOrder_Click()
LS.KitchenSetFocusToOrder getks(), txtId

End Sub

Private Sub cmdFre_Click()
LS.KitchenSetFlashFrequency getks(), CInt(txtFlashInterval.Text), CInt(txtSHInterval.Text)


End Sub

Private Sub cmdHelp_Click()
Dim s As String
s = "Test Data Help (Function Key)" + Chr$(&HD)

s = s + "======================================" + Chr$(&HD)
s = s + "Single User:" + Chr$(&HD)
s = s + "[ Home ]    Set Focus to first order" + Chr$(&HD)
s = s + "[ -> ]      Move Focus to next order" + Chr$(&HD)
s = s + "[ <- ]      Move Focus to previous order" + Chr$(&HD)
s = s + "[ Del ]     Delete current order" + Chr$(&HD)
s = s + "[Insert]    Restore deleted order" + Chr$(&HD)
s = s + "[Arrow UP]  Redraw Screen" + Chr$(&HD)

s = s + "======================================" + Chr$(&HD)
s = s + "Multiple User Key:" + Chr$(&HD)
s = s + "--------------------------------------" + Chr$(&HD)
s = s + Chr$(&HD)
s = s + "   Top User:" + Chr$(&HD)
s = s + "[ -> ]      Move Focus to next order" + Chr$(&HD)
s = s + "[ <- ]      Move Focus to previous order" + Chr$(&HD)
s = s + "[ Del ]     Delete current order" + Chr$(&HD)
s = s + "[Insert]    Restore deleted order" + Chr$(&HD)
s = s + "[Arrow UP]  Redraw Screen" + Chr$(&HD)
s = s + Chr$(&HD)
s = s + " Bottom User:" + Chr$(&HD)

s = s + "[ n ]      Move Focus to next order" + Chr$(&HD)
s = s + "[ p ]      Move Focus to previous order" + Chr$(&HD)
s = s + "[ d ]     Delete current order" + Chr$(&HD)
s = s + "[ r ]    Restore deleted order" + Chr$(&HD)
s = s + "[Arrow UP]  Redraw Screen"

MsgBox s
End Sub

Private Sub cmdHidBtmMsg_Click()
LS.KitchenHideBottomMessage getks()

End Sub

Private Sub cmdInsertItem_Click()
LS.KitchenInsertOrderItem getks(), txtId, CInt(txtItemIndex), CInt(txtQty), txtName, getattribute()


End Sub

Private Sub cmdInsertModifier_Click()
Dim nadd As Boolean

If cmbAdd.ListIndex = 0 Then
    nadd = True
ElseIf cmbAdd.ListIndex = 1 Then
    nadd = False
Else
    nadd = 0
End If

LS.KitchenInsertModifier getks(), txtId, CInt(txtItemIndex), CInt(txtModifier.Text), nadd, txtName, getattribute()
'LS.KitchenInsertModifier getks(), txtId, CInt(txtItemIndex), CInt(txtModifier.Text), chkadd.Value, txtName, getattribute()

'ls.KitchenInsertModifier getks(),txtid,cint(txtitemindex),cint(txtmodifier.Text),
End Sub

Private Sub cmdInsItemNote_Click()
LS.KitchenInsertItemNote getks(), txtId, CInt(txtItemIndex.Text), CInt(txtModifier.Text), txtName.Text, getattribute()

End Sub

Private Sub cmdItem_Click()
Dim ret As Boolean

ret = LS.KitchenAddOrderItem(getks(), txtId, CInt(txtQty), txtName, cmbOrdFont.ItemData(cmbOrdFont.ListIndex) + cmbOrdBack.ItemData(cmbOrdBack.ListIndex))
If Not ret Then
    MsgBox "Can't add the order item, no room"
    
End If


End Sub

Private Sub cmdItemContent_Click()
Dim attr As Integer
Dim name As String

LS.KitchenGetItemContent getks(), txtId.Text, CInt(txtItem.Text), CInt(txtItemContent.Text), name, attr


lblItemContent.Caption = name + " " + "attr = " + CStr(attr)

End Sub

Private Sub cmdItemContentCount_Click()
Dim n As Integer

n = LS.KitchenGetItemContentCount(getks(), txtId, CInt(txtItemContentCount))
lblItemContentCount.Caption = CStr(n)

 


End Sub

Private Sub cmdItemsCount_Click()
Dim n As Integer

n = LS.KitchenGetOrderItemsCount(getks(), txtId)
lblItemsCount.Caption = n

 


End Sub

Private Sub cmdLeftTitle_Click()
LS.KitchenMultiUserSetLeftTitle getks(), txtMultiTitle


End Sub

Private Sub cmdModifyAttr_Click()
LS.KitchenModifyOrderItemAttr getks(), txtId, CInt(txtItemIndex.Text), getattribute()



End Sub

Private Sub cmdModifyName_Click()
LS.KitchenModifyOrderItemString getks(), txtId, CInt(txtItemIndex.Text), txtName



End Sub

Private Sub cmdModifyQty_Click()
LS.KitchenModifyOrderItemQty getks(), txtId, CInt(txtItemIndex.Text), CInt(txtQty)


End Sub

Private Sub cmdMoveNext_Click()
LS.KitchenSetFocusMoveNext getks()


End Sub

Private Sub cmdMovePrev_Click()
LS.KitchenSetFocusMovePrev getks()

End Sub

Private Sub cmdMultiAddOrder_Click()

Dim ret As Boolean

ret = LS.KitchenMultiUserAddNewOrder(getks(), getuser(), txtId)
If Not ret Then
    MsgBox " can't add new order"
    
End If

End Sub

Private Sub cmdNew_Click()
Dim ret As Boolean

ret = LS.KitchenAddNewOrder(getks(), txtId)
If Not ret Then
    MsgBox " can't add new order"
    
End If

End Sub

Private Sub cmdNewOrderID_Click()
Dim nId As Integer
nId = CInt(txtId.Text)
nId = nId + 1
txtId.Text = CStr(nId)


End Sub

Private Sub cmdNormalAttr_Click()
LS.KitchenSetOrderNormalAttr getks(), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdOrderAttr_Click()
LS.KitchenSetOrderBackAttribute getks(), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdOrderTitle_Click()
LS.KitchenAddOrderTitle getks(), txtId, txtOrderTitle


End Sub

Private Sub cmdPanel_Click()
LS.KitchenSetDisplayPanelNumber getks(), chkShow.Value, getattribute()

End Sub

Private Sub cmdPingTimer_Click()
LS.KitchenSetPingTimerInterval CLng(txtPing.Text)


End Sub

Private Sub cmdRedraw_Click()
LS.KitchenDrawScreen getks()

End Sub

Private Sub cmdRemoveIp_Click()
LS.KmRemoveStation getks()

End Sub

Private Sub cmdRestore_Click()
LS.KitchenRestoreDelOrder getks()

End Sub

Private Sub cmdRightTitle_Click()
LS.KitchenMultiUserSetRightTitle getks(), txtMultiTitle

End Sub

Private Sub cmdRoom_Click()
lblRoom = LS.KitchenGetReservedSpace(getks())

End Sub

Private Sub cmdRush_Click()
Dim ret As Boolean

ret = LS.KitchenAddRushOrder(getks(), txtId)
If Not ret Then
    MsgBox " can't add new order"
    
End If

End Sub

Private Sub cmdScrAttr_Click()
LS.KitchenSetBackScreenAttribute getks(), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)

End Sub

Private Sub cmdSetAuto_Click()
    If chkAuto.Value = 1 Then
        LS.KitchenMultiUserAutoArrange getks(), True
    Else
        LS.KitchenMultiUserAutoArrange getks(), False
    End If
    
    
End Sub

Private Sub cmdScrChar_Click()
LS.KitchenSetBackgroundChar getks(), CInt(txtH.Text), CInt(txtV), CInt(txtC)

End Sub

Private Sub cmdSetIP_Click()
'Dim s As String
'Dim address As Integer
'address = getks()
's = txtIp
'
'If (address = 0) Then
'    LS.LS00IpAndPort = s
'
'ElseIf (address = 1) Then
'    LS.LS01IpAndPort = s
'ElseIf address = 2 Then
'     LS.LS02IpAndPort = s
'ElseIf address = 3 Then
'     LS.LS03IpAndPort = s
'ElseIf address = 4 Then
'     LS.LS04IpAndPort = s
'ElseIf address = 5 Then
'     LS.LS05IpAndPort = s
'ElseIf address = 6 Then
'     LS.LS06IpAndPort = s
'ElseIf address = 7 Then
'     LS.LS07IpAndPort = s
'ElseIf address = 8 Then
'     LS.LS08IpAndPort = s
'ElseIf address = 9 Then
'     LS.LS09IpAndPort = s
'ElseIf address = 10 Then
'     LS.LS10IpAndPort = s
'ElseIf address = 11 Then
'     LS.LS11IpAndPort = s
'ElseIf address = 12 Then
'     LS.LS12IpAndPort = s
'ElseIf address = 13 Then
'     LS.LS13IpAndPort = s
'ElseIf address = 14 Then
'     LS.LS14IpAndPort = s
'ElseIf address = 15 Then
'     LS.LS15IpAndPort = s
'
'End If
'txtIp = s


End Sub

Private Sub cmdShowSum_Click()
LS.KitchenShowSumPanel getks(), True

End Sub

Private Sub cmdSquare_Click()
LS.KitchenSetSquareNumber getks(), cmbSquare.ItemData(cmbSquare.ListIndex)

End Sub

Private Sub cmdStart_Click()


LS.KmRun

'LS.KitchenDrawScreen getks()

End Sub

Private Sub cmdStop_Click()
LS.KmClose

End Sub

Private Sub cmdTest_Click()
LS.KitchenTest

End Sub

Private Sub cmdTest2_Click()
LS.KitchenTest2

End Sub

Private Sub cmdSumAddItem_Click()
LS.KitchenAddSumPanelItem getks(), txtSumItem.Text, getattribute()

End Sub

Private Sub cmdSumClear_Click()
LS.KitchenClearSumPanel getks()

End Sub

Private Sub cmdSumHide_Click()
LS.KitchenShowSumPanel getks(), False

End Sub

Private Sub cmdSumScrollDown_Click()
LS.KitchenScrollSumPanelDown getks(), CInt(txtScrollLines.Text)


End Sub

Private Sub cmdSumScrollUp_Click()
LS.KitchenScrollSumPanelUp getks(), CInt(txtScrollLines.Text)


End Sub

Private Sub cmdSumSetTitle_Click()
LS.KitchenSetSumPanelTitle getks(), txtSumTitle.Text, getattribute()

End Sub

Private Sub cmdTime_Click()
lblTime.Caption = LS.KitchenGetOrderWaitSeconds(getks(), txtId.Text)


End Sub

Private Sub cmdUpdate_Click()
LS.KitchenUpdateWaitTime getks()

End Sub

Private Sub Command1_Click()
LS.KitchenMultiUserRestoreOrder getks(), getuser()

End Sub

Private Sub Command10_Click()
LS.KitchenSendMessage getks(), txtInfo.Text, cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
End Sub

Private Sub Command11_Click()
LS.KitchenHideMessage getks()

End Sub

Private Sub Command12_Click()
LS.KitchenMarkedOrderItem getks(), txtId.Text, CInt(txtItemIndex.Text), True

End Sub

Private Sub Command13_Click()
LS.KitchenMarkedOrderItem getks(), txtId.Text, CInt(txtItemIndex.Text), False

End Sub

Private Sub Command14_Click()
Dim id As String
id = "100"
Dim orderid As String

Dim i As Integer
Dim j As Integer


Dim count As Integer
count = CInt(txtOrders.Text)

If LS.KitchenCheckSingleUserMode(getks()) Or (Not LS.KitchenCheckSingleUserMode(getks()) And LS.KitchenMultiUserGetAutoArrange(getks())) Then

        
    
    For i = 1 To count
        orderid = id + CStr(i)
        sendoneorder orderid, getks(), i
        
    Next
        
ElseIf (Not LS.KitchenCheckSingleUserMode(getks())) Then
    
    For i = 1 To count
        orderid = id + CStr(i)
        sendoneorder orderid, getks(), i
        
    Next

End If

End Sub

Private Sub Command15_Click()
lblID.Caption = LS.KitchenGetOrderID(getks(), CInt(txtIndex.Text))

End Sub

Private Sub Command16_Click()
lblCount.Caption = CStr(LS.KitchenGetOrdersCount(getks()))

End Sub

Private Sub Command17_Click()
Dim qty As Integer
Dim attr As Integer
Dim name As String

LS.KitchenGetOrderItem getks(), txtId.Text, CInt(txtItem.Text), qty, name, attr

lblItem.Caption = CStr(qty) + " " + name + " " + "attr = " + CStr(attr)

End Sub

Private Sub Command2_Click()
LS.KitchenMultiUserSetFocusToPrev getks(), getuser()

End Sub

Private Sub Command3_Click()
LS.KitchenMultiUserSetFocusToNext getks(), getuser()

End Sub

Private Sub Command4_Click()
lblMultiID.Caption = LS.KitchenMultiUserGetCurrentOrder(getks(), getuser())


End Sub

Private Sub Command5_Click()
LS.KitchenStartUpdateTimeTimer getks()

End Sub

Private Sub Command6_Click()
LS.KitchenStopUpdateTimeTimer getks()

End Sub

Private Sub Command7_Click()
LS.KitchenSetUpdateTimeInterval getks(), CInt(txtTime.Text)

End Sub

Private Sub Command8_Click()
lblCurID.Caption = LS.KitchenGetCurrentOrderID(getks())


End Sub

Private Sub Command9_Click()
LS.KitchenSendOrderToKs getks(), txtId, CInt(txtLs.Text)

End Sub

Private Sub Form_Load()

addColor cmbFont, 0
addColor cmbBack, 1
addColor cmbOrdFont, 0
addColor cmbOrdBack, 1

cmbFont.ListIndex = 15
cmbBack.ListIndex = 0
cmbOrdFont.ListIndex = 15
cmbOrdBack.ListIndex = 0

cmbKS.ListIndex = 1

cmbSquare.Clear


cmbSquare.AddItem "2"
cmbSquare.ItemData(0) = 2
cmbSquare.AddItem "4"
cmbSquare.ItemData(1) = 4
cmbSquare.AddItem "8"
cmbSquare.ItemData(2) = 8
cmbSquare.AddItem "-4"
cmbSquare.ItemData(3) = -4
cmbSquare.AddItem "-8"
cmbSquare.ItemData(4) = -8


cmbSquare.ListIndex = 2

cmbUser.ListIndex = 0
cmbAdd.ListIndex = 0
lastidnum = 0

LS.KmAddStation 1, "192.168.0.100", 8888


End Sub

Private Sub Form_Unload(Cancel As Integer)
LS.KmClose

End Sub

Private Sub LS_OnKsReturnKey(ByVal address As Integer, ByVal AscKeyCode As Integer)
'redraw screen
If AscKeyCode = KS_KEY_ARROW_UP Then
    LS.KitchenDrawScreen address
    
End If

If LS.KitchenCheckSingleUserMode(address) Then 'single user

    'set focus to first order
    If AscKeyCode = KS_KEY_HOME Then
        LS.KitchenSetFocusToFirst address
    End If
    ' move focus to previous order
    If AscKeyCode = KS_KEY_ARROW_LEFT Then
        LS.KitchenSetFocusMovePrev address
        
    End If
    'move focus to next order
    If AscKeyCode = KS_KEY_ARROW_RIGHT Then
        LS.KitchenSetFocusMoveNext address
    End If
    'delete current order
    If AscKeyCode = KS_KEY_DEL Then
        LS.KitchenDeleteCurrentOrder address
        sendoneorder CStr(lastidnum), 2, lastidnum
        lastidnum = lastidnum + 1
        
        
    End If
    ' restore deleted order
    If AscKeyCode = KS_KEY_INSERT Then
        LS.KitchenRestoreDelOrder address
    End If
    
Else 'multiple user

    '==========LEFT user
    ' move focus to previous order
    If AscKeyCode = KS_KEY_ARROW_LEFT Then
        LS.KitchenMultiUserSetFocusToPrev address, 0
        
    End If
    'move focus to next order
    If AscKeyCode = KS_KEY_ARROW_RIGHT Then
        LS.KitchenMultiUserSetFocusToNext address, 0
    End If
    'delete current order
    If AscKeyCode = KS_KEY_DEL Then
        LS.KitchenDeleteOrder address, LS.KitchenMultiUserGetCurrentOrder(address, 0)
        
        
    End If
    ' restore deleted order
    If AscKeyCode = KS_KEY_INSERT Then
        LS.KitchenMultiUserRestoreOrder address, 0
        
    End If
    '===============RIGHT user
    'MsgBox (Str(AscKeyCode))
    
    If AscKeyCode = KS_KEY_p Then
        LS.KitchenMultiUserSetFocusToPrev address, 1
        
    End If
    'move focus to next order
    If AscKeyCode = KS_KEY_n Then
        LS.KitchenMultiUserSetFocusToNext address, 1
    End If

    If AscKeyCode = KS_KEY_d Then
        LS.KitchenDeleteOrder address, LS.KitchenMultiUserGetCurrentOrder(address, 1)
    End If
    
    If AscKeyCode = KS_KEY_r Then
        LS.KitchenMultiUserRestoreOrder address, 1
    End If
    
    
    
End If



End Sub

