Attribute VB_Name = "KsConst"

'=====================================
'screen mode
Global Const Mode_40 = 1
Global Const Mode_80 = 0

'=====================================
'keyboard mode
'
Global Const KeyMode_String = &H17
Global Const KeyMode_Scan = &H18
Global Const KeyMode_ASCII = &H19

'=====================================
'rs232 port baud rate

Global Const Baud_2400 = 0
Global Const Baud_4800 = 1
Global Const Baud_9600 = 2
Global Const Baud_19200 = 3

'=====================================
'ks font color

Global Const Ks_FG_Blue = &H1                                   'text color contains blue.
Global Const Ks_FG_Green = &H2                                  'text color contains green.
Global Const Ks_FG_Red = &H4                                    'text color contains red.
Global Const Ks_FG_Intensity = &H8                              'text color is intensified.

    'Derived colors�

Global Const Ks_FG_Black = &H0                                  'text color is black.
Global Const Ks_FG_Purple = Ks_FG_Red + Ks_FG_Blue

Global Const Ks_FG_Brown = Ks_FG_Green + Ks_FG_Red
Global Const Ks_FG_Aqua = Ks_FG_Green + Ks_FG_Blue

Global Const Ks_FG_Grey = Ks_FG_Green + Ks_FG_Red + Ks_FG_Blue
Global Const Ks_FG_Light_Blue = Ks_FG_Intensity + Ks_FG_Blue

Global Const Ks_FG_Pink = Ks_FG_Intensity + Ks_FG_Red
Global Const Ks_FG_Magenta = Ks_FG_Intensity + Ks_FG_Red + Ks_FG_Blue

Global Const Ks_FG_Light_Green = Ks_FG_Intensity + Ks_FG_Green
Global Const Ks_FG_Cyan = Ks_FG_Intensity + Ks_FG_Green + Ks_FG_Blue

Global Const Ks_FG_Yellow = Ks_FG_Intensity + Ks_FG_Green + Ks_FG_Red
Global Const Ks_FG_White = Ks_FG_Intensity + Ks_FG_Green + Ks_FG_Red + Ks_FG_Blue
'===========================================
'ks back color

     ' Primary background colors...
Global Const Ks_BG_Blue = &H10                              ' background color contains blue.
Global Const Ks_BG_Green = &H20                             ' background color contains green.
Global Const Ks_BG_Red = &H40                               ' background color contains red.
Global Const Ks_BG_Intensity = &H80                         ' background color is intensified.

     ' Derived colors�

Global Const Ks_BG_Black = &H0                              ' background color is black.
Global Const Ks_BG_Purple = Ks_BG_Red + Ks_BG_Blue
Global Const Ks_BG_Brown = Ks_BG_Green + Ks_BG_Red

Global Const Ks_BG_Aqua = Ks_BG_Green + Ks_BG_Blue
Global Const Ks_BG_Grey = Ks_BG_Green + Ks_BG_Red + Ks_BG_Blue

Global Const Ks_BG_Light_Blue = Ks_BG_Intensity + Ks_BG_Blue
Global Const Ks_BG_Pink = Ks_BG_Intensity + Ks_BG_Red

Global Const Ks_BG_Magenta = Ks_BG_Intensity + Ks_BG_Red + Ks_BG_Blue
Global Const Ks_BG_Litht_Green = Ks_BG_Intensity + Ks_BG_Green

Global Const Ks_BG_Cyan = Ks_BG_Intensity + Ks_BG_Green + Ks_BG_Blue
Global Const Ks_BG_Yellow = Ks_BG_Intensity + Ks_BG_Green + Ks_BG_Red

Global Const Ks_BG_White = Ks_BG_Intensity + Ks_BG_Green + Ks_BG_Red + Ks_BG_Blue
'===============================================
' Color combinations�

Global Const Ks_Color_CLS = Ks_BG_Blue + Ks_FG_White

'key definition


Global Const KS_KEY_ARROW_LEFT = &HAE
Global Const KS_KEY_ARROW_RIGHT = &HAD
Global Const KS_KEY_ARROW_UP = &HAB
Global Const KS_KEY_DEL = &HB0
Global Const KS_KEY_INSERT = &HAF

Global Const KS_KEY_HOME = &HB1
Global Const KS_KEY_END = &HB2

Global Const KS_KEY_ENTER = &HD
Global Const KS_KEY_p = &H70
Global Const KS_KEY_d = &H64

Global Const KS_KEY_n = &H6E
Global Const KS_KEY_r = &H72

