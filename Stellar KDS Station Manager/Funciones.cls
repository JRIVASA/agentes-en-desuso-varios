VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long
Public Function sgetini(SIniFile As String, SSection As String, SKey _
    As String, SDefault As String) As String
    Dim STemp As String * 256
    Dim NLength As Integer
    
    STemp = Space$(256)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, 255, SIniFile)
    sgetini = Left$(STemp, NLength)
End Function

Public Sub ConectarBDD()

    On Error GoTo Errores
    Dim Setup As String
    
    Setup = App.Path & "\setup.ini"

    G_Server_Local = sgetini(Setup, "Server Local", "Servidor Local", "?")
    
    If G_Server_Local = "?" Then
        Call MsgBox("El servidor no se encuentra definido.", vbExclamation, "Error")
        End
    End If
    
    G_BDServer_Local = sgetini(Setup, "Server Local", "Base de Datos Local", "?")
    
    If G_BDServer = "?" Then
        Call MsgBox("La Base de Datos no se encuentra definida.", vbExclamation, "Error")
        End
    End If
    
    G_ServerLogin = sgetini(Setup, "Server Local", "Servidor Login", "SA")
    G_ServerPassword = sgetini(Setup, "Server Local", "Servidor Password", "")
    
'    G_Excedente = sgetini(Setup, "Configuracion", "Solo Excedente", "0")
'    G_Tope = sgetini(Setup, "Configuracion", "Fecha Tope", "")
'    'G_Gracia = sgetini(setup, "Configuracion", "Tolerancia Descanso", 0)
'    G_MostrarProgreso = sgetini(Setup, "Configuracion", "Mostrar Ventana", "0")
'    G_CargaDiaria = sgetini(Setup, "Configuracion", "Carga Diaria", 0)
'    G_Desde = sgetini(Setup, "Configuracion", "FECHA INICIO", "")
'    G_TiempoConsideraHoraExtra = sgetini(Setup, "Configuracion", "Tiempo a Considerar HoraExtra", "0")
'
'    G_FinJornadaDiurna = Format(sgetini(Setup, "Configuracion", _
'    "HoraFin_JornadaDiurna", "19:00:00"), "HH:mm:ss")
'    G_FinJornadaNocturna = Format(sgetini(Setup, "Configuracion", _
'    "HoraFin_JornadaNocturna", "05:00:00"), "HH:mm:ss")
'
'    G_HorasDiaDomingoT = Val(sgetini(Setup, "Configuracion", "Horas_DiaDomingoTrabajado", "6"))
'    G_HorasDiaFeriadoT = Val(sgetini(Setup, "Configuracion", "Horas_DiaFeriadoTrabajado", "6"))
'    G_HorasDiaLibreT = Val(sgetini(Setup, "Configuracion", "Horas_DiaLibreTrabajado", "6"))
'
'    G_MinutosSignificativos_HorasFaltantes = Val( _
'    sgetini(Setup, "Configuracion", "MinutosSignificativos_HorasFaltantes", "30"))
'    G_MinutosSignificativos_Medio_Dia = Val( _
'    sgetini(Setup, "Configuracion", "MinutosSignificativos_Medio_Dia", "480"))
'    G_MinutosSignificativos_Dia = Val( _
'    sgetini(Setup, "Configuracion", "MinutosSignificativos_Dia", "480"))

    
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, False)
    

    
    Exit Sub
    
Errores:

    Call MsgBox(Err.Description, vbCritical, "Error")
    
End Sub


Public Function Consecutivos(pConexion As Object, Campo As String, Incrementar As Boolean) As String
    Dim Correlativos As Object
    On Error GoTo Falla_Local
    Set Correlativos = CreateObject("recsun.cls_datos")
    
    Consecutivos = Correlativos.NO_CONSECUTIVO(ProgramId, pConexion, Campo, Incrementar)
    
    Set Correlativos = Nothing
    If Trim(Consecutivos) = "" Then GoTo Falla_Local
    Exit Function
Falla_Local:
    'Mensajes "Ocurri� un error con los consecutivos. Windows se reiniciar�", False
    'Call ReiniciarWindows(EWX_REBOOT)
End Function

Private Function ExisteCampoCorrelativo(pCn, pCampo) As Boolean
    Dim mRs As New ADODB.Recordset
    
    mRs.Open "Select * from ma_correlativos where cu_campo='" & pCampo & "' ", pCn, adOpenDynamic, adLockOptimistic, adCmdText
    ExisteCampoCorrelativo = Not mRs.EOF
    
End Function

Public Function IsValidIPAddress(ByVal strIPAddress As String) As Boolean
    On Error GoTo Handler
    Dim varAddress As Variant, n As Long, lCount As Long
    varAddress = Split(strIPAddress, ".", , vbTextCompare)
    '//
    If IsArray(varAddress) Then
        For n = LBound(varAddress) To UBound(varAddress)
            lCount = lCount + 1
            varAddress(n) = CByte(varAddress(n))
        Next
        '//
        IsValidIPAddress = (lCount = 4)
    End If
    '//
Handler:
End Function
