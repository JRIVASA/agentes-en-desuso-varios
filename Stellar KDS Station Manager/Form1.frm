VERSION 5.00
Object = "{785C818B-3B27-11D6-85F9-52544C194B3E}#1.0#0"; "LS6.ocx"
Begin VB.Form KDSManager 
   Appearance      =   0  'Flat
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "KDS Manager"
   ClientHeight    =   5370
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   8265
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   8265
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "Configuration"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1695
      Left            =   3960
      TabIndex        =   12
      Top             =   240
      Visible         =   0   'False
      Width           =   3855
      Begin VB.CommandButton Command3 
         Caption         =   "AddStation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   240
         TabIndex        =   16
         Top             =   840
         Width           =   1575
      End
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   675
         TabIndex        =   15
         Text            =   "10.10.1.90"
         Top             =   360
         Width           =   1695
      End
      Begin VB.TextBox txtStation 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3075
         TabIndex        =   14
         Text            =   "1"
         Top             =   360
         Width           =   495
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Remove"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   2040
         TabIndex        =   13
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "N�"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   2595
         TabIndex        =   18
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "IP"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   240
         TabIndex        =   17
         Top             =   360
         Width           =   165
      End
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "Attribute"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   3015
      Left            =   3960
      TabIndex        =   1
      Top             =   1920
      Visible         =   0   'False
      Width           =   3855
      Begin VB.CommandButton cmdDeliveryAttr 
         Appearance      =   0  'Flat
         Caption         =   "Set Deliver Attr"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2040
         TabIndex        =   11
         Top             =   2280
         Width           =   1575
      End
      Begin VB.CommandButton cmdItemAttr 
         Appearance      =   0  'Flat
         Caption         =   "Set Item Attr"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   10
         Top             =   2280
         Width           =   1575
      End
      Begin VB.ComboBox cmbBack 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2400
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   600
         Width           =   1215
      End
      Begin VB.ComboBox cmbFont 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Form1.frx":628A
         Left            =   600
         List            =   "Form1.frx":628C
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdNormalAttr 
         Appearance      =   0  'Flat
         Caption         =   "Set Normal Attr"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   5
         Top             =   1680
         Width           =   1575
      End
      Begin VB.CommandButton cmdFocusAttr 
         Appearance      =   0  'Flat
         Caption         =   "Set Focus Attr"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   1575
      End
      Begin VB.CommandButton cmdOrderAttr 
         Appearance      =   0  'Flat
         Caption         =   "Set Order Attr"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2040
         TabIndex        =   3
         Top             =   1080
         Width           =   1575
      End
      Begin VB.CommandButton cmdScrAttr 
         Appearance      =   0  'Flat
         Caption         =   "Set Scr Attr"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2040
         TabIndex        =   2
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "Font:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   495
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         Caption         =   "Back:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1920
         TabIndex        =   8
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Timer Timer1 
      Left            =   1680
      Top             =   120
   End
   Begin LS6Lib.LS6 LS 
      Height          =   4575
      Left            =   405
      TabIndex        =   0
      Top             =   360
      Width           =   3375
      _Version        =   65536
      _ExtentX        =   5953
      _ExtentY        =   8070
      _StockProps     =   0
   End
End
Attribute VB_Name = "KDSManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim isshowing As Boolean
Dim TotalOrders As Integer
Dim attr As Integer
Dim IDLS() As Integer
Dim TotalLS As Integer
Dim sql As String
Dim sql2 As String
Dim sql3 As String
Dim SumTotal As Integer
Dim SumNombre() As String
Dim SumCantidad() As Double
Dim SumEspaciado() As String
Dim Begin As Boolean
Dim TotalRecall() As String
Dim R As Integer
Dim TieneDepartamento As Boolean

Private Sub cmdDeliveryAttr_Click()
    
    For J = 1 To TotalLS
        Call SetColor("DeliveryColor", IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
    Next J
    
End Sub

Private Sub cmdFocusAttr_Click()

    For J = 1 To TotalLS
        Call SetColor("FocusColor", IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
        LS.KitchenSetOrderFocusAttr IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
    Next J

End Sub

Private Sub cmdItemAttr_Click()
    For J = 1 To TotalLS
        Call SetColor("ItemColor", IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
    Next J
End Sub

Private Sub cmdNormalAttr_Click()

    For J = 1 To TotalLS
        Call SetColor("OrderColor", IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
        LS.KitchenSetOrderNormalAttr IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
    Next J

End Sub

Private Sub cmdOrderAttr_Click()

    For J = 1 To TotalLS
        Call SetColor("BackColor", IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
        LS.KitchenSetOrderBackAttribute IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
    Next J

End Sub

Private Sub cmdScrAttr_Click()

    For J = 1 To TotalLS
        Call SetColor("ScreenColor", IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex))
        LS.KitchenSetBackScreenAttribute IDLS(J), cmbFont.ItemData(cmbFont.ListIndex) + cmbBack.ItemData(cmbBack.ListIndex)
    Next J

End Sub

Private Sub Command1_Click()
    
    LoadKDS
    LS.KmRun
    
'    LS.KitchenSetOrderNormalAttr IDLS(j), 47
'    LS.KitchenSetOrderFocusAttr IDLS(j), 31  '138
    
End Sub

Private Sub Command2_Click()
    
    LS.KmClose
    
End Sub

Private Sub Command3_Click()

On Error GoTo AddError
    
    Dim ret As Boolean
    Dim IP As String
    Dim rs As New ADODB.Recordset
    Dim rs2 As New ADODB.Recordset
    Dim Funciones As New Funciones
    
    IP = Trim(Text1.Text)
    
    sql = "insert into MA_KDS (KDS_ID,IP) values('" & txtStation.Text & "','" & IP & "')"
    sql2 = "select count(*) as IP from MA_KDS where IP = '" & Text1.Text & "'"
    sql3 = "select count(*) as ID from MA_KDS where KDS_ID = '" & txtStation.Text & "'"
    
    If (Trim(txtStation.Text) = "") Then Exit Sub
    
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, True)
    
    rs.Open sql2, Conexion, adOpenDynamic, adLockBatchOptimistic
    rs2.Open sql3, Conexion, adOpenDynamic, adLockBatchOptimistic

    If rs!IP > 0 Or rs2!ID > 0 Then
    
        MsgBox "No se puede a�adir esta estacion, verifique que el numero de estacion o IP no esten repetidos"
        If rs.State = 1 Then rs.Close
        If rs2.State = 1 Then rs2.Close

        Exit Sub
    Else
        
        If Funciones.IsValidIPAddress(IP) Then
            ret = LS.KmAddStation(CInt(txtStation.Text), IP, 8888)
            Conexion.Execute (sql)
        Else
            MsgBox "Formato de IP invalido, verifique la IP introducida"
        End If
        
    End If

    If rs.State = 1 Then rs.Close
    If rs2.State = 1 Then rs2.Close
    
   Exit Sub
   
AddError: Debug.Print Err.Description
   
End Sub

Private Sub Command4_Click()
On Error GoTo Error

    If (Trim(txtStation.Text) = "") Then Exit Sub
    
    sql = "delete from MA_KDS where KDS_ID = '" & txtStation.Text & "'"
    Conexion.Execute (sql), lRecordsAffected
    
    If (lRecordsAffected = 0) Then
        MsgBox ("No se pudo eliminar la estacion, verifique que el numero de estacion exista")
    Else
    
        LS.KmRemoveStation txtStation.Text
    
    End If
    
Exit Sub
    
Error:     Debug.Print (Err.Description)
    
End Sub
Private Sub SetColor(ByVal Condicion As String, ByVal ID As Integer, ByVal Valor As Integer)
On Error GoTo Error
    
    sql = "UPDATE MA_KDS SET " & Condicion & " = '" & Valor & "' WHERE KDS_ID = '" & ID & "'"
    Conexion.Execute sql, lRecordsAffected

    Exit Sub
    
Error:
Debug.Print Err.Description
    
End Sub
Private Sub Form_Load()

On Error GoTo LoadError
    
    If Frame1.Visible = False Then KDSManager.Width = 4250
    
    isshowing = False
    R = 1
    
    Dim rs As New ADODB.Recordset
    Dim Funciones As New Funciones
    
    addColor cmbFont, 0
    addColor cmbBack, 1

    cmbFont.ListIndex = 15
    cmbBack.ListIndex = 0
    
    Timer1.Interval = 10000
    'Aqui se llama al metodo ConectarBDD que se encarga de establecer las conexiones.
    Call Funciones.ConectarBDD
    Call LoadKDS
    LS.KmRun
    
    sql = "SELECT * FROM MA_KDS"
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, True)
    
    rs.Open sql, Conexion, adOpenDynamic, adLockBatchOptimistic
    
    For J = 1 To TotalLS
        LS.KitchenCreateSumPanel IDLS(J), 15, 5, 50, 15, 240, "Summary"
        LS.KitchenSetOrderNormalAttr IDLS(J), rs!OrderColor
        LS.KitchenSetOrderFocusAttr IDLS(J), rs!FocusColor
        LS.KitchenSetOrderBackAttribute IDLS(J), rs!BackColor
        LS.KitchenSetBackScreenAttribute IDLS(J), rs!ScreenColor
        
        rs.MoveNext
    
    Next J
    
    If rs.State = 1 Then rs.Close
    
    Exit Sub
    
LoadError:
Debug.Print Err.Description
    
End Sub
Private Sub addColor(cmb As ComboBox, i As Integer)
    
    With cmb
    '============
    .AddItem "Blue"
    
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Blue
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Blue
    End If
    '=============
    .AddItem "Green"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Green
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Green
    End If
    
    .AddItem "Red"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Red
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Red
    End If
    
    .AddItem "Intensity"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Intensity
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Intensity
    End If
    
    .AddItem "Black"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Black
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Black
    End If
    
    .AddItem "Purple"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Purple
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Purple
    End If
    
    .AddItem "Brown"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Brown
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Brown
    End If
    
    .AddItem "Aqua"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Aqua
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Aqua
    End If
    
    .AddItem "Grey"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Grey
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Grey
    End If
    
    .AddItem "Light Blue"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Light_Blue
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Light_Blue
    End If
    
    .AddItem "Pink"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Pink
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Pink
    End If
    
    .AddItem "Magenta"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Magenta
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Magenta
    End If
    
    .AddItem "Light Green"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Light_Green
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Litht_Green
    End If
    
    .AddItem "Cyan"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Cyan
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Cyan
    End If
    
    .AddItem "Yellow"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_Yellow
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_Yellow
    End If
    
    .AddItem "White"
    If i = 0 Then
        .ItemData(.NewIndex) = KsConst.Ks_FG_White
    Else
        .ItemData(.NewIndex) = KsConst.Ks_BG_White
    End If
    
    End With
    
End Sub

Public Sub LoadKDS()
    
On Error GoTo LoadError
    
    Dim rs As New ADODB.Recordset
    Dim ID As String
    Dim IP As String
    Dim Port As String
    Dim red As Boolean
    sql = "SELECT * FROM MA_KDS"
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, True)
    If rs.State = 1 Then rs.Close
    
    rs.Open sql, Conexion, adOpenDynamic, adLockBatchOptimistic

    
    If rs.EOF Then Exit Sub
    
    rs.MoveFirst
    
    TotalLS = 0
    
    While Not rs.EOF
        res = LS.KmAddStation(CInt(rs!KDS_ID), rs!IP, rs!Port)
        rs.MoveNext
        TotalLS = TotalLS + 1
    Wend
    
    rs.MoveFirst
    
    ReDim IDLS(TotalLS)
    
    For i = 0 To TotalLS
    
        IDLS(i) = rs!KDS_ID
    
    Next i

    Exit Sub
    
LoadError:
MsgBox Err.Description

End Sub

Private Sub LS_OnKsReturnKey(ByVal address As Integer, ByVal AscKeyCode As Integer)
On Error GoTo Error
    
    Timer1.Enabled = False
    Timer1.Enabled = True
    Dim currentid As String
    Dim RsOrders As New ADODB.Recordset
    Dim Order As String
    Dim cambio As Boolean
    
    '    MsgBox (AscKeyCode)
    
    'redraw screen
    If AscKeyCode = 13 Then
        LS.KitchenClearScreen address
        LS.KitchenDrawScreen address
        
    End If
    
    If LS.KitchenCheckSingleUserMode(address) Then 'single user
        
        ' move focus to previous order
        If AscKeyCode = 173 Then
            If isshowing = False Then
                TotalOrders = LS.KitchenGetOrdersCount(address)
                If TotalOrders > 1 Then
                    LS.KitchenSetFocusMovePrev address
                Else
                    LS.KitchenSetFocusToFirst address
                End If
            Else
                LS.KitchenScrollSumPanelDown address, 1
            End If
        End If
        
        ' move focus to next order
        If AscKeyCode = 32 Then
            If isshowing = False Then
                TotalOrders = LS.KitchenGetOrdersCount(address)
                If TotalOrders > 1 Then
                    LS.KitchenSetFocusMoveNext address
                Else
                    LS.KitchenSetFocusToFirst address
                End If
            Else
                LS.KitchenScrollSumPanelUp address, 1
            End If
        End If
        
        ' delete current order
        If AscKeyCode = 127 Then
            currentid = LS.KitchenGetCurrentOrderID(address)
            'sql = "UPDATE MA_CONSUMO SET EstatusKDS = 0 WHERE cs_CODIGOSERVICIO = '" & currentid & "'"
            sql = "UPDATE TR_CONSUMO SET EstatusKDS = 1 WHERE cs_COMANDA = '" & currentid & "'"
            Conexion.Execute sql, lRecordsAffected
            
            ReDim Preserve TotalRecall(R)
            TotalRecall(R) = currentid
            R = R + 1
            
            LS.KitchenDeleteCurrentOrder address
            LS.KitchenSetFocusToFirst address
        End If
        
        ' restore deleted order
        If AscKeyCode = 45 Then
            LS.KitchenRestoreDelOrder address
            'sql = "UPDATE MA_CONSUMO SET EstatusKDS = 1 WHERE cs_CODIGOSERVICIO = '" & TotalRecall(R - 1) & "'"
            sql = "UPDATE TR_CONSUMO SET EstatusKDS = 0 WHERE cs_CODIGOSERVICIO = '" & TotalRecall(R - 1) & "'"
            If R > 1 Then
                R = R - 1
            End If
            
            Conexion.Execute sql, lRecordsAffected
        End If
        
        ' summary
        If AscKeyCode = 172 Then
            If isshowing = True Then
                LS.KitchenShowSumPanel address, False
                isshowing = False
            Else
                LS.KitchenShowSumPanel address, True
                isshowing = True
            End If
        End If
        
        ' set focus to first order
        If AscKeyCode = 48 Then
            LS.KitchenSetFocusToFirst address
        End If
        
        ' set focus to second order
        If AscKeyCode = 49 Then
            Order = LS.KitchenGetOrderID(1, 8)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
         ' set focus to third order
        If AscKeyCode = 50 Then
            Order = LS.KitchenGetOrderID(1, 16)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
         ' set focus to fourth order
        If AscKeyCode = 51 Then
            Order = LS.KitchenGetOrderID(1, 24)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
        ' set focus to fifth order
        If AscKeyCode = 52 Then
            Order = LS.KitchenGetOrderID(1, 32)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
        ' set focus to sixth order
        If AscKeyCode = 53 Then
            Order = LS.KitchenGetOrderID(1, 40)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
        ' set focus to seventh order
        If AscKeyCode = 54 Then
            Order = LS.KitchenGetOrderID(1, 48)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
        ' set focus to eighth order
        If AscKeyCode = 55 Then
            Order = LS.KitchenGetOrderID(1, 56)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
        ' set focus to ninth order
        If AscKeyCode = 56 Then
            Order = LS.KitchenGetOrderID(1, 64)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
        ' set focus to tenth order
        If AscKeyCode = 57 Then
            Order = LS.KitchenGetOrderID(1, 72)
            cambio = LS.KitchenSetFocusToOrder(1, Order)
        End If
        
    Else 'multiple user
    
        '==========LEFT user
        ' move focus to previous order
        If AscKeyCode = KS_KEY_ARROW_LEFT Then
            LS.KitchenMultiUserSetFocusToPrev address, 0
            
        End If
        'move focus to next order
        If AscKeyCode = KS_KEY_ARROW_RIGHT Then
            LS.KitchenMultiUserSetFocusToNext address, 0
        End If
        'delete current order
        If AscKeyCode = KS_KEY_DEL Then
            LS.KitchenDeleteOrder address, LS.KitchenMultiUserGetCurrentOrder(address, 0)
            
            
        End If
        ' restore deleted order
        If AscKeyCode = KS_KEY_INSERT Then
            LS.KitchenMultiUserRestoreOrder address, 0
            
        End If
        '===============RIGHT user
        'MsgBox (Str(AscKeyCode))
        
        If AscKeyCode = KS_KEY_p Then
            LS.KitchenMultiUserSetFocusToPrev address, 1
            
        End If
        'move focus to next order
        If AscKeyCode = KS_KEY_n Then
            LS.KitchenMultiUserSetFocusToNext address, 1
        End If
    
        If AscKeyCode = KS_KEY_d Then
            LS.KitchenDeleteOrder address, LS.KitchenMultiUserGetCurrentOrder(address, 1)
        End If
        
        If AscKeyCode = KS_KEY_r Then
            LS.KitchenMultiUserRestoreOrder address, 1
        End If
        
    End If
    
    Exit Sub
    
Error:
Debug.Print Err.Description

End Sub
Private Sub AddSummary(ByVal cs_DESCRIPCION_PRODUCTO As String, ByVal nu_CANTIDAD As Integer) 'proceso para a�adir al summary del KDS
On Error GoTo Error
   
    Dim Summary As Boolean
    Dim Espaciado As String
    Dim LenProducto As Integer
    Dim Necesario As Integer
    
    Summary = False
    
    For i = 1 To SumTotal
                    
        'redefinir las variables del summary cada vez que se agrega una comida nueva
        ReDim Preserve SumNombre(SumTotal)
        ReDim Preserve SumEspaciado(SumTotal)
        ReDim Preserve SumCantidad(SumTotal)
        
        If SumNombre(i) = cs_DESCRIPCION_PRODUCTO Then ' si se consigue la comida, se actualiza la cantidad
            SumCantidad(i) = SumCantidad(i) + nu_CANTIDAD
            Summary = True
        End If
    Next i

    If Summary = False Then ' si no esta a�adida la comida al summary, procede a hacer el formato
        Espaciado = " "
        LenProducto = Len(cs_DESCRIPCION_PRODUCTO)
        Necesario = 42 - LenProducto
        
        While Len(Espaciado) < Necesario - 1
            Espaciado = Espaciado + "-"
        Wend
        
        SumNombre(SumTotal) = cs_DESCRIPCION_PRODUCTO
        SumEspaciado(SumTotal) = Espaciado
        SumCantidad(SumTotal) = nu_CANTIDAD
        SumTotal = SumTotal + 1
        
    End If
    Exit Sub
    
Error:
Debug.Print Err.Description

End Sub

Private Sub DeleteFinishedOrder(ByVal RsMaConsumo As ADODB.Recordset) ' proceso para revisar si la comida fue reintegrada para luego ser eliminada de la pantalla del KDS
On Error GoTo Error
    Dim Order As Boolean
    Dim RsConsumoCount As New ADODB.Recordset
    Dim OrderCount As Integer
    Dim DeleteOrder As Boolean
        
    sql3 = "SELECT TOP 100 COUNT(*) AS CONTADORORDEN FROM MA_CONSUMO WHERE bs_REINTEGRADA = 0 AND EstatusKDS = 1"
    RsConsumoCount.Open sql3, Conexion, adOpenForwardOnly, adLockReadOnly
    
    For i = 0 To RsConsumoCount!CONTADORORDEN
        OrderID = LS.KitchenGetOrderID(IDLS(J), i)
        Order = False

        RsMaConsumo.MoveFirst
        
        Do While Not RsMaConsumo.EOF
        
            If RsMaConsumo!cs_CODIGOSERVICIO = OrderID Then
                Order = True
                Exit Do
            End If
            
            RsMaConsumo.MoveNext
    
        Loop
        
        If Order = False Then ' si no se consigue la orden en la consulta, se elimina del KDS
            Delete = LS.KitchenDeleteOrder(IDLS(J), OrderID)
        End If
        
        OrderCount = LS.KitchenGetOrderItemsCount(IDLS(J), OrderID)

        If OrderCount < 1 Then
            DeleteOrder = LS.KitchenDeleteOrder(IDLS(J), OrderID)
        End If
        
    Next i
    
    If RsConsumoCount.State = adStateOpen Then RsConsumoCount.Close
    Exit Sub
    
Error:
Debug.Print Err.Description

End Sub

Private Sub AddOrderItem(ByVal cs_CODIGOSERVICIO As String, ByVal ID As Integer, ByVal cu_KDS As String)
On Error GoTo Error
    
    Dim RsTRConsumo As New ADODB.Recordset
    Dim RsKDS As New ADODB.Recordset
    Dim Comandas As Boolean
    Dim PComandados As Boolean
    Dim Contador As Integer
    Dim Cantidad As Integer
    Dim Nombre As String
    Dim OrderItemQty As Integer
    Dim TmpCantidad() As Integer
    Dim TmpNombre() As String
    Dim Tmp As Integer
    Dim Encontrado As Boolean
    Dim msgAlerta As String
    
    Tmp = 1
    Encontrado = False
    ReDim Preserve TmpCantidad(1)
    ReDim Preserve TmpNombre(1)
    
    'sql = "select cs_DESCRIPCION_PRODUCTO, nu_tiposervicio, sum(nu_CANTIDAD) as nu_CANTIDAD,cu_CODIGO_PRODUCTO " _
    & " from TR_CONSUMO where cs_CODIGOSERVICIO= '" & cs_CODIGOSERVICIO & "' AND cu_KDS = '" & cu_KDS & "'" _
    & "group by cu_CODIGO_PRODUCTO,cs_DESCRIPCION_PRODUCTO, nu_tiposervicio, ns_linea " _
    & " order by ns_LINEA"
    
'    sql = "select * from TR_CONSUMO where cs_CODIGOSERVICIO= '" & cs_CODIGOSERVICIO & "' AND cu_KDS = '" & cu_KDS & "'"
'    sql2 = "select * from MA_KDS where KDS_ID = '" & ID & "'"
'    sql3 = "select * from TR_CONSUMO where cs_CODIGOSERVICIO= '" & cs_CODIGOSERVICIO & "'"
    
    sql = "select * from TR_CONSUMO where cs_COMANDA= '" & cs_CODIGOSERVICIO & "' AND cu_KDS = '" & cu_KDS & "'"
    sql2 = "select * from MA_KDS where KDS_ID = '" & ID & "'"
    sql3 = "select * from TR_CONSUMO where cs_COMANDA= '" & cs_CODIGOSERVICIO & "'"
    
    'sql3 = "select cs_DESCRIPCION_PRODUCTO, nu_tiposervicio, sum(nu_CANTIDAD) as nu_CANTIDAD,cu_CODIGO_PRODUCTO " _
    & " from TR_CONSUMO where cs_CODIGOSERVICIO= '" & cs_CODIGOSERVICIO & "'" _
    & "group by cu_CODIGO_PRODUCTO,cs_DESCRIPCION_PRODUCTO, nu_tiposervicio, ns_linea " _
    & " order by ns_LINEA"
    
    If RsTRConsumo.State = 1 Then RsTRConsumo.Close
    If RsKDS.State = 1 Then RsKDS.Close
    
    RsKDS.Open sql2, Conexion, adOpenForwardOnly, adLockReadOnly
    
    'si tiene departamento, que solo muestre las ordenes que tengan su ip, sino tiene, que muestre todo
    If TieneDepartamento Then
        RsTRConsumo.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly
    Else
        RsTRConsumo.Open sql3, Conexion, adOpenForwardOnly, adLockReadOnly
    End If
    
    Eliminar = LS.KitchenDeleteAllOrderItems(IDLS(J), cs_CODIGOSERVICIO) ' eliminar las comidas de cada orden para poder ser actualizadas
    msgAlerta = ""
    While Not RsTRConsumo.EOF
        
        OrderItemQty = LS.KitchenGetOrderItemsCount(IDLS(J), cs_CODIGOSERVICIO) ' obtener la cantidad de items en la orden
        Contador = 0
        OrderItem = False
        CantidadTmp = RsTRConsumo!nu_CANTIDAD
        ReDim Preserve TmpCantidad(Tmp)
        ReDim Preserve TmpNombre(Tmp)
        
        If CantidadTmp < 1 Then
            For k = 1 To Tmp
                If RsTRConsumo!cs_DESCRIPCION_PRODUCTO = TmpNombre(k) Then
                    TmpCantidad(k) = TmpCantidad(k) - 1
                    Encontrado = True
                End If
            Next k
            
            If Encontrado = False Then
                
                ReDim Preserve TmpCantidad(Tmp)
                ReDim Preserve TmpNombre(Tmp)
                
                TmpCantidad(Tmp) = TmpCantidad(Tmp) - 1
                TmpNombre(Tmp) = RsTRConsumo!cs_DESCRIPCION_PRODUCTO
                Tmp = Tmp + 1
            End If
            
            CantidadTmp = 0
        End If

        For i = 0 To OrderItemQty
        
            LS.KitchenGetOrderItem IDLS(J), cs_CODIGOSERVICIO, Contador, Cantidad, Nombre, attr
            
            If attr = RsKDS!ItemColor Then
                tiposervicio = 0
            Else
                tiposervicio = 1
            End If

'            If RsTRConsumo!cs_DESCRIPCION_PRODUCTO = Nombre And i = Contador And RsTRConsumo!nu_tiposervicio = tiposervicio And CantidadTmp <> 0 Then ' si la comida esta agregada, se modifica la cantidad
'
'                LS.KitchenModifyOrderItemQty IDLS(j), cs_CODIGOSERVICIO, Contador, Cantidad + CantidadTmp
'                OrderItem = True
'
'            End If
            
            Contador = Contador + 1

        Next i
        
        If OrderItem = False Then ' And CantidadTmp <> 0 Then ' si la comida no esta en la orden, procede a ser agregada
               
            Comandas = LS.KitchenAddNewOrder(IDLS(J), cs_CODIGOSERVICIO) ' a�adir una nueva orden
            
'alerta caiman
'            If LS.KitchenGetOrderWaitSeconds(IDLS(J), cs_CODIGOSERVICIO) > 40 Then
'                'ss = "Hu"
'                'cs = LS.KitchenAddOrderTitle(IDLS(j), cs_CODIGOSERVICIO, ss)
'
'                msgAlerta = msgAlerta & "-" & cs_CODIGOSERVICIO
'                alerta = LS.KitchenSendMessage(IDLS(j), "Hurry " & msgAlerta, 252)
'                'alerta = LS.KitchenSendMessage(IDLS(J), "Hurry " & cs_CODIGOSERVICIO, 252)
'                'alerta = LS.KsBeep(IDLS(J), 2)
'            Else
'
'            End If
            
            
            
            If RsTRConsumo!nu_tiposervicio = "0" Then ' si es para COMER EN SITIO
                
                If RsTRConsumo!CS_CODIGO_PRODUCTO_PADRE = "" Then 'SI ES MODIFICARDOR
                    PComandados = LS.KitchenAddOrderItem(IDLS(J), cs_CODIGOSERVICIO, CantidadTmp, RsTRConsumo!cs_DESCRIPCION_PRODUCTO, RsKDS!ItemColor)
                Else 'MODIFCADORES
                    If RsTRConsumo!nu_CANTIDAD = 0 Then 'SI NO LLEVA ESE PRODUCTO
                        PComandados = LS.KitchenAddModifier(IDLS(J), cs_CODIGOSERVICIO, False, RsTRConsumo!cs_DESCRIPCION_PRODUCTO, 252)
                    Else
                        PComandados = LS.KitchenAddModifier(IDLS(J), cs_CODIGOSERVICIO, True, RsTRConsumo!cs_DESCRIPCION_PRODUCTO, RsKDS!ItemColor)
                    End If
                End If
                
            Else ' SI ES PARA LLEVAR
                If RsTRConsumo!CS_CODIGO_PRODUCTO_PADRE = "" Then 'SI ES MODIFICARDOR
                    PComandados = LS.KitchenAddOrderItem(IDLS(J), cs_CODIGOSERVICIO, CantidadTmp, RsTRConsumo!cs_DESCRIPCION_PRODUCTO, RsKDS!DeliveryColor) '249
                Else
                    If RsTRConsumo!nu_CANTIDAD = 0 Then 'SI NO LLEVA ESE PRODUCTO
                        PComandados = LS.KitchenAddModifier(IDLS(J), cs_CODIGOSERVICIO, False, RsTRConsumo!cs_DESCRIPCION_PRODUCTO, 252)
                    Else
                        PComandados = LS.KitchenAddModifier(IDLS(J), cs_CODIGOSERVICIO, True, RsTRConsumo!cs_DESCRIPCION_PRODUCTO, RsKDS!DeliveryColor)
                    End If
                End If
            End If
'Jesus
            'If RsTRConsumo!cu_texto <> "" Then 'si el producto tiene comentario entonces lo agrega en una nueva linea
                'PComandados = LS.KitchenAddItemNote(IDLS(J), cs_CODIGOSERVICIO, RsTRConsumo!cu_texto, 246) '(IDLS(j), cs_CODIGOSERVICIO, 0, RsTRConsumo!cu_texto, RsKDS!ItemColor)
            'End If
            
            Set Lineas = DividirStringLineas(RsTRConsumo!cu_texto, 15)
            
            For Each Linea In Lineas
                Call LS.KitchenAddItemNote(IDLS(J), cs_CODIGOSERVICIO, Linea, 246) '(IDLS(j), cs_CODIGOSERVICIO, 0, Linea , RsKDS!ItemColor)
            Next
            
        End If
        If CantidadTmp <> 0 Then
            Call AddSummary(RsTRConsumo!cs_DESCRIPCION_PRODUCTO, RsTRConsumo!nu_CANTIDAD)
        End If
        RsTRConsumo.MoveNext
    Wend
    Dim Cant As Integer
    Dim Nom As String
    Dim At As Integer

    For h = 0 To OrderItemQty + 1
        LS.KitchenGetOrderItem IDLS(J), cs_CODIGOSERVICIO, h, Cant, Nom, At
        For k = 1 To Tmp - 1
            If TmpNombre(k) = Nom And Nom <> "" Then
                If Cant + TmpCantidad(k) < 1 Then
                    Borrado = LS.KitchenDeleteOrderItem(IDLS(J), cs_CODIGOSERVICIO, h)
                    h = h - 1
                Else
                    LS.KitchenModifyOrderItemQty IDLS(J), cs_CODIGOSERVICIO, k, Cant + TmpCantidad(h)
                End If
            End If
        Next k
    Next h
    
    If RsTRConsumo.State = 1 Then RsTRConsumo.Close
    If RsKDS.State = 1 Then RsKDS.Close
    Exit Sub
    
Error:
Debug.Print Err.Description

End Sub

Private Sub Timer1_Timer()
On Error GoTo Error
    
    Dim RsMaConsumo As New ADODB.Recordset
    Dim RecordKDS As New ADODB.Recordset
    Dim cs_CODIGOSERVICIO As String
    Dim N_Mesa As String
    Dim cu_KDS As String
    Dim SumItem As String
    Dim OrderItem As Boolean
    Dim RsDepartamentoTmp As New ADODB.Recordset
    Dim Rssitios As New ADODB.Recordset
    
    Call LoadKDS
    If TotalLS < 1 Then Exit Sub
    
    If Timer1.Interval <> 2000 Then ' para reestablecer el tiempo del timer luego de utilizar el teclado del KDS
        Timer1.Enabled = False
        Timer1.Interval = 2000
        Timer1.Enabled = True
    End If
    
    If Begin = False Then ' para realizar una limpieza de pantalla antes de empezar el proceso
        LS.KitchenClearScreen IDLS(J)
        Begin = True
    End If
    
    LS.KitchenDrawScreen IDLS(J)
    
    SumTotal = 1 ' se establece para no tener errores al redimensionar los arreglos del summary
    
    'sql = "SELECT cs_CODIGOSERVICIO, bs_CONSOLIDADA FROM MA_CONSUMO WHERE bs_REINTEGRADA = 0 AND ns_TOTAL != 0 AND EstatusKDS = 1"
    
    sql = "SELECT tr.cs_COMANDA as cs_CODIGOSERVICIO, bs_CONSOLIDADA, cs_CODIGOMESA " _
    & "FROM MA_CONSUMO as MA " _
    & "inner join TR_CONSUMO as TR " _
    & "on ma.cs_CODIGOSERVICIO=tr.cs_CODIGOSERVICIO " _
    & "WHERE ma.bs_REINTEGRADA = 0 AND ma.ns_TOTAL != 0 AND tr.EstatusKDS = 0 " _
    & "group by tr.cs_COMANDA,bs_CONSOLIDADA,cs_CODIGOMESA"
    
    sql2 = "SELECT * FROM MA_KDS"
    
    RsMaConsumo.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly
    
    NO = LS.KitchenSendMessage(IDLS(J), "", 252)
    
    If RsMaConsumo.EOF Then
        If RsMaConsumo.State = adStateOpen Then RsMaConsumo.Close
        For J = 1 To TotalLS ' para realizar el proceso en todos los LS que fueron configurados al inicio
            '
            NO = LS.KitchenClearScreen(IDLS(J))
            NO = LS.KitchenSendMessage(IDLS(J), "", 252)
            'NO = LS.KitchenDrawScreen(IDLS(J))
            'LS.KmReset
        Next J
        Exit Sub
    End If
    
    RecordKDS.Open sql2, Conexion, adOpenForwardOnly, adLockReadOnly
    
    Call DeleteFinishedOrder(RsMaConsumo) ' para eliminar del KDS las ordenes que fueron reintegradas
    
    For J = 1 To TotalLS ' para realizar el proceso en todos los LS que fueron configurados al inicio
    
        While Not RecordKDS.EOF
        
            cu_KDS = RecordKDS!IP
            
            sql4 = "select * from MA_KDS_DEPARTAMENTO WHERE cu_KDS = '" & cu_KDS & "'"
            RsDepartamentoTmp.Open sql4, Conexion, adOpenForwardOnly, adLockReadOnly
    
            If RsDepartamentoTmp.EOF Then
                TieneDepartamento = False
            Else
                TieneDepartamento = True
            End If
            
            If RsDepartamentoTmp.State = adStateOpen Then RsDepartamentoTmp.Close
            
            LS.KitchenSetUpdateTimeInterval IDLS(J), 1
            LS.KitchenStopUpdateTimeTimer IDLS(J)
            LS.KitchenStartUpdateTimeTimer IDLS(J)
            LS.KitchenClearSumPanel IDLS(J)
            LS.KitchenSetSumPanelTitle IDLS(J), "Item Name                                Sum", 47
        
            RsMaConsumo.MoveFirst
            
            While Not RsMaConsumo.EOF
            
                cs_CODIGOSERVICIO = RsMaConsumo!cs_CODIGOSERVICIO
                N_Mesa = RsMaConsumo!cs_CODIGOMESA
                LS.KitchenEnablePanelFooterMsg IDLS(J), True
                
                Call AddOrderItem(cs_CODIGOSERVICIO, IDLS(J), cu_KDS)
                sqlSitios = "select cu_descripcion from ma_sitio where cs_codigositio='" & N_Mesa & "'"
                Rssitios.Open sqlSitios, Conexion, adOpenForwardOnly, adLockReadOnly
                If Not Rssitios.EOF Then
                    footer = LS.KitchenAddOrderFooterMsg(IDLS(J), cs_CODIGOSERVICIO, 0, Rssitios!cu_descripcion, 30)
                Else
                    footer = LS.KitchenAddOrderFooterMsg(IDLS(J), cs_CODIGOSERVICIO, 0, "Llevar", 30)
                End If
                Rssitios.Close
                If RsMaConsumo!bs_CONSOLIDADA = True Then
                    LS.KitchenAddOrderTitle IDLS(J), cs_CODIGOSERVICIO, " $"
                    Call LS.KitchenDeleteOrder(IDLS(J), cs_CODIGOSERVICIO)
                Else
                    'LS.KitchenAddOrderTitle IDLS(J), cs_CODIGOSERVICIO, N_Mesa
                End If
        
                RsMaConsumo.MoveNext
                
            Wend
            
            For i = 1 To SumTotal - 1 ' despues de guardar todo el summary, se a�ade todo al KDS
                SumItem = ""
                SumItem = SumNombre(i) & SumEspaciado(i) & " " & SumCantidad(i)
                LS.KitchenAddSumPanelItem IDLS(J), SumItem, 240
            Next i ' aqui termina el summary
        RecordKDS.MoveNext
        Wend
    Next J
    
    If RsMaConsumo.State = adStateOpen Then RsMaConsumo.Close
    If RecordKDS.State = adStateOpen Then RecordKDS.Close
    
    Erase SumNombre
    Erase SumCantidad
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    Debug.Print (Err.Description)
    
End Sub
