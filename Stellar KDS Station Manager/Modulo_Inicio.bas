Attribute VB_Name = "Modulo_Inicio"
Global Conexion As New ADODB.Connection
Global G_Server_Local
Global G_BDServer_Local
Global G_ServerLogin        As String
Global G_ServerPassword     As String

Global G_MostrarProgreso

Public Sub Crear_Conexiones(BaseDatos1 As String, SrvLogin As String, srvPassword As String, Optional Opcion As Boolean = 0)

    On Error GoTo ErrorConexion

    Dim ConexString1

    If Opcion = True Then
        ConexString1 = "Provider =MSDataShape.1;Persist Security Info=False;Data Source=" & G_Server_Local & ";User ID=" & SrvLogin & ";Password=" & srvPassword & ";Initial Catalog=" & G_BDServer_Local & ";Data Provider=SQLOLEDB.1"
    Else
        ConexString1 = "Driver={SQL Server};Server=" & G_Server_Local & ";Database=" & G_BDServer_Local & ";Uid=" & SrvLogin & ";Pwd=" & srvPassword & ";"
    End If
    
    If Conexion.State = adStateOpen Then Conexion.Close
    Conexion.ConnectionString = ConexString1
    
    If SrvLogin <> "" And G_BDServer_Local <> "" And G_Server_Local <> "" Then
        Conexion.Open
        
        
    Else
        MsgBox "No se ha definido el Servidor o la Base de Datos.", vbExclamation, "Información Stellar"
        End
    End If
    
    Exit Sub

ErrorConexion:
    
   Debug.Print Err.Description
    MsgBox "No se puede localizar el servidor o la base de datos.", vbExclamation, "Información Stellar"
    End
    
End Sub

Public Function DividirStringLineas(ByVal pText As String, ByVal pMaxLineChars As Long) As Collection
    
    Dim TmpList As New Collection
    Set TmpList = New Collection
    
    Dim TmpText As String
    TmpText = pText
    
    Do Until Len(TmpText) <= 0
        Linea = Mid(TmpText, 1, 15)
        TmpText = Mid(TmpText, 16)
        TmpList.Add Linea
    Loop
    
    Set DividirStringLineas = TmpList
    
End Function
