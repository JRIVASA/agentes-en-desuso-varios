VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clstbGold"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Fecha , FechaV, codcliente, Concepto, Documento, NControl, Subtotal, Base, Impuesto, CodConcepto, DocRelacion, NoComprobante, NOCONTROL, FechaComprobante
Enum eCamposArchivo
    ecaFecha
    ecaFechaV
    ecaCodCliente
    ecaConcepto
    ecaDocumento
    ecaNControl
    ecaSubtotal
    ecaBase
    ecaImpuesto
    ecaCodConcepto
    ecaDocRelacion
    ecaConRelacion
    ecaNoComprobante
    ecaFechaCom
End Enum

Enum eTipoLog
    etlError
    etlNoClienteRel
    etlNoConceptotoRel
    etlNoFacturaPagar
    etlFacCancelada
    etlFechaVMenorAFecha
    etlImpuestoMayorASubtotal
    etlSubtotalIgualACero
End Enum

Public Sub ProcesarArchivo(pArchivo As String)
    Dim mCanal As Integer
    Dim mCadena As String
    Dim mIdtr As Long
    
    mCanal = FreeFile()
    'mIdtr = Val(BuscarCorrelativo(AppConfig.ConexionStellar, "IdAgentGold"))
    NumCorrida = Val(BuscarCorrelativo(AppConfig.ConexionStellar, "IdAgentGold"))
    Open AppConfig.RutaArchivos & "\" & pArchivo For Input Access Read As #mCanal
    'GrabarMA_Agente_Log AppConfig.RutaArchivos & "\" & pArchivo, ObtenerNombreEquipo(), Now, mIdtr
    GrabarMA_Agente_Log AppConfig.RutaArchivos & "\" & pArchivo, ObtenerNombreEquipo(), Now, NumCorrida
    Do While Not EOF(mCanal)
        Line Input #mCanal, mCadena
        'LineaProcesar mIdtr, mCadena, pArchivo, AppConfig.Separador
        LineaProcesar NumCorrida, mCadena, pArchivo, AppConfig.Separador
    Loop
    Close #mCanal
    
End Sub

Public Function LineaProcesar(pIdtr As String, pCadena As String, pArchivo As String, pSeparador As String)
'Public Function LineaProcesar(pIdtr As Long, pCadena As String, pArchivo As String, pSeparador As String)
    
    Dim mLinea As clsLineaArchivo
    Dim mValores() As String
    Dim Fecha As Date
    Dim nCampo As eCamposArchivo
    Dim i As Integer
    Dim CodClienteStellar As String, CodConceptoStellar As String
    
    frmEjecucion.ProgressBar1.value = "50"
    frmEjecucion.statusLabel.Caption = "PROCESANDO ARCHIVOS"
    
    Set mLinea = New clsLineaArchivo
    If (Trim(pSeparador) = "|") Or (Trim(pSeparador) = ";") Then
        mValores = Split(pCadena, pSeparador)
        If (UBound(mValores) <> eCamposArchivo.ecaFechaCom) Then
            If UBound(mValores) = "-1" Then Exit Function
            'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, "La l�nea no contiene todos los campos. " & UBound(mValores)
            GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, "La l�nea no contiene todos los campos. " & UBound(mValores)
        Else
            i = 0
            For nCampo = ecaFecha To ecaFechaCom
                AsignarValor nCampo, mValores(i), mLinea
                i = i + 1
            Next nCampo
            '*********************************************************DATOS CLIENTES
            CodClienteStellar = BuscarRelacionStellar(mLinea.Cliente)
            If CodClienteStellar = vbNullString Then
                'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlNoClienteRel, "Cliente No Relacionado: " & mLinea.Cliente
                GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlNoClienteRel, "Cliente No Relacionado: " & mLinea.Cliente
                Exit Function
            End If
             '******************************************************DATOS CONCEPTO
           
            CodConceptoStellar = BuscarRelacionStellar(mLinea.CodConcepto, BuscarIdProceso(mLinea.Concepto))
            If CodConceptoStellar = vbNullString Then
                'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlNoConceptotoRel, "Concepto No Relacionado: " & mLinea.Concepto & ", " & mLinea.CodConcepto
                GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlNoConceptotoRel, "Concepto No Relacionado: " & mLinea.Concepto & ", " & mLinea.CodConcepto
                Exit Function
            End If
             '******************************************************
             
             '****************************************************** VALIDAR FECHAS
             If mLinea.FechaV < mLinea.Fecha Then
                'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlFechaVMenorAFecha, "Fecha de Vencimiento(" & mLinea.FechaV & ") < Fecha de Compra(" & mLinea.Fecha & ")"
                GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlFechaVMenorAFecha, "Fecha de Vencimiento(" & mLinea.FechaV & ") < Fecha de Compra(" & mLinea.Fecha & ")"
                Exit Function
             End If
             '******************************************************
             
             '****************************************************** VALIDAR MONTOS
             If mLinea.Impuesto > mLinea.Subtotal Then
                'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlImpuestoMayorASubtotal, "IVA(" & mLinea.Impuesto & ") > Subtotal(" & mLinea.Subtotal & ") "
                GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlImpuestoMayorASubtotal, "IVA(" & mLinea.Impuesto & ") > Subtotal(" & mLinea.Subtotal & ") "
                Exit Function
            'Por solicitud del cliente se elimin� esta validaci�n.
'             ElseIf mLinea.Subtotal = 0 Then
'                GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlSubtotalIgualACero, "Subtotal de la factura igual a cero."
'                Exit Function
             End If
             '******************************************************
             frmEjecucion.ProgressBar1 = "75"
             frmEjecucion.statusLabel = "PROCESANDO REGISTROS"
            'ProcesarLinea mLinea, CodClienteStellar, CodConceptoStellar, pCadena, pIdtr, IIf(mLinea.Documento <> "", mLinea.Documento, "")
            ProcesarLinea mLinea, CodClienteStellar, CodConceptoStellar, pCadena, NumCorrida, IIf(mLinea.Documento <> "", mLinea.Documento, "")
        End If
    Else
        'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, "Separador No Admitido"
        GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, "Separador No Admitido"
    End If
    
End Function

Private Function ObtenerFecha(pCampo) As Date
    Dim mFecha As Date
    Dim mDato As String

    On Error GoTo Errores
    mDato = Trim(pCampo)
    If Trim(mDato) <> "" Then
        ObtenerFecha = DateSerial(Right(mDato, 4), mId(mDato, 3, 2), Left(mDato, 2))
    Else
        ObtenerFecha = "01/01/" & Year(Date)
    End If
    Exit Function
Errores:
    ObtenerFecha = FechaError
    mError = "Error en Fecha"
End Function

Private Function ObtenerMonto(pCampo)
    Dim mFecha As Date
    Dim mDato As String

    On Error GoTo Errores
    mDato = Trim(pCampo)
    ObtenerMonto = mDato / 100
    Exit Function
Errores:
    ProcesarError Err, "clstbGold", "ObtenerMonto()"
End Function

Private Function AsignarValor(pCampo As eCamposArchivo, pCadena, mLinea As clsLineaArchivo)
    Select Case pCampo
         Case ecaFecha
            mLinea.Fecha = ObtenerFecha(pCadena)
         Case ecaFechaV
            mLinea.FechaV = ObtenerFecha(pCadena)
         Case ecaCodCliente
            mLinea.Cliente = pCadena
         Case ecaConcepto
           mLinea.Concepto = BuscarConceptoStellar(pCadena)
         Case ecaDocumento
            mLinea.Documento = pCadena
         Case ecaNControl
           mLinea.NControl = pCadena
         Case ecaSubtotal
            mLinea.Subtotal = ObtenerMonto(pCadena)
         Case ecaBase
            mLinea.Base = ObtenerMonto(pCadena)
         Case ecaImpuesto
            mLinea.Impuesto = ObtenerMonto(pCadena)
         Case ecaCodConcepto
            mLinea.CodConcepto = pCadena
         Case ecaDocRelacion
            mLinea.DocRelacion = pCadena
         Case ecaConRelacion
            mLinea.ConceptoRel = BuscarConceptoStellar(pCadena)
         Case ecaNoComprobante
            mLinea.NoComprobante = pCadena
         Case ecaFechaCom
            mLinea.FechaComprobante = ObtenerFecha(pCadena)
    End Select
End Function
'****************************************Procesa la linea del archivo
'Public Function ProcesarLinea(pLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String, pCadena As String, pIdtr As Long, Optional correlativo As String)
Public Function ProcesarLinea(pLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String, pCadena As String, pIdtr As String, Optional correlativo As String)

    Select Case pLinea.Concepto
        Case "VEN"
            'GrabarVenta pLinea, pCliente, pCodConcepto, pCadena, pIdtr, IIf(correlativo = "", "", correlativo)
            GrabarVenta pLinea, pCliente, pCodConcepto, pCadena, NumCorrida, IIf(correlativo = "", "", correlativo)
        Case "N_D"
            'GrabarNDD pLinea, pCliente, pCodConcepto, pCadena, pIdtr
            GrabarNDD pLinea, pCliente, pCodConcepto, pCadena, NumCorrida
        Case "N_C"
            'GrabarNDC pLinea, pCliente, pCodConcepto, pCadena, pIdtr
            GrabarNDC pLinea, pCliente, pCodConcepto, pCadena, NumCorrida
        Case "PAG"
            'GrabarPagosCxC pLinea, pCliente, pCodConcepto, pCadena, pIdtr
            GrabarPagosCxC pLinea, pCliente, pCodConcepto, pCadena, NumCorrida
        Case Else
            'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, "ERROR CONCEPTO NO VALIDO"
            GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, "ERROR CONCEPTO NO VALIDO"
    End Select
                 
End Function
'******************************************************* Agente ******************************************
Private Sub GrabarNDD(pLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String, pCadena As String, pIdtr As String)
    Dim mCorrelativo As String
   
    AppConfig.ConexionStellar.BeginTrans
    mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_notad")
   
    GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "N_D", 0, pCodConcepto, False, "NOTA DEBITO_" & pLinea.Documento
                                                                                                
    GrabarImpuestos AppConfig.ConexionStellar, pLinea, pCliente, mCorrelativo, "N_D", False
    AppConfig.ConexionStellar.CommitTrans
    Exit Sub
Errores:
    If mInicio Then AppConfig.ConexionStellar.RollbackTrans
    'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, "ERROR GUARDANDO NOTA DE DEBITO "
    GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, "ERROR GUARDANDO NOTA DE DEBITO "
End Sub

Private Sub GrabarNDC(pLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String, pCadena As String, pIdtr As String)
    Dim mCorrelativo As String
    Dim mSaldoPendiente As Double
    Dim mMontoPagar As Double, mMontoAnt As Double
    Dim mPostRatear As Boolean
    
    mSaldoPendiente = BuscarSaldoDocumentoAfectado(AppConfig.ConexionStellar, pCliente, pLinea.DocRelacion, pLinea.ConceptoRel)
    'MsgBox "saldo pendiente:" & mSaldoPendiente
    AppConfig.ConexionStellar.BeginTrans
    mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_notac")
    mMontoAnt = Round(mSaldoPendiente - pLinea.Total, 2)
    If mMontoAnt < -0.1 Then
        'monto de n_c es mayor al pendiente
        'cancelo saldo pendiente y creo anticipo por la diferencia
        mMontoPagar = mSaldoPendiente
        mMontoAnt = Abs(mMontoAnt)
        mPostRatear = True
    Else
        mMontoPagar = pLinea.Total
        mMontoAnt = 0
    End If
    If mSaldoPendiente = 0 Then
        mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_a_p")
        GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "A_P", mMontoAnt, pCodConcepto, mPostRatear, "Anticipo Generado por Pago " & pLinea.Documento
    Else
        If PagarFacturaAfectada(AppConfig.ConexionStellar, pCliente, pLinea, mMontoPagar) Then
            GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "N_C", mMontoPagar, pCodConcepto, mPostRatear, "N_C"
            GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, pLinea.DocRelacion, pCliente, "N_Cx" & pLinea.ConceptoRel, mMontoPagar, pCodConcepto, mPostRatear, "N_Cx" & pLinea.ConceptoRel
            GrabarImpuestos AppConfig.ConexionStellar, pLinea, pCliente, mCorrelativo, "N_C"
            If mMontoAnt > 0.1 Then
                mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_a_p")
                GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "A_P", mMontoAnt, AppConfig.CodAnticipo, mPostRatear, "Anticipo Generado por N_C " & pLinea.Documento
                
            End If
            
        End If
     End If
    AppConfig.ConexionStellar.CommitTrans
    Exit Sub
Errores:
   AppConfig.ConexionStellar.RollbackTrans
   'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, "ERROR GRABANDO NOTA DE CREDITO"
   GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, "ERROR GRABANDO NOTA DE CREDITO"
End Sub

Private Function GrabarPagosCxC(pLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String, pCadena As String, pIdtr As String)
    Dim mCorrelativo As String
    Dim mSaldoPendiente As Double
    Dim mMontoPagar As Double, mMontoAnt As Double
    Dim mPostRatear As Boolean
    Dim mInicio As Boolean
   
    On Error GoTo Errores
    mSaldoPendiente = BuscarSaldoDocumentoAfectado(AppConfig.ConexionStellar, pCliente, pLinea.DocRelacion, pLinea.ConceptoRel)
    AppConfig.ConexionStellar.BeginTrans: mInicio = True
    mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_pagos")
    mMontoAnt = Round(mSaldoPendiente - pLinea.Total, 2)
    If mMontoAnt < -0.1 Then
        'monto de n_c es mayor al pendiente
        'cancelo saldo pendiente y creo anticipo por la diferencia
        mMontoPagar = mSaldoPendiente
        mMontoAnt = Abs(mMontoAnt)
        mPostRatear = True
    Else
        mMontoPagar = pLinea.Total
        mMontoAnt = 0
    End If
   If mSaldoPendiente = 0 Then
        mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_a_p")
        GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "A_P", mMontoAnt, pCodConcepto, mPostRatear, "Anticipo Generado por Pago " & pLinea.Documento
   Else
        If PagarFacturaAfectada(AppConfig.ConexionStellar, pCliente, pLinea, mMontoPagar) Then
            
            GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "PAG", mMontoPagar, pCodConcepto, mPostRatear, " Pago " & pLinea.Documento
            GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, pLinea.DocRelacion, pCliente, "PAGx" & pLinea.ConceptoRel, mMontoPagar, pCodConcepto, mPostRatear, " Pago " & pLinea.Documento
            GrabarDetallePago AppConfig.ConexionStellar, pLinea, mCorrelativo, mMontoPagar
            If mMontoAnt > 0.1 Then
                mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "cxc_a_p")
                GrabarCxC AppConfig.ConexionStellar, pLinea, mCorrelativo, "", pCliente, "A_P", mMontoAnt, pCodConcepto, mPostRatear, "Anticipo Generado por Pago " & pLinea.Documento
            End If
        End If
    End If
    AppConfig.ConexionStellar.CommitTrans
     
    Exit Function
Errores:
   If mInicio Then AppConfig.ConexionStellar.RollbackTrans
   'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, "ERROR GUARDANDO PAGO CXC"
   GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, "ERROR GUARDANDO PAGO CXC"
End Function

Private Function PagarFacturaAfectada(pCn As ADODB.Connection, pCliente As String, pLinea As clsLineaArchivo, pMonto As Double) As Boolean
    
    Dim mSQl As String
      
    mSQl = "Update ma_cxc set n_pagado=n_pagado+" & pMonto _
        & "where (c_codigo='" & pCliente & "') " _
        & "and (" & IIf(pConcepto = "N_D", "cs_documento_ndc", "C_Documento") & "='" & pLinea.DocRelacion & "') " _
        & "and (c_concepto='" & pLinea.ConceptoRel & "')"
    pCn.Execute mSQl, reg
    PagarFacturaAfectada = reg = 1
    
End Function

Private Sub GrabarDetallePago(pCn As ADODB.Connection, pLinea As clsLineaArchivo, pCorrelativo As String, pMontoPago As Double)
    
    Dim mRs As ADODB.Recordset

    Set mRs = New ADODB.Recordset
    mRs.Open "select * from MA_CXC_DETPAG where 1=2 ", pCn, adOpenDynamic, adLockOptimistic, adCmdText
    mRs.AddNew
        mRs!c_concepto = "PAG"
        mRs!C_Documento = pCorrelativo
        mRs!f_fecha = pLinea.Fecha
        mRs!c_codmoneda = AppConfig.MonedaPred
        mRs!n_factor = AppConfig.FactorMon
        mRs!c_coddenomina = "Efectivo"
        mRs!c_codbanco = ""
        mRs!C_NUM_CHEQUE = ""
        mRs!n_cantidad = pMontoPago
        mRs!n_monto = pMontoPago
        mRs!cs_codlocalidad = AppConfig.Localidad
    mRs.Update

End Sub

'********************************************  ventas  ***********************************************************+
Public Sub GrabarVenta(mLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String, pCadena As String, pIdtr As String, Optional correlativo As String)
    Dim mCorrelativo As String
    Dim mInicio As Boolean
    Dim temp As String
    
    On Error GoTo Errores
    AppConfig.ConexionStellar.BeginTrans: mInicio = True
    
    If correlativo = "" Then
        mCorrelativo = BuscarCorrelativo(AppConfig.ConexionStellar, "Factura")
    Else
        mCorrelativo = Format(correlativo, "000000000")
        temp = ActualizarCorrelativo("Factura", CStr(Int(mCorrelativo)))
    End If
    
    GrabarMAVentas AppConfig.ConexionStellar, mCorrelativo, mLinea, pCliente, pCodConcepto
    If (mLinea.Impuesto > 0) Then
       GrabarImpuestos AppConfig.ConexionStellar, mLinea, pCliente, mCorrelativo, "VEN"
       GrabarImpuestos AppConfig.ConexionStellar, mLinea, pCliente, mLinea.Documento, "VEN", False
    End If
    GrabarCxC AppConfig.ConexionStellar, mLinea, mLinea.Documento, mCorrelativo, pCliente, "VEN", 0, pCodConcepto, False, "FACTURA  No." & mCorrelativo
    'GrabarImpuestos AppConfig.ConexionStellar, mLinea, pCliente, mLinea.Documento, "VEN", False
    AppConfig.ConexionStellar.CommitTrans
    
    Exit Sub
Errores:
    If mInicio Then AppConfig.ConexionStellar.RollbackTrans
    Debug.Print Err.Description
    'GrabarTR_Agente_Log pIdtr, pCadena, eTipoLog.etlError, Err.Description
    GrabarTR_Agente_Log NumCorrida, pCadena, eTipoLog.etlError, Err.Description
End Sub
   
 Public Sub GrabarMAVentas(pCn As ADODB.Connection, pCorrelativo As String, mLinea As clsLineaArchivo, pCliente As String, pCodConcepto As String)
    
    Dim RsFacturas As ADODB.Recordset
    Set RsFacturas = New ADODB.Recordset
    Dim RifClienteStellar As String, DireccionClienteStellar As String, TelefonoClienteStellar As String
    Dim mRs As New ADODB.Recordset
    
    mRs.Open "SELECT * FROM MA_CLIENTES WHERE c_CODCLIENTE = '" & pCliente & "'", AppConfig.ConexionStellar, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        RifClienteStellar = mRs!c_rif
        DireccionClienteStellar = mRs!c_DIRECCION
        TelefonoClienteStellar = mRs!c_TELEFONO
    End If
    mRs.Close
    
    RsFacturas.Open "SELECT * FROM MA_VENTAS WHERE 1=2", pCn, adOpenStatic, adLockOptimistic, adCmdText
    RsFacturas.AddNew
        RsFacturas!cs_ORGANIZACION = ""
        RsFacturas!C_Documento = pCorrelativo
        RsFacturas!c_concepto = mLinea.Concepto
        RsFacturas!d_FECHA = mLinea.Fecha
        RsFacturas!c_descripcion = BuscarDescriCliente(pCn, pCliente)
        RsFacturas!c_DIRECCION = DireccionClienteStellar
        RsFacturas!c_rif = RifClienteStellar 'mLinea.Cliente
        RsFacturas!c_nit = ""
        RsFacturas!c_TELEFONO = TelefonoClienteStellar
        RsFacturas!c_status = "DCO"
        RsFacturas!c_codcliente = pCliente
        RsFacturas!c_codlocalidad = AppConfig.Localidad
        RsFacturas!c_codmoneda = AppConfig.MonedaPred
        RsFacturas!n_FACTORCAMBIO = AppConfig.FactorMon
        RsFacturas!n_DESCUENTO = 0
        RsFacturas!c_observacion = ""
        RsFacturas!C_Relacion = mLinea.Documento
        RsFacturas!NS_BASE_RETENCION = 0
        RsFacturas!NS_RETENCION = 0
        RsFacturas!n_subtotal = mLinea.Subtotal
        RsFacturas!n_impuesto = mLinea.Impuesto
        RsFacturas!NU_MONTO_SERVICIO = 0
        RsFacturas!n_total = mLinea.Total
        RsFacturas!NU_MONTO_CANCELADO = 0
        RsFacturas!C_CODDEPOSITO = AppConfig.Deposito 'new
        RsFacturas!n_baseimp = mLinea.Base
        RsFacturas!c_orden_compra = ""
        RsFacturas!CodConcepto = pCodConcepto
        RsFacturas!c_CODVENDEDOR = AppConfig.Usuario
        'new
        RsFacturas!cs_codlocalidad = AppConfig.Localidad
        RsFacturas.Update
    Exit Sub

 End Sub
Public Sub GrabarImpuestos(pCn As ADODB.Connection, mLinea As clsLineaArchivo, pCliente As String, pDoc As String, pConcepto As String, Optional pEsVenta As Boolean = True)
    
    Dim RsVentasImpuesto As New ADODB.Recordset
       
    RsVentasImpuesto.Open "SELECT * FROM MA_" & IIf(pEsVenta, "VENTAS", "CXC") & "_IMPUESTOS WHERE 1=2", pCn, adOpenStatic, adLockOptimistic, adCmdText
    RsVentasImpuesto.AddNew
        RsVentasImpuesto!cs_documento = pDoc
        RsVentasImpuesto!cs_TIPODOC = pConcepto
        RsVentasImpuesto!cs_codProveedor = pCliente
        RsVentasImpuesto!cs_codlocalidad = AppConfig.Localidad
        RsVentasImpuesto!ns_BaseImpuesto = mLinea.Base
        RsVentasImpuesto!ns_MontoImpuesto = mLinea.Impuesto
        If Not mLinea.Impuesto = 0 And mLinea.Base = 0 Then
            RsVentasImpuesto!ns_porcentajeimpuesto = FormatNumber(mLinea.Impuesto / mLinea.Base, 2) * 100
        Else
            RsVentasImpuesto!ns_porcentajeimpuesto = 0
        End If
    RsVentasImpuesto.Update
    
 End Sub
Private Function GrabarCxC(pCn As ADODB.Connection, pLinea As clsLineaArchivo, pDoc As String, pRel As String, pCliente As String, pConcepto As String, pMontoPago As Double, pCodConcepto As String, pPostRatear As Boolean, Optional pDetalle)
    
    Dim mRs As ADODB.Recordset
     
    Set mRs = New ADODB.Recordset
    mRs.Open "SELECT * FROM MA_CXC WHERE 1=2", pCn, adOpenDynamic, adLockOptimistic, adCmdText
    mRs.AddNew
      
        mRs!C_Documento = pDoc
         
        mRs!C_Relacion = pRel
        mRs!C_Codigo = pCliente
        mRs!C_Localidad = AppConfig.Localidad
        mRs!cs_codlocalidad = AppConfig.Localidad
        mRs!C_usuario = AppConfig.Usuario
        mRs!f_fechae = pLinea.Fecha
        mRs!f_fechav = pLinea.FechaV
        mRs!c_concepto = pConcepto
        mRs!c_observacion = "Factura migrada desde la interfaz Stellar - TbGold"
        
        If Not IsMissing(pDetalle) Then
            mRs!c_detalle = pDetalle
        End If
        If Not pPostRatear Then
            mRs!n_total = pLinea.Total
            If pMontoPago > 0 Then
                mRs!n_Pagado = mRs!n_total
                mRs!n_PagadoImp = pLinea.Impuesto
            End If
            mRs!n_impuesto = pLinea.Impuesto
            mRs!n_bimpuesto = pLinea.Base
            mRs!n_subtotal = pLinea.Subtotal
        Else
            mFactor = pMontoPago / pLinea.Total
            
            mRs!n_total = pLinea.Total * mFactor
            If pMontoPago > 0 Then
                mRs!n_Pagado = mRs!n_total
                mRs!n_PagadoImp = pLinea.Impuesto
            End If
            mRs!n_impuesto = pLinea.Impuesto
            mRs!n_bimpuesto = pLinea.Base
            mRs!n_subtotal = pLinea.Subtotal
        End If
        mRs!c_codmoneda = AppConfig.MonedaPred
        mRs!n_factor = AppConfig.FactorMon
        mRs!CodConcepto = pCodConcepto
        If pLinea.Concepto <> "VEN" And pLinea.Concepto <> "N_D" Then
            mRs!F_CANCELACION = pLinea.Fecha
        End If
        If pLinea.Concepto = "N_D" Or pLinea.Concepto = "N_C" Then
            mRs!cs_documento_ndc = pLinea.Documento
        End If
        mRs!ds_fecha_ret = pLinea.FechaComprobante
        mRs!cs_comprobante_ret = pLinea.NoComprobante
        mRs!ds_fechaemision_nota = pLinea.FechaComprobante
        'new
        mRs!cs_ncontrol = pLinea.NControl
        
        
    mRs.Update
    mRs.Close
    Set mRs = Nothing
      
End Function

Private Function BuscarCorrelativo(pCn As ADODB.Connection, pCampo As String) As String
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "SELECT * FROM MA_CORRELATIVOS WHERE (CU_Campo = '" & pCampo & "')"
    mRs.Open mSQl, AppConfig.ConexionStellar, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_valor = mRs!nu_valor + 1
        mRs.Update
        If (pCampo = "Factura") Then
            BuscarCorrelativo = Format(mRs!nu_valor, String(TamanoDocVentas, "0"))
        Else
            BuscarCorrelativo = Format(mRs!nu_valor, mRs!CU_FORMATO)
        End If
    Else
        mRs.AddNew
            mRs!cu_campo = pCampo
            mRs!nu_valor = 1
            mRs!CU_FORMATO = "000000000"
        mRs.Update
        BuscarCorrelativo = "000000001"
    End If
End Function

Private Function ActualizarCorrelativo(campo As String, Optional value As String)
     Dim mSQl As String
     
     If value = "" Then
        mSQl = "Update MA_CORRELATIVOS set NU_VALOR = NU_VALOR+'1' WHERE (CU_Campo='" & campo & "')"
        AppConfig.ConexionStellar.Execute mSQl, reg
     Else
        mSQl = "UPDATE MA_CORRELATIVOS SET NU_VALOR = '" & value & "' WHERE (CU_Campo='" & campo & "')"
        AppConfig.ConexionStellar.Execute mSQl, reg
     End If
    
End Function

Public Function GrabarMA_Agente_Log(pArchivo As String, pEquipo As String, pFecha As Date, pIdtr As String)
 'Public Function GrabarMA_Agente_Log(pArchivo As String, pEquipo As String, pFecha As Date, pIdtr As Long)
    Dim RsAgenteLog As New ADODB.Recordset
    
    RsAgenteLog.Open "SELECT * FROM MA_AGENTE_LOG WHERE 1=2", AppConfig.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
    RsAgenteLog.AddNew
     
    RsAgenteLog!archivo = pArchivo
    RsAgenteLog!equipo = pEquipo
    RsAgenteLog!FECHAHORA = pFecha
    'RsAgenteLog!idtr = pIdtr
    RsAgenteLog!idtr = NumCorrida
     
    RsAgenteLog.UpdateBatch
 End Function

Public Function GrabarTR_Agente_Log(pIdrel As String, pTraza As String, pTipo As Integer, pDescripcion As String)
 'Public Function GrabarTR_Agente_Log(pIdrel As Long, pTraza As String, pTipo As Integer, pDescripcion As String)
    Dim RsTrAgenteLog As New ADODB.Recordset
    
    RsTrAgenteLog.Open "SELECT * FROM TR_AGENTE_LOG WHERE 1=2", AppConfig.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
    RsTrAgenteLog.AddNew
     
    'RsTrAgenteLog!idrel = pIdrel
    RsTrAgenteLog!idrel = NumCorrida
    RsTrAgenteLog!traza = mId(pTraza, 1, 255)
    RsTrAgenteLog!tipo = pTipo
    RsTrAgenteLog!Descripcion = mId(pDescripcion, 1, 255)
     
    RsTrAgenteLog.UpdateBatch
    
    frmEjecucion.errorDetectado
 End Function
 
Private Function BuscarRelacionStellar(pCodigoRel As String, Optional pProceso) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    If IsMissing(pProceso) Then
        mSQl = "Select CODIGO from MA_CLIENTES_REL WHERE LTRIM(RELACION)='" & pCodigoRel & "' "
    Else
        mSQl = "Select CODIGO from MA_CONCEPTOS_REL WHERE LTRIM(RELACION)='" & pCodigoRel & "' AND IDPROCESO=" & pProceso
    End If
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, AppConfig.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarRelacionStellar = mRs!Codigo
    Else
        BuscarRelacionStellar = vbNullString
    End If
    
End Function

Private Function BuscarIdProceso(pConcepto As String) As Long
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
      
    Select Case pConcepto
       Case "N_D"
          pConcepto = "NDE"
       Case "N_C"
         pConcepto = "NDC"
    End Select
    
    mSQl = "Select idproceso from ma_procesos where tabla='MA_CXC' AND concepto='" & Replace(UCase(Trim(pConcepto)), "NDD", "NDE") & "'"
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, AppConfig.ConexionStellar, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarIdProceso = mRs!idproceso
    End If
End Function

Private Function BuscarDescriCliente(pCn As ADODB.Connection, pCliente As String) As String
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "SELECT C_DESCRIPCIO FROM MA_CLIENTES WHERE C_CODCLIENTE='" & pCliente & "'"
    Set mRs = New ADODB.Recordset
    mRs.CursorLocation = adUseClient
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    If Not mRs.EOF Then
        BuscarDescriCliente = mRs!C_DESCRIPCIO
    End If
End Function

Private Function BuscarSaldoDocumentoAfectado(pCn As ADODB.Connection, pCliente As String, pDocumento As String, pConcepto As String) As Double
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    Dim Documento As String
    Dim pagado As Double, Total As Double
     mSQl = "SELECT  * From MA_CxC " _
     & "where (c_codigo='" & pCliente & "') " _
     & "and (" & IIf(pConcepto = "N_D", "cs_documento_ndc", "C_Documento") & "='" & pDocumento & "') " _
     & "and (c_concepto='" & pConcepto & "')"
       
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    Do While Not mRs.EOF
       Documento = mRs!C_Documento
      pagado = mRs!n_Pagado
       Total = mRs!n_total
       mRs.MoveNext
    Loop
    BuscarSaldoDocumentoAfectado = Total - pagado
     
End Function

Private Function BuscarConceptoStellar(pConcepto) As String
    
    Dim mConcepto As String
    
    mConcepto = UCase(Trim(pConcepto))
    Select Case mConcepto
        Case "VEN", "PAG"
            BuscarConceptoStellar = mConcepto
        Case "NDC"
            BuscarConceptoStellar = "N_C"
        Case "NDD"
            BuscarConceptoStellar = "N_D"
        Case Else
            BuscarConceptoStellar = ""
    End Select
   
End Function
Private Function TamanoDocVentas() As Integer
    Dim RsFacturas As ADODB.Recordset
    Set RsFacturas = New ADODB.Recordset
    
    RsFacturas.Open "SELECT * FROM MA_VENTAS WHERE 1=2", AppConfig.ConexionStellar, adOpenStatic, adLockOptimistic, adCmdText
    TamanoDocVentas = RsFacturas.Fields("c_DOCUMENTO").DefinedSize
    RsFacturas.Close
    Set RsFacturas = Nothing
End Function
