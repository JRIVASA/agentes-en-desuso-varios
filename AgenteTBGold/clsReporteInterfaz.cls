VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsReporteInterfaz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Sub ImprimirReporteEjecuciones(correlativo As String)
    Dim queryResumen As String, queryDetail As String, queryShape As String
    Dim mRep As Variant
    Dim rsResumen As New ADODB.Recordset, rsDetail As New ADODB.Recordset, rsCorrida As New ADODB.Recordset
    Dim mCn As Object
    Set mCn = New ADODB.Connection
    
    If AbrirCnShape(mCn, AppConfig.BaseDatos) Then
    
        Call Apertura_Recordset(rsResumen)
        Call Apertura_Recordset(rsDetail)
        Call Apertura_Recordset(rsCorrida)
        
        Call REPO_CABE(drCorrida, "RESULTADO DE CORRIDAS", True)
        
        rsCorrida.Open "SELECT DISTINCT IDTR FROM MA_AGENTE_LOG WHERE IDTR = '" & correlativo & "'", AppConfig.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsCorrida.EOF Then
            drCorrida.Sections("enc_pag").Controls("lbl_corrida").Caption = "CORRIDA N�: " & rsCorrida!idtr
        End If
        
        queryResumen = "SELECT TIPO, CASE WHEN TIPO = '1' THEN 'CLIENTE NO RELACIONADO' WHEN TIPO = '2' THEN 'CONCEPTO NO RELACIONADO' WHEN TIPO = '6' THEN 'IVA MAYOR AL SUBTOTAL' WHEN TIPO = '7' THEN 'SUBTOTAL IGUAL A CERO' WHEN TIPO = '0' THEN 'FACTURA YA EXISTE' ELSE DESCRIPCION END AS DESCRIPCION, COUNT(*) AS cant, IDREL " & _
            "FROM TR_AGENTE_LOG WHERE IDREL = '" & correlativo & "' GROUP BY TIPO, CASE WHEN TIPO = '1' THEN 'CLIENTE NO RELACIONADO' WHEN TIPO = '2' THEN 'CONCEPTO NO RELACIONADO' WHEN TIPO = '6' THEN 'IVA MAYOR AL SUBTOTAL' WHEN TIPO = '7' THEN 'SUBTOTAL IGUAL A CERO' WHEN TIPO = '0' THEN 'FACTURA YA EXISTE' ELSE DESCRIPCION END, IDREL"
        
        queryDetail = "SELECT TIPO, TRAZA, DESCRIPCION, IDREL FROM TR_AGENTE_LOG WHERE IDREL = '" & correlativo & "'"
        
        queryShape = "Shape {" & queryResumen & "} Append ({" & queryDetail _
            & "} relate TIPO to TIPO) as Detalles"
        
        'rsResumen.Open queryResumen, AppConfig.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        rsResumen.Open queryShape, mCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        If Not rsResumen.EOF Then
            Set mRep = drCorrida
            Set mRep.DataSource = rsResumen
            mRep.Orientation = rptOrientPortrait
            mRep.Show vbModal
        Else
            MsgBox ("No hubo errores en la corrida seleccionada."), vbInformation
        End If
    
        Call Cerrar_Recordset(rsResumen)
        Call Cerrar_Recordset(rsDetail)
        Call Cerrar_Recordset(rsCorrida)
    End If
End Sub

Public Sub REPO_CABE(ByRef Reporte, Titulo, Optional pMayuscula As Boolean = True)
    On Error Resume Next
    Dim rs As New ADODB.Recordset
    Call Apertura_Recordset(rs)
    rs.Open "SELECT * FROM VAD10..ESTRUC_SIS", AppConfig.ConexionStellar, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Reporte.Caption = Titulo
    If Not rs.EOF Then
        Reporte.Sections("enc_pag").Controls("lbl_tit_rep").Caption = Titulo
        Reporte.Sections("enc_pag").Controls("lbl_empresa").Caption = rs!nom_org
        Reporte.Sections("enc_pag").Controls("lbl_direccion").Caption = rs!dir_org
        Reporte.Sections("enc_pag").Controls("lbl_telefonos").Caption = rs!tlf_org
        Reporte.Sections("enc_pag").Controls("lbl_ciudad_pais").Caption = rs!loc_org
        Reporte.Sections("enc_pag").Controls("lbl_emprif").Caption = "R.I.F.: " & rs!rif_org
        Reporte.Sections("enc_pag").Controls("lbl_tit_rep").Caption = IIf(pMayuscula, UCase(Titulo), Titulo)
    End If

    Call Cerrar_Recordset(rs)
    Err.Clear
End Sub

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    Rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If Rec.State = adStateOpen Then Rec.Close
    Set Rec = Nothing
End Sub

Public Function AbrirCnShape(ByRef pCn As Object, Optional pCatalogo As String = "", Optional esLibro As Boolean = False, Optional pServidor As String = "") As Boolean
    Dim mServidor As String
    
    On Error GoTo Errores
    If pServidor <> "" Then mServidor = AppConfig.Servidor
    If pCn.State = adStateOpen Then pCn.Close
    pCn.CommandTimeOut = AppConfig.CommandTimeOut
    pCn.ConnectionTimeOut = AppConfig.ConnectionTimeOut
    pCn.Open "Provider=MSDataShape.1;Data Provider=SQLOLEDB.1;" _
            & "user id=" & AppConfig.UserBD & ";initial catalog=" & AppConfig.BaseDatos _
            & ";Data Source=" & AppConfig.Servidor
            
'    With AppConfig
'        Set .Conexion = New ADODB.Connection
'        .Conexion.CommandTimeOut = .CommandTimeOut
'        .Conexion.ConnectionTimeOut = .ConnectionTimeOut
'        .Conexion.Open "provider=MSDataShape.1;initial catalog=" & .BaseDatos _
'                     & ";user id=" & .UserBD & ";password=" & .PwdBD & ";data source=" & .Servidor
'    End With
            
    AbrirCnShape = True
    Exit Function
            
Errores:
    MsgBox "Error: " & Err.Description, vbCritical, "Abriendo Conexion. Servidor(" & IIf(Trim(mServidor) <> "", Trim(mServidor), SRV_LOCAL) & ")"
    AbrirCnShape = False
End Function
