Attribute VB_Name = "modTbGold"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long

Private Declare Function GetComputerName Lib "kernel32" _
Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As _
Long) As Long

Type EstrucApp
    Conexion                                                    As ADODB.Connection
    ConexionStellar                                             As ADODB.Connection
    Servidor                                                    As String
    ServidorStellar                                             As String
    UserBD                                                      As String
    UserBDStellar                                               As String
    PwdBD                                                       As String
    PwdBDStellar                                                As String
    BaseDatos                                                   As String
    ArchivoConfig                                               As String
    ArchivoError                                                As String
    MostrarInterfaz                                             As Boolean
    CommandTimeOut                                              As Long
    ConnectionTimeOut                                           As Long
    RutaArchivos                                                As String
    Separador                                                   As String
    Localidad                                                   As String
    Deposito                                                    As String
    MonedaPred                                                  As String
    FactorMon                                                   As Double
    Usuario                                                     As String
    CodAnticipo                                                 As String
End Type

Public AppConfig                                                As EstrucApp
Global Const FechaError   As Date = "01/01/1900"

Public Function sgetini(sinifile As String, ssection As String, skey _
        As String, sdefault As String) As String
    Dim stemp As String * 256
    Dim nlength As Integer
    
    stemp = Space$(256)
    nlength = GetPrivateProfileString(ssection, skey, sdefault, stemp, 255, sinifile)
    sgetini = Left$(stemp, nlength)
End Function

Sub Main()
    Dim mCls As clsInterfaz
    
    If App.PrevInstance Then
        End
    End If
    
    On Error GoTo Errores
    
    If IniciarProceso Then
        If AppConfig.MostrarInterfaz Then
            Set mCls = New clsInterfaz
            mCls.IniciarInterfaz
            Set mCls = Nothing
        Else
            IniciarAgente
        End If
    End If
    
    'End
    If frmEjecucion.ErrorLabel.Visible = True Then frmEjecucion.reportButton.Visible = True
    Exit Sub
Errores:
    ProcesarError Err, "Main", "Inicio"
    frmEjecucion.ProgressBar1.value = "100"
    frmEjecucion.statusLabel.Caption = "FINALIZADO"
    If frmEjecucion.ErrorLabel.Visible = True Then frmEjecucion.reportButton.Visible = True
End Sub

Private Function IniciarProceso() As Boolean
    
    With AppConfig
        .ArchivoConfig = App.Path & "\Config.ini"
        .ArchivoError = App.Path & "\LogError.txt"
        .Servidor = sgetini(.ArchivoConfig, "Server", "Server", "")
        .ServidorStellar = sgetini(.ArchivoConfig, "Server", "ServerStellar", "")
        .UserBD = sgetini(.ArchivoConfig, "Server", "UserID", "sa")
        .UserBDStellar = sgetini(.ArchivoConfig, "Server", "UserIDStellar", "sa")
        .PwdBD = sgetini(.ArchivoConfig, "Server", "PWD", "")
        .PwdBDStellar = sgetini(.ArchivoConfig, "Server", "PWDStellar", "")
        .BaseDatos = sgetini(.ArchivoConfig, "Server", "BDGold", "VADGold")
        .CommandTimeOut = Val(sgetini(.ArchivoConfig, "Server", "CommandTimeOut", "15"))
        .ConnectionTimeOut = Val(sgetini(.ArchivoConfig, "Server", "ConnectionTimeOut", "30"))
        .RutaArchivos = sgetini(.ArchivoConfig, "Configuracion", "RUTAARCHIVO", "")
        .Separador = sgetini(.ArchivoConfig, "Configuracion", "SEPARADOR", "")
        .Localidad = sgetini(.ArchivoConfig, "Configuracion", "Localidad", "")
        .Deposito = sgetini(.ArchivoConfig, "Configuracion", "Deposito", "")
        .MostrarInterfaz = Val(sgetini(.ArchivoConfig, "Configuracion", "MostrarInterfaz", "")) = 1
        .CodAnticipo = Val(sgetini(.ArchivoConfig, "Configuracion", "ConceptoAnticipo", "16"))
        
    End With
    
    If CrearConexion Then
        If CrearConexionStellar Then
            AppConfig.Usuario = BuscarUsuarioPred()
             IniciarProceso = BuscarMonedaPredeterminada
        End If
    End If
    
End Function

Public Function CrearConexion() As Boolean
    
    On Error GoTo Errores
    With AppConfig
        Set .Conexion = New ADODB.Connection
        .Conexion.CommandTimeOut = .CommandTimeOut
        .Conexion.ConnectionTimeOut = .ConnectionTimeOut
        .Conexion.Open "provider=sqloledb.1;initial catalog=" & .BaseDatos _
                     & ";user id=" & .UserBD & ";password=" & .PwdBD & ";data source=" & .Servidor
        CrearConexion = True
    End With
    Exit Function
Errores:
    ProcesarError Err, "Creando Conexion", "modulo Inicio"

End Function

Public Function CrearConexionStellar() As Boolean
    
    On Error GoTo Errores
    With AppConfig
        Set .ConexionStellar = New ADODB.Connection
        .ConexionStellar.CommandTimeOut = .CommandTimeOut
        .ConexionStellar.ConnectionTimeOut = .ConnectionTimeOut
        .ConexionStellar.Open "provider=sqloledb.1;initial catalog=VAD10" _
                     & ";user id=" & .UserBDStellar & ";password=" & .PwdBDStellar & ";data source=" & .ServidorStellar
        CrearConexionStellar = True
    End With
    Exit Function
Errores:
    ProcesarError Err, "Creando Conexion Stellar", "modulo Inicio"

End Function

Public Function ProcesarError(pError As ErrObject, pRutina As String, pModulo As String)
    If AppConfig.MostrarInterfaz Then
        MsgBox pError.Description, vbCritical
    Else
        EscribirErrores pModulo, pRutina, pError
    End If
End Function

Public Function ReasignarPwdConnString(pConnString As String, pStellar As Boolean)

    Debug.Print pConnString
    
    Dim TempPassword As String
    
    If UCase(pConnString) Like "*PASSWORD=*" Or UCase(pConnString) Like "*PWD=*" Then
    
        'La conexi�n tiene los valores, no deber�a haber problemas al abrirla.
        
        ReasignarPwdConnString = pConnString
        
    Else
    
        If pStellar Then
            TempPassword = AppConfig.PwdBDStellar
        Else
            TempPassword = AppConfig.PwdBD
        End If
    
        ReasignarPwdConnString = pConnString & IIf(Right(pConnString, 1) <> ";", ";", "") & "Password=" & TempPassword & ";"
    
    End If
    
    Debug.Print ReasignarPwdConnString

End Function

Private Sub EscribirErrores(pModulo As String, pRutina As String, pError As ErrObject)
    Dim mCanal As Integer
    
    mCanal = FreeFile
        
    Open AppConfig.ArchivoError For Append Access Write As #mCanal
    Print #mCanal, Now & ", Modulo: " & pModulo & ", Rutina: " & pRutina & ", Error (" & pError.Number & ") " & pError.Description
    Close #mCanal
        
End Sub

Public Function BuscarArchivosActualizar(pRutaBusqueda As String, Optional pCorrelativo, Optional pMover As Boolean) As Collection
    Dim mArchivos As Collection
    Dim mArchivo As String
    
    Set mArchivos = New Collection
    
    If IsMissing(pCorrelativo) Then
        If pMover Then
            mArchivo = Dir(pRutaBusqueda & "\*.adtg")
        Else
            mArchivo = Dir(pRutaBusqueda & "\*pend*.adtg")
        End If
    Else
        If pMover Then
            mArchivo = Dir(pRutaBusqueda & "\*" & pCorrelativo & ".adtg")
        Else
            mArchivo = Dir(pRutaBusqueda & "\*pend*" & pCorrelativo & ".adtg")
        End If
    End If
    Do While mArchivo <> ""
        mArchivos.Add mArchivo
        mArchivo = Dir
    Loop
    Set BuscarArchivosActualizar = mArchivos
End Function

Public Sub MostrarEditorTexto(pFrm As Form, pGrd As Object, ByRef txtEditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, pKeyascii As Integer, Optional pNumerico As Boolean = False, Optional pAutotecla As Boolean = True)
    Dim mEscribio As Boolean
    
    If txtEditor.Visible Then Exit Sub
    txtEditor.Alignment = IIf(pNumerico, 1, 0)
    With pGrd

        .RowSel = .Row
        .ColSel = .Col
        A = .Left + .CellLeft
        b = .Top + .CellTop
        txtEditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - pFrm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - pFrm.ScaleY(1, vbPixels, vbTwips)
        txtEditor.Font = .Font
        txtEditor.Text = .Text
        If pAutotecla Then
            If pNumerico Then
                If IsNumeric(Chr(pKeyascii)) Then
                    txtEditor.Text = Chr(pKeyascii)
                    mEscribio = True
                End If
            Else
                If pKeyascii > 32 Then
                    txtEditor.Text = Chr(pKeyascii)
                    mEscribio = True
                End If
            End If
        End If
        
        txtEditor.Visible = True
        txtEditor.ZOrder
        txtEditor.SetFocus
        If mEscribio Then
            txtEditor.SelStart = Len(txtEditor)
        Else
            txtEditor.SelStart = 0
            txtEditor.SelLength = Len(txtEditor)
        End If
            
        cellRow = .Row
        cellCol = .Col

     End With

End Sub

Private Sub IniciarAgente()
    Dim mArchivo As String
    Dim mTbGold As New clstbGold
    
    frmEjecucion.Show
    frmEjecucion.ProgressBar1.value = "25"
    frmEjecucion.statusLabel.Caption = "EN EJECUCI�N"
    
    mArchivo = Dir(AppConfig.RutaArchivos & "*.txt", vbArchive)
    VerificarCarpeta
    Do While mArchivo <> ""
       mTbGold.ProcesarArchivo mArchivo
       Name AppConfig.RutaArchivos & mArchivo As App.Path & "\procesados\" & mArchivo
       mArchivo = Dir
    Loop
    
    frmEjecucion.ProgressBar1.value = "100"
    frmEjecucion.statusLabel.Caption = "FINALIZADO"
End Sub

Private Sub VerificarCarpeta()
    If Dir(App.Path & "\procesados", vbDirectory) = "" Then
        MkDir App.Path & "\procesados"
    End If
End Sub

Public Function BuscarCorrelativo(pCn As ADODB.Connection, pCampo As String) As Long
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String
    
    mSQl = "Select * from ma_correlativos where cu_campo='" & pCampo & "' "
    mRs.Open mSQl, pCn, adOpenDynamic, adLockOptimistic, adCmdText
    If Not mRs.EOF Then
        mRs!nu_valor = mRs!nu_valor + 1
        mRs.Update
        BuscarCorrelativo = mRs!nu_valor
    End If
End Function

Public Function ObtenerNombreEquipo() As String
    Dim sEquipo As String * 255
    
    GetComputerName sEquipo, 255
    ObtenerNombreEquipo = Replace(sEquipo, Chr(0), "")
End Function

Private Function BuscarMonedaPredeterminada() As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    On Error GoTo Errores
    mSQl = "Select * from ma_monedas order by b_preferencia desc"
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, AppConfig.ConexionStellar, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        AppConfig.MonedaPred = mRs!c_codmoneda
        AppConfig.FactorMon = mRs!n_factor
        BuscarMonedaPredeterminada = True
    Else
        Err.Raise "9999999", , "No hay Moneda Predeterminada"
    End If
    Exit Function
Errores:
    ProcesarError Err, "BuscarMonedaPredeterminada", "Inicio"
    
End Function

Private Function BuscarUsuarioPred() As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    Dim Usuario As String
    On Error GoTo Errores
    mSQl = "Select * from ma_usuarios where codusuario like '999999%' order by codusuario desc"
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, AppConfig.ConexionStellar, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        Usuario = mRs!codusuario
                
    Else
        Err.Raise "9999999", , "No Existe el Usuario"
    End If
  BuscarUsuarioPred = Usuario
    Exit Function
Errores:
    ProcesarError Err, "BuscarUsuarioPrede", "Inicio"
    
End Function
