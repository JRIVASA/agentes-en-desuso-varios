VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsInterfaz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eEstadoForm
    efrmMain
    efrmClientes
    efrmConceptos
End Enum

Private mEstado                                                     As eEstadoForm
Private mItms                                                       As Collection

Property Get Estado() As eEstadoForm
    Estado = mEstado
End Property

Property Get Items() As Collection
    Set Items = mItms
End Property

Property Let Estado(pValor As eEstadoForm)
    mEstado = pValor
End Property

Public Sub IniciarItems()
    Set mItms = New Collection
End Sub

Public Sub IniciarInterfaz()
    mEstado = efrmMain
    IniciarItems
    Set frmMain.fInterfaz = Me
    frmMain.Show vbModal
    Set frmMain = Nothing
End Sub

Public Function VerificarCambios() As Boolean
    
    If ExistenCambios Then
        'If MsgBox("Existen Cambios Desea Guardarlos S/N", vbYesNo) = vbYes Then
        If MsgBox("Ha realizado cambios en la interfaz." & vbNewLine & " �Desea guardarlos?", vbYesNo) = vbYes Then
            VerificarCambios = GrabarCambios
            Exit Function
        End If
    End If
    VerificarCambios = True
   
End Function

Private Function CambiarEstadoItms() As Boolean
    Dim mItm As clsRelacionStellar
    Dim nItm As Long
    
    For Each mItm In mItms
        nItm = nItm + 1
        If mItm.TipoCambio = etcActualiza Or mItm.TipoCambio = etcNuevo Then
            mItm.TipoCambio = etcNinguno
        ElseIf mItm.TipoCambio = etcEliminar Then
            mItms.Remove (nItm)
        End If
    Next
    CambiarEstadoItms = True
End Function

Private Function ExistenCambios() As Boolean
    
    Dim mItm As clsRelacionStellar
    
    For Each mItm In mItms
        If mItm.TipoCambio <> etcNinguno Then
            ExistenCambios = True
            Exit Function
        End If
    Next
    
End Function

Public Function GrabarCambios() As Boolean
    Dim mItm As clsRelacionStellar
    Dim mSQl As String
    
    On Error GoTo Errores
    If ValidarDatos Then
        AppConfig.Conexion.BeginTrans
        For Each mItm In mItms
            Select Case mItm.TipoCambio
                Case etcActualiza
                    mSQl = SqlActualizar(mItm)
                Case etcNuevo
                    mSQl = SqlNuevo(mItm)
                Case etcEliminar
                    mSQl = SqlEliminar(mItm)
            End Select
            If mSQl <> "" Then AppConfig.Conexion.Execute mSQl
        Next
        AppConfig.Conexion.CommitTrans
        GrabarCambios = CambiarEstadoItms
    End If
    Exit Function
Errores:
    AppConfig.Conexion.RollbackTrans
    ProcesarError Err, "GrabarCambios", "clsInterfaz"
    Err.Clear
End Function

Private Function ValidarDatos() As Boolean
    Dim mItm As clsRelacionStellar
    Dim mSQl As String
    
    On Error GoTo Errores
    
    For Each mItm In mItms
        Select Case mItm.TipoCambio
            Case etcActualiza, etcNuevo
                If Trim(mItm.Codigo) = "" Or Trim(mItm.Relacion) = "" Then
                    MsgBox "No se puede guardar el item: " & vbNewLine & Chr(34) & mItm.Descripcion & Chr(34) & vbNewLine & "Por favor, revise sus datos.", vbExclamation
                    Exit Function
                End If
                
            Case Else
        End Select
    Next
    ValidarDatos = True
   
    Exit Function
Errores:
    AppConfig.Conexion.RollbackTrans
    ProcesarError Err, "GrabarCambios", "clsInterfaz"
    Err.Clear
End Function

Public Sub CargarItems(pConceptoActual As Long)
    Dim mRs As ADODB.Recordset
    Dim mItm As clsRelacionStellar
    Dim mSQl As String
    
    On Error GoTo Errores
    mSQl = SqlDatos(pConceptoActual)
    Set mRs = New ADODB.Recordset
    mRs.CursorLocation = adUseClient
    mRs.Open mSQl, AppConfig.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    mRs.Sort = "Codigo"
    Do While Not mRs.EOF
        Set mItm = New clsRelacionStellar
        mItms.Add mItm.AddItmRelacion(AppConfig.ConexionStellar, mRs!Codigo, mRs!Relacion, mEstado, mRs!idproceso)
        mRs.MoveNext
    Loop
    Exit Sub
Errores:
    IniciarItems
    ProcesarError Err, "CargarItems", "clsInterfaz"
    Err.Clear
End Sub

Private Function SqlDatos(Optional pIdProceso As Long) As String
    Select Case mEstado
        Case efrmClientes
            SqlDatos = "Select codigo,relacion,0 as idproceso from ma_clientes_rel "
        Case efrmConceptos
            SqlDatos = "Select codigo,relacion, idproceso from ma_conceptos_rel where idproceso= " & pIdProceso
    End Select
End Function

Private Function SqlActualizar(pItm As clsRelacionStellar) As String
    Select Case mEstado
        Case efrmClientes
            SqlActualizar = "update ma_clientes_rel set codigo='" & pItm.Codigo & "',relacion='" & pItm.Relacion & "' where codigo='" & pItm.CodigoOriginal & "' "
        Case efrmConceptos
            SqlActualizar = "update ma_conceptos_rel set codigo='" & pItm.Codigo & "',relacion='" & pItm.Relacion & "' where codigo='" & pItm.CodigoOriginal & "' and idproceso=" & pItm.RelacionProc
            
    End Select
End Function

Private Function SqlNuevo(pItm As clsRelacionStellar) As String
    Select Case mEstado
        Case efrmClientes
            SqlNuevo = "Insert into  ma_clientes_rel (codigo,relacion) values " _
                & "('" & pItm.Codigo & "','" & pItm.Relacion & "')"
        Case efrmConceptos
              SqlNuevo = "Insert into  ma_conceptos_rel (codigo,relacion, idproceso) values " _
                & "('" & pItm.Codigo & "','" & pItm.Relacion & "'," & pItm.RelacionProc & ")"
    End Select
End Function

Private Function SqlEliminar(pItm As clsRelacionStellar) As String
    Select Case mEstado
        Case efrmClientes
            SqlEliminar = "delete from ma_clientes_rel where codigo='" & pItm.CodigoOriginal & "' "
        Case efrmConceptos
            SqlEliminar = "delete from ma_conceptos_rel where codigo='" & Val(pItm.CodigoOriginal) & "' and idproceso=" & Val(pItm.RelacionProc)
            
    End Select
End Function
