VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmEjecucion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "isAGENT_TBGOLD"
   ClientHeight    =   2175
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2175
   ScaleWidth      =   4470
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton reportButton 
      Caption         =   "Visualizar Reporte"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1080
      TabIndex        =   3
      Top             =   1680
      Visible         =   0   'False
      Width           =   2295
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   735
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   1296
      _Version        =   393216
      Appearance      =   0
   End
   Begin VB.Label ErrorLabel 
      Alignment       =   2  'Center
      Caption         =   "ERRORES DETECTADOS"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Visible         =   0   'False
      Width           =   3975
   End
   Begin VB.Label statusLabel 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   3975
   End
End
Attribute VB_Name = "frmEjecucion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    ErrorLabel.Visible = False
End Sub

Public Sub errorDetectado()
    ErrorLabel.Visible = True
End Sub

Private Sub reportButton_Click()
    Dim mReporteInterfaz As New clsReporteInterfaz
    mReporteInterfaz.ImprimirReporteEjecuciones (NumCorrida)
End Sub
