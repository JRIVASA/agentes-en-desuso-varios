VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Principal"
   ClientHeight    =   6825
   ClientLeft      =   150
   ClientTop       =   135
   ClientWidth     =   5805
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6825
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   240
      Top             =   6240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":05A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0B0C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":10A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1640
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tb 
      Align           =   1  'Align Top
      Height          =   570
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   1005
      ButtonWidth     =   1958
      ButtonHeight    =   1005
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Clientes"
            Key             =   "clientes"
            Object.ToolTipText     =   "Relacionar Clientes Tecla Acceso F3"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "C&onceptos"
            Key             =   "conceptos"
            Object.ToolTipText     =   "Relacionar Conceptos Tecla Acceso F5"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "grabar"
            Object.ToolTipText     =   "Grabar Tecla Acceso F4"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "sep"
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "salir"
            Object.ToolTipText     =   "Salir Tecla Acceso F12"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "reporte"
            Object.ToolTipText     =   "Visualizar Reporte Tecla Acceso F4"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   6135
      Left            =   30
      TabIndex        =   0
      Top             =   600
      Width           =   5775
      Begin VB.TextBox txtedit 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   2160
         TabIndex        =   5
         Top             =   1440
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   0
         TabIndex        =   2
         Top             =   5400
         Width           =   4095
         Begin VB.ComboBox cboSubProceso 
            Height          =   315
            Left            =   120
            Style           =   2  'Dropdown List
            TabIndex        =   3
            Top             =   240
            Width           =   3855
         End
         Begin VB.Label lblSubProceso 
            Caption         =   "Proceso CxC"
            Height          =   255
            Left            =   120
            TabIndex        =   4
            Top             =   0
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grid 
         Height          =   5175
         Left            =   60
         TabIndex        =   1
         Top             =   240
         Width           =   5655
         _ExtentX        =   9975
         _ExtentY        =   9128
         _Version        =   393216
         FixedCols       =   0
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum eBotonToolbar
    etbClientes = 1
    etbConceptos
    etbGrabar
    etbSalir = 5
    etbReporte = 6
End Enum

Dim mReporteInterfaz As New clsReporteInterfaz
Public fInterfaz As clsInterfaz
Private mRowCell As Long, mColCell As Long
Private mConceptoActual As Long
Private mItmSel As clsRelacionStellar

Private Sub cboSubProceso_Click()
   
    If fInterfaz.Estado = efrmConceptos And mConceptoActual <> cboSubProceso.ItemData(cboSubProceso.ListIndex) Then
        
        If fInterfaz.VerificarCambios Then
             fInterfaz.IniciarItems
             mConceptoActual = cboSubProceso.ItemData(cboSubProceso.ListIndex)
             fInterfaz.CargarItems mConceptoActual
             IniciarGrid
             CargarGrid
        Else
            cboSubProceso.ListIndex = BuscarIndice(mConceptoActual)
        End If
        
    End If
    
End Sub

Private Function BuscarIndice(pConcepto As Long)
    For i = 0 To cboSubProceso.ListCount - 1
        If cboSubProceso.ItemData(i) = pConcepto Then
            cboSubProceso.ListIndex = i
            Exit Function
        End If
    Next
End Function

Private Sub Clientes()
   CambiarEstadoForm efrmClientes
   fInterfaz.CargarItems 0
   CargarGrid
End Sub

Private Sub cmdAceptar()
    If fInterfaz.GrabarCambios Then
        cmdCancelar
    End If
End Sub

Private Sub cmdCancelar()
    If fInterfaz.VerificarCambios Then
        CambiarEstadoForm efrmMain
        IniciarGrid
        IniciarCombo
    End If
End Sub

Private Sub Conceptos()
    CambiarEstadoForm efrmConceptos
    fInterfaz.CargarItems cboSubProceso.ItemData(cboSubProceso.ListIndex)
    mConceptoActual = cboSubProceso.ItemData(cboSubProceso.ListIndex)
    IniciarGrid
    CargarGrid
End Sub

Private Sub Reporte()

    Dim mObj As Object
    Dim mConsulta As String, mCorrelativo As String, mResultado As Variant
    
    Set mObj = CreateObject("recsun.obj_busqueda")
    
    mConsulta = "SELECT IDTR, FECHAHORA, ARCHIVO, EQUIPO FROM MA_AGENTE_LOG"
    mObj.Inicializar mConsulta, "CORRIDAS DEL AGENTE", ReasignarPwdConnString(AppConfig.Conexion.ConnectionString, True)
    mObj.Add_Campos "CODIGO", "IDTR", 1000, 0
    mObj.Add_Campos "FECHA", "FECHAHORA", 2700, 0
    mObj.Add_Campos "ARCHIVO", "ARCHIVO", 3500, 0
    mObj.Add_Campos "EQUIPO", "EQUIPO", 2500, 0
    mObj.Add_CamposBusqueda "Codigo", "IDTR"
    mObj.Add_CamposBusqueda "Fecha", "FECHAHORA"
    mObj.Add_CamposBusqueda "Equipo", "EQUIPO"
    mObj.Add_CamposBusqueda "Archivo", "ARCHIVO"
    mResultado = mObj.EJECUTAR
    If Not IsEmpty(mResultado) Then
        mCorrelativo = mResultado(0)
        Call mReporteInterfaz.ImprimirReporteEjecuciones(mCorrelativo)
    End If
    Set mObj = Nothing
    Me.Refresh
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF3
            'clientes
            If ValidarBoton(etbClientes) Then
                Clientes
            End If
        Case vbKeyF4
            If ValidarBoton(etbGrabar) Then
                cmdAceptar
            End If
        Case vbKeyF5
            If ValidarBoton(etbConceptos) Then
                Conceptos
            End If
        Case vbKeyF12
            If fInterfaz.Estado = efrmMain Then
                Unload Me
            Else
                cmdCancelar
            End If
        Case vbKeyReturn
            If Not Me.ActiveControl Is Nothing Then
                If ActiveControl.Name <> Me.grid.Name Then
                    SendKeys Chr(vbKeyTab)
                End If
            End If
        Case vbKeyF8
            If ValidarBoton(etbReporte) Then
                Reporte
            End If
    End Select
            
End Sub

Private Sub Form_Load()
    cmdCancelar
End Sub

Private Sub grid_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF2
            If grid.Col = 0 Then
                Set mItmSel = SeleccionarItm(grid.RowData(grid.Row))
                BuscarRelacionStellar grid.Row
            End If
        Case vbKeyDelete
            Set mItmSel = SeleccionarItm(grid.RowData(grid.Row))
            If MsgBox("�Est� seguro de eliminar el item: " & vbNewLine & Chr(34) & mItmSel.Descripcion & Chr(34) & "?", vbYesNo) = vbYes Then
                If grid.Rows - 1 > 1 Then
                    grid.RemoveItem grid.Row
                Else
                    IniciarGrid
                End If
                mItmSel.TipoCambio = etcEliminar
            End If
    End Select
End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If grid.Col = 1 Then
        Set mItmSel = SeleccionarItm(grid.RowData(grid.Row))
        If Not mItmSel Is Nothing Then
            MostrarEditorTexto Me, Me.grid, txtedit, mRowCell, mColCell, KeyAscii
        End If
    End If
End Sub


'************************************************************************************************************************************************************

Private Sub CambiarEstadoForm(pEstado As eEstadoForm)
    fInterfaz.Estado = pEstado
    fInterfaz.IniciarItems
    Me.Caption = BuscarCaptionForm(pEstado)
    tb.Buttons(eBotonToolbar.etbClientes).Visible = pEstado = efrmMain
    tb.Buttons(eBotonToolbar.etbConceptos).Visible = pEstado = efrmMain
    tb.Buttons(eBotonToolbar.etbGrabar).Visible = pEstado <> efrmMain
    Frame1.Visible = pEstado <> efrmMain
    Frame2.Visible = pEstado = efrmConceptos
    If pEstado <> efrmMain Then IniciarGrid
    mConceptoActual = 0
End Sub

Private Function BuscarCaptionForm(pEstado As eEstadoForm)
    Select Case pEstado
        Case efrmClientes
            BuscarCaptionForm = "Interfaz Stellar-TBGold (Clientes)"
        Case efrmConceptos
            BuscarCaptionForm = "Interfaz Stellar-TBGold (Conceptos)"
        Case Else
            BuscarCaptionForm = "Interfaz Stellar-TBGold"
    End Select
End Function

Private Sub IniciarGrid()
    grid.Clear
    grid.Rows = 2
    grid.Cols = 2
    grid.ColWidth(0) = 3500
    grid.ColWidth(1) = 1800
    grid.TextMatrix(0, 0) = IIf(fInterfaz.Estado = efrmClientes, "Clientes", "Concepto STELLAR")
    grid.TextMatrix(0, 1) = "Codigo TBGOLD"
    grid.Row = 0
    grid.Col = 0: grid.CellAlignment = 4
    grid.Col = 1: grid.CellAlignment = 4
    grid.Row = 1: grid.Col = 0
End Sub

Private Sub IniciarCombo()
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    cboSubProceso.Clear
    mSQl = "SELECT IDProceso, Proceso " _
        & "From MA_PROCESOS WHERE (Tabla = 'MA_CXC') ORDER BY IDProceso"
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, AppConfig.ConexionStellar, adOpenForwardOnly, adLockReadOnly, adCmdText
    Do While Not mRs.EOF
        cboSubProceso.AddItem mRs!proceso
        cboSubProceso.ItemData(cboSubProceso.NewIndex) = mRs!idproceso
        mRs.MoveNext
    Loop
    cboSubProceso.ListIndex = 0
End Sub

Private Sub BuscarRelacionStellar(pFila As Long)
    
    Dim mItm As clsRelacionStellar
    Dim mSQl As String, mTitulo As String
    Dim mId As Long
    Dim mResul As Variant
    
    
    If fInterfaz.Estado = efrmClientes Then
        BuscarClienteRelacion pFila
    Else
        BuscarConceptoRelacion pFila
    End If
    
End Sub

Private Sub BuscarClienteRelacion(pFila As Long)
    Dim mBusqueda As Object
    Dim mItm As clsRelacionStellar
    Dim mSQl As String
    Dim mIdRel As Long
    
    mSQl = "Select c_codcliente,c_descripcio from ma_clientes "
    Set mBusqueda = CreateObject("recsun.obj_busqueda")
    mBusqueda.Inicializar mSQl, "Clientes", ReasignarPwdConnString(AppConfig.ConexionStellar.ConnectionString, True)
    mBusqueda.Add_Campos "Codigo", "c_codcliente", 1500, 0
    mBusqueda.Add_Campos "Descripcion", "c_descripcio", 3500, 0
    mBusqueda.Add_CamposBusqueda "Codigo", "c_codcliente"
    mBusqueda.Add_CamposBusqueda "Descripcion", "c_descripcio"
    mResul = mBusqueda.EJECUTAR
    If Not IsEmpty(mResul) Then
        If ExisteEnGrid(mResul(0), pFila) Then Exit Sub
        If mItmSel Is Nothing Then
            Set mItm = New clsRelacionStellar
            fInterfaz.Items.Add mItm.NuevoItem(CStr(mResul(0)), CStr(mResul(1)), 0)
            mIdRel = fInterfaz.Items.Count
        Else
            Set mItm = mItmSel
            mItm.Codigo = mResul(0)
            mItm.Descripcion = mResul(1)
            mItm.TipoCambio = etcActualiza
            mIdRel = grid.RowData(pFila)
        End If
        CargarLineaGrid pFila, mItm, mIdRel
    End If
End Sub

Private Sub BuscarConceptoRelacion(pFila As Long)
    Dim mBusqueda As Object
    Dim mItm As clsRelacionStellar
    Dim mSQl As String
    Dim mIdRel As Long
    
    On Error GoTo Errores
    mSQl = "Select idconcepto,descripcion from ma_conceptos where idproceso= " & Me.cboSubProceso.ItemData(cboSubProceso.ListIndex)
    Set mBusqueda = CreateObject("recsun.obj_busqueda")
    mBusqueda.Inicializar mSQl, "Conceptos", ReasignarPwdConnString(AppConfig.ConexionStellar.ConnectionString, True)
    mBusqueda.Add_Campos "Codigo", "idconcepto", 1500, 0
    mBusqueda.Add_Campos "Descripcion", "descripcion", 3500, 0
    mBusqueda.Add_CamposBusqueda "Codigo", "idconcepto"
    mBusqueda.Add_CamposBusqueda "Descripcion", "descripcion"
    mResul = mBusqueda.EJECUTAR
    If Not IsEmpty(mResul) Then
        If ExisteEnGrid(mResul(0), pFila) Then Exit Sub
        If mItmSel Is Nothing Then
            Set mItm = New clsRelacionStellar
            fInterfaz.Items.Add mItm.NuevoItem(CStr(mResul(0)), CStr(mResul(1)), cboSubProceso.ItemData(cboSubProceso.ListIndex))
            mIdRel = fInterfaz.Items.Count
        Else
            Set mItm = mItmSel
            mItm.Codigo = mResul(0)
            mItm.Descripcion = mResul(1)
            mIdRel = grid.RowData(pFila)
        End If
        CargarLineaGrid pFila, mItm, mIdRel
    End If
    Exit Sub
Errores:
    MsgBox Err.Description, vbCritical
    Err.Clear
End Sub

Private Function SeleccionarItm(pIndice As Long) As clsRelacionStellar
    On Error Resume Next
    Set SeleccionarItm = fInterfaz.Items(pIndice)
End Function

Private Sub CargarGrid()
    Dim mItm As clsRelacionStellar
    Dim mId As Long, mFila As Long
    
    For Each mItm In fInterfaz.Items
        mId = mId + 1
        If Not mItm.TipoCambio = etcEliminar Then
            mFila = mFila + 1
            CargarLineaGrid mFila, mItm, mId
        End If
    Next
End Sub

Private Sub CargarLineaGrid(pFila As Long, pItm As clsRelacionStellar, pId As Long)
   ' If pFila > grid.Rows - 1 Then grid.Rows = grid.Rows + 1
    grid.TextMatrix(pFila, 0) = pItm.Descripcion
    grid.TextMatrix(pFila, 1) = pItm.Relacion
    grid.RowData(pFila) = pId
    If grid.Rows - 1 = pFila Then grid.Rows = grid.Rows + 1
End Sub

Private Sub salir_Click()
    Unload Me
End Sub

Private Sub tb_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "clientes"
            Form_KeyDown vbKeyF3, 0
        Case "conceptos"
            Form_KeyDown vbKeyF5, 0
        Case "grabar"
            Form_KeyDown vbKeyF4, 0
        Case "salir"
            Form_KeyDown vbKeyF12, 0
        Case "reporte"
            Form_KeyDown vbKeyF8, 0
    End Select
End Sub

Private Sub txtedit_LostFocus()
    If Not txtedit.Visible Then Exit Sub
    If Not mItmSel Is Nothing Then
        mItmSel.Relacion = txtedit.Text
        If mItmSel.TipoCambio = etcNinguno Then mItmSel.TipoCambio = etcActualiza
        CargarLineaGrid mRowCell, mItmSel, grid.RowData(mRowCell)
    End If
    txtedit.Visible = False
End Sub

Private Function ExisteEnGrid(pCodigo, pFila As Long) As Boolean
    
    Dim mItm As clsRelacionStellar
    
    For mFila = 1 To grid.Rows - 1
        If mFila <> pFila Then
            Set mItm = SeleccionarItm(grid.RowData(mFila))
            If Not mItm Is Nothing Then
                If Trim(UCase(mItm.Codigo)) = Trim(UCase(pCodigo)) Then
                    MsgBox "El c�digo ya est� relacionado.", vbExclamation
                    ExisteEnGrid = True
                    Exit Function
                End If
            End If
        End If
    Next mFila
        
End Function

Private Function ValidarBoton(pBoton) As Boolean
    ValidarBoton = tb.Buttons(pBoton).Visible
End Function
