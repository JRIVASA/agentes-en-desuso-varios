VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCodigoDescripcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eTipoCambio
    etcNinguno
    etcNuevo
    etcActualiza
    etcEliminar
End Enum

Private mvarCodigo                                          As String
Private mvarDescripcion                                     As String
Private mvarTipoCambio                                      As eTipoCambio


Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Get TipoCambio() As eTipoCambio
    TipoCambio = mvarTipoCambio
End Property
'******************************************************************************************************************
Property Let Codigo(pValor As String)
    mvarCodigo = pValor
End Property

Property Let Descripcion(pValor As String)
    mvarDescripcion = pValor
End Property

Property Let TipoCambio(pValor As eTipoCambio)
    mvarTipoCambio = pValor
End Property


Public Function AgregarItem(pCodigo As String, pDescripcion As String, pTipoCambio As eTipoCambio) As clsCodigoDescripcion
    mvarCodigo = pCodigo
    mvarDescripcion = pDescripcion
    mvarTipoCambio = pTipoCambio
    Set agregaritm = Me
End Function
