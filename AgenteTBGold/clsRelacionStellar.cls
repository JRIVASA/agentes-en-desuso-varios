VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRelacionStellar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eTipoCambio
    etcNinguno
    etcNuevo
    etcActualiza
    etcEliminar
End Enum

Private mvarCodigo                                          As String
Private mvarCodigoOriginal                                  As String
Private mvarDescripcion                                     As String
Private mvarRelacion                                        As String
Private mvarRelacionProc                                    As Integer
Private mvarTipoCambio                                      As eTipoCambio

Private rs As ADODB.Recordset

Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Get Relacion() As String
    Relacion = mvarRelacion
End Property

Property Get RelacionProc() As String
    RelacionProc = mvarRelacionProc
End Property

Property Get TipoCambio() As eTipoCambio
    TipoCambio = mvarTipoCambio
End Property

Property Get CodigoOriginal() As String
    CodigoOriginal = mvarCodigoOriginal
End Property

Property Let Codigo(pValor As String)
    mvarCodigo = pValor
End Property

Property Let Descripcion(pValor As String)
    mvarDescripcion = pValor
End Property

Property Let RelacionProc(pValor As String)
    mvarRelacionProc = pValor
End Property

Property Let Relacion(pValor As String)
    mvarRelacion = pValor
End Property

Property Let TipoCambio(pValor As eTipoCambio)
    mvarTipoCambio = pValor
End Property

Public Function NuevoItem(pCodigo As String, pDescripcion As String, pIdProceso As Long) As clsRelacionStellar
    mvarCodigo = pCodigo
    mvarDescripcion = pDescripcion
    mvarTipoCambio = etcNuevo
    mvarRelacionProc = pIdProceso
    Set NuevoItem = Me
End Function

Public Function AddItmRelacion(pCnStellar As ADODB.Connection, pCodigoStellar As String, pCodigoRel As String, pTipoInterfaz As eEstadoForm, Optional pRelacionProc As Integer = 0) As clsRelacionStellar
    mvarCodigo = pCodigoStellar
    mvarCodigoOriginal = pCodigoStellar
    mvarRelacionProc = pRelacionProc
    mvarDescripcion = BuscarDescripcionStellar(pCnStellar, pTipoInterfaz)
    mvarRelacion = pCodigoRel
    
    mvarTipoCambio = etcNinguno
    Set AddItmRelacion = Me
End Function

Private Function BuscarDescripcionStellar(pCn As ADODB.Connection, pTipoInterfaz As eEstadoForm) As String
    Dim mRs As ADODB.Recordset
    Dim mSQl As String
    
    mSQl = BuscarSql(pTipoInterfaz)
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarDescripcionStellar = mRs!Descripcion
    End If
End Function

Private Function BuscarSql(pTipo As eEstadoForm) As String
    Select Case pTipo
        Case efrmClientes
            BuscarSql = "Select c_descripcio as descripcion from ma_clientes where c_codcliente='" & mvarCodigo & "' "
        Case efrmConceptos
            BuscarSql = "Select descripcion from ma_conceptos where idconcepto='" & mvarCodigo & "' and idproceso=" & mvarRelacionProc
    End Select
End Function
