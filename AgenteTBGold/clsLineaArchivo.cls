VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLineaArchivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarFecha                                                       As Date
Private mvarFechaV                                                      As Date
Private mvarCliente                                                     As String
Private mvarConcepto                                                    As String
Private mvarDocumento                                                   As String
Private mvarNControl                                                    As String
Private mvarSubtotal                                                    As Double
Private mvarBase                                                        As Double
Private mvarImpuesto                                                    As Double
Private mvarCodConcepto                                                 As Long
Private mvarDocRelacion                                                 As String
Private mvarConceptoRel                                                 As String
Private mvarNoComprobante                                               As String
Private mvarNControlComp                                                As String
Private mvarFechaComprobante                                            As Date

Property Get Fecha() As Date
    Fecha = mvarFecha
End Property

Property Get FechaV() As Date
    FechaV = mvarFechaV
End Property

Property Get Cliente() As String
    Cliente = mvarCliente
End Property

Property Get Concepto() As String
    Concepto = mvarConcepto
End Property

Property Get Documento() As String
    Documento = mvarDocumento
End Property

Property Get NControl() As String
    NControl = mvarNControl
End Property

Property Get Subtotal() As Double
    Subtotal = mvarSubtotal
End Property

Property Get Base() As Double
    Base = mvarBase
End Property

Property Get Impuesto() As Double
    Impuesto = mvarImpuesto
End Property

Property Get Total() As Double
    Total = mvarSubtotal + mvarImpuesto
End Property

Property Get CodConcepto() As Long
    CodConcepto = mvarCodConcepto
End Property

Property Get DocRelacion() As String
    DocRelacion = mvarDocRelacion
End Property

Property Get ConceptoRel() As String
    ConceptoRel = mvarConceptoRel
End Property

Property Get NoComprobante() As String
    NoComprobante = mvarNoComprobante
End Property

Property Get NControlComp() As String
    NControlComp = mvarNControlComp
End Property

Property Get FechaComprobante() As String
    FechaComprobante = mvarFechaComprobante
End Property

Property Let Fecha(pValor As Date)
    mvarFecha = pValor
End Property

Property Let FechaV(pValor As Date)
    mvarFechaV = pValor
End Property

Property Let Cliente(pValor As String)
    mvarCliente = pValor
End Property

Property Let Concepto(pValor As String)
    mvarConcepto = pValor
End Property

Property Let Documento(pValor As String)
    mvarDocumento = pValor
End Property

Property Let NControl(pValor As String)
    mvarNControl = pValor
End Property

Property Let Subtotal(pValor As Double)
    mvarSubtotal = pValor
End Property

Property Let Base(pValor As Double)
    mvarBase = pValor
End Property

Property Let Impuesto(pValor As Double)
    mvarImpuesto = pValor
End Property

Property Let CodConcepto(pValor As Long)
    mvarCodConcepto = pValor
End Property

Property Let DocRelacion(pValor As String)
    mvarDocRelacion = pValor
End Property

Property Let ConceptoRel(pValor As String)
    mvarConceptoRel = pValor
End Property

Property Let NoComprobante(pValor As String)
    mvarNoComprobante = pValor
End Property

Property Let NControlComp(pValor As String)
    mvarNControlComp = pValor
End Property

Property Let FechaComprobante(pValor As String)
    mvarFechaComprobante = pValor
End Property
