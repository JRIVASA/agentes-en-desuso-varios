VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPreCargaMasivaDiaria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Const MaxTemplateSize = 1024
Private ArrTemplates() As Byte
'Private ArrTemplateSizes() As Long
'Private ArrPersonasID() As String
Private NumRegistrosLote As Long

Private TemplateLote() As Variant
Private SizeLote() As Variant
Private IdLote() As Variant

Private Function ObtenerRegistros(pSql As String, pCn As ADODB.Connection) As ADODB.Recordset
    
    Dim pCnTemp As New ADODB.Connection
    
    pCnTemp.ConnectionTimeout = pCn.ConnectionTimeout
    
    pCnTemp.Open pCn.ConnectionString
    
    Dim mRs As New ADODB.Recordset
    
    pCnTemp.CommandTimeOut = 0
    
    mRs.Open pSql, pCnTemp, adOpenKeyset, adLockReadOnly, adCmdText
    
    If Not mRs.RecordCount = 0 Then
        Set ObtenerRegistros = mRs
    Else
        Set ObtenerRegistros = Nothing
    End If
    
    Exit Function
    
End Function

Public Sub IniciarInterfaz()

    On Error GoTo Err

    Dim mRs As ADODB.Recordset, mRsUpload As ADODB.Recordset
    Dim mSql As String
    Dim pUltimoIDProcesado As Long
    Dim Inicio As Boolean
    Dim LoteActual As Long
    
    Dim TransIniciada As Boolean
    
    EstrucApp.Conexion.Conexion.BeginTrans
    
    TransIniciada = True
    
    Inicio = True
    
    Dim TmpCount As Long
    
    Dim mTopSql As String
    mTopSql = IIf(EstrucApp.NumRegXLote <= 0, "", "Top (" & EstrucApp.NumRegXLote & ")")
    
    'ReDim TemplateLote(0) As Variant
    'ReDim SizeLote(0) As Variant
    'ReDim IdLote(0) As Variant
    
    Dim TmpArrSizes As String
    Dim TmpArrPersonas As String
    
    Do While Inicio Or Not (mRs Is Nothing)
    
        If Inicio Then
            mSql = "SELECT " & mTopSql & " ID, bin_DataArray, n_TemplateSize, c_CodCliente FROM MA_RACIONALIZACION_CLIENTES_HUELLAS ORDER BY ID ASC"
        Else
            mSql = "SELECT " & mTopSql & " ID, bin_DataArray, n_TemplateSize, c_CodCliente FROM MA_RACIONALIZACION_CLIENTES_HUELLAS WHERE (ID > (" & pUltimoIDProcesado & ")) ORDER BY ID ASC"
        End If
        
        Set mRs = ObtenerRegistros(mSql, EstrucApp.Conexion.Conexion)
        
        If Not mRs Is Nothing Then
        
            TmpCount = 0
        
            ReDim ArrTemplates(MaxTemplateSize - 1, mRs.RecordCount - 1) As Byte
            'ReDim ArrTemplateSizes(mRs.RecordCount - 1) As Long
            'ReDim ArrPersonasID(mRs.RecordCount - 1) As String
        
            Do While Not mRs.EOF
            
                TmpCount = TmpCount + 1
                
                For i = 0 To mRs!n_TemplateSize - 1
                    ArrTemplates(i, TmpCount - 1) = mRs!bin_DataArray(i)
                Next i
                
                TmpArrSizes = TmpArrSizes & IIf(TmpArrSizes = "", "", " ") & mRs!n_TemplateSize 'ArrTemplateSizes(i) = mRs!n_TemplateSize
                
                TmpArrPersonas = TmpArrPersonas & IIf(TmpArrPersonas = "", "", " ") & mRs!c_CodCliente 'ArrPersonasID(i) = mRs!c_CodCliente
                
                pUltimoIDProcesado = mRs!Id
                
                mRs.MoveNext
                
            Loop
            
            If Inicio Then
                EstrucApp.Conexion.Conexion.Execute ("Truncate Table TMP_RACIONALIZACION_CLIENTES_HUELLAS_PRECARGADAS")
            
                'TemplateLote(0) = ArrTemplates
                'SizeLote(0) = ArrTemplateSizes
                'IdLote(0) = ArrPersonasID
                
                Inicio = False
            Else
                
                'ReDim TemplateLote(UBound(TemplateLote) + 1) As Variant
                'ReDim SizeLote(UBound(SizeLote) + 1) As Variant
                'ReDim IdLote(UBound(IdLote) + 1) As Variant
                
                'TemplateLote(UBound(TemplateLote)) = ArrTemplates
                'SizeLote(UBound(SizeLote)) = ArrTemplateSizes
                'IdLote(UBound(IdLote)) = ArrPersonasID
                
            End If
            
            LoteActual = LoteActual + 1
            
            Set mRsUpload = New ADODB.Recordset
            
            mRsUpload.Open "SELECT * FROM TMP_RACIONALIZACION_CLIENTES_HUELLAS_PRECARGADAS WHERE 1 = 2", _
            EstrucApp.Conexion.Conexion, adOpenDynamic, adLockBatchOptimistic
            
            With mRsUpload
                .AddNew
                !n_Lote = LoteActual
                !bin_Templates = ArrTemplates
                !CU_Arr_TemplateSizes = TmpArrSizes
                !CU_Arr_Personas = TmpArrPersonas
                !d_FechaAct = Format(FechaServidor, "DD/MM/YYYY HH:mm:SS")
                .UpdateBatch
            End With
            
            TmpArrSizes = ""
            TmpArrPersonas = ""
            ReDim ArrTemplates(0, 0) As Byte
            
        End If
        
    Loop
    
    EstrucApp.Conexion.Conexion.CommitTrans
    
    Exit Sub
    
Err:

    Debug.Print Err.Description

    If TransIniciada Then EstrucApp.Conexion.Conexion.RollbackTrans

    EscribirLog "Generando Datos", Err.Description & vbNewLine & _
    "Ultimo ID Procesado: (" & pUltimoIDProcesado & ")", Err.Number

End Sub
