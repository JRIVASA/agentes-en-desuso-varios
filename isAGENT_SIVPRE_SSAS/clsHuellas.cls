VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsHuellas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Implements IXMLPropertyBag

Private Bytes() As Byte

Public Property Let SetBytes(ByRef sValue As Variant)

   Bytes = sValue

End Property

Public Property Get GetBytes() As Variant

   GetBytes = Bytes

End Property

Private Sub IXMLPropertyBag_ReadProperties(ByVal PropertyBag As vbalXMLPBag6.XMLPropertyBag)
    Bytes = PropertyBag.ReadProperty("Bytes", "")
 End Sub

 Private Sub IXMLPropertyBag_WriteProperties(ByVal PropertyBag As vbalXMLPBag6.XMLPropertyBag)
    Dim TmpVal() As Byte
    PropertyBag.WriteProperty "Bytes", Bytes, TmpVal
 End Sub
