VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsMantenimiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mRubroRac                                                                                                           As clsEstructuraRubro
Private mvarCn                                                                                                              As ADODB.Connection
Private mRubros                                                                                                             As Collection

Public Sub IniciarInterfaz()
    Set mvarCn = EstrucApp.Conexion.Conexion
    Call BuscarRubros(mvarCn)
    MantenimientoTblRacionalizacion mvarCn
End Sub

Private Sub BuscarRubros(pCn As ADODB.Connection)

    Dim mRs As ADODB.Recordset
    
    On Error GoTo Errores
    
    Set mRubros = New Collection
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open "ma_racionalizacion_rubros", pCn, adOpenForwardOnly, adLockReadOnly, adCmdTable
    
    Do While Not mRs.EOF
        Set mRubroRac = New clsEstructuraRubro
        If mRubroRac.AsignarRubroRs(mRs) Then
            mRubros.Add mRubroRac
        End If
        mRs.MoveNext
    Loop
    
    Exit Sub
    
Errores:
    
    EscribirLog "Buscando Rubros", Err.Description, Err.Number
    
End Sub


Private Sub MantenimientoTblRacionalizacion(pCn As ADODB.Connection)
    
    Dim mRubro As clsEstructuraRubro
    Dim mSql As String
    
    On Error GoTo Errores
    
    For Each mRubro In mRubros
        mSql = "Delete from tr_racionalizacion where (c_codrubro='" & mRubro.Rubro & "') " _
        & BuscarWhere(mRubro.Intervalo)
        
        pCn.Execute mSql, reg
    Next
    
    Exit Sub

Errores:
    
    EscribirLog "Eliminado Registros ", Err.Description, Err.Number
    
End Sub

Private Function BuscarWhere(pIntervalo As eintervaloRubroRac) As String
    If Not EstrucApp.SoloBorrarMensual Then
         Select Case pIntervalo
            Case eintervaloRubroDiaro
                BuscarWhere = " and  DATEDIFF(d,f_fechahora,GETDATE()) > 0 " ' si es mayor a 0 ya paso un dia y lo podemos eliminar
            Case eintervaloRubroInterDiaro
                 BuscarWhere = " and  DATEDIFF(d,f_fechahora,GETDATE()) > 1 " ' si es mayor a 1 pasaron dos dias y se pueden eliminar
            Case eintervaloRubroMensual
                BuscarWhere = " and  DATEDIFF(m,f_fechahora,GETDATE()) > 0 " ' si es mayor a 0 ya paso un mes y lo podemos eliminar
            Case eintervaloRubroQuincenal
                BuscarWhere = " and  DATEDIFF(d,f_fechahora,GETDATE()) > 15 " ' si es mayor a 15 pasaron 15 dias y pordemos eliminar
            Case eintervaloRubroSemanal
                BuscarWhere = " and  DATEDIFF(d,f_fechahora,GETDATE()) > 7 " ' si es mayor a siete paso una semana podemos eliminar
        End Select
    Else
        BuscarWhere = " and  DATEDIFF(m,f_fechahora,GETDATE()) > 0 "  ' si es mayor a 0 ya paso un mes y lo podemos eliminar
    End If
End Function

