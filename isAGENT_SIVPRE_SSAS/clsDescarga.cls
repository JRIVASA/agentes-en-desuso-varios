VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDescarga"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mProductos                                          As Collection
Private mArchivo                                            As String
Private mMinId                                              As Long
Private mMaxId                                              As Long

Public Sub IniciarInterfaz()

    Dim mCollectionProcesados As New Collection
    Dim mCollectionNoProcesados As New Collection
    Dim I As Long
    
    mArchivo = Dir(EstrucApp.Archivos.RutaArchivoDescarga & "\*.txt", vbArchive)
    
    Do While mArchivo <> ""
        mCollectionNoProcesados.Add mArchivo
        
        mArchivo = UCase(mArchivo)
        
        If Left(mArchivo, 1) = "G" Then
            If BuscarProductos(mArchivo) Then
                If InsertarProductos(mArchivo) Then
                    mCollectionProcesados.Add mArchivo
                    mCollectionNoProcesados.Remove mCollectionNoProcesados.Count
                End If
            End If
        Else
            'quiere decir q es rubro
            ActualizarRubros mArchivo
        End If
        
        mArchivo = Dir
    Loop
    
    For I = 1 To mCollectionNoProcesados.Count
        MoverArchivoNoProcesado mCollectionNoProcesados(I)
    Next I
    
    For I = 1 To mCollectionProcesados.Count
        MoverArchivoProcesado mCollectionProcesados(I)
    Next I
    
End Sub

Private Function BuscarProductos(pArchivo As String) As Boolean
    
    Dim mArchivo As String
    Dim mCanal As Integer
    Dim mCls As clsEstructuraTransaccion
    Dim mLinea As String
    
    On Error GoTo Errores
    Set mProductos = New Collection
    
    If EstrucApp.Archivos.ConvertirArchivo Then ConvertirArchivo (pArchivo)
    
    mCanal = FreeFile()
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        
        Set mCls = BuscarEstructuraTrans(mLinea)
        
        If Not mCls Is Nothing Then
            mProductos.Add mCls
        End If
    Loop
    
    Close #mCanal
    
    BuscarProductos = True
    
    Exit Function
    
Errores:
    
    EscribirLog "Error Buscando Archivo " & pArchivo, Err.Description, Err.Number
    On Error Resume Next
    Close #mCanal
    Err.Clear
    
End Function

Private Sub ConvertirArchivo(pArchivo)

    On Error GoTo Errores

    Dim mLinea As String
    Dim mCanal As Integer
    Dim mTexto As String

    mCanal = FreeFile()
    
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        mTexto = mTexto & mLinea
    Loop
    
    Close #mCanal
    
    Kill EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo
    
    mTexto = Replace(mTexto, vbLf, vbCrLf)
    
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & mArchivo For Output Access Write As #mCanal
        Print #mCanal, mTexto;
    Close #mCanal
    
    Exit Sub
    
Errores:
    
    EscribirLog "Error convirtiendo Archivo: " & Chr(34) & pArchivo & Chr(34), Err.Description, Err.Number
    
End Sub

Private Function BuscarEstructuraTrans(pLinea As String) As clsEstructuraTransaccion
    
    Dim mCls As New clsEstructuraTransaccion
    
    On Error GoTo Errores
    
    mCls.Cedula = ObtenerCadena(pLinea, 1, 12)
    mCls.Rubro = ObtenerCadena(pLinea, 13, 4)
    mCls.Cantidad = Val(ObtenerCadena(pLinea, 17, 6)) / 1000
    mCls.Fecha = ObtenerFecha(pLinea)
    mCls.Hora = ObtenerHora(pLinea)
    mCls.Tienda = ObtenerCadena(pLinea, 37, 6)
    mCls.Suspendido = Val(ObtenerCadena(pLinea, 45, 1)) = 1
    
    Set BuscarEstructuraTrans = mCls
    
    Exit Function

Errores:
    
    EscribirLog "Error Leyendo linea " & pLinea, Err.Description, Err.Number
    
End Function

Private Function ObtenerFecha(pCad As String) As String
    
    Dim mDia, mMes, mAno
    
    mDia = ObtenerCadena(pCad, 29, 2)
    mMes = ObtenerCadena(pCad, 27, 2)
    mAno = ObtenerCadena(pCad, 23, 4)
    
    ObtenerFecha = DateSerial(mAno, mMes, mDia)
    
End Function

Private Function ObtenerHora(pCad As String) As String
    
    Dim mHora, mMin, mSeg
    
    mHora = ObtenerCadena(pCad, 31, 2)
    mMin = ObtenerCadena(pCad, 33, 2)
    mSeg = ObtenerCadena(pCad, 35, 2)
    
    ObtenerHora = TimeSerial(mHora, mMin, mSeg)
    
End Function

Private Function InsertarProductos(pArchivo As String) As Boolean
    
    Dim mCls As clsEstructuraTransaccion
    
    For Each mCls In mProductos
        If Left(mCls.Tienda, 6) <> EstrucApp.Tienda.Tienda & EstrucApp.Tienda.Sucursal Then
            EjecutarSql mCls, pArchivo
        End If
    Next
    
    InsertarProductos = True
    
End Function

Private Function EjecutarSql(pCls As clsEstructuraTransaccion, pArchivo) As Boolean
    
    Dim mSql As String
    
    On Error GoTo Errores
    
    mSql = BuscarSql(pCls, pArchivo)
    
    EstrucApp.Conexion.Conexion.Execute mSql
    
    Exit Function

Errores:
    
    'MsgBox Err.Description
    EscribirLog "Ejecutando Sql " & mSql, Err.Description, Err.Number
    
End Function

Private Function BuscarSql(pCls As clsEstructuraTransaccion, pArchivo) As String
       
    BuscarSql = "insert into tr_racionalizacion (c_documento,c_concepto,c_documentorel,f_fechahora,c_rif, " _
        & "c_codarticulo,c_codrubro,n_cantidad,n_presenta,c_archivo,b_enviado,b_estado)" _
        & "values ('Importado','VEN','','" & Format(pCls.Fecha & " " & pCls.Hora, "dd/MM/yyyy HH:mm:ss") & "','" & pCls.Cedula & "'," _
        & "'','" & pCls.Rubro & "'," & pCls.Cantidad & ",1,'" & pArchivo _
        & IIf(pCls.Tienda <> "", "|" & pCls.Tienda, "") & "',1," & IIf(pCls.Suspendido, 1, 0) & ")"
        
   ' MsgBox BuscarSql
    
End Function

'********************************************************
Private Sub ActualizarRubros(pArchivo As String)

    Dim mCanal As Long
    Dim mRubro As clsEstructuraRubro
    Dim mLinea As String
    
    mCanal = FreeFile()
    
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        Set mRubro = New clsEstructuraRubro
        If mRubro.AsignarRubro(mLinea) Then
            ActualizarRegistro mRubro
        End If
    Loop
    
End Sub

Private Sub ActualizarRegistro(pRubro As clsEstructuraRubro)
    
    On Error GoTo Errores
    
    mSql = "update ma_racionalizacion_rubros set c_descripcion='" & pRubro.Descripcion & "'," _
    & ",n_cantidadmax=" & pRubro.Cantidad & ",b_activo=" & IIf(pRubro.Activo, 1, 0) _
    & ",n_intervalo=" & pRubro.Periodo _
    & "where c_codigo='" & pRubro.Rubro & "'"
        
    EstrucApp.Conexion.Conexion.Execute mSql
    
    Exit Sub
    
Errores:
    
    EscribirLog "Error actualizando rubro " & pRubro.Rubro, Err.Description, Err.Number
    
End Sub
