VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDescargaHuellasIni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mArchivo As String
Private mHuellas As String
Private mRegistroHuellas() As Object 'mRegistroHuellas as new collection
Private mCountReg As Long
'Private HuellasBD As New ADODB.Recordset
'Private ConsultaRS As String

Public Sub IniciarInterfaz()
    
    On Error GoTo ErrInicio
    
    CrearTablaTemporal
    
    Dim mCollectionProcesados As New Collection
    Dim mCollectionNoProcesados As New Collection
    
    'HuellasBD.CursorLocation = adUseClient
    
    'ConsultaRS = "select c_CodCliente as Cedula, bin_DataArray as TemplateSimple, n_TemplateSize as TemplateSize from ma_racionalizacion_clientes_huellas"
    'HuellasBD.Open ConsultaRS, EstrucApp.Conexion.Conexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    'Set HuellasBD.ActiveConnection = Nothing
    
    Debug.Print EstrucApp.Archivos.RutaDescargaInicialHuellas
    
    mArchivo = Dir(EstrucApp.Archivos.RutaDescargaInicialHuellas & "\*.csv", vbArchive)
    
    Do While mArchivo <> ""
        
        mCollectionNoProcesados.Add mArchivo
        mArchivo = UCase(mArchivo)
        
            If BuscarHuellas(mArchivo) Then
                mCollectionProcesados.Add mArchivo
                mCollectionNoProcesados.Remove mCollectionNoProcesados.Count
            End If

        mArchivo = Dir
        
    Loop
    
    For i = 1 To mCollectionNoProcesados.Count
        MoverArchivoNoProcesadoHuellasIniciales mCollectionNoProcesados(i)
    Next i
    
    For i = 1 To mCollectionProcesados.Count
        MoverArchivoProcesadoHuellasIniciales mCollectionProcesados(i)
    Next i
    
    Call ProcedimientoFinal
    
    'If HuellasBD.State = adStateOpen Then HuellasBD.Close
    
    Exit Sub
    
ErrInicio:
    
    Debug.Print Err.Description
    
    On Error Resume Next
    
    'If HuellasBD.State = adStateOpen Then HuellasBD.Close
    
    EscribirLog "Error al Cargar Huellas Iniciales.", Err.Description, Err.Number
    
End Sub

Private Function CrearTablaTemporal() As Boolean

    On Error GoTo ErrBD

    Dim SegundoIntento As Boolean
    
Reintentar:
    
    If SegundoIntento Then
        On Error GoTo Out
    End If
    
    Dim Query As String
    
    Query = "IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TMP_CLIENTES_CARGAINICIAL')" _
    & vbNewLine & "BEGIN" _
    & vbNewLine & vbTab & "DROP TABLE TMP_CLIENTES_CARGAINICIAL" _
    & vbNewLine & "END" _
    & vbNewLine _
    & vbNewLine & "CREATE TABLE [dbo].[TMP_CLIENTES_CARGAINICIAL](" _
    & vbNewLine & "[c_Rif] [nvarchar](50) NOT NULL PRIMARY KEY," _
    & vbNewLine & "[c_Nombre] [nvarchar](40) NOT NULL DEFAULT ('')," _
    & vbNewLine & "[c_Apellido] [nvarchar](40) NOT NULL DEFAULT ('')," _
    & vbNewLine & "[b_Activo] [bit] NOT NULL DEFAULT (1)," _
    & vbNewLine & "[n_PlantillaBiometrico] [int] NOT NULL DEFAULT(0)," _
    & vbNewLine & "[c_Archivo] [nvarchar](50) NOT NULL DEFAULT ('')," _
    & vbNewLine & "[b_Enviado] [bit] NOT NULL DEFAULT (0)," _
    & vbNewLine & ")"
    
    Query = Query _
    & vbNewLine & "" _
    & vbNewLine & "IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TMP_HUELLAS_CARGAINICIAL')" _
    & vbNewLine & "BEGIN" _
    & vbNewLine & vbTab & "DROP TABLE TMP_HUELLAS_CARGAINICIAL" _
    & vbNewLine & "END" _
    & vbNewLine _
    & vbNewLine & "CREATE TABLE [dbo].[TMP_HUELLAS_CARGAINICIAL](" _
    & vbNewLine & "[ID_Dispositivo] [nvarchar](50) NOT NULL," _
    & vbNewLine & "[c_CodCliente] [nvarchar](25) NOT NULL," _
    & vbNewLine & "[c_Dedo] [nvarchar](20) NOT NULL," _
    & vbNewLine & "[c_TipoHuella] [nvarchar](100) NOT NULL," _
    & vbNewLine & "[Bin_Data] [nvarchar](max) NOT NULL DEFAULT ('')," _
    & vbNewLine & "[Bin_DataArray] [varbinary](max) NOT NULL DEFAULT (0)," _
    & vbNewLine & "[n_TemplateSize] [numeric](18,0) NOT NULL DEFAULT (0)," _
    & vbNewLine & "[c_Archivo] [nvarchar](50) NOT NULL DEFAULT ('')," _
    & vbNewLine & "[b_Enviado] [bit] NOT NULL DEFAULT (0)," _
    & vbNewLine & "[d_Fecha] [datetime] NOT NULL DEFAULT getDate()," _
    & vbNewLine & "[c_Cadena_Tienda] [nvarchar](10) NOT NULL," _
    & vbNewLine & "[c_CodUsuario] [nvarchar](50) NOT NULL DEFAULT 'Externo'," _
    & vbNewLine & "[ID] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1) NOT NULL," _
    & vbNewLine & ")"
    
    EstrucApp.Conexion.Conexion.Execute (Query)
    
    CrearTablaTemporal = True
    
    Exit Function
    
ErrBD:
    SegundoIntento = True
    GoTo Reintentar
Out:
    Exit Function
End Function

Private Function ProcedimientoFinal() As Boolean

    On Error GoTo ErrBD

    Dim SegundoIntento As Boolean
    
Reintentar:
    
    EstrucApp.Conexion.Conexion.CommandTimeOut = 0
    EstrucApp.Conexion.Conexion.BeginTrans
    
    If SegundoIntento Then
        On Error GoTo Out
    End If
    
    Dim Query As String, mRs As ADODB.Recordset
    
    'VERIFICAR SI HUBO UN ARCHIVO Y SE INGRESARON DATOS.
    
    Query = "SELECT SUM(CLIENTES) AS CLIENTES, SUM(HUELLAS) AS HUELLAS FROM(" _
    & vbNewLine & "SELECT COUNT(C_RIF) AS CLIENTES, 0 AS HUELLAS FROM TMP_CLIENTES_CARGAINICIAL" _
    & vbNewLine & "UNION" _
    & vbNewLine & "SELECT 0 AS CLIENTES, COUNT(C_CODCLIENTE) AS HUELLAS FROM TMP_HUELLAS_CARGAINICIAL" _
    & vbNewLine & ") TB"
    
    'Debug.Print Query
    
    Set mRs = EstrucApp.Conexion.Conexion.Execute(Query)
    
    If mRs!Clientes <= 0 Or mRs!Huellas <= 0 Then
        ProcedimientoFinal = True
        GoTo Out
    End If
    
    mRs.Close
    
    'CREAR INDICES EN TABLA
    
    Query = "CREATE NONCLUSTERED INDEX [Verificacion] ON [dbo].[TMP_CLIENTES_CARGAINICIAL]" _
    & vbNewLine & "(" _
    & vbNewLine & vbTab & "[c_Rif] Asc" _
    & vbNewLine & ")" _
    & vbNewLine & "INCLUDE ( [c_Nombre]," _
    & vbNewLine & "[c_Apellido]," _
    & vbNewLine & "[b_Activo]," _
    & vbNewLine & "[n_PlantillaBiometrico]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]"
        
    Query = Query & vbNewLine _
    & vbNewLine & "CREATE NONCLUSTERED INDEX [Verificacion] ON [dbo].[TMP_HUELLAS_CARGAINICIAL]" _
    & vbNewLine & "(" _
    & vbNewLine & vbTab & "[c_CodCliente] Asc" _
    & vbNewLine & ")" _
    & vbNewLine & "INCLUDE ( [Bin_DataArray]," _
    & vbNewLine & "[n_TemplateSize]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]"
            
    Query = Query & vbNewLine _
    & vbNewLine & "CREATE NONCLUSTERED INDEX [Identificacion] ON [dbo].[TMP_HUELLAS_CARGAINICIAL]" _
    & vbNewLine & "(" _
    & vbNewLine & vbTab & "[c_CodCliente] Asc" _
    & vbNewLine & ")" _
    & vbNewLine & "INCLUDE ( [Bin_Data]," _
    & vbNewLine & "[n_TemplateSize]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]"
                           
    'Debug.Print Query
    
    EstrucApp.Conexion.Conexion.Execute (Query)
                           
    If ExisteTabla(EstrucApp.Conexion.Conexion, "MA_RACIONALIZACION_CLIENTES") _
    And ExisteTabla(EstrucApp.Conexion.Conexion, "MA_RACIONALIZACION_CLIENTES_HUELLAS") Then
                           
        'RECUPERAR CLIENTES REGISTRADOS POR EL COMERCIO.
        'SI ESTOS CLIENTES YA EXISTEN EN EL ARCHIVO MIGRADO
        'ENTONCES SE MANTIENEN LOS DEL ARCHIVO, PUESTO QUE FUERON LOS REGISTROS ACTUALES
        'QUE RESIDEN EN LA BASE DE DATOS CENTRALIZADA.
        
        Query = "INSERT INTO TMP_CLIENTES_CARGAINICIAL" _
        & vbNewLine & "SELECT * FROM MA_RACIONALIZACION_CLIENTES WHERE C_RIF IN(" _
        & vbNewLine & "SELECT C_CODCLIENTE FROM MA_RACIONALIZACION_CLIENTES_HUELLAS" _
        & vbNewLine & "WHERE  ID_DISPOSITIVO <> 'EXTERNO'" _
        & vbNewLine & "AND C_CODUSUARIO <> 'EXTERNO'" _
        & vbNewLine & "AND C_CODCLIENTE NOT IN (SELECT C_RIF FROM TMP_CLIENTES_CARGAINICIAL)" _
        & vbNewLine & ")"
    
        EstrucApp.Conexion.Conexion.Execute (Query)
    
        Query = "INSERT INTO TMP_HUELLAS_CARGAINICIAL" _
        & vbNewLine & "SELECT ID_Dispositivo, c_CodCliente, c_Dedo, c_TipoHuella, Bin_Data, Bin_DataArray, n_TemplateSize, c_Archivo, b_Enviado, d_Fecha, c_Cadena_Tienda, c_CodUsuario" _
        & vbNewLine & "From MA_RACIONALIZACION_CLIENTES_HUELLAS" _
        & vbNewLine & "WHERE  ID_DISPOSITIVO <> 'EXTERNO'" _
        & vbNewLine & "AND C_CODUSUARIO <> 'EXTERNO'" _
        & vbNewLine & "AND C_CODCLIENTE NOT IN (SELECT C_CODCLIENTE FROM TMP_HUELLAS_CARGAINICIAL)"
        
        EstrucApp.Conexion.Conexion.Execute (Query)
        
        'POR ULTIMO, SE INGRESAN TODOS AQUELLOS CLIENTES BLOQUEADOS QUE FALTEN EN EL ARCHIVO
        'Y AQUELLOS QUE YA EXISTAN EN EL ARCHIVO SIMPLEMENTE SE LES COLOCA EL ESTATUS INACTIVO.
        
        Query = "INSERT INTO TMP_CLIENTES_CARGAINICIAL" _
        & vbNewLine & "SELECT CLI.* FROM TMP_CLIENTES_CARGAINICIAL TMP RIGHT JOIN" _
        & vbNewLine & "MA_RACIONALIZACION_CLIENTES CLI" _
        & vbNewLine & "ON TMP.C_RIF = CLI.c_RIF" _
        & vbNewLine & "Where TMP.C_RIF Is Null" _
        & vbNewLine & "AND CLI.b_Activo = 0"
    
        EstrucApp.Conexion.Conexion.Execute (Query)
    
        Query = "Update" _
        & vbNewLine & vbTab & "TMP_CLIENTES_CARGAINICIAL" _
        & vbNewLine & "SET" _
        & vbNewLine & vbTab & "B_ACTIVO = CLI.B_ACTIVO" _
        & vbNewLine & "From" _
        & vbNewLine & vbTab & "TMP_CLIENTES_CARGAINICIAL TMP" _
        & vbNewLine & "Inner Join" _
        & vbNewLine & vbTab & "MA_RACIONALIZACION_CLIENTES CLI" _
        & vbNewLine & "ON" _
        & vbNewLine & vbTab & "TMP.C_RIF = CLI.C_RIF"
        
        EstrucApp.Conexion.Conexion.Execute (Query)
        
    End If
    
    'PREPARAR TABLAS Y DEJAR TODO LISTO...
    
    Query = "IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TMP_RESPALDO_RACIONALIZACION_CLIENTES')" _
    & vbNewLine & "BEGIN" _
    & vbNewLine & vbTab & "DROP TABLE TMP_RESPALDO_RACIONALIZACION_CLIENTES" _
    & vbNewLine & "END" _
    & vbNewLine _
    & vbNewLine & "IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TMP_RESPALDO_RACIONALIZACION_CLIENTES_HUELLAS')" _
    & vbNewLine & "BEGIN" _
    & vbNewLine & vbTab & "DROP TABLE TMP_RESPALDO_RACIONALIZACION_CLIENTES_HUELLAS" _
    & vbNewLine & "END"
    
    EstrucApp.Conexion.Conexion.Execute (Query)
    
    Query = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_RACIONALIZACION_CLIENTES'"

    If Not EstrucApp.Conexion.Conexion.Execute(Query).EOF Then
        Query = "SP_RENAME 'MA_RACIONALIZACION_CLIENTES', 'TMP_RESPALDO_RACIONALIZACION_CLIENTES'"
        EstrucApp.Conexion.Conexion.Execute (Query)
    End If
    
    Query = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_RACIONALIZACION_CLIENTES_HUELLAS'"
    
    If Not EstrucApp.Conexion.Conexion.Execute(Query).EOF Then
        Query = "SP_RENAME 'MA_RACIONALIZACION_CLIENTES_HUELLAS', 'TMP_RESPALDO_RACIONALIZACION_CLIENTES_HUELLAS'"
        EstrucApp.Conexion.Conexion.Execute (Query)
    End If
    
    '
    
    Query = "SP_RENAME 'TMP_CLIENTES_CARGAINICIAL', 'MA_RACIONALIZACION_CLIENTES'"
    
    EstrucApp.Conexion.Conexion.Execute (Query)
    
    Query = "SP_RENAME 'TMP_HUELLAS_CARGAINICIAL', 'MA_RACIONALIZACION_CLIENTES_HUELLAS'"
    
    EstrucApp.Conexion.Conexion.Execute (Query)
    
    'END
    
    EstrucApp.Conexion.Conexion.CommitTrans
    
    ProcedimientoFinal = True
    
    Exit Function
    
ErrBD:
    
    Debug.Print Err.Description & vbNewLine & Query
    EstrucApp.Conexion.Conexion.RollbackTrans
    SegundoIntento = True
    GoTo Reintentar
    
Out:
    
    EstrucApp.Conexion.Conexion.RollbackTrans
    Exit Function
    
End Function

Private Function BuscarHuellas(pArchivo As String) As Boolean
    
    Dim mArchivo As String
    Dim mCanal As Integer
    Dim mCls As clsEstructuraDeHuellas
    Dim mLinea As String
    Dim ContLinea As Double
    Dim nRegistros As Double
    Dim nRegistrosLeidos As Double
    Dim LecturaUnica As Boolean
    
    nRegistros = 10000000
    
    On Error GoTo Errores
    
    'Set mRegistroHuellas = New Collection
    
    'If EstrucApp.Archivos.ConvertirArchivo Then ConvertirArchivo (pArchivo)
    
    mCanal = FreeFile()
    Open EstrucApp.Archivos.RutaDescargaInicialHuellas & "\" & pArchivo For Input Access Read As #mCanal
    
    Dim mLinea2 As String
    
    Dim ArrLineas() As String
    Dim ArrLineasCompleto() As String
    
    mCountReg = 0
    
    Do While Not EOF(mCanal)
       
        If mLinea <> "" Then
            
            If nRegistrosLeidos + nRegistros >= LOF(mCanal) Then
                nRegistros = LOF(mCanal) - nRegistrosLeidos
            End If
            
            ArrLineas = Split(Input(nRegistros, #mCanal), vbLf)
            
            mLinea2 = ArrLineas(0)
            
            'Debug.Print mLinea2
            
            ArrLineas(0) = mLinea & mLinea2
            
            If Not (nRegistrosLeidos + nRegistros >= LOF(mCanal)) Then
                mLinea = ArrLineas(UBound(ArrLineas))
                ReDim Preserve ArrLineas(UBound(ArrLineas) - 1) 'ArrLineas(UBound(ArrLineas)) = ""
            End If
            
            InsertarLote ArrLineas, pArchivo
            
        Else
        
            LecturaUnica = LOF(mCanal) < nRegistros
        
            ArrLineas = Split(Input(IIf(LecturaUnica, LOF(mCanal), nRegistros), #mCanal), vbLf)

            'For Each StrData In ArrLineas
                'Debug.Print StrData
            'Next
        
            mLinea = ArrLineas(UBound(ArrLineas))
            
            If Not LecturaUnica Then
                ReDim Preserve ArrLineas(UBound(ArrLineas) - 1) 'ArrLineas(UBound(ArrLineas)) = ""
            End If
            
            InsertarLote ArrLineas, pArchivo
            
        End If

        nRegistrosLeidos = nRegistrosLeidos + nRegistros

    Loop
    
    Close #mCanal
            
    BuscarHuellas = True
    
    Exit Function
    
Errores:

    'If Err.Number = 62 Then
        'On Error GoTo Errores
        'Resume Next
    'End If
    
    Debug.Print Err.Number
    
    EscribirLog "Error Buscando Archivo " & pArchivo, Err.Description, Err.Number
    On Error Resume Next
    Close #mCanal
    Err.Clear
    
End Function

Public Function InsertarLote(ByRef pArreglo, pArchivo As String) As Boolean
    
    Dim mCls As clsEstructuraDeHuellas
    
    ReDim mRegistroHuellas(0) As Object
    
    For i = 0 To UBound(pArreglo)
        
        mCountReg = mCountReg + 1
        
        Set mCls = BuscarEstructuraHuellasIniciales(CStr(pArreglo(i)))
        
        If Not mCls Is Nothing Then
            ReDim Preserve mRegistroHuellas(i) As Object
            Set mRegistroHuellas(i) = mCls
        End If
        
    Next i
    
    InsertarLote = InsertarHuellas(pArchivo)
    
End Function

Private Function BuscarEstructuraHuellasIniciales(pLinea As String) As clsEstructuraDeHuellas
    
    Dim mCls As New clsEstructuraDeHuellas
    
    On Error GoTo Errores
    
    Dim Data() As String
    
    Data = Split(pLinea, ";")
    
    mCls.Cedula = Data(0)
    mCls.Cliente = Data(1)
    mCls.TemplateIzq = HexToString(Data(2))
    mCls.TemplateDer = HexToString(Data(3))
    mCls.Tienda = Data(4)
    mCls.Fecha = Replace(Data(5), "-", "")
    
    If mCls.Cedula = "" Or (mCls.TemplateIzq = "" And mCls.TemplateDer = "") Or mCls.Tienda = "" Or mCls.Fecha = "" Then
        'Debug.Print pLinea
        EscribirLog "Error Leyendo linea, Datos o formato inv�lidos. -> " & pLinea, , 0
        Set BuscarEstructuraHuellasIniciales = Nothing: Exit Function
    End If
    
    Set BuscarEstructuraHuellasIniciales = mCls
    
    Exit Function

Errores:
    
    Set BuscarEstructuraHuellasIniciales = Nothing

    EscribirLog "Error Leyendo L�nea N� " & mCountReg & " (" & Err.Description & "). -> " & pLinea, , Err.Number
    
End Function

Private Function ObtenerFecha(pCad As String) As String
    Dim mDia, mMes, mAno
    
    mDia = ObtenerCadena(pCad, 3072, 2)
    mMes = ObtenerCadena(pCad, 3070, 2)
    mAno = ObtenerCadena(pCad, 3066, 4)
    ObtenerFecha = DateSerial(mAno, mMes, mDia)
End Function

Private Function InsertarHuellas(pArchivo As String) As Boolean

    Dim mCls As clsEstructuraDeHuellas
    Dim PoseeFormatoEstandar As Boolean
    'Variable para procesar a la BD
    Dim Procesar As Boolean
    Const MaxTemplateSize = 1024
    Dim ArregloParcialIZQ() As Byte, ArregloFullIZQ() As Byte, SizeIZQ As Long
    Dim ArregloParcialDER() As Byte, ArregloFullDER() As Byte, SizeDER As Long
    Dim ArregloBD() As Byte, SizeBD As Long
    'Dim BioMatcher As Long, mStat As UFM_STATUS
    'Dim MatchIZQ As Long, MatchDER As Long
    'Dim MatchInfoIZQ As Collection, MatchInfoDER As Collection
    'Dim MatchCountIZQ As Long, MatchCountDER As Long
    
    'Inicializar SDK

    'mStat = UFM_Create(BioMatcher)

    'If mStat <> UFM_STATUS.OK Then
        'EscribirLog "Error inicializando SDK.", "Codigo de Error: " & CStr(mStat)
        'Exit Function
    'End If
    
    'mStat = UFM_SetTemplateType(BioMatcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_ISO19794_2)
    
    'If mStat <> UFM_STATUS.OK Then
        'EscribirLog "Error inicializando SDK.", "Codigo de Error: " & CStr(mStat)
        'Exit Function
    'End If
    
    ' Security level ranges from 1 to 7
    
    'mStat = UFM_SetParameter(BioMatcher, UFM_PARAM.SECURITY_LEVEL, 6)
    
    'mStat = UFM_SetParameter(BioMatcher, UFM_PARAM.FAST_MODE, 1)
    
    'Procesamiento de Huellas.
    
    For k = 0 To UBound(mRegistroHuellas)
    
        Set mCls = mRegistroHuellas(k)
    
        'If Left(mCls.Tienda, 3) <> EstrucApp.Tienda.Tienda Then
            
            PoseeFormatoEstandar = ((mCls.TemplateIzq Like "*FMR*") And (mCls.TemplateIzq Like "*20*")) _
            Or ((mCls.TemplateDer Like "*FMR*") And (mCls.TemplateDer Like "*20*")) 'IIf(, True, False)
            
            If PoseeFormatoEstandar Then
                
                'Obtener Arreglos
                            
                SizeIZQ = Len(mCls.TemplateIzq)
                SizeDER = Len(mCls.TemplateDer)
                
                ReDim ArregloParcialIZQ(SizeIZQ) As Byte
                ReDim ArregloParcialDER(SizeDER) As Byte
                'ReDim ArregloFullIZQ(MaxTemplateSize - 1) As Byte
                'ReDim ArregloFullDER(MaxTemplateSize - 1) As Byte
                
                'For i = 1 To SizeIZQ
                    'ArregloParcialIZQ(i - 1) = Asc(Mid(mCls.TemplateIzq, i, 1))
                    'ArregloFullIZQ(i - 1) = ArregloParcialIZQ(i - 1)
                'Next i
                
                ArregloParcialIZQ = StrConv(mCls.TemplateIzq, vbFromUnicode)
                
                'For i = 1 To SizeDER
                    'ArregloParcialDER(i - 1) = Asc(Mid(mCls.TemplateDer, i, 1))
                    'ArregloFullDER(i - 1) = ArregloParcialDER(i - 1)
                'Next i
                
                ArregloParcialDER = StrConv(mCls.TemplateDer, vbFromUnicode)
                
                Procesar = True
                
                'Comprobaci�n
                
                'If Not HuellasBD.RecordCount = 0 Then HuellasBD.MoveFirst
                
                'MatchCountIZQ = 0: MatchCountDER = 0
                'Set MatchInfoIZQ = New Collection
                'Set MatchInfoDER = New Collection
                
                'Do While Not HuellasBD.EOF
                                   
                    'Debug.Print HuellasBD.AbsolutePosition
                                   
                    'ArregloBD = HuellasBD!TemplateSimple
                    'SizeBD = HuellasBD!TemplateSize
                    
                    'mStat = UFM_Verify(BioMatcher, ArregloParcialIZQ(0), SizeIZQ, ArregloBD(0), SizeBD, MatchIZQ)
                    
                    'If CBool(MatchIZQ) Then
                        'If Not Collection_ExisteKey(MatchInfoIZQ, HuellasBD!Cedula) Then MatchInfoIZQ.Add CStr(HuellasBD!Cedula), CStr(HuellasBD!Cedula)
                        'MatchCountIZQ = MatchCountIZQ + 1
                    'End If
                    
                    'mStat = UFM_Verify(BioMatcher, ArregloParcialDER(0), SizeDER, ArregloBD(0), SizeBD, MatchDER)
                    
                    'If CBool(MatchDER) Then
                        'If Not Collection_ExisteKey(MatchInfoDER, HuellasBD!Cedula) Then MatchInfoDER.Add CStr(HuellasBD!Cedula), CStr(HuellasBD!Cedula)
                        'MatchCountDER = MatchCountDER + 1
                    'End If
                    
                    'HuellasBD.MoveNext
                                   
                'Loop
                
                'Verificar Resultados
                
                'INDICE IZQUIERDO
                
'                If MatchCountIZQ <= 0 Then
'                    Procesar = True
'                ElseIf MatchCountIZQ = 1 And MatchInfoIZQ.Count = 1 Then
'                    If UCase(MatchInfoIZQ.Item(1)) <> UCase(mCls.Cedula) Then
'                        '"Robo de Identidad" 'Chequear
'                        Procesar = False
'                    Else
'                        '"Huella confirmada"
'                        Procesar = True
'                    End If
'                ElseIf MatchCountIZQ <= 2 And MatchInfoIZQ.Count > 1 Then
'                    '"Robo de Identidad" 'Chequear
'                    Procesar = False
'                ElseIf MatchCountIZQ >= 2 And MatchInfoIZQ.Count = 1 Then
'                    '"Registros multiples" ' Chequear...
'                    Procesar = False
'                ElseIf MatchCountIZQ > 2 And MatchInfoIZQ.Count > 1 Then
'                    '"Registros Multiples y Robo de Identidad" 'Chequear
'                    Procesar = False
'                End If
                
                'INDICE DERECHO
                
'                If Procesar Then
'
'                    If MatchCountDER <= 0 Then
'                        Procesar = True
'                    ElseIf MatchCountDER = 1 And MatchInfoDER.Count = 1 Then
'                        If UCase(MatchInfoDER.Item(1)) <> UCase(mCls.Cedula) Then
'                            '"Robo de Identidad" 'Chequear
'                            Procesar = False
'                        Else
'                            '"Huella confirmada"
'                            Procesar = True
'                        End If
'                    ElseIf MatchCountDER <= 2 And MatchInfoDER.Count > 1 Then
'                        '"Robo de Identidad" 'Chequear
'                        Procesar = False
'                    ElseIf MatchCountDER >= 2 And MatchInfoDER.Count = 1 Then
'                        '"Registros multiples" ' Chequear...
'                        Procesar = False
'                    ElseIf MatchCountDER > 2 And MatchInfoDER.Count > 1 Then
'                        '"Registros Multiples y Robo de Identidad" 'Chequear
'                        Procesar = False
'                    End If
'
'                End If
                             
            Else
                Procesar = False
            End If
            
            If Procesar Then
                'Procesar = EjecutarSql(mCls, pArchivo, ArregloParcialIZQ, ArregloParcialDER, ArregloFullIZQ, ArregloFullDER, SizeIZQ, SizeDER, False)
                Procesar = EjecutarSql(mCls, pArchivo, ArregloParcialIZQ, ArregloParcialDER, Nothing, Nothing, SizeIZQ, SizeDER, False)
                Debug.Print "PASO"
            Else
                EscribirLog "La(s) Huella(s) no cumplen el Est�ndar ISO -> " & mCls.Cedula & ";" & mCls.Cliente & ";" & mCls.TemplateIzq & ";" & mCls.TemplateDer & ";" & mCls.Tienda & ";" & mCls.Fecha, "", 0
                Debug.Print "INVALIDO"
            End If
        
        'Else
        
            'Procesar = False
        
        'End If
        
        'Debug.Print "Listo" & " - " & mCls.Cedula
            
    Next k
    
    'UFM_Delete (Matcher)
    
    InsertarHuellas = True
    
End Function

Private Function EjecutarSql(pCls As clsEstructuraDeHuellas, pArchivo As String, pArrIzq As Variant, pArrDer As Variant, _
pArrFullIzq As Variant, pArrFullDer As Variant, pArrSizeIzq As Long, pArrSizeDer As Long, Optional pA�adirAlRs As Boolean = True) As Boolean
    
    Dim mSql As String
    Dim mSql2 As String
    Dim PoseeFormatoEstandar As Boolean, Plantilla As Long
    
    On Error GoTo Errores
    
    Dim TransaccionIniciada As Boolean
    
    EstrucApp.Conexion.Conexion.BeginTrans
    TransaccionIniciada = True
    
    Dim mRsIzq As New ADODB.Recordset
    Dim mRsDer As New ADODB.Recordset
    Dim mRsCliente As New ADODB.Recordset
    
    Dim mDeleted As Double
    
    EstrucApp.Conexion.Conexion.CommandTimeOut = 0
    
    'mSql = "IF EXISTS(SELECT c_CodCliente FROM TMP_HUELLAS_CARGAINICIAL WHERE c_CodCliente = '" & pCls.Cedula & "')" & vbNewLine & "BEGIN" & vbNewLine & "DELETE FROM TMP_HUELLAS_CARGAINICIAL WHERE c_CodCliente = '" & pCls.Cedula & "'" & vbNewLine & "END"
    
    'Debug.Print mSql
    
    'EstrucApp.Conexion.Conexion.Execute mSql, mDeleted
    
    'If mDeleted > 0 Then
        'Debug.Print mDeleted
        'EscribirLog "Registros sobrescritos para la C�dula " & Chr(34) & pCls.Cedula & Chr(34) & ". Chequear duplicaci�n en archivos.", "Advertencia.", 0
    'End If
    
    PoseeFormatoEstandar = ((pCls.TemplateIzq Like "*FMR*") And (pCls.TemplateIzq Like "*20*"))
    
    If PoseeFormatoEstandar Then
        
        With mRsIzq
            
            .Open "SELECT * FROM TMP_HUELLAS_CARGAINICIAL WHERE 1 = 2", EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
            
            .AddNew
            
            !c_CodCliente = pCls.Cedula
            !ID_Dispositivo = "Externo"
            !c_Dedo = "Indice Izquierdo"
            !c_TipoHuella = "Est�ndar ISO 19794-2"
            !Bin_Data = pCls.TemplateIzq
            !bin_DataArray = pArrIzq
            '!Bin_DataArrayFull = pArrFullIzq
            !n_TemplateSize = pArrSizeIzq
            !c_Archivo = pArchivo
            !b_Enviado = 1
            !c_Cadena_Tienda = pCls.Tienda
            !d_Fecha = CDate(Mid(pCls.Fecha, 1, 4) & "/" & Mid(pCls.Fecha, 5, 2) & "/" & Mid(pCls.Fecha, 7, 2))
            
            .UpdateBatch
            
            Plantilla = Plantilla + CH_Dedos.IndiceIzquierdo
            
        End With
        
    End If
    
    PoseeFormatoEstandar = ((pCls.TemplateDer Like "*FMR*") And (pCls.TemplateDer Like "*20*"))
    
    If PoseeFormatoEstandar Then
    
        With mRsDer
            
            .Open "SELECT * FROM TMP_HUELLAS_CARGAINICIAL WHERE 1 = 2", EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
            
            .AddNew
            
            !c_CodCliente = pCls.Cedula
            !ID_Dispositivo = "Externo"
            !c_Dedo = "Indice Derecho"
            !c_TipoHuella = "Est�ndar ISO 19794-2"
            !Bin_Data = pCls.TemplateDer
            !bin_DataArray = pArrDer
            '!Bin_DataArrayFull = pArrFullDer
            !n_TemplateSize = pArrSizeDer
            !c_Archivo = pArchivo
            !b_Enviado = 1
            !c_Cadena_Tienda = pCls.Tienda
            !d_Fecha = CDate(Mid(pCls.Fecha, 1, 4) & "/" & Mid(pCls.Fecha, 5, 2) & "/" & Mid(pCls.Fecha, 7, 2))
            
            .UpdateBatch
            
            Plantilla = Plantilla + CH_Dedos.IndiceDerecho
            
        End With
    
    End If
    
    With mRsCliente
    
        '.Open "SELECT * FROM TMP_CLIENTES_CARGAINICIAL where c_Rif = '" & pCls.Cedula & "'", EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
        
        .Open "SELECT * FROM TMP_CLIENTES_CARGAINICIAL WHERE 1 = 2", EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
        
        'If .EOF Then
            .AddNew
        'Else
            '.Update
        'End If
        
        Dim Descripcion As Variant
        Descripcion = DescripcionCliente(pCls.Cliente)
        
        !c_Rif = pCls.Cedula
        !c_Nombre = Descripcion(1)
        !c_Apellido = Descripcion(0)
        
        'If IsEmpty(!b_Activo) Then
            !b_activo = 1
        'End If
        
        !n_PlantillaBiometrico = Plantilla '72 'Ambos Indices en la Plantilla de los dedos registrados.
        !c_Archivo = pArchivo
        !b_Enviado = 1
        
        .UpdateBatch
        
    End With
    
    EstrucApp.Conexion.Conexion.CommitTrans
    
    'If pA�adirAlRs Then A�adirHuellasRs
    
    mRsIzq.Close
    mRsDer.Close
    mRsCliente.Close
    
    EjecutarSql = True
    
    Exit Function

Errores:

    'Debug.Print Err.Description

    EjecutarSql = False

    If TransaccionIniciada Then EstrucApp.Conexion.Conexion.RollbackTrans
    
    'MsgBox Err.Description
    
    EscribirLog "Insertando registros en la Base de datos (" & Err.Description & ") -> " & _
    pCls.Cedula & ";" & pCls.Cliente & ";" & pCls.TemplateIzq & ";" & pCls.TemplateDer & ";" & pCls.Tienda & ";" & pCls.Fecha
    
End Function

Private Function A�adirHuellasRs() As Boolean

    On Error GoTo Err

    Dim InicioAddNew As Boolean

    Dim NuevasHuellas As ADODB.Recordset
    Set NuevasHuellas = EstrucApp.Conexion.Conexion.Execute("SELECT TOP (2) * FROM MA_RACIONALIZACION_CLIENTES_HUELLAS ORDER BY ID DESC")

    With HuellasBD
    
        .AddNew
        InicioAddNew = True
        
        !Cedula = NuevasHuellas!c_CodCliente
        !TemplateSimple = NuevasHuellas!bin_DataArray
        !TemplateSize = NuevasHuellas!n_TemplateSize
        
        .UpdateBatch
        
    End With
    
    A�adirHuellasRs = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    On Error Resume Next
    
    If InicioAddNew Then HuellasBD.CancelUpdate
    
End Function

Public Function HexToString(ByVal HexToStr As String) As String

    Dim strTemp As String
    Dim StrReturn As String
    Dim i As Long
    
    For i = 1 To Len(HexToStr) Step 2
    strTemp = Chr$(Val("&H" & Mid$(HexToStr, i, 2)))
    StrReturn = StrReturn & strTemp
    Next i
    
    HexToString = StrReturn

End Function

Public Function DescripcionCliente(pCadena As String) As Variant
 
    On Error GoTo Err
 
    Dim Temporal As Variant
    Dim nPalabras As Integer
    Dim Datos(1) As String
    Temporal = Split(pCadena, " ")
    nPalabras = UBound(Temporal) + 1
    
    If (nPalabras Mod 2 = 0) Then
        For i = 0 To (nPalabras / 2) - 1
            Datos(0) = Datos(0) & IIf(Datos(0) = "", "", " ") & Temporal(i)
        Next i
        For i = (nPalabras / 2) To (nPalabras - 1)
            Datos(1) = Datos(1) & IIf(Datos(1) = "", "", " ") & Temporal(i)
        Next i
    Else
        If (nPalabras = 1) Then
            Datos(0) = Temporal(0)
        Else
            For i = 0 To Fix((nPalabras / 2) - 1)
                Datos(0) = Datos(0) & IIf(Datos(0) = "", "", " ") & Temporal(i)
            Next i
            For i = Fix(nPalabras / 2) To (nPalabras - 1)
                Datos(1) = Datos(1) & IIf(Datos(1) = "", "", " ") & Temporal(i)
            Next i
        End If
    End If
    
    DescripcionCliente = Datos
    
    Exit Function
    
Err:
    
    DescripcionCliente = Split(".", ".")
    
End Function

