VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsEstructuraTransaccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCedula                                      As String
Private mvarRubro                                       As String
Private mvarSigno                                       As String
Private mvarCantidad                                    As Single
Private mvarFecha                                       As String
Private mvarHora                                        As String
Private mvarTienda                                      As String
Private mvarSuspendido                                  As Boolean
Private mvarId                                          As Long
Private mvarHuellaSSAS                                  As String

'*********************************** propertys let ************************************************

Property Get Cedula() As String
    Cedula = mvarCedula
End Property

Property Get TipoIdentificacion() As String
    Select Case UCase(Left(Trim(mvarCedula), 1))
        Case "J", "E", "P", "G"
            TipoIdentificacion = UCase(Left(Trim(mvarCedula), 1))
        Case Else
            TipoIdentificacion = "V"
    End Select
End Property

Property Get Rubro() As String
    Rubro = mvarRubro
End Property

Property Get Signo() As String
    Signo = mvarSigno
End Property

Property Get Cantidad() As Single
    Cantidad = mvarCantidad
End Property

Property Get Fecha() As String
    Fecha = mvarFecha
End Property

Property Get Hora() As String
    Hora = mvarHora
End Property

Property Get Tienda() As String
    Tienda = mvarTienda
End Property

Property Get Id() As Long
    Id = mvarId
End Property

Property Get Suspendido() As Boolean
    Suspendido = mvarSuspendido
End Property

'*********************************** properties let ************************************************

Property Let Cedula(pValor As String)
    mvarCedula = pValor
End Property

Property Let Rubro(pValor As String)
    mvarRubro = pValor
End Property
Property Let HuellaSSAS(pValor As String)
    mvarHuellaSSAS = pValor
End Property

Property Let Signo(pValor As String)
    mvarSigno = pValor
End Property

Property Let Cantidad(pValor As Single)
    mvarCantidad = pValor
End Property

Property Let Fecha(pValor As String)
    mvarFecha = pValor
End Property

Property Let Hora(pValor As String)
    mvarHora = pValor
End Property

Property Let Tienda(pValor As String)
    mvarTienda = pValor
End Property

Property Let Suspendido(pValor As Boolean)
    mvarSuspendido = pValor
End Property

Public Function AgregarRegistroRs(pRs As ADODB.Recordset) As clsEstructuraTransaccion
    'mvarCedula = UCase(Trim(Replace(pRs!c_rif, "-", "")))
    mvarCedula = ValidarRif(CStr(pRs!c_Rif))
    'Validación sencilla para prevenir errores humanos.
    mvarRubro = pRs!c_codrubro
    mvarSigno = IIf(pRs!n_cantidad < 0, "-", " ")
    mvarCantidad = Abs(pRs!n_cantidad * IIf(pRs!n_presenta > 0, pRs!n_presenta, 1))
    mvarFecha = Format(pRs!f_fechahora, "dd/MM/yyyy")
    mvarHora = Format(pRs!f_fechahora, "HH:mm:ss")
    mvarTienda = BuscarNombreTienda() & Mid(Trim(pRs!c_documento), 2, 2)
    mvarId = pRs!Id
    Set AgregarRegistroRs = Me
End Function
