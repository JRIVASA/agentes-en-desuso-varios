VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDescargaHuellas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mArchivo As String
Private mHuellas As String
Private mRegistroHuellas As Collection
Private HuellasBD As New ADODB.Recordset
Private ConsultaRS As String

Public Sub IniciarInterfaz()
    
    On Error GoTo ErrInicio
    
    Dim mCollectionProcesados As New Collection
    Dim mCollectionNoProcesados As New Collection
    
    HuellasBD.CursorLocation = adUseClient
    
    ConsultaRS = "select c_CodCliente as Cedula, bin_DataArray as TemplateSimple, n_TemplateSize as TemplateSize from ma_racionalizacion_clientes_huellas"
    HuellasBD.Open ConsultaRS, EstrucApp.Conexion.Conexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    Set HuellasBD.ActiveConnection = Nothing
    
    Debug.Print EstrucApp.Archivos.RutaDescargaHuellas
    
    mArchivo = Dir(EstrucApp.Archivos.RutaDescargaHuellas & "\*.txt", vbArchive)
    
    Do While mArchivo <> ""
        
        mCollectionNoProcesados.Add mArchivo
        mArchivo = UCase(mArchivo)
        
        If Left(mArchivo, 2) = "HG" Then
            If BuscarHuellas(mArchivo) Then
                If InsertarHuellas(mArchivo) Then
                    mCollectionProcesados.Add mArchivo
                    mCollectionNoProcesados.Remove mCollectionNoProcesados.Count
                End If
            End If
        End If
        
        mArchivo = Dir
        
    Loop
    
    For i = 1 To mCollectionNoProcesados.Count
        MoverArchivoNoProcesadoHuellas mCollectionNoProcesados(i)
    Next i
    
    For i = 1 To mCollectionProcesados.Count
        MoverArchivoProcesadoHuellas mCollectionProcesados(i)
    Next i
    
    If HuellasBD.State = adStateOpen Then HuellasBD.Close
    
    Exit Sub
    
ErrInicio:
    
    On Error Resume Next
    
    If HuellasBD.State = adStateOpen Then HuellasBD.Close
    
    EscribirLog "Error al inicializar Recordset de Huellas", Err.Description, Err.Number
    
End Sub

Private Function BuscarHuellas(pArchivo As String) As Boolean
    
    Dim mArchivo As String
    Dim mCanal As Integer
    Dim mCls As clsEstructuraDeHuellas
    Dim mLinea As String
    
    On Error GoTo Errores
    
    Set mRegistroHuellas = New Collection
    
    If EstrucApp.Archivos.ConvertirArchivo Then ConvertirArchivo (pArchivo)
    
    mCanal = FreeFile()
    Open EstrucApp.Archivos.RutaDescargaHuellas & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        
        Set mCls = BuscarEstructuraTrans(mLinea)
        
        If Not mCls Is Nothing Then
            mRegistroHuellas.Add mCls
        End If
    Loop
    
    Close #mCanal
    
    BuscarHuellas = True
    
    Exit Function
    
Errores:

    EscribirLog "Error Buscando Archivo " & pArchivo, Err.Description, Err.Number
    On Error Resume Next
    Close #mCanal
    Err.Clear
    
End Function

Private Sub ConvertirArchivo(pArchivo)

    On Error GoTo Errores

    Dim mLinea As String
    Dim mCanal As Integer
    Dim mTexto As String

    mCanal = FreeFile()
    
    Open EstrucApp.Archivos.RutaDescargaHuellas & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        mTexto = mTexto & mLinea
    Loop
    
    Close #mCanal
    
    Kill EstrucApp.Archivos.RutaDescargaHuellas & "\" & pArchivo
    
    mTexto = Replace(mTexto, vbLf, vbCrLf)
    
    Open EstrucApp.Archivos.RutaDescargaHuellas & "\" & mArchivo For Output Access Write As #mCanal
        Print #mCanal, mTexto;
    Close #mCanal
    
    Exit Sub
    
Errores:
    
    EscribirLog "Error convirtiendo Archivo: " & Chr(34) & pArchivo & Chr(34), Err.Description, Err.Number
    
End Sub

Private Function BuscarEstructuraTrans(pLinea As String) As clsEstructuraDeHuellas
    
    Dim mCls As New clsEstructuraDeHuellas
    
    On Error GoTo Errores
    
    mCls.Cedula = ObtenerCadena(pLinea, 1, 12)
    mCls.Cliente = ObtenerCadena(pLinea, 13, 50)
    mCls.TemplateIzq = HexToString(ObtenerCadena(pLinea, 63, 1500))
    mCls.TemplateDer = HexToString(ObtenerCadena(pLinea, 1563, 1500))
    mCls.Tienda = ObtenerCadena(pLinea, 3063, 3)
    mCls.Fecha = ObtenerCadena(pLinea, 3066, 8)
    
    If mCls.Cedula = "" Or (mCls.TemplateIzq = "" And mCls.TemplateDer = "") Or mCls.Tienda = "" Or mCls.Fecha = "" Then
        'Debug.Print pLinea
        EscribirLog "Error Leyendo l�nea, datos o formato inv�lidos. -> " & pLinea, , 0
        Set BuscarEstructuraTrans = Nothing: Exit Function
    End If
    
    Set BuscarEstructuraTrans = mCls
    
    Exit Function

Errores:
    
    Set BuscarEstructuraTrans = Nothing

    EscribirLog "Error Leyendo L�nea (" & Err.Description & "). -> " & pLinea, , Err.Number
    
End Function

Private Function ObtenerFecha(pCad As String) As String
    Dim mDia, mMes, mAno
    
    mDia = ObtenerCadena(pCad, 3072, 2)
    mMes = ObtenerCadena(pCad, 3070, 2)
    mAno = ObtenerCadena(pCad, 3066, 4)
    ObtenerFecha = DateSerial(mAno, mMes, mDia)
End Function

Private Function InsertarHuellas(pArchivo As String) As Boolean

    Dim mCls As clsEstructuraDeHuellas
    Dim PoseeFormatoEstandar As Boolean
    'Variable para procesar a la BD
    Dim Procesar As Boolean
    Const MaxTemplateSize = 1024
    Dim ArregloParcialIZQ() As Byte, ArregloFullIZQ() As Byte, SizeIZQ As Long
    Dim ArregloParcialDER() As Byte, ArregloFullDER() As Byte, SizeDER As Long
    Dim ArregloBD() As Byte, SizeBD As Long
    Dim BioMatcher As Long ', mStat As UFM_STATUS
    Dim MatchIZQ As Long, MatchDER As Long
    Dim MatchInfoIZQ As Collection, MatchInfoDER As Collection
    Dim MatchCountIZQ As Long, MatchCountDER As Long
    
    'Inicializar SDK

    'mStat = UFM_Create(BioMatcher)

    'If mStat <> UFM_STATUS.OK Then
        'EscribirLog "Error inicializando SDK.", "Codigo de Error: " & CStr(mStat)
        'Exit Function
    'End If
    
    'mStat = UFM_SetTemplateType(BioMatcher, UFM_TEMPLATE_TYPE.UFM_TEMPLATE_TYPE_ISO19794_2)
    
    'If mStat <> UFM_STATUS.OK Then
        'EscribirLog "Error inicializando SDK.", "Codigo de Error: " & CStr(mStat)
        'Exit Function
    'End If
    
    ' Security level ranges from 1 to 7
    
    'mStat = UFM_SetParameter(BioMatcher, UFM_PARAM.SECURITY_LEVEL, 6)
    
    'mStat = UFM_SetParameter(BioMatcher, UFM_PARAM.FAST_MODE, 1)
    
    'Procesamiento de Huellas.
    
    For Each mCls In mRegistroHuellas
    
        If Left(mCls.Tienda, 3) <> EstrucApp.Tienda.Tienda Then
            
            PoseeFormatoEstandar = ((mCls.TemplateIzq Like "*FMR*") And (mCls.TemplateIzq Like "*20*")) _
            Or ((mCls.TemplateDer Like "*FMR*") And (mCls.TemplateDer Like "*20*")) 'IIf(, True, False)
            
            If PoseeFormatoEstandar Then
                
                'Obtener Arreglos
                            
                SizeIZQ = Len(mCls.TemplateIzq)
                SizeDER = Len(mCls.TemplateDer)
                
                ReDim ArregloParcialIZQ(SizeIZQ) As Byte
                ReDim ArregloParcialDER(SizeDER) As Byte
                'ReDim ArregloFullIZQ(MaxTemplateSize - 1) As Byte
                'ReDim ArregloFullDER(MaxTemplateSize - 1) As Byte
                
                'For i = 1 To SizeIZQ
                    'ArregloParcialIZQ(i - 1) = Asc(Mid(mCls.TemplateIzq, i, 1))
                    'ArregloFullIZQ(i - 1) = ArregloParcialIZQ(i - 1)
                'Next i
                
                'For i = 1 To SizeDER
                    'ArregloParcialDER(i - 1) = Asc(Mid(mCls.TemplateDer, i, 1))
                    'ArregloFullDER(i - 1) = ArregloParcialDER(i - 1)
                'Next i
                
                ArregloParcialIZQ = StrConv(mCls.TemplateIzq, vbFromUnicode)
                ArregloParcialDER = StrConv(mCls.TemplateDer, vbFromUnicode)
                
                'Comprobaci�n
                
'                If Not HuellasBD.RecordCount = 0 Then HuellasBD.MoveFirst
'
'                MatchCountIZQ = 0: MatchCountDER = 0
'                Set MatchInfoIZQ = New Collection
'                Set MatchInfoDER = New Collection
'
'                Do While Not HuellasBD.EOF
'
'                    'Debug.Print HuellasBD.AbsolutePosition
'
'                    ArregloBD = HuellasBD!TemplateSimple
'                    SizeBD = HuellasBD!TemplateSize
'
'                    mStat = UFM_Verify(BioMatcher, ArregloParcialIZQ(0), SizeIZQ, ArregloBD(0), SizeBD, MatchIZQ)
'
'                    If CBool(MatchIZQ) Then
'                        If Not Collection_ExisteKey(MatchInfoIZQ, HuellasBD!Cedula) Then MatchInfoIZQ.Add CStr(HuellasBD!Cedula), CStr(HuellasBD!Cedula)
'                        MatchCountIZQ = MatchCountIZQ + 1
'                    End If
'
'                    mStat = UFM_Verify(BioMatcher, ArregloParcialDER(0), SizeDER, ArregloBD(0), SizeBD, MatchDER)
'
'                    If CBool(MatchDER) Then
'                        If Not Collection_ExisteKey(MatchInfoDER, HuellasBD!Cedula) Then MatchInfoDER.Add CStr(HuellasBD!Cedula), CStr(HuellasBD!Cedula)
'                        MatchCountDER = MatchCountDER + 1
'                    End If
'
'                    HuellasBD.MoveNext
'
'                Loop
                
                'Verificar Resultados
                
                'INDICE IZQUIERDO
                
'                If MatchCountIZQ <= 0 Then
'                    Procesar = True
'                ElseIf MatchCountIZQ = 1 And MatchInfoIZQ.Count = 1 Then
'                    If UCase(MatchInfoIZQ.Item(1)) <> UCase(mCls.Cedula) Then
'                        '"Robo de Identidad" 'Chequear
'                        Procesar = False
'                    Else
'                        '"Huella confirmada"
'                        Procesar = True
'                    End If
'                ElseIf MatchCountIZQ <= 2 And MatchInfoIZQ.Count > 1 Then
'                    '"Robo de Identidad" 'Chequear
'                    Procesar = False
'                ElseIf MatchCountIZQ >= 2 And MatchInfoIZQ.Count = 1 Then
'                    '"Registros multiples" ' Chequear...
'                    Procesar = False
'                ElseIf MatchCountIZQ > 2 And MatchInfoIZQ.Count > 1 Then
'                    '"Registros Multiples y Robo de Identidad" 'Chequear
'                    Procesar = False
'                End If
'
'                'INDICE DERECHO
'
'                If Procesar Then
'
'                    If MatchCountDER <= 0 Then
'                        Procesar = True
'                    ElseIf MatchCountDER = 1 And MatchInfoDER.Count = 1 Then
'                        If UCase(MatchInfoDER.Item(1)) <> UCase(mCls.Cedula) Then
'                            '"Robo de Identidad" 'Chequear
'                            Procesar = False
'                        Else
'                            '"Huella confirmada"
'                            Procesar = True
'                        End If
'                    ElseIf MatchCountDER <= 2 And MatchInfoDER.Count > 1 Then
'                        '"Robo de Identidad" 'Chequear
'                        Procesar = False
'                    ElseIf MatchCountDER >= 2 And MatchInfoDER.Count = 1 Then
'                        '"Registros multiples" ' Chequear...
'                        Procesar = False
'                    ElseIf MatchCountDER > 2 And MatchInfoDER.Count > 1 Then
'                        '"Registros Multiples y Robo de Identidad" 'Chequear
'                        Procesar = False
'                    End If
'
'                End If

                Procesar = True
                             
            Else
                Procesar = False
            End If
            
            If Procesar Then
                'Procesar = EjecutarSql(mCls, pArchivo, ArregloParcialIZQ, ArregloParcialDER, ArregloFullIZQ, ArregloFullDER, SizeIZQ, SizeDER)
                Procesar = EjecutarSql(mCls, pArchivo, ArregloParcialIZQ, ArregloParcialDER, Nothing, Nothing, SizeIZQ, SizeDER)
            End If
        
        Else
        
            Procesar = False
        
        End If
        
        'Debug.Print "Listo" & " - " & mCls.Cedula
            
    Next
    
    'UFM_Delete (Matcher)
    
    InsertarHuellas = True
    
End Function

Private Function EjecutarSql(pCls As clsEstructuraDeHuellas, pArchivo As String, pArrIzq As Variant, pArrDer As Variant, _
pArrFullIzq, pArrFullDer, pArrSizeIzq As Long, pArrSizeDer As Long) As Boolean
    
    Dim mSql As String
    Dim mSql2 As String
    Dim PoseeFormatoEstandar As Boolean, Plantilla As Long
    
    On Error GoTo Errores
    
    Dim TransaccionIniciada As Boolean
    
    EstrucApp.Conexion.Conexion.BeginTrans
    TransaccionIniciada = True
    
    Dim mRsIzq As New ADODB.Recordset
    Dim mRsDer As New ADODB.Recordset
    Dim mRsCliente As New ADODB.Recordset
    
    mSql = "DELETE FROM MA_RACIONALIZACION_CLIENTES_HUELLAS WHERE c_CodCliente = '" & pCls.Cedula & "'"
    EstrucApp.Conexion.Conexion.Execute mSql
    
    PoseeFormatoEstandar = ((pCls.TemplateIzq Like "*FMR*") And (pCls.TemplateIzq Like "*20*"))
    
    If PoseeFormatoEstandar Then
        
        With mRsIzq
        
            .Open "SELECT * FROM MA_RACIONALIZACION_CLIENTES_HUELLAS WHERE 1 = 2", EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
            
            .AddNew
            
            !c_CodCliente = pCls.Cedula
            !ID_Dispositivo = "Externo"
            !c_Dedo = "Indice Izquierdo"
            !c_TipoHuella = "Est�ndar ISO 19794-2"
            !Bin_Data = pCls.TemplateIzq
            !bin_DataArray = pArrIzq
            '!Bin_DataArrayFull = pArrFullIzq
            !n_TemplateSize = pArrSizeIzq
            !c_Archivo = pArchivo
            !b_Enviado = 1
            !c_Cadena_Tienda = pCls.Tienda
            !d_Fecha = CDate(Mid(pCls.Fecha, 1, 4) & "/" & Mid(pCls.Fecha, 5, 2) & "/" & Mid(pCls.Fecha, 7, 2))
            
            .UpdateBatch
            
            Plantilla = Plantilla + CH_Dedos.IndiceIzquierdo
            
        End With
        
    End If
    
    PoseeFormatoEstandar = ((pCls.TemplateDer Like "*FMR*") And (pCls.TemplateDer Like "*20*"))
    
    If PoseeFormatoEstandar Then
        
        With mRsDer
            
            .Open "SELECT * FROM MA_RACIONALIZACION_CLIENTES_HUELLAS WHERE 1 = 2", EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
            
            .AddNew
            
            !c_CodCliente = pCls.Cedula
            !ID_Dispositivo = "Externo"
            !c_Dedo = "Indice Derecho"
            !c_TipoHuella = "Est�ndar ISO 19794-2"
            !Bin_Data = pCls.TemplateDer
            !bin_DataArray = pArrDer
            '!Bin_DataArrayFull = pArrFullDer
            !n_TemplateSize = pArrSizeDer
            !c_Archivo = pArchivo
            !b_Enviado = 1
            !c_Cadena_Tienda = pCls.Tienda
            !d_Fecha = CDate(Mid(pCls.Fecha, 1, 4) & "/" & Mid(pCls.Fecha, 5, 2) & "/" & Mid(pCls.Fecha, 7, 2))
            
            .UpdateBatch
            
            Plantilla = Plantilla + CH_Dedos.IndiceDerecho
            
        End With
        
    End If
    
    With mRsCliente
    
        .Open "SELECT * FROM MA_RACIONALIZACION_CLIENTES where c_Rif = '" & pCls.Cedula & "'", _
        EstrucApp.Conexion.Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
        
        If .EOF Then
            .AddNew
        Else
            .Update
        End If
        
        Dim Descripcion As Variant
        Descripcion = DescripcionCliente(pCls.Cliente)
        
        !c_Rif = pCls.Cedula
        !c_Nombre = Descripcion(1)
        !c_Apellido = Descripcion(0)
        
        If IsEmpty(!b_activo) Then
            !b_activo = 1
        End If
        
        !n_PlantillaBiometrico = Plantilla '72 'Ambos Indices en la Plantilla de los dedos registrados.
        !c_Archivo = pArchivo
        !b_Enviado = 1
        
        .UpdateBatch
        
    End With
    
    EstrucApp.Conexion.Conexion.CommitTrans
    
    A�adirHuellasRs
    
    mRsIzq.Close
    mRsDer.Close
    mRsCliente.Close
    
    EjecutarSql = True
    
    Exit Function

Errores:

    Debug.Print Err.Description

    EjecutarSql = False

    If TransaccionIniciada Then EstrucApp.Conexion.Conexion.RollbackTrans
    
    'MsgBox Err.Description
    
    EscribirLog "Insertando registros en la Base de datos (" & Err.Description & ") -> " & _
    pCls.Cedula & ";" & pCls.Cliente & ";" & pCls.TemplateIzq & ";" & pCls.TemplateDer & ";" & pCls.Tienda & ";" & pCls.Fecha
    
End Function

Private Function A�adirHuellasRs() As Boolean

    On Error GoTo Err

    Dim InicioAddNew As Boolean

    Dim NuevasHuellas As ADODB.Recordset
    Set NuevasHuellas = EstrucApp.Conexion.Conexion.Execute("SELECT TOP (2) * FROM MA_RACIONALIZACION_CLIENTES_HUELLAS ORDER BY ID DESC")

    With HuellasBD
    
        .AddNew
        InicioAddNew = True
        
        !Cedula = NuevasHuellas!c_CodCliente
        !TemplateSimple = NuevasHuellas!bin_DataArray
        !TemplateSize = NuevasHuellas!n_TemplateSize
        
        .UpdateBatch
        
    End With
    
    A�adirHuellasRs = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    On Error Resume Next
    
    If InicioAddNew Then HuellasBD.CancelUpdate
    
End Function

Public Function HexToString(ByVal HexToStr As String) As String

    Dim strTemp As String
    Dim StrReturn As String
    Dim i As Long
    
    For i = 1 To Len(HexToStr) Step 2
    strTemp = Chr$(Val("&H" & Mid$(HexToStr, i, 2)))
    StrReturn = StrReturn & strTemp
    Next i
    
    HexToString = StrReturn

End Function

Public Function DescripcionCliente(pCadena As String) As Variant
 
    On Error GoTo Err
 
    Dim Temporal As Variant
    Dim nPalabras As Integer
    Dim Datos(1) As String
    Temporal = Split(pCadena, " ")
    nPalabras = UBound(Temporal) + 1
    
    If (nPalabras Mod 2 = 0) Then
        For i = 0 To (nPalabras / 2) - 1
            Datos(0) = Datos(0) & IIf(Datos(0) = "", "", " ") & Temporal(i)
        Next i
        For i = (nPalabras / 2) To (nPalabras - 1)
            Datos(1) = Datos(1) & IIf(Datos(1) = "", "", " ") & Temporal(i)
        Next i
    Else
        If (nPalabras = 1) Then
            Datos(0) = Temporal(0)
        Else
            For i = 0 To Fix((nPalabras / 2) - 1)
                Datos(0) = Datos(0) & IIf(Datos(0) = "", "", " ") & Temporal(i)
            Next i
            For i = Fix(nPalabras / 2) To (nPalabras - 1)
                Datos(1) = Datos(1) & IIf(Datos(1) = "", "", " ") & Temporal(i)
            Next i
        End If
    End If
    
    DescripcionCliente = Datos
    
    Exit Function
    
Err:
    
    DescripcionCliente = Split(".", ".")
    
End Function
