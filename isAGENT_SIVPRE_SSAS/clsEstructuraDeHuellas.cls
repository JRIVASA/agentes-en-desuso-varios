VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsEstructuraDeHuellas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCedula                                          As String
Private mvarDescripcionCliente                              As String
Private mvarTemplateIZQ                                     As String
Private mvarTemplateDER                                     As String
Private mvarTienda                                          As String
Private mvarFecha                                           As String

'*********************************** properties get ************************************************
Property Get Cedula() As String
    Cedula = mvarCedula
End Property

Property Get Cliente() As String
    Cliente = mvarDescripcionCliente
End Property

Property Get TemplateIzq() As String
    TemplateIzq = mvarTemplateIZQ
End Property

Property Get TemplateDer() As String
    TemplateDer = mvarTemplateDER
End Property

Property Get Tienda() As String
    Tienda = mvarTienda
End Property

Property Get Fecha() As String
    Fecha = mvarFecha
End Property

'*********************************** properties let ************************************************

Property Let Cedula(pValor As String)
    mvarCedula = pValor
End Property

Property Let Cliente(pValor As String)
    mvarDescripcionCliente = pValor
End Property

Property Let TemplateIzq(pValor As String)
    mvarTemplateIZQ = pValor
End Property

Property Let TemplateDer(pValor As String)
    mvarTemplateDER = pValor
End Property

Property Let Tienda(pValor As String)
    mvarTienda = pValor
End Property

Property Let Fecha(pValor As String)
    mvarFecha = pValor
End Property

Public Function AgregarRegistro(pCedula As String, pCliente As String, pHuellaIzq As String, pHuellaDer As String, pFecha As Date) As clsEstructuraDeHuellas
    mvarCedula = Left(pCedula, 12)
    mvarDescripcionCliente = Left(pCliente, 50)
    mvarTemplateIZQ = Left(StringToHex(pHuellaIzq), 1500)
    mvarTemplateDER = Left(StringToHex(pHuellaDer), 1500)
    mvarTienda = BuscarNombreTiendaHuellas()
    mvarFecha = Format(pFecha, "yyyyMMdd")
    Set AgregarRegistro = Me
End Function
