VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCargaHuellas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'modulo de carga de huellas para racionamiento
Private mRegistroHuella                                     As Collection
Private mArchivo                                            As String

Public Sub IniciarInterfaz()
    
    If BuscarRegistros Then
        If CrearArchivo Then
            If MarcarEnviado Then
                MoverArchivoHuellas (mArchivo)
                Exit Sub
            End If
        End If
    End If
    
    If Dir(App.Path & "\" & mArchivo) <> "" Then
        Kill App.Path & "\" & mArchivo
    End If
    
End Sub

Private Function BuscarRegistros() As Boolean
    
    Dim mRsClientes As New ADODB.Recordset
    Dim mRsHIzq As New ADODB.Recordset, mRsHDer As New ADODB.Recordset
    Dim mSql As String
    Dim mClsTr As clsEstructuraDeHuellas
    Dim mCodCliente As String, mHuellaIzq As String, mHuellaDer As String
    Dim mDescripcionCliente As String, pFecha As Date
    Dim Omitir As Boolean
    
    On Error GoTo Errores
    
    Set mRegistroHuella = New Collection
    
    mSql = "SELECT *, GetDate() as Fecha from MA_RACIONALIZACION_CLIENTES where c_Archivo='' and b_Enviado=0"
    
    mRsClientes.CursorLocation = adUseClient
    
    mRsClientes.Open mSql, EstrucApp.Conexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Set mRsClientes.ActiveConnection = Nothing
    
    Do While Not mRsClientes.EOF
    
        mCodCliente = mRsClientes!c_Rif
        
        Set mRsHIzq = New ADODB.Recordset: Set mRsHDer = New ADODB.Recordset
        
        mSql = "SELECT TOP(2) * FROM MA_RACIONALIZACION_CLIENTES_HUELLAS WHERE c_CodCliente = '" & mCodCliente & "' AND c_Dedo LIKE '%Izquierdo%'"
        mRsHIzq.Open mSql, EstrucApp.Conexion.Conexion, adOpenStatic, adLockReadOnly, adCmdText
        
        mSql = "SELECT TOP(2) * FROM MA_RACIONALIZACION_CLIENTES_HUELLAS WHERE c_CodCliente = '" & mCodCliente & "' AND c_Dedo LIKE '%Derecho%'"
        mRsHDer.Open mSql, EstrucApp.Conexion.Conexion, adOpenStatic, adLockReadOnly, adCmdText
        
        If mRsHIzq.RecordCount >= 1 And mRsHDer.RecordCount >= 1 Then
            mHuellaIzq = mRsHIzq!Bin_Data
            pFecha = mRsHIzq!d_Fecha
            mHuellaDer = mRsHDer!Bin_Data
            Omitir = False
        ElseIf mRsHIzq.RecordCount = 2 And mRsHDer.RecordCount = 0 Then
            mHuellaIzq = mRsHIzq!Bin_Data
            pFecha = mRsHIzq!d_Fecha
            mRsHIzq.MoveNext
            mHuellaDer = mRsHIzq!Bin_Data
            Omitir = False
        ElseIf mRsHIzq.RecordCount = 0 And mRsHDer.RecordCount = 2 Then
            mHuellaIzq = mRsHDer!Bin_Data
            pFecha = mRsHDer!d_Fecha
            mRsHDer.MoveNext
            mHuellaDer = mRsHDer!Bin_Data
            Omitir = False
        'ElseIf mRsHIzq.RecordCount = 1 And mRsHDer.RecordCount = 0 Then
            'mHuellaIzq = mRsHIzq!Bin_Data
            'pFecha = mRsHIzq!d_Fecha
            'mHuellaDer = ""
            'Omitir = False
        'ElseIf mRsHIzq.RecordCount = 0 And mRsHDer.RecordCount = 1 Then
            'mHuellaDer = mRsHDer!Bin_Data
            'pFecha = mRsHDer!d_Fecha
            'mHuellaIzq = ""
            'Omitir = False
        Else
            Omitir = True
        End If
        
        If Not Omitir Then
            mDescripcionCliente = mRsClientes!c_Apellido & IIf(mRsClientes!c_Apellido <> "", " ", "") & mRsClientes!c_Nombre
            
            Set mClsTr = New clsEstructuraDeHuellas
            mRegistroHuella.Add mClsTr.AgregarRegistro(mCodCliente, _
            mDescripcionCliente, mHuellaIzq, mHuellaDer, pFecha)
        End If
        
        mRsClientes.MoveNext
        
    Loop
    
    BuscarRegistros = True
    
    Exit Function
    
Errores:

    EscribirLog "Error al buscar informaci�n de huellas digitales.", Err.Description, Err.Number
    
End Function

Private Function CrearArchivo() As Boolean

    Dim mClsTr As clsEstructuraDeHuellas
    Dim mCadena As String, mRutina As String
    Dim mCanal As Integer, mLinea As String
    Dim mRegistrosCount As Integer, TempLineCount As Integer
    
    On Error GoTo Errores
    
    mRutina = "BuscarNombreArchivo"
    mArchivo = BuscarNombreArchivo()
    mRutina = "CrearArchivo"
    mCanal = FreeFile()
    
    If mRegistroHuella.Count = 0 Then
        CrearArchivo = False
        EscribirLog mRutina, "No se encontraron registros para enviar.", 0
        Exit Function
    End If
    
    mRegistrosCount = mRegistroHuella.Count
    
    TempLineCount = 0
    
    For Each mClsTr In mRegistroHuella
    
        TempLineCount = TempLineCount + 1
        
        mCadena = mCadena & BuscarCadena(mClsTr) & _
        IIf(TempLineCount >= mRegistrosCount, vbNewLine, vbCrLf)

        Debug.Print mCadena

    Next
    
    'Debug.Print mCadena
      
    Open App.Path & "\" & mArchivo For Output Access Write As #mCanal
        Print #mCanal, mCadena;
    Close #mCanal
    
    'Atenci�n: Se modific� el codigo para generar el archivo sin la ultima linea en blanco
    'Puesto que Print sin el ; al final de la sentencia envia siempre la cadena mas un
    'vbCrLF (Retorno de Carro -> Nueva Linea) y se requiere que el archivo
    'se pueda leer directamente sin tomar en cuenta estos detalles.
    
    CrearArchivo = True
    
    Exit Function
    
Errores:

    EscribirLog mRutina, Err.Description, Err.Number
    
End Function

Private Function BuscarCadena(pClsTr As clsEstructuraDeHuellas) As String
    
    Dim mCadena As String
    
    mCadena = mCadena & FormatearCadena(Left(pClsTr.Cedula, 12), 12, eAgregarAlFinal, " ")
    mCadena = mCadena & FormatearCadena(Left(pClsTr.Cliente, 50), 50, eAgregarAlFinal, " ")
    mCadena = mCadena & FormatearCadena(Left(pClsTr.TemplateIzq, 1500), 1500, eAgregarAlFinal, " ")
    mCadena = mCadena & FormatearCadena(Left(pClsTr.TemplateDer, 1500), 1500, eAgregarAlFinal, " ")
    mCadena = mCadena & Left(pClsTr.Tienda, 3)
    mCadena = mCadena & pClsTr.Fecha
    
    BuscarCadena = mCadena
    
End Function

Private Function MarcarEnviado() As Boolean
    
    On Error GoTo Errores
   
    Dim InCedulas As String
    
    Dim mSql As String
    
    For Each Registro In mRegistroHuella
        InCedulas = InCedulas & IIf(Len(InCedulas) <= 0, "'", ",  '") & Registro.Cedula & "'"
    Next
    
    EstrucApp.Conexion.Conexion.BeginTrans
    
    mSql = "UPDATE MA_RACIONALIZACION_CLIENTES SET c_Archivo = '" & mArchivo & "', b_Enviado=1 WHERE c_Rif IN (" & InCedulas & ")"
   
    EstrucApp.Conexion.Conexion.Execute mSql
   
    mSql = "UPDATE MA_RACIONALIZACION_CLIENTES_HUELLAS SET c_Archivo = '" & mArchivo & "', b_Enviado=1 WHERE c_CodCliente IN (" & InCedulas & ")"

    EstrucApp.Conexion.Conexion.Execute mSql
    
    EstrucApp.Conexion.Conexion.CommitTrans
    
    MarcarEnviado = True
    
    Exit Function
    
Errores:
    
    Debug.Print Err.Description
    
    On Error Resume Next
    
    EstrucApp.Conexion.Conexion.RollbackTrans
    
    EscribirLog "Marcar Registros de Huellas Enviados", Err.Description, Err.Number
    
End Function

