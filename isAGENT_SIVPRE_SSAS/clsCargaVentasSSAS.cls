VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCargaVentasSSAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' modulo de carga lleva las ventas del supermercado al servidor central
Private mProductos                                          As Collection
Private mArchivo                                            As String
Private mMinId                                              As Long
Private mMaxId                                              As Long

Public Sub IniciarInterfaz()
    
    If BuscarProductos() Then
        If CrearArchivo Then
            If MarcarEnviado Then
                MoverArchivo mArchivo
                Exit Sub
            End If
        End If
    End If
    
    If mArchivo <> "" Then
        If Dir(App.Path & "\" & mArchivo) <> "" Then
            Kill App.Path & "\" & mArchivo
        End If
    End If
    
End Sub

Private Function BuscarProductos() As Boolean

    Dim mRs As ADODB.Recordset
    Dim mSql As String
    Dim mClsTr As clsEstructuraTransaccion
    Dim mTempRif As String
    Dim ExisteCampo_EnvioSelectivo As Boolean
    
    On Error GoTo Errores
    
    Set mProductos = New Collection
    
    ExisteCampo_EnvioSelectivo = ExisteCampoTabla("n_EnvioSivpre", , "SELECT n_EnvioSivpre FROM MA_RACIONALIZACION_RUBROS WHERE 1=2", , EstrucApp.Conexion.Conexion)
    
    mSql = "Select * from tr_racionalizacion where (c_archivo='') and (b_enviado=0) and (c_documento <> 'importado')"
    
    Set mRs = New ADODB.Recordset
    
    mRs.CursorLocation = adUseClient
    mRs.Open mSql, EstrucApp.Conexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    mRs.ActiveConnection = Nothing
    mRs.Sort = "id"
    
    Do While Not mRs.EOF
        mTempRif = ValidarRif(CStr(mRs!c_Rif))
        
        Set mClsTr = BuscarRubroEnCollection(mTempRif, mRs!c_codrubro, Format(mRs!f_fechahora, "yyyyMMdd"))
        
        If mMinId > mRs!Id Or mMinId = 0 Then mMinId = mRs!Id
        If mMaxId < mRs!Id Then mMaxId = mRs!Id
        
        If AplicaEnvioRubro(CStr(mRs!c_codrubro), EstrucApp.Conexion.Conexion, ExisteCampo_EnvioSelectivo) Then
        
            If mClsTr Is Nothing Then
                Set mClsTr = New clsEstructuraTransaccion
                mProductos.Add mClsTr.AgregarRegistroRs(mRs)
            Else
                mClsTr.Cantidad = mClsTr.Cantidad + (mRs!n_cantidad * IIf(mRs!n_presenta > 0, mRs!n_presenta, 1))
                If Trim(mClsTr.Signo) <> "" And mClsTr.Cantidad > 0 Then
                    mClsTr.Signo = " "
                End If
            End If
        
        End If
        
        mRs.MoveNext
    Loop
    
    BuscarProductos = True
    
    Exit Function
    
Errores:

    EscribirLog "Error al buscar informacion ventas", Err.Description, Err.Number
    
End Function

Private Function CrearArchivo() As Boolean

    Dim mClsTr As clsEstructuraTransaccion
    Dim mCadena As String, mRutina As String
    Dim mCanal As Integer, mLinea As String
    Dim mProductosCount As Long, TempLineCount As Long
    
    On Error GoTo Errores
    mRutina = "BuscarNombreArchivo"
    mArchivo = BuscarNombreArchivo()
    mRutina = "CrearArchivo"
    mCanal = FreeFile()
    
    If mProductos.Count = 0 Then
        CrearArchivo = False
        EscribirLog mRutina, "No se encontraron transacciones para enviar.", 0
        Exit Function
    End If
    
    mProductosCount = mProductos.Count
    TempLineCount = 0
    
    For Each mClsTr In mProductos
        TempLineCount = TempLineCount + 1
        
        If ValidarCantidadRubro(mClsTr) Then
            mCadena = mCadena & BuscarCadena(mClsTr) & _
            IIf(TempLineCount >= mProductosCount, "", vbCrLf)
        End If
    Next
    
    'Debug.Print mCadena
      
    Open App.Path & "\" & mArchivo For Output Access Write As #mCanal
        Print #mCanal, mCadena;
    Close #mCanal
    
    'Atenci�n: Se modific� el codigo para generar el archivo sin la ultima linea en blanco
    'Puesto que Print sin el ; al final de la sentencia envia siempre la cadena mas un
    'vbCrLF (Retorno de Carro -> Nueva Linea) y se requiere que el archivo
    'se pueda leer directamente sin tomar en cuenta estos detalles.
    
    CrearArchivo = True
    
    Exit Function
    
Errores:
    
    EscribirLog mRutina, Err.Description, Err.Number

End Function

Private Function BuscarCadena(pClsTr As clsEstructuraTransaccion) As String
    
    Dim mCadena As String
    
    'primer posicion V,J,E,P tipo identificacion
    mCadena = Chr(34) & " " & Chr(34) & ","
    mCadena = mCadena & Chr(34) & pClsTr.TipoIdentificacion
    
    'Debug.Print FormatNumber(CDbl(pClsTr.Cantidad), 2)
    'Debug.Print Replace(Format(pClsTr.Cantidad, "###0.00"), ".", ",")
    
    If IsNumeric(Left(Trim(pClsTr.Cedula), 1)) Then
        mCadena = mCadena & FormatearCadena(Mid(pClsTr.Cedula, 1, 11), 8, eAgregarAlFinal) & Chr(34) & ","
    Else
        mCadena = mCadena & FormatearCadena(Mid(pClsTr.Cedula, 2, 12), 8, eAgregarAlFinal) & Chr(34) & ","
    End If
    mCadena = mCadena & Chr(34) & FormatearCadena(pClsTr.Fecha & " " & pClsTr.Hora, 16, eAgregarAlFinal) & Chr(34) & ","
    mCadena = mCadena & Chr(34) & pClsTr.Rubro & Chr(34) & ","
    'mCadena = mCadena & pClsTr.Cantidad & ","
    mCadena = mCadena & Chr(34) & Replace(Format(pClsTr.Cantidad, "###0.00"), ".", ",") & Chr(34) & ","
    mCadena = mCadena & Chr(34) & EstrucApp.Tienda.Tienda & Chr(34) & ","
    mCadena = mCadena & Chr(34) & EstrucApp.Tienda.Sucursal & Chr(34) & ","
    mCadena = mCadena & Chr(34) & EstrucApp.Tienda.Estado & Chr(34)
    
    BuscarCadena = mCadena
    
End Function

Private Function ValidarCantidadRubro(pClsTr As clsEstructuraTransaccion) As Boolean
    ValidarCantidadRubro = (pClsTr.Cantidad <> 0)
End Function

Private Function MarcarEnviado() As Boolean

    Dim mSql As String
    On Error GoTo Errores
   
    mSql = "Update tr_racionalizacion set c_archivo='" & RTrim(LTrim(Mid(mArchivo, 1, 45))) & "',b_enviado=1 " _
        & "where (b_enviado=0) and (id between " & mMinId & " and " & mMaxId & ")"
    
    EstrucApp.Conexion.Conexion.Execute mSql
    MarcarEnviado = True
    Exit Function
    
Errores:

    MsgBox Err.Description, vbCritical
    EscribirLog "MarcarEnviado", Err.Description, Err.Number
    
End Function

Private Function BuscarRubroEnCollection(pCedula As String, pRubro As String, pFecha As String) As clsEstructuraTransaccion
    
    Dim mClsTr As clsEstructuraTransaccion
    
    For Each mClsTr In mProductos
        If mClsTr.Fecha = mClsTr.Fecha And mClsTr.Cedula = UCase(Trim(Replace(pCedula, "-", ""))) And mClsTr.Rubro = pRubro Then
            Set BuscarRubroEnCollection = mClsTr
        End If
    Next
    
End Function

Private Function AplicaEnvioRubro(pCodRubro As String, pCn As ADODB.Connection, pExisteCampo As Boolean) As Boolean

    If Not pExisteCampo Then AplicaEnvioRubro = True: Exit Function

    On Error GoTo Err
    
    Dim mRs As ADODB.Recordset
    
    Set mRs = pCn.Execute("SELECT n_EnvioSivpre FROM MA_RACIONALIZACION_RUBROS WHERE c_Codigo = '" & pCodRubro & "'")
    
    If Not mRs.EOF Then
        AplicaEnvioRubro = CBool(mRs!n_EnvioSivpre)
    End If
    
    Exit Function
    
Err:

    'Si ocurre alg�n error en la comprobaci�n, pues que se incluya en el archivo.

    AplicaEnvioRubro = True

End Function



