VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDescargaVentasSSAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mProductos                                          As Collection
Private mArchivo                                            As String
Private mMinId                                              As Long
Private mMaxId                                              As Long

Public Sub IniciarInterfaz()

    Dim mCollectionProcesados As New Collection
    Dim mCollectionNoProcesados As New Collection
    Dim i As Long
    
    mArchivo = Dir(EstrucApp.Archivos.RutaArchivoDescarga & "\*.csv", vbArchive)
    
    Do While mArchivo <> ""
        mCollectionNoProcesados.Add mArchivo
        
        mArchivo = UCase(mArchivo)
'GoTo NoLeer
        If Left(mArchivo, 1) <> "G" Then
            If BuscarProductos(mArchivo) Then
                If InsertarProductos(mArchivo) Then
'NoLeer:
                    mCollectionProcesados.Add mArchivo
                    mCollectionNoProcesados.Remove mCollectionNoProcesados.Count
                End If
            End If
        Else
            'quiere decir q es rubro
            ActualizarRubros mArchivo
        End If
        
        mArchivo = Dir
    Loop
    
    For i = 1 To mCollectionNoProcesados.Count
        MoverArchivoNoProcesado mCollectionNoProcesados(i)
    Next i
    
    For i = 1 To mCollectionProcesados.Count
        MoverArchivoProcesado mCollectionProcesados(i)
    Next i
    
End Sub

Private Function BuscarProductos(pArchivo As String) As Boolean
    
    Dim mArchivo As String
    Dim mCanal As Integer
    Dim mCls As clsEstructuraTransaccion
    Dim mLinea As String
    
    On Error GoTo errores
    Set mProductos = New Collection
    
    If EstrucApp.Archivos.ConvertirArchivo Then ConvertirArchivo (pArchivo)
    
    mCanal = FreeFile()
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        
        Set mCls = BuscarEstructuraTrans(mLinea)
        
        If Not mCls Is Nothing Then
            mProductos.Add mCls
        End If
    Loop
    
    Close #mCanal
    
    BuscarProductos = True
    
    Exit Function
    
errores:
    
    EscribirLog "Error Buscando Archivo " & pArchivo, Err.Description, Err.Number
    On Error Resume Next
    Close #mCanal
    Err.Clear
    
End Function

Private Sub ConvertirArchivo(pArchivo)

    On Error GoTo errores

    Dim mLinea As String
    Dim mCanal As Integer
    Dim mTexto As String

    mCanal = FreeFile()
    
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        mTexto = mTexto & mLinea
    Loop
    
    Close #mCanal
    
    Kill EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo
    
    mTexto = Replace(mTexto, vbLf, vbCrLf)
    
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & mArchivo For Output Access Write As #mCanal
        Print #mCanal, mTexto;
    Close #mCanal
    
    Exit Sub
    
errores:
    
    EscribirLog "Error convirtiendo Archivo: " & Chr(34) & pArchivo & Chr(34), Err.Description, Err.Number
    
End Sub

Private Function BuscarEstructuraTrans(pLinea As String) As clsEstructuraTransaccion
    
    Dim mCls As New clsEstructuraTransaccion
    
    On Error GoTo errores
    
    Dim Arreglo() As String
    Arreglo = Split(pLinea, ",")
    mCls.cedula = Arreglo(0)
    mCls.rubro = Arreglo(1)
    mCls.HuellaSSAS = Arreglo(2)
    'mCls.Cedula = ObtenerCadena(pLinea, 1, 10)
    'mCls.Rubro = ObtenerCadena(pLinea, 12, 3)
    'mCls.Cantidad = Val(ObtenerCadena(pLinea, 17, 6)) / 1000
    'mCls.Fecha = ObtenerFecha(pLinea)
    'mCls.Hora = ObtenerHora(pLinea)
    'mCls.Tienda = ObtenerCadena(pLinea, 37, 6)
    'mCls.Suspendido = Val(ObtenerCadena(pLinea, 45, 1)) = 1
    
    
    Set BuscarEstructuraTrans = mCls
    
    Exit Function

errores:
    
    EscribirLog "Error Leyendo linea " & pLinea, Err.Description, Err.Number
    
End Function

Private Function ObtenerFecha(pCad As String) As String
    
    Dim mDia, mMes, mAno
    
    mDia = ObtenerCadena(pCad, 29, 2)
    mMes = ObtenerCadena(pCad, 27, 2)
    mAno = ObtenerCadena(pCad, 23, 4)
    
    ObtenerFecha = DateSerial(mAno, mMes, mDia)
    
End Function

Private Function ObtenerHora(pCad As String) As String
    
    Dim mHora, mMin, mSeg
    
    mHora = ObtenerCadena(pCad, 31, 2)
    mMin = ObtenerCadena(pCad, 33, 2)
    mSeg = ObtenerCadena(pCad, 35, 2)
    
    ObtenerHora = TimeSerial(mHora, mMin, mSeg)
    
End Function

Private Function InsertarProductos(pArchivo As String) As Boolean
    
    Dim mCls As clsEstructuraTransaccion
    
    For Each mCls In mProductos
        'Debug.Print mCls.rubro
        'Debug.Print Replace(mCls.cedula, "-", "")
        'Debug.Print mProductos.Count
        
        If Not ExisteEnTrRac(Replace(mCls.cedula, "-", ""), mCls.rubro) Then
            If Left(mCls.Tienda, 6) <> EstrucApp.Tienda.Tienda & EstrucApp.Tienda.Sucursal Then
                EjecutarSql mCls, pArchivo
            End If
        End If
    Next
    
    InsertarProductos = True
    
End Function

Private Function ExisteEnTrRac(cedula As String, rubro As String) As Boolean
    Dim rsBachaquero As New ADODB.Recordset
    On Error GoTo errores
    
    rsBachaquero.Open "SELECT * FROM TR_RACIONALIZACION WHERE c_rif = '" & cedula & "' AND c_codrubro = '" & rubro & "'", EstrucApp.Conexion.Conexion, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If rsBachaquero.EOF Then
        ExisteEnTrRac = False
    Else
        ExisteEnTrRac = True
    End If
    
    Exit Function
    
errores:
    EscribirLog "Verificando si existe cedula: ", Err.Description, Err.Number
    ExisteEnTrRac = False
End Function

Private Function EjecutarSql(pCls As clsEstructuraTransaccion, pArchivo) As Boolean
    
    Dim mSql As String
    
    On Error GoTo errores
    
    mSql = BuscarSql(pCls, pArchivo)
    
    EstrucApp.Conexion.Conexion.Execute mSql
    
    Exit Function

errores:
    
    'MsgBox Err.Description
    EscribirLog "Ejecutando Sql " & mSql, Err.Description, Err.Number
    
End Function

Private Function BuscarSql(pCls As clsEstructuraTransaccion, pArchivo) As String
       
    BuscarSql = "insert into tr_racionalizacion (c_documento,c_concepto,c_documentorel,f_fechahora,c_rif, " _
        & "c_codarticulo,c_codrubro,n_cantidad,n_presenta,c_archivo,b_enviado,b_estado)" _
        & "values ('Importado','VEN','','" & Format(Format(Now, "dd/MM/yyyy") & " " & Format(Now, "hh:mm:ss"), "dd/MM/yyyy HH:mm:ss") & "','" & UCase(Replace(pCls.cedula, "-", "")) & "'," _
        & "'','" & pCls.rubro & "',99,1,'" & pArchivo _
        & IIf(pCls.Tienda <> "", "|" & pCls.Tienda, "") & "',1," & IIf(pCls.Suspendido, 1, 0) & ")"
        
   'MsgBox BuscarSql
    
End Function

'********************************************************
Private Sub ActualizarRubros(pArchivo As String)

    Dim mCanal As Long
    Dim mRubro As clsEstructuraRubro
    Dim mLinea As String
    
    mCanal = FreeFile()
    
    Open EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo For Input Access Read As #mCanal
    
    Do While Not EOF(mCanal)
        Line Input #mCanal, mLinea
        Set mRubro = New clsEstructuraRubro
        If mRubro.AsignarRubro(mLinea) Then
            ActualizarRegistro mRubro
        End If
    Loop
    
End Sub

Private Sub ActualizarRegistro(pRubro As clsEstructuraRubro)
    
    On Error GoTo errores
    
    mSql = "update ma_racionalizacion_rubros set c_descripcion='" & pRubro.Descripcion & "'," _
    & ",n_cantidadmax=" & pRubro.Cantidad & ",b_activo=" & IIf(pRubro.Activo, 1, 0) _
    & ",n_intervalo=" & pRubro.Periodo _
    & "where c_codigo='" & pRubro.rubro & "'"
        
    EstrucApp.Conexion.Conexion.Execute mSql
    
    Exit Sub
    
errores:
    
    EscribirLog "Error actualizando rubro " & pRubro.rubro, Err.Description, Err.Number
    
End Sub


