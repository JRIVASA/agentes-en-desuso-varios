VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsEstructuraRubro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarRubro                             As String
Private mvarPeriodo                           As String
Private mvarIntervalo                         As eintervaloRubroRac
Private mvarCantidad                          As Double
Private mvarDescripcion                       As String
Private mvarActivo                            As Boolean

Property Get Rubro() As String
    Rubro = mvarRubro
End Property

Property Get Periodo() As Integer
    Select Case UCase(mvarPeriodo)
        Case "I"
            Periodo = eintervaloRubroRac.eintervaloRubroInterDiaro
        Case "S"
            Periodo = eintervaloRubroRac.eintervaloRubroSemanal
        Case "Q"
            Periodo = eintervaloRubroRac.eintervaloRubroQuincenal
        Case "M"
            Periodo = eintervaloRubroRac.eintervaloRubroSemanal
        Case Else
            Periodo = eintervaloRubroRac.eintervaloRubroDiaro
    End Select
End Property

Property Get Cantidad() As Single
    Cantidad = mvarCantidad
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Get Activo() As Boolean
    Activo = mvarActivo
End Property

Property Get Intervalo() As eintervaloRubroRac
    Intervalo = mvarIntervalo
End Property

'*****************************************************************************

Public Function AsignarRubro(pLinea As String) As Boolean
    
    On Error GoTo Errores
    
    mvarRubro = ObtenerCadena(pLinea, 1, 4)
    mvarCantidad = Val(ObtenerCadena(pLinea, 5, 5)) / 1000
    mvarPeriodo = ObtenerCadena(pLinea, 10, 1)
    mvarDescripcion = ObtenerCadena(pLinea, 11, 40)
    mvarActivo = ObtenerCadena(pLinea, 61, 1) = "1"
    
    AsignarRubro = True
    
    Exit Function
    
Errores:
    
    EscribirLog "Asignando Rubro linea " & pLinea, Err.Description, Err.Number
    Err.Clear
    
End Function

Public Function AsignarRubroRs(pRs As ADODB.Recordset) As Boolean

    On Error GoTo Errores
    
    mvarRubro = pRs!c_codigo
    mvarCantidad = pRs!n_cantidadmax
    mvarIntervalo = pRs!n_intervalo
    mvarDescripcion = pRs!c_descripcion
    mvarActivo = pRs!b_activo
    
    AsignarRubroRs = True
    
    Exit Function
    
Errores:
    
    EscribirLog "Asignando Rubro Rs ", Err.Description, Err.Number
    Err.Clear
    
End Function

