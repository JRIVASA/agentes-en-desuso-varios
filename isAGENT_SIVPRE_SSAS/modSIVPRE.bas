Attribute VB_Name = "modSIVPRE"
' ************************************** Enumeraciones *************************************************************************

Public Enum CH_Dedos

    Me�iqueIzquierdo = 1
    AnularIzquierdo = 2
    MedioIzquierdo = 4
    IndiceIzquierdo = 8
    PulgarIzquierdo = 16
    
    PulgarDerecho = 32
    IndiceDerecho = 64
    MedioDerecho = 128
    AnularDerecho = 256
    Me�iqueDerecho = 512

End Enum

Enum eTipointerfaz
    eInterfazDesconocida
    eInterfazCarga
    eInterfazDescarga
    eInterfazMantenimiento
    eInterfazHuellaCarga
    eInterfazHuellaDescarga
    eInterfazHuellaDescargaInicial
    eInterfazCargaVentasSSAS
    eInterfazDescargaVentasSSAS
End Enum

Enum eAddCaracterRelleno
    eAgregarAlFinal
    eAgregarAlPrincipio
End Enum

Enum eintervaloRubroRac
    eintervaloRubroDiaro
    eintervaloRubroInterDiaro
    eintervaloRubroSemanal
    eintervaloRubroQuincenal
    eintervaloRubroMensual
End Enum

' ************************************** Estructuras *************************************************************************

Type EstrucConexion
    Conexion                    As ADODB.Connection
    Servidor                    As String
    BasedeDatos                 As String
    UsuarioBD                   As String
    PasswordBD                  As String
    CommandTimeOut              As Long
    ConnectTimeOut              As Long
End Type

Type EstrucTienda
    Tienda                      As String
    Sucursal                    As String
    Estado                      As String
End Type

Type EstrucArchivos
    ArchivoConfig               As String
    ArchivoLog                  As String
    RutaArchivoCarga            As String
    RutaArchivoDescarga         As String
    RutaCargaHuellas            As String
    RutaDescargaHuellas         As String
    RutaDescargaInicialHuellas  As String
    ConvertirArchivo            As Boolean
End Type

Type EstrucApp
    Archivos                    As EstrucArchivos
    Conexion                    As EstrucConexion
    Tienda                      As EstrucTienda
    TipoInterfaz                As eTipointerfaz
    SoloBorrarMensual           As Boolean
End Type

' ************************************** API *************************************************************************
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Public Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long

' ************************************** Variable *************************************************************************
Global EstrucApp                As EstrucApp

'**************************************** Main **************************************************************
    
Sub Main()

    Dim mCls As Object
    
    IniciarVariables
    
    If CrearConexion Then
        If EstrucApp.TipoInterfaz = eInterfazCarga Then
            Set mCls = New clsCarga
        ElseIf EstrucApp.TipoInterfaz = eInterfazDescarga Then
            Set mCls = New clsDescarga
        ElseIf EstrucApp.TipoInterfaz = eInterfazMantenimiento Then
           Set mCls = New clsMantenimiento
        ElseIf EstrucApp.TipoInterfaz = eInterfazHuellaCarga Then
            Set mCls = New clsCargaHuellas
        ElseIf EstrucApp.TipoInterfaz = eInterfazHuellaDescarga Then
            Set mCls = New clsDescargaHuellas
        ElseIf EstrucApp.TipoInterfaz = eInterfazHuellaDescargaInicial Then
            Set mCls = New clsDescargaHuellasIni
        ElseIf EstrucApp.TipoInterfaz = eInterfazCargaVentasSSAS Then
            Set mCls = New clsCargaVentasSSAS
        ElseIf EstrucApp.TipoInterfaz = eInterfazDescargaVentasSSAS Then
            Set mCls = New clsDescargaVentasSSAS
        End If
        
        mCls.IniciarInterfaz
        
        Set mCls = Nothing
        
        End
        
    End If
    
End Sub

'****************************************** Metodos *********************************************************

Private Function ObtenerConfiguracion(SIniFile As String, SSection As String, SKey _
As String, sdefault As String) As String
    Dim stemp As String * 256
    Dim nlength As Integer
    
    stemp = Space$(256)
    nlength = GetPrivateProfileString(SSection, SKey, sdefault, stemp, 255, SIniFile)
    ObtenerConfiguracion = Left$(stemp, nlength)
End Function

Private Function EscribirConfiguracion(SIniFile As String, SSection As String, SKey _
As String, svalue As String) As String

    Dim stemp As String
    Dim n As Integer

    stemp = svalue
    n = WritePrivateProfileString(SSection, SKey, svalue, SIniFile)
End Function

Private Sub IniciarVariables()
    IniciarVariablesArchivo
    IniciarVariablesConexion
    IniciarVariablesTienda
    EstrucApp.TipoInterfaz = BuscarTipoInterfaz
    EstrucApp.SoloBorrarMensual = Val(ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, "Configuracion", "SoloBorrarMensual", "0")) = 1
End Sub

Private Sub IniciarVariablesArchivo()
    With EstrucApp.Archivos
        .ArchivoConfig = App.Path & "\Configuracion.ini"
        .ArchivoLog = App.Path & "\LogAgente.log"
        .RutaArchivoCarga = Trim(ObtenerConfiguracion(.ArchivoConfig, "CarpetaArchivos", "RutaArchivoCarga", ""))
        If Len(.RutaArchivoCarga) > 1 Then
            .RutaArchivoCarga = IIf(Right(.RutaArchivoCarga, 1) = "\", Left(.RutaArchivoCarga, Len(.RutaArchivoCarga) - 1), .RutaArchivoCarga)
        End If
        .RutaArchivoDescarga = Trim(ObtenerConfiguracion(.ArchivoConfig, "CarpetaArchivos", "RutaArchivoDescarga", ""))
        If Len(.RutaArchivoDescarga) > 1 Then
            .RutaArchivoDescarga = IIf(Right(.RutaArchivoDescarga, 1) = "\", Left(.RutaArchivoDescarga, Len(.RutaArchivoDescarga) - 1), .RutaArchivoDescarga)
        End If
        .RutaCargaHuellas = Trim(ObtenerConfiguracion(.ArchivoConfig, "CarpetaArchivos", "RutaCargaHuellas", ""))
        If Len(.RutaCargaHuellas) > 1 Then
            .RutaCargaHuellas = IIf(Right(.RutaCargaHuellas, 1) = "\", Left(.RutaCargaHuellas, Len(.RutaCargaHuellas) - 1), .RutaCargaHuellas)
        End If
        .RutaDescargaHuellas = Trim(ObtenerConfiguracion(.ArchivoConfig, "CarpetaArchivos", "RutaDescargaHuellas", ""))
        If Len(.RutaDescargaHuellas) > 1 Then
            .RutaDescargaHuellas = IIf(Right(.RutaDescargaHuellas, 1) = "\", Left(.RutaDescargaHuellas, Len(.RutaDescargaHuellas) - 1), .RutaDescargaHuellas)
        End If
        .RutaDescargaInicialHuellas = Trim(ObtenerConfiguracion(.ArchivoConfig, "CarpetaArchivos", "RutaDescargaInicialHuellas", ""))
        If Len(.RutaDescargaInicialHuellas) > 1 Then
            .RutaDescargaInicialHuellas = IIf(Right(.RutaDescargaInicialHuellas, 1) = "\", Left(.RutaDescargaInicialHuellas, Len(.RutaDescargaInicialHuellas) - 1), .RutaDescargaInicialHuellas)
        End If
        
        .ConvertirArchivo = ObtenerConfiguracion(.ArchivoConfig, "OpcionesArchivos", "ConvertirArchivo", "0") = "1"
    End With
End Sub

Private Sub IniciarVariablesConexion()
    With EstrucApp.Conexion
        .BasedeDatos = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Conexion", "BD", "VAD10")
        .CommandTimeOut = Val(ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Conexion", "TiempoComando", "0"))
        .ConnectTimeOut = Val(ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Conexion", "TiempoConexion", "15"))
        .PasswordBD = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Conexion", "PWD", "")
        .Servidor = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Conexion", "Servidor", "")
        .UsuarioBD = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Conexion", "Usuario", "sa")
    End With
End Sub

Private Sub IniciarVariablesTienda()
    With EstrucApp.Tienda
        .Sucursal = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Tienda", "Sucursal", "")
        .Tienda = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Tienda", "Tienda", "")
        .Estado = ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, _
            "Tienda", "Estado", "")
    End With
End Sub

Private Function BuscarTipoInterfaz() As eTipointerfaz

    Dim mAux As Long
    
    mAux = Val(ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, "Configuracion", "TipoInterfaz", ""))
    
    Select Case mAux
        Case eInterfazCarga
            BuscarTipoInterfaz = eInterfazCarga
        Case eInterfazDescarga
            BuscarTipoInterfaz = eInterfazDescarga
        Case eInterfazMantenimiento
            BuscarTipoInterfaz = eInterfazMantenimiento
        Case eInterfazHuellaCarga
            BuscarTipoInterfaz = eInterfazHuellaCarga
        Case eInterfazHuellaDescarga
            BuscarTipoInterfaz = eInterfazHuellaDescarga
        Case eInterfazHuellaDescargaInicial
            BuscarTipoInterfaz = eInterfazHuellaDescargaInicial
        Case eInterfazCargaVentasSSAS
            BuscarTipoInterfaz = eInterfazCargaVentasSSAS
        Case eInterfazDescargaVentasSSAS
            BuscarTipoInterfaz = eInterfazDescargaVentasSSAS
        Case Else
            BuscarTipoInterfaz = eInterfazDesconocida
    End Select
    
End Function

Private Function CrearConexion() As Boolean
    
    On Error GoTo Errores
    
    With EstrucApp.Conexion
        Set EstrucApp.Conexion.Conexion = New Connection
        .Conexion.ConnectionTimeout = .ConnectTimeOut
        .Conexion.ConnectionString = "provider=sqloledb.1;initial catalog=" & .BasedeDatos _
        & ";user id=" & .UsuarioBD & ";password=" & .PasswordBD & ";data source=" & .Servidor
        .Conexion.Open
        CrearConexion = True
    End With
    
    Exit Function
    
Errores:
    
    EscribirLog "Abriendo Conexion", Err.Description, Err.Number
    
End Function

Public Sub EscribirLog(pCadena As String, Optional pError As String = "", Optional pNumeroError As Long = 0)
    
    Dim mCanal As Long
    Dim mCadena As String
    
    On Error Resume Next
    mCanal = FreeFile()
    
    Open EstrucApp.Archivos.ArchivoLog For Append Access Write As #mCanal
    mCadena = Now & ", " & pCadena & IIf(Len(pError) > 0, ", Error (" & pNumeroError & "), " & pError, "")
    
    Print #mCanal, mCadena
    Close #mCanal
    
End Sub

Public Function BuscarTipoArchivo() As String
    
    ''BuscarTipoArchivo = IIf(EstrucApp.TipoInterfaz = eInterfazCarga, "E", "G")
    
    If (EstrucApp.TipoInterfaz = eInterfazCarga) Then
        BuscarTipoArchivo = "E"
    End If
    
    If (EstrucApp.TipoInterfaz = eInterfazDescarga) Then
        BuscarTipoArchivo = "G"
    End If
    
    If (EstrucApp.TipoInterfaz = eInterfazHuellaCarga) Then
        BuscarTipoArchivo = "HE"
    End If
    
    If (EstrucApp.TipoInterfaz = eInterfazHuellaDescarga) Then
        BuscarTipoArchivo = "HG"
    End If
    
End Function

Public Function BuscarNombreTienda() As String
    With EstrucApp.Tienda
        BuscarNombreTienda = .Tienda & .Sucursal
    End With
End Function

Public Function BuscarNombreTiendaHuellas() As String
    With EstrucApp.Tienda
        BuscarNombreTiendaHuellas = .Tienda
    End With
End Function

Public Function BuscarNombreArchivo() As String
    Dim mAux As Long
    
    mAux = Val(ObtenerConfiguracion(EstrucApp.Archivos.ArchivoConfig, "Configuracion", "TipoInterfaz", ""))
    
    Select Case mAux
        Case eInterfazCargaVentasSSAS
            If EstrucApp.Tienda.Sucursal = "" Then
                BuscarNombreArchivo = EstrucApp.Tienda.Tienda & "_" & Format(FechaServidor, "YYYYMMDDHHmm") & ".csv"
            Else
                BuscarNombreArchivo = EstrucApp.Tienda.Sucursal & "_" & EstrucApp.Tienda.Tienda & "_" & Format(FechaServidor, "YYYYMMDDHHmm") & ".csv"
            End If
            'BuscarNombreArchivo = EstrucApp.Tienda.Sucursal & "_" & EstrucApp.Tienda.Tienda & "_" & Format(FechaServidor, "YYYYMMDDHHmm") & ".csv"
        Case Else
            BuscarNombreArchivo = BuscarTipoArchivo & BuscarNombreTienda & Format(FechaServidor, "DDMMYYHHmm") & ".txt"
    End Select
End Function

Private Function FechaServidor() As String
    
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    
    mSql = "Select GetDate() as Fecha"
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSql, EstrucApp.Conexion.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    FechaServidor = mRs!Fecha
    
End Function

Public Function FormatearCadena(pCadena As String, pLongitud As Integer, pDondeRellenar As eAddCaracterRelleno, Optional pCaracterRelleno As String = " ") As String
    If Len(pCadena) < pLongitud Then
        If pDondeRellenar = eAgregarAlFinal Then
            FormatearCadena = pCadena & String(pLongitud - Len(pCadena), pCaracterRelleno)
        
        Else
            FormatearCadena = String(pLongitud - Len(pCadena), pCaracterRelleno) & pCadena
        End If
    Else
        FormatearCadena = pCadena
    End If
End Function

Public Sub VerficarCarpeta(pCarpeta As String)
    If Dir(pCarpeta, vbDirectory) = "" Then CrearCarpeta pCarpeta
End Sub

Public Sub MoverArchivo(pArchivo As String)
    If Dir(EstrucApp.Archivos.RutaArchivoCarga, vbDirectory) = "" Then CrearCarpeta EstrucApp.Archivos.RutaArchivoCarga
    MoveFile App.Path & "\" & pArchivo, EstrucApp.Archivos.RutaArchivoCarga & "\" & pArchivo
End Sub

Public Sub MoverArchivoHuellas(pArchivo As String)
    If Dir(EstrucApp.Archivos.RutaCargaHuellas, vbDirectory) = "" Then CrearCarpeta EstrucApp.Archivos.RutaCargaHuellas
    MoveFile App.Path & "\" & pArchivo, EstrucApp.Archivos.RutaCargaHuellas & "\" & pArchivo
End Sub

Public Sub MoverArchivoProcesado(pArchivo As String)
    If Dir(App.Path & "\Procesados", vbDirectory) = "" Then CrearCarpeta App.Path & "\Procesados"
    MoveFile EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo, App.Path & "\Procesados\" & pArchivo
End Sub

Public Sub MoverArchivoNoProcesado(pArchivo As String)
    If Dir(App.Path & "\NoProcesados", vbDirectory) = "" Then CrearCarpeta App.Path & "\NoProcesados"
    MoveFile EstrucApp.Archivos.RutaArchivoDescarga & "\" & pArchivo, App.Path & "\NoProcesados\" & pArchivo
End Sub

Public Sub MoverArchivoProcesadoHuellas(pArchivo As String)
    If Dir(App.Path & "\Procesados", vbDirectory) = "" Then CrearCarpeta App.Path & "\Procesados"
    MoveFile EstrucApp.Archivos.RutaDescargaHuellas & "\" & pArchivo, App.Path & "\Procesados\" & pArchivo
End Sub

Public Sub MoverArchivoNoProcesadoHuellas(pArchivo As String)
    If Dir(App.Path & "\NoProcesados", vbDirectory) = "" Then CrearCarpeta App.Path & "\NoProcesados"
    MoveFile EstrucApp.Archivos.RutaDescargaHuellas & "\" & pArchivo, App.Path & "\NoProcesados\" & pArchivo
End Sub

Public Sub MoverArchivoProcesadoHuellasIniciales(pArchivo As String)
    If Dir(App.Path & "\Procesados", vbDirectory) = "" Then CrearCarpeta App.Path & "\Procesados"
    MoveFile EstrucApp.Archivos.RutaDescargaInicialHuellas & "\" & pArchivo, App.Path & "\Procesados\" & pArchivo
End Sub

Public Sub MoverArchivoNoProcesadoHuellasIniciales(pArchivo As String)
    If Dir(App.Path & "\NoProcesados", vbDirectory) = "" Then CrearCarpeta App.Path & "\NoProcesados"
    MoveFile EstrucApp.Archivos.RutaDescargaInicialHuellas & "\" & pArchivo, App.Path & "\NoProcesados\" & pArchivo
End Sub

Public Sub CrearCarpeta(pCarpeta As String)
    MkDir pCarpeta
End Sub

Public Function ObtenerCadena(pCadena As String, pIni As Integer, pLen As Integer, Optional pEliminarBlancos As Boolean = True) As String
    If pEliminarBlancos Then
        ObtenerCadena = Trim(Mid(pCadena, pIni, pLen))
    Else
        ObtenerCadena = Mid(pCadena, pIni, pLen)
    End If
End Function

Public Function ValidarRif(pRif As String) As String

    On Error GoTo ErrorValidar
    
    Dim Regex As New RegExp
    Dim strPattern As String
    Dim mTempString As String
    Dim mResult As String
    
    strPattern = "[^a-zA-Z0-9]"
    Regex.Global = True
    Regex.Pattern = strPattern
    
    mTempString = UCase(pRif)
    
    mResult = Regex.Replace(mTempString, "")
    
    mTempString = mResult
    
    'mTempString = UCase(Trim(ReplaceMultiple(mTempString, "", ",", ".", "-", "_", "!", Chr(34), _
        "�", "$", "%", "&", "/", "(", ")", "=", "'", "�", "`", "+", "�", "�", "<", ">", "*", _
        "�", "\", "@", "�", "�", "�", "|", "�", "�", "�", "�", ":", ";", "�", "�", "�", "�", _
        "�", "�", " ")))
    
    If Len(mTempString) >= 2 Then
    
        Dim mTempStr As String
    
        strPattern = "[^0-9]"
        mResult = ""
        mTempStr = mTempString
        
        Regex.Pattern = strPattern
        
        mResult = Regex.Replace(Mid(mTempStr, 2, Len(mTempStr) - 1), "")
    
        mTempString = Left(mTempString, 1) & "-" & mResult
        
    End If
    
    ValidarRif = mTempString
    
    Exit Function
    
ErrorValidar:
    
    ValidarRif = pRif
    
    'Debug.Print Err.Description
    
End Function

Public Function ReplaceMultiple(ByVal OrigString As String, _
ByVal ReplaceString As String, ParamArray FindChars()) _
As String

    '*********************************************************
    'PURPOSE: Replaces multiple substrings in a string with the
    'character or string specified by ReplaceString
    
    'PARAMETERS: OrigString -- The string to replace characters in
    '            ReplaceString -- The replacement string
    '            FindChars -- comma-delimited list of
    '                 strings to replace with ReplaceString
    '
    'RETURNS:    The String with all instances of all the strings
    '            in FindChars replaced with Replace String
    'EXAMPLE:    s= ReplaceMultiple("H;*()ello", "", ";", ",", "*", "(", ")") -
                 'Returns Hello
    'CAUTIONS:   'Overlap Between Characters in ReplaceString and
    '             FindChars Will cause this function to behave
    '             incorrectly unless you are careful about the
    '             order of strings in FindChars
    '***************************************************************

    Dim lLBound As Long
    Dim lUBound As Long
    Dim lCtr As Long
    Dim sAns As String
    
    lLBound = LBound(FindChars)
    lUBound = UBound(FindChars)
    
    sAns = OrigString
    
    For lCtr = lLBound To lUBound
        sAns = Replace(sAns, CStr(FindChars(lCtr)), ReplaceString)
    Next
    
    ReplaceMultiple = sAns
        
End Function

Public Function StringToHex(ByVal StrToHex As String) As String

    Dim strTemp As String
    Dim StrReturn As String
    Dim i As Long
    
    For i = 1 To Len(StrToHex)
    strTemp = Hex$(Asc(Mid$(StrToHex, i, 1)))
    If Len(strTemp) = 1 Then strTemp = "0" & strTemp
    StrReturn = StrReturn & Space$(0) & strTemp
    Next i
    
    StringToHex = StrReturn

End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    
    Dim mAux As Variant
    Dim mRs As New ADODB.Recordset
    
    On Error GoTo Error
    
    If Not IsMissing(pRs) Then
        mAux = pRs.Fields(pCampo).Value
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        ExisteCampoTabla = True
        mRs.Close
    End If
    
    Exit Function
    
Error:
    
    'MsgBox Err.Description
    Err.Clear
    ExisteCampoTabla = False
    
End Function

Public Function ExisteTabla(pCn As ADODB.Connection, pTabla As String) As Boolean
    
    On Error GoTo Falla_Local
    
    Dim mRs As New ADODB.Recordset
    
    mSql = "SELECT     name, type  From sysobjects WHERE " _
        & " (name='" & pTabla & "') and (type = 'U')"
        
    mRs.Open mSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ExisteTabla = Not mRs.EOF
    
    mRs.Close
    
    Set mRs = Nothing
    
    Exit Function
    
Falla_Local:

End Function
